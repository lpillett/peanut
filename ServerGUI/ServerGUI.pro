
TEMPLATE = app
TARGET = ServerGUI
INCLUDEPATH += .

CONFIG+=debug
#CONFIG+=release
CONFIG += c++11
CONFIG += warn_on
CONFIG+=console


QT += widgets
QT += websockets

# Input
HEADERS += ../ClientCompanion/BehaviorBlock.hpp  ../ClientCompanion/Behavior.hpp  ../ClientCompanion/BehaviorType.hpp  MainWindow.hpp  ../ClientCompanion/Synchronization.hpp  
SOURCES += main.cpp MainWindow.cpp ../ClientCompanion/BehaviorBlock.cpp  ../ClientCompanion/Synchronization.cpp

macx {
QMAKE_MACOSX_DEPLOYMENT_TARGET=10.7
CONFIG += x86_64
QMAKE_CXXFLAGS += -stdlib=libc++ -std=c++11 
QMAKE_LDFLAGS += -macosx-version-min=$$QMAKE_MACOSX_DEPLOYMENT_TARGET
QMAKE_LDFLAGS += -stdlib=libc++ -std=c++11
}

QMAKE_CXXFLAGS += -Wall -Wextra

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include "../ClientCompanion/BehaviorType.hpp"

class QPushButton;
class QComboBox;
class QSlider;
class QTableWidget;
class QWebSocketServer;
class QWebSocket;


class MainWindow : public QWidget //QMainWindow
{
	Q_OBJECT

public:
	MainWindow();

	~MainWindow();

signals:
	void closed();

protected slots:

	void addBehavior();
	void clearTable();
	void trigger();


    void onNewConnection();
    void processReceivedTextMessage(const QString &message);
    void processReceivedBinaryMessage(const QByteArray &message);
    void socketDisconnected();
	bool sendMessage(const QByteArray &ba);
	void updatePhrases();

protected:
	void buildGUI();

	void populateLang();
	//void populatePhrases();
	//void populateSpeechs();
	void populateFaces();

	void disableTrigger();
	void enableTrigger();
	void updateTrigger();
  

private:
	QPushButton *m_addButton;
	QPushButton *m_clearButton;
	QPushButton *m_triggerButton;

	QComboBox *m_langCB;
	QComboBox *m_speechCB;
	QComboBox *m_faceCB;
	QSlider *m_intensitySlider;

	QTableWidget *m_tableWidget;

    QString m_phrasesRootDir;

	QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;
	QList<bool> m_waitings;
    bool m_debug;


};

#endif /* ! MAINWINDOW_HPP */

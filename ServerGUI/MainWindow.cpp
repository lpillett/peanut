#include "MainWindow.hpp"

#include <cassert>

#include <QLabel>
#include <QPushButton>
#include <QBoxLayout>
#include <QComboBox>
#include <QTableWidget>
#include <QFileDialog>
#include <QSlider>
#include <QDirIterator>

#include <QDebug>
#include <QDir>
#include <QWebSocketServer>
#include <QWebSocket>
#include <QNetworkInterface>

#include "../ClientCompanion/BehaviorType.hpp"
#include "../ClientCompanion/BehaviorBlock.hpp"


static const QString RSRC_PATH = "../../ClientCompanion/rsrc/"; //TODO:UGLY: should be defined by build process !!!
static const QString SPEECH_PATH = "voices"; 
static const QString PHRASES_PATH = "phrases";

static const quint16 PORT = 1234;


MainWindow::MainWindow()
	: QWidget() //QMainWindow()
	, m_pWebSocketServer(new QWebSocketServer(QStringLiteral("Server"),
											  QWebSocketServer::NonSecureMode, this))
	, m_clients(), m_waitings(), m_debug(true)
{
	

	buildGUI();


	const quint16 port = PORT;

	disableTrigger();

	if (m_pWebSocketServer->listen(QHostAddress::Any, port)) {
	  if (m_debug)
            qDebug() << "Echoserver listening on port" << port;
	  connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
		  this, &MainWindow::onNewConnection);
	  connect(m_pWebSocketServer, &QWebSocketServer::closed, this, &MainWindow::closed);
	}
	else {
	}

}


MainWindow::~MainWindow()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
	m_waitings.clear();
}

enum {SpeechColumn=0, FaceColumn, IntensityColumn};

static
QString
getIPAddress()
{
	QString s;
	bool prev = false;
	foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
	  if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {
	    if (prev) 
	      s+= " ;";
	    s += address.toString();
	    prev = true;
	  }
	}
	return s;
}
 
void
MainWindow::buildGUI()
{
	QLabel *ipLabel = new QLabel(tr("IP address: "), this);
	QLabel *ipLabelV = new QLabel(this);
	ipLabelV->setText(getIPAddress());
	QLabel *portLabel = new QLabel(tr("Port: "), this);
	QLabel *portLabelV = new QLabel(this);
	portLabelV->setText(QString::number(PORT));

	QLabel *langLabel = new QLabel(tr("Lang: "), this);
	m_langCB = new QComboBox(this);
	QLabel *speechLabel = new QLabel(tr("Speech: "), this);
	m_speechCB = new QComboBox(this);
	m_speechCB->setSizePolicy(QSizePolicy::Expanding, m_speechCB->sizePolicy().verticalPolicy());
	QLabel *faceLabel = new QLabel(tr("Face: "), this);
	m_faceCB = new QComboBox(this);
	QLabel *intensityLabel = new QLabel(tr("Intensity: "), this);
	m_intensitySlider = new QSlider(Qt::Horizontal, this);
	m_intensitySlider->setMinimum(0);
	m_intensitySlider->setMaximum(100);
	m_intensitySlider->setValue(100);

	m_addButton = new QPushButton(tr("Add"), this);

	m_tableWidget = new QTableWidget(this);
	m_tableWidget->setColumnCount(3);
	QStringList headers;
	headers << tr("Speech") << tr("Face") << tr("intensity");
	m_tableWidget->setHorizontalHeaderLabels(headers);

	m_clearButton = new QPushButton(tr("clear"), this);
	m_triggerButton = new QPushButton(tr("trigger"), this);

	connect(m_langCB, SIGNAL(currentIndexChanged(int)), this, SLOT(updatePhrases()));
	connect(m_addButton, SIGNAL(clicked()), this, SLOT(addBehavior()));
	connect(m_clearButton, SIGNAL(clicked()), this, SLOT(clearTable()));
	connect(m_triggerButton, SIGNAL(clicked()), this, SLOT(trigger()));

	populateLang();
	//populateSpeechs();
	populateFaces();

	QHBoxLayout *h0 = new QHBoxLayout;
	h0->addWidget(ipLabel);
	h0->addWidget(ipLabelV);
	h0->addWidget(portLabel);
	h0->addWidget(portLabelV);
	h0->addStretch();

	QHBoxLayout *v1 = new QHBoxLayout;
	v1->addWidget(langLabel);
	v1->addWidget(m_langCB);
	v1->addWidget(speechLabel);
	v1->addWidget(m_speechCB);
	QHBoxLayout *v2 = new QHBoxLayout;
	v2->addWidget(faceLabel);
	v2->addWidget(m_faceCB);
	v2->addWidget(intensityLabel);
	v2->addWidget(m_intensitySlider);
	v2->addStretch();

	QVBoxLayout *h1 = new QVBoxLayout;
	h1->addLayout(v1);
	h1->addLayout(v2);

	QHBoxLayout *h2 = new QHBoxLayout;
	h2->addWidget(m_addButton);
	h2->addStretch();
	h2->addWidget(m_clearButton);

	QHBoxLayout *h3 = new QHBoxLayout;
	h3->addStretch();
	h3->addWidget(m_triggerButton);

	QVBoxLayout *v = new QVBoxLayout;
	v->addLayout(h0);
	v->addLayout(h1);
	v->addLayout(h2);
	v->addWidget(m_tableWidget);
	v->addLayout(h3);

	setLayout(v);
}

void
MainWindow::populateLang()
{
	m_langCB->clear();

    QString voicesPath = RSRC_PATH + SPEECH_PATH;

    QFileInfo fi(voicesPath);
    if (! fi.isDir()) {
        qDebug()<<"voicesPath="<<voicesPath<<"="<<QFileInfo(voicesPath).absolutePath()<<" does not exist";

        voicesPath = QString("../ClientCompanion/rsrc/") + SPEECH_PATH;
        fi.setFile(voicesPath);
        if (! fi.isDir()) {
            qDebug()<<"voicesPath="<<voicesPath<<"="<<QFileInfo(voicesPath).absolutePath()<<" does not exist";
            qDebug()<<"ERROR: directory for voices not found";
            exit(10);
        }
    }

    m_phrasesRootDir = voicesPath;
	
	QDir dir(voicesPath);
	QStringList listP = dir.entryList(QDir::Dirs|QDir::NoDotAndDotDot|QDir::Readable, QDir::Name);

    if (listP.isEmpty())
    {
        qDebug()<<"ERROR: no lang found";
        exit(10);
    }

    for (const QString &lang : listP)
	{
		m_langCB->addItem(lang);
	}
}

void
MainWindow::updatePhrases()
{
	clearTable();

	m_speechCB->clear();

	QString lang = m_langCB->currentText();

    const QString phrasesPath = m_phrasesRootDir + "/" + lang + "/" + PHRASES_PATH;
	
	qDebug()<<"updatePhrases phrasesPath="<<phrasesPath;


	QDir in(phrasesPath);
	QStringList filters;
	filters << QStringLiteral("*.txt");
	QStringList listP = in.entryList(filters, QDir::Files|QDir::Readable, QDir::Name);
	//  allow to remove . & .., and sort by name.

	for (const QString &file : listP)
	{
			const int pos = file.lastIndexOf(QStringLiteral("."));
			QString text = file.left(pos);

			QString displayedText = text;
			displayedText.replace('_', ' ');
			
			m_speechCB->addItem(displayedText, QVariant(text)); 
	}
	
	
}

/*
void 
MainWindow::populateSpeechs()
{
	const QString dirname = RSRC_PATH + SPEECH_PATH;

	QDir in(dirname);
	QStringList filters;

	qDebug()<<"MainWindow::populateSpeechs dirname="<<dirname;

	//filters << QStringLiteral("*.phseg");
	filters << QStringLiteral("*.wav") << QStringLiteral("*.ogg") << QStringLiteral("*.mp3"); //TODO: also in upper case ?
	QStringList listP = in.entryList(filters, QDir::Files|QDir::Readable, QDir::Name);
	int index = 0;
	for (const QString &file : listP)
	{
	  //int pos = file.lastIndexOf(QStringLiteral(".phseg"));
	  int pos = file.lastIndexOf(QStringLiteral("."));
	  QString text = file.left(pos);

	  text.replace('_', ' ');

	  m_speechCB->addItem(text, QVariant(file)); //QVariant(SPEECH_PATH + file));

	  ++index;
	}

	if (m_speechCB->count() == 0) {
		qDebug()<<"Warning: no sound file found in directory: "<<dirname;
	}
}
*/

void
MainWindow::populateFaces()
{
	m_faceCB->addItem("Idle", QVariant(Idle)); 
	m_faceCB->addItem("Happy", QVariant(Happy)); 
	m_faceCB->addItem("Sad", QVariant(Sad)); 
	m_faceCB->addItem("Angry", QVariant(Angry)); 
	m_faceCB->addItem("Surprised", QVariant(Surprised)); 
    //m_faceCB->addItem("Blink", QVariant(Blink));
    m_faceCB->addItem("Trust", QVariant(Trust));
    m_faceCB->addItem("Bored", QVariant(Bored));
	//...
}

void
MainWindow::addBehavior()
{
	QTableWidgetItem *newItemS = new QTableWidgetItem(m_speechCB->currentText());
	newItemS->setData(Qt::UserRole, m_speechCB->currentData());

	qDebug()<<"addBehavior "<<m_speechCB->currentText();

	QTableWidgetItem *newItemF = new QTableWidgetItem(m_faceCB->currentText());
	newItemF->setData(Qt::UserRole, m_faceCB->currentData());

	float intensity = m_intensitySlider->value()/(double)m_intensitySlider->maximum();

	QTableWidgetItem *newItemI = new QTableWidgetItem(QString::number(intensity));
	newItemI->setData(Qt::UserRole, QVariant(intensity));


	const int row = m_tableWidget->rowCount();
	m_tableWidget->setRowCount(row+1);

	m_tableWidget->setItem(row, SpeechColumn, newItemS);
	m_tableWidget->setItem(row, FaceColumn, newItemF);
	m_tableWidget->setItem(row, IntensityColumn, newItemI);

	m_tableWidget->resizeColumnToContents(0);
	m_tableWidget->resizeColumnToContents(1);
	m_tableWidget->resizeColumnToContents(2);

	updateTrigger();
}

void
MainWindow::clearTable()
{
	m_tableWidget->clearContents();
	m_tableWidget->setRowCount(0);

	updateTrigger();
}

void
MainWindow::trigger()
{
	BehaviorBlock bl;

	Behavior *prev = nullptr;


	int rows = m_tableWidget->rowCount();

	for (int row=0; row<rows; ++row) {
		
	  //QString speechFileIndex = m_tableWidget->item(row, SpeechColumn)->data(Qt::UserRole).toString();
	  //QString speechFilename = SPEECH_PATH + m_tableWidget->item(row, SpeechColumn)->text() + ".ogg"; //TODO:UGLY
		//QString speechFilename = QString(":") + m_tableWidget->item(row, SpeechColumn)->data(Qt::UserRole).toString(); //TODO:UGLY: we should not have to give an exact (rsrc) path for the client... //TODO: transfer only index ???

		const QString speechFilename = m_tableWidget->item(row, SpeechColumn)->data(Qt::UserRole).toString();
		qDebug()<<"MainWindow::trigger() speechFilename="<<speechFilename;

		const FaceBehaviorType faceType = (FaceBehaviorType)m_tableWidget->item(row, FaceColumn)->data(Qt::UserRole).toUInt();
		float intensity = m_tableWidget->item(row, IntensityColumn)->data(Qt::UserRole).toDouble();

		FaceBehavior *fb = new FaceBehavior(faceType, intensity);
		//SoundFileSpeechBehavior *sb = new SoundFileSpeechBehavior(speechFileIndex);
		SoundFileSpeechBehavior *sb = new SoundFileSpeechBehavior(speechFilename);
		
		bl.addBehavior(fb);
		bl.addBehavior(sb);

		bl.addSynchronization(Synchronization(fb, Behavior::AttackPeak, sb, Behavior::Start));
		bl.addSynchronization(Synchronization(fb, Behavior::Relax, sb, Behavior::End));
		
		if (prev != nullptr) {
		  bl.addSynchronization(Synchronization(prev, Behavior::End, fb, Behavior::Start));
		}
		
		prev = fb;

	}


	QByteArray msg;
	QDataStream strm(&msg, QIODevice::WriteOnly);
	bool ok = serialize(strm, bl);

	if (ok) {
		bool sent = sendMessage(msg);
		if (sent) 
		  disableTrigger();
	}

}

void
MainWindow::disableTrigger()
{
	assert(m_triggerButton);
	m_triggerButton->setEnabled(false);
}

void
MainWindow::enableTrigger()
{
	assert(m_triggerButton);
	m_triggerButton->setEnabled(true);
}

void
MainWindow::updateTrigger()
{
	bool enable = false;
	if (m_tableWidget->rowCount() > 0) {
		bool waiting = false;
		for (int i=0; i<m_waitings.size(); ++i) {
			if (m_waitings[i]) {
				waiting = true;
				break;
			}
		}
		if (! waiting) {
			enable = true;
		}
	}
	if (enable) {
		enableTrigger();
	}
	else {
		disableTrigger();
	}
}

void MainWindow::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &MainWindow::processReceivedTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &MainWindow::processReceivedBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &MainWindow::socketDisconnected);

    m_clients << pSocket;
	m_waitings << false;
	assert(m_waitings.size() == m_clients.size());
}
 
void MainWindow::processReceivedTextMessage(const QString &message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "Message received:" << message;
	assert(m_waitings.size() == m_clients.size());
	for (int i=0; i<m_clients.size(); ++i) {
		if (m_clients[i] == pClient) {
			if (! m_waitings[i]) {
				qDebug()<<"ERROR: not wating and receiving an answer";
			}
			//TODO: check the kind of message 
			m_waitings[i] = false;
			break;
		}
	}
}

void MainWindow::processReceivedBinaryMessage(const QByteArray &message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "Binary Message received:" << message;

    assert(m_waitings.size() == m_clients.size());
    for (int i=0; i<m_clients.size(); ++i) {
      if (m_clients[i] == pClient) {
	if (! m_waitings[i]) {
	  qDebug()<<"ERROR: not wating and receiving an answer";
	}
	
	QByteArray ba = message;
	QDataStream strm(&ba, QIODevice::ReadOnly);
	quint8 r;
        strm >> r;
        if (strm.status() == QDataStream::Ok) {
			qDebug()<<"Answer received: "<<r;
        }

	m_waitings[i] = false;

	updateTrigger();

	break;
      }
    }
}

void MainWindow::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (m_debug)
        qDebug() << "socketDisconnected:" << pClient;
    if (pClient) {
      //m_clients.removeAll(pClient);
      //m_waitings.clear();
      //pClient->deleteLater();

      assert(m_waitings.size() == m_clients.size());
      QList<bool>::iterator itW = m_waitings.begin();
      QList<QWebSocket *>::iterator itC = m_clients.begin();
      for ( ; itC != m_clients.end(); ) {
	if (*itC == pClient) {
	  pClient->deleteLater();
	  itW = m_waitings.erase(itW);
	  itC = m_clients.erase(itC);
	  break;
	}
	else {
	  ++itC;
	  ++itW;
	}
      }
      assert(m_waitings.size() == m_clients.size());
      updateTrigger();
    }
}

bool MainWindow::sendMessage(const QByteArray &msg)
{
  bool sent = false;
  assert(m_waitings.size() == m_clients.size());
  for (int i=0; i<m_clients.size(); ++i) {
    if (! m_waitings[i]) {
      m_waitings[i] = true;
      m_clients[i]->sendBinaryMessage(msg);
      sent = true;
    }
  }
  return sent;
}


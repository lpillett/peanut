cmake_minimum_required(VERSION 2.8.11)

project(ServerGUI)

# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)

# Find the QtWidgets library
find_package(Qt5Widgets)
find_package(Qt5WebSockets)


#C++11 support (for all executables)
IF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  EXECUTE_PROCESS(COMMAND ${CMAKE_CXX_COMPILER} -dumpversion OUTPUT_VARIABLE GCC_VERSION)
  IF (GCC_VERSION VERSION_LESS 4.7)
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
  ELSE()
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
  ENDIF()
ELSEIF ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++")
ENDIF()
#VS2013 supports c++11 without any additional flags

IF("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" 
    OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" 
    OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")
ENDIF()


set(HEADERS MainWindow.hpp ../ClientCompanion/BehaviorBlock.hpp  ../ClientCompanion/Behavior.hpp  ../ClientCompanion/BehaviorType.hpp ../ClientCompanion/Synchronization.hpp)

set(SOURCES main.cpp MainWindow.cpp ../ClientCompanion/BehaviorBlock.cpp  ../ClientCompanion/Synchronization.cpp)

# Tell CMake to create the helloworld executable
add_executable(ServerGUI ${SOURCES} ${HEADERS})

# Use the Widgets module from Qt 5.
target_link_libraries(ServerGUI Qt5::Widgets Qt5::WebSockets)

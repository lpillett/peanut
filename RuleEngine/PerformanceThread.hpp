#ifndef PERFORMANCETHREAD_HPP
#define PERFORMANCETHREAD_HPP

#include <memory>
#include <QThread>

namespace lsl {
	class stream_inlet;
}


class PerformanceThread : public QThread
{
	Q_OBJECT

public:

	PerformanceThread(bool *running, const QString &name = QString(), QObject *parent = 0);

	~PerformanceThread();

	//must be called before run()
	void setName(const QString &name);


	void run() Q_DECL_OVERRIDE;

signals:
	
	void receivedPerformance(float perf);
		
protected:
	bool *m_running;
	QString m_name;
	std::shared_ptr<lsl::stream_inlet> m_inlet;

};

#endif /* ! PERFORMANCETHREAD_HPP */

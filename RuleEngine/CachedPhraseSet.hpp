#ifndef CACHEDPHRASESET_HPP
#define CACHEDPHRASESET_HPP

#include "PhraseSet.hpp"

class CachedPhraseSet : public PhraseSet
{
public:
	
	explicit CachedPhraseSet(int cacheSize = 3);
	CachedPhraseSet(const QString &dirname, int cacheSize = 3);

	void setCacheSize(int cacheSize);

	/**
	 * Get random index in [0; size()[
	 *
	 * Try to not give an index already in cache.
	 */
	virtual int randomIndex() const;
    int getCacheSize();
    int getCacheCapacity();
    void emptyCache();
    bool isCacheFull();
	
protected:

	bool isInCache(int index, int &indexCache) const;
	bool isInCache(int index) const;
	void insertInCache(int index) const;

protected:

	mutable QVector<int> m_cache;
};

#endif /* ! CACHEDPHRASESET_HPP */

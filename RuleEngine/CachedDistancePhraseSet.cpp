
#include "CachedDistancePhraseSet.hpp"

#include <cassert>
#include <random>

#include <QString>

#include <iostream>

CachedDistancePhraseSet::CachedDistancePhraseSet(int cacheSize)
	: CachedPhraseSet(cacheSize)
{

}

CachedDistancePhraseSet::CachedDistancePhraseSet(const QString &dirName, int cacheSize)
	: CachedPhraseSet(dirName, cacheSize)
{

}



/**
  Compute Levenshtein distance between two QStrings.

   adapted from code from https://github.com/wooorm/levenshtein.c
MIT licensed.
Copyright (c) 2015 Titus Wormer <tituswormer@gmail.com>
 */
unsigned int
levenshtein(const QString &a, const QString &b) {
    unsigned int length = a.length();
    unsigned int bLength = b.length();
	
    QVector<unsigned int> c(length, 0);//TODO:OPTIM: if several distances are computed, this vector should be kept alive...
    unsigned int *cache = &c[0]; //calloc(length, sizeof(unsigned int));

    unsigned int index = 0;
    unsigned int bIndex = 0;
    unsigned int distance;
    unsigned int bDistance;
    unsigned int result;
    QChar code;

    /*
     * Shortcut optimizations / degenerate cases.
     */

    if (a == b) {
        return 0;
    }

    if (length == 0) {
        return bLength;
    }

    if (bLength == 0) {
        return length;
    }

    /*
     * initialize the vector.
     */

    while (index < length) {
        cache[index] = index + 1;
        index++;
    }

    /*
     * Loop.
     */

    while (bIndex < bLength) {
        code = b[bIndex];
        result = distance = bIndex++;
        index = -1;

        while (++index < length) {
            bDistance = code == a[index] ? distance : distance + 1;
            distance = cache[index];

            cache[index] = result = distance > result
                ? bDistance > result
                    ? result + 1
                    : bDistance
                : bDistance > distance
                    ? distance + 1
                    : bDistance;
        }
    }

    //free(cache);

    return result;
}


QVector<unsigned int>
CachedDistancePhraseSet::computeDistances(int index) const
{
	QVector<unsigned int> distances(size(), 0);

	assert(index < size());

	const QString phrs = phrase(index);

	for (int i = 0; i<size(); ++i) {
	  distances[i] = levenshtein(phrs, phrase(i));
	}
	
	return distances;
}

struct Sorter
{
	const QVector<unsigned int> &m_distances;

	explicit Sorter(const QVector<unsigned int> &distances)
		: m_distances(distances)
		{}

	bool operator()(int i, int j) const 
		{
			return m_distances[i] > m_distances[j];
		}
};

int
CachedDistancePhraseSet::furthestDistanceIndex() const
{
	assert(m_cache.size() == m_distances.size());
	if (m_distances.size() == 0) {
		return PhraseSet::randomIndex();
	}

	if (m_cache.size() > 0) {

        const int nbPhrases = size();

		//compute sum of distances of indices in m_cache
		assert(m_distances.size() > 0);
        QVector<unsigned int> sums(nbPhrases, 0);
		for (int i=0; i<m_distances.size(); ++i) {
            assert(m_distances[i].size() == nbPhrases);
            for (int k=0; k<nbPhrases; ++k) {
				sums[k] += (m_distances[i])[k];
			}
		}
        QVector<int> indices(nbPhrases, 0);
        for (int k=0; k<nbPhrases; ++k) {
			indices[k] = k;
		}
		//sort them in decreasing order
		std::sort(indices.begin(), indices.end(), Sorter(sums));
		
#if 0
		//take the furthest away
		// [this strategy could produce cyclic results, depending on cache size]
		for (QVector<int>::const_iterator it = indices.begin();
			 it != indices.end();
			 ++it) {
			if (! isInCache(*it)) {
				return *it;
			}
		}
		
#else
		//get random index in first part of further indices/phrases
        // [this strategy should be more random, and less dependant of cache size, than previous one]
        int index = nbPhrases;
        int sz = nbPhrases > 3 ? nbPhrases/3 : nbPhrases;
        int count = 0;
        static const int MAX_COUNT = 3;
		do {
            assert(sz>0);
			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_int_distribution<int> dis(0, sz);
			const int ind = dis(gen);
			index = indices[ind];
			if (! isInCache(index)) {
				return index;
			}
            //to stop the loop !
            if (count == MAX_COUNT) {
                return indices[0];
            }
            index = nbPhrases;
			sz = std::min(size()-1, sz+2);

            ++count;

            //DEBUG
            if (index == nbPhrases) std::cerr<<"CachedDistancePS::furthestDistanceIndex() index==size=="<<index<<" (sz="<<sz<<")\n";
		}
        while (index == nbPhrases);

#endif

	
		return m_cache[0];
	}
	
	if (m_distances.size() == 0) {
		return PhraseSet::randomIndex();
	}

	return 0;
}

//REM: declared 'const' (because called from randomIndex())
//      but still modify mutable m_cache
void
CachedDistancePhraseSet::insertInCache(int index) const
{
	//TODO: check not already there ???

	QVector<unsigned int> dists = computeDistances(index);

	if (m_cache.size() < m_cache.capacity()) {
        //there is still room in the cache
        // add @a index as last element of the cache
		m_cache.push_back(index);
		m_distances.push_back(dists);
	}
	else {
        //cache is already full :
        // remove first element, move all element foward,
        // and add @à index as last element

		for (int i=1; i<m_cache.size(); ++i) { //start from 1
			m_cache[i-1] = m_cache[i];
			std::swap(m_distances[i-1], m_distances[i]);
		}
		assert(m_cache.size()>0);
		m_cache[m_cache.size()-1] = index;
		m_distances[m_cache.size()-1] = dists;
	}
}

int
CachedDistancePhraseSet::randomIndex() const
{
    const int nbPhrases = size();
    if (nbPhrases <= 1)
		return 0;

    int index = nbPhrases;

	do {

		index = furthestDistanceIndex();
        assert(index>=0 && index <nbPhrases);
		int indexCache = -1;
		const bool inCache = CachedPhraseSet::isInCache(index, indexCache);
		if (inCache) {
            if (m_cache.size() >= nbPhrases) {
				//use the oldest cache entry
				index = m_cache[0];
				break;
			}
            index = nbPhrases;
		}
        //DEBUG
        if (index >= nbPhrases) std::cerr<<"CachedDistancePS::randomIndex() index="<<index<<" >= size="<<size()<<"\n";

	}
    while (index >= nbPhrases);

    assert(index >= 0 && index < nbPhrases);

	insertInCache(index);

	return index;	
}

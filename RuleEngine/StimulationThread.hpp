#ifndef STIMULATIONTHREAD_HPP
#define STIMULATIONTHREAD_HPP

#include <memory>
#include <QThread>

namespace lsl
{
	class stream_inlet;
}

class StimulationThread : public QThread
{
	Q_OBJECT

public:
	
	explicit StimulationThread(bool *running, const QString &name, QObject *parent = 0);

	~StimulationThread();

	//must be called before run()
	void setName(const QString &name);	

	void run() Q_DECL_OVERRIDE;

signals:
	
	void receivedStimulation(int s);
		
protected:

	bool *m_running;
	QString m_name;
	std::shared_ptr<lsl::stream_inlet> m_inlet;

};

#endif /* ! STIMULATIONTHREAD_HPP */

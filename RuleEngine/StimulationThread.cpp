#include "StimulationThread.hpp"

#include "lsl_cpp.h"

#include <iostream>
#include <cassert>

//CODE DUPLICATION with PerformanceThread
// but we can not use templates because of signals/slots...

static const double RESOLVE_TIMEOUT=3; //in seconds
static const double PULL_TIMEOUT=1; //in seconds

StimulationThread::StimulationThread(bool *running, const QString &name, QObject *parent)
	: QThread(parent), m_running(running), m_name(name), m_inlet(nullptr)
{}

StimulationThread::~StimulationThread()
{
}

void
StimulationThread::setName(const QString &name)
{
	m_name = name;
}

void StimulationThread::run()
{
	std::cerr<<"StimulationThread run this="<<this<<" m_name="<<m_name.toStdString()<<"\n";
	
	int stim;
	std::string known_uid;
	std::string known_source_id;
	std::vector<lsl::stream_info> results;
	const std::string name = m_name.toStdString();

	assert(m_running);

	while (*m_running) {

		const int minNumberOfStreamsToReturn = 1;
		results = lsl::resolve_stream("name", name, minNumberOfStreamsToReturn, RESOLVE_TIMEOUT);

		if (results.empty()) {
			std::cerr<<"no results for name "<<name<<"\n";
			if (m_inlet.get()) {
				std::cerr<<"reset inlet\n";
				m_inlet.reset();
			}
				
		}

		for (unsigned k=0; k<results.size(); ++k) {
			if (results[k].channel_count() == 1 && 
				results[k].channel_format() == lsl::cf_int32) {
				if (known_uid.empty() || known_uid != results[k].uid()) {
					if (known_source_id.empty() ||
						! (! results[k].source_id().empty() && known_source_id != results[k].source_id())) {
						known_uid = results[k].uid();
						known_source_id = results[k].source_id();
						std::cerr<<" create inlet : uid="<<known_uid<<" source_id="<<known_source_id<<"\n";
						m_inlet.reset(new lsl::stream_inlet(results[k]));
						break;
					}
				}
			}
		}


		if (m_inlet) {

			double captureTime = 0;
			try {
				captureTime = m_inlet->pull_sample(&stim, 1, PULL_TIMEOUT);
			}
			catch (const std::runtime_error &e) {
				std::cerr<<e.what()<<"\n";
				m_inlet.reset();
				captureTime = 0;
			}			


			if (captureTime) {
			  //std::cerr<<"StimulationThread this="<<this<<"  stim="<<stim<<"\n";
				
				emit receivedStimulation(stim);
			}
		}
	}

}

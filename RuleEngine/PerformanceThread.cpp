#include "PerformanceThread.hpp"

#include "lsl_cpp.h"

#include <iostream>
#include <cassert>

static const double RESOLVE_TIMEOUT=3; //in seconds
static const double PULL_TIMEOUT=1; //in seconds

PerformanceThread::PerformanceThread(bool *running, const QString &name, QObject *parent)
	: QThread(parent), m_running(running), m_name(name), m_inlet(0)
{}

PerformanceThread::~PerformanceThread()
{
}

void
PerformanceThread::setName(const QString &name)
{
	m_name = name;
}

void
PerformanceThread::run()
{
	std::cerr<<"PerformanceThread run this="<<this<<" m_name="<<m_name.toStdString()<<"\n";

	float perf;
	std::string known_uid;
	std::string known_source_id;
	std::vector<lsl::stream_info> results;
	const std::string name = m_name.toStdString();

	assert(m_running);

	while (*m_running) {

		const int minNumberOfStreamsToReturn = 1;
		results = lsl::resolve_stream("name", name, minNumberOfStreamsToReturn, RESOLVE_TIMEOUT);
		
		if (results.empty()) {
			std::cerr<<"no results for name "<<name<<"\n";
			if (m_inlet.get()) {
				std::cerr<<"reset inlet\n";
				m_inlet.reset();
			}
				
		}

		for (unsigned k=0; k<results.size(); ++k) {
			
			if (results[k].channel_count() == 1 && 
				results[k].channel_format() == lsl::cf_float32) {
				if (known_uid.empty() || known_uid != results[k].uid()) {
					if (known_source_id.empty() ||
						! (! results[k].source_id().empty() && known_source_id != results[k].source_id())) {

						known_uid = results[k].uid();
						known_source_id = results[k].source_id();
						std::cerr<<" create inlet : uid="<<known_uid<<" source_id="<<known_source_id<<"\n";

						//std::cerr<<"info from resolve:\n"<<results[k].as_xml()<<"\n";

						m_inlet.reset(new lsl::stream_inlet(results[k]));

						
						//std::cerr<<"info from inlet:\n"<<m_inlet->info().as_xml()<<"\n";

						break;
					}
				}
			}
		}

		if (m_inlet) {

			double captureTime = 0;
			try {
				captureTime = m_inlet->pull_sample(&perf, 1, PULL_TIMEOUT);
			}
			catch (const std::runtime_error &e) {
				std::cerr<<e.what()<<"\n";
				m_inlet.reset();
				captureTime = 0;
			}

			if (captureTime) {				
				emit receivedPerformance(perf);
			}

		}
	}
	
}

#include "PhraseSet.hpp"

#include <cassert>
#include <random>

#include <QDebug>
#include <QDir>
#include <iostream>//DEBUG
#include <time.h>


PhraseSet::PhraseSet()
{

}

PhraseSet::PhraseSet(const QString &dirName)
{
	load(dirName);
}


static
QString
readFile(const QString &filename)
{
	QString text;
	QFile f(filename);
	if (f.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		while (! f.atEnd())
		{
			QString line = f.readLine();
			text += line;
		}
	}
	else 
	{
		qDebug()<<"Unable to open file "<<filename;
	}
	return text;
}

bool
PhraseSet::addPhraseFile(const QString &filename)
{
  QString text = readFile(filename);
  if (! text.isEmpty()) {
    m_filePhrases.push_back(QPair<QString,QString>(filename, text));
    return true;
  }
  return false;
}

bool
PhraseSet::load(const QString &dirName)
{
	QDir in(dirName);
	if (! in.exists()) {
		qDebug()<<"invalid input directory "<<dirName;
		return false;
	}
	in.setFilter(QDir::Files|QDir::NoDotAndDotDot);
	QFileInfoList list = in.entryInfoList();
	qDebug()<<"list.size()="<<list.size()<<"\n";
	m_filePhrases.reserve(list.count());
	for (int i=0; i<list.count(); ++i) {
		const QFileInfo &file = list.at(i);
		if (file.isReadable()) {
			QString text = readFile(file.absoluteFilePath());
			if (! text.isEmpty()) 
			  m_filePhrases.push_back(QPair<QString, QString>(file.path(), text));
		}
	}
	
	return true;
}

int
PhraseSet::size() const
{
	return m_filePhrases.size();
}

QString
PhraseSet::phrase(int index) const
{
	assert(index>=0 && index<m_filePhrases.size());
	return m_filePhrases[index].second;
}

QString
PhraseSet::filename(int index) const
{
	assert(index>=0 && index<m_filePhrases.size());
	return m_filePhrases[index].first;
}



int
PhraseSet::randomIndex() const
{
	const int nbPhrases = size();
    if (nbPhrases <= 1)
        return 0;

    return randomIndex_aux();
}

int
PhraseSet::randomIndex_aux() const
{
    const int nbPhrases = size();
    assert(nbPhrases > 1);
    //std::random_device rd;
    //std::mt19937 gen(rd());
    std::mt19937 gen(time(0));
    std::uniform_int_distribution<int> dis(0, nbPhrases-1);
    const int index = dis(gen);
    assert(index>=0 && index<nbPhrases);

    return index;
}



QString
PhraseSet::randomPhrase() const
{
	const int index = randomIndex();
	if (index < size())	
		return m_filePhrases[index].second;
	return QString();
}

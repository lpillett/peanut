#include "MainWindow.hpp"

#include <cassert>
#include <random>
#include <QBoxLayout>
#include <QDebug>
#include <QFile>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QSpinBox>
#include <QGroupBox>

#include <QWebSocketServer>
#include <QWebSocket>
#include <QNetworkInterface>

#include <QFileInfo>
#include <fstream>

#include "openvibeDefines.hpp"
#include "PerformanceThread.hpp"
#include "StimulationThread.hpp"

#include "../ClientCompanion/BehaviorBlock.hpp"

#include <ctime>
#include <cmath>
#include <iostream>//DEBUG

static const quint16 PORT = 1234;  //TODO:settings in config file


const QString DEFAULT_START_RUN = "OVTK_StimulationId_BaselineStart";
const QString DEFAULT_END_RUN = "OVTK_GDF_End_Of_Session";
const QString DEFAULT_START_TRIAL = "OVTK_GDF_Start_Of_Trial";
const QString DEFAULT_END_TRIAL = "OVTK_GDF_End_Of_Trial";
const QString DEFAULT_END_MEASURE = "OVTK_StimulationId_TrialStop";

//const QString DEFAULT_STIM_OUTLET_NAME = "openvibeMarkers";

const int DEFAULT_NB_RUNS = 5;
const int DEFAULT_NB_TRIALS = 45;

const float DEFAULT_USER_EMOTIONAL_SUPPORT = 0.5f;
const float DEFAULT_USER_SOCIAL_PRESENCE = 6.0f;

const double COEFF_R2_BAD = 1.1525;
const double COEFF_R2_GOOD = 1.1249;

const double COEFF_S1_PERF_BAD = 1.2166;
const double COEFF_S1_PERF_GOOD = 0.9971;

const double COEFF_S1_PROG_BAD = -0.8536;
const double COEFF_S1_PROG_GOOD = 0.9660;

const bool ADAPTED_FB = true;

std::random_device RD;
std::mt19937 GEN(RD());

std::ofstream saveFile;

const int DEFAULT_NB_TRIALS_FOR_PROGRESS = 6;

static const QString PATH_ROOT = "../RuleEngine";
static const QString CONFIG_FILENAME = "inputs.cfg";
static const QString PHRASES_CATEGORIES = "phrases_categories.txt";
static const QString PHRASES_PATH = "../ClientCompanion/rsrc/voices/fr/phrases/";

static const int INVALID_STIM = -1;

MainWindow::MainWindow()
    : QWidget()
{
    //Test regression lineaire
    //TestRegressionLineaire();
    ///////////////////////////////////////////////////
    m_NUM_SUJET = 11;
    m_NUM_SESSION = 1;
    m_NUM_RUN = 2;

    m_perf_thresh_bad_R2 = 0.0;
    m_perf_thresh_good_R2 = 0.0;

    m_perf_thresh_good_S1 = 0.0;
    m_perf_thresh_bad_S1 = 0.0;

    m_prog_thresh_good = 0.0;
    m_prog_thresh_bad = 0.0;

    //////////////////////////////////////////////

    if(m_NUM_SESSION == 1 && m_NUM_RUN == 1){
        m_NUM_RUN = 2;
    }

    m_startRunStim = INVALID_STIM;
    m_endRunStim = INVALID_STIM;
    m_startTrialStim = INVALID_STIM;
    m_endTrialStim = INVALID_STIM ;
    m_endMeasureStim = INVALID_STIM;

    m_currentRun = -1;
    m_currentTrialTotal = -1;
    m_lastSocialInteraction = -1;

    m_result_act = 0.5;
    m_progress_act = 0.5;

    m_last_perf_emotion1 = None;
    m_last_perf_emotion2 = None;

    m_gMotivate = false;

    m_firstRunSignaled = false;
    m_midTrialSignaled = false;
    m_lastTrialSignaled = false;
    m_trialOngoing = false;

    build();

    populateCBs();

    setNbRuns(DEFAULT_NB_RUNS);
    setNbTrials(DEFAULT_NB_TRIALS);
    setUserEmotionalSupport(DEFAULT_USER_EMOTIONAL_SUPPORT);
    setUserSocialPresence(DEFAULT_USER_SOCIAL_PRESENCE);

    initCategories();

    QString path = PATH_ROOT;
    if (! QFileInfo(path).isDir()) {
        path = "../RuleEngine";
        if (! QFileInfo(path).isDir()) {
            qDebug()<<"ERROR: RuleEngine root dir not found";
            exit(10);
        }
    }
    path += "/";

    readConfigFile(path + CONFIG_FILENAME);

    readPhrases(path + PHRASES_CATEGORIES);

    initFaceBehaviorType2String();

    //TODO: - pouvoir charger des fichiers de configs
    //TODO: - avoir spécification des probas & seuils dans fichier de config
    //TODO: - influence présence sociale/emotional support sur probas

    populateSignals();

    startThreads();

    initWebSocketServer();
    updateConnectionState();

}

void
MainWindow::breakTime( int seconds)
{
    clock_t temp;
    temp = clock () + seconds * CLOCKS_PER_SEC ;
    while (clock() < temp) {}
}

void
MainWindow::TestRegressionLineaire()
{
    /*std::vector<double> x1 = {17, 19, 18};
    std::vector<double> y1 = {255, 280, 274};
    double res1 = getSlope(x1, y1);

    std::vector<double> x2 = {20, 22, 25};
    std::vector<double> y2 = {289, 336, 364};
    double res2 = getSlope(x2, y2);

    std::vector<double> x3 = {19, 16, 24};
    std::vector<double> y3 = {285, 245, 360};
    double res3 = getSlope(x3, y3);

    std::vector<double> x4 = {23, 21, 17};
    std::vector<double> y4 = {341, 316, 280};
    double res4 = getSlope(x4, y4);

    std::vector<double> x5 = {18, 20, 24};
    std::vector<double> y5 = {240, 297, 355};
    double res5 = getSlope(x5, y5);*/

    std::vector<double> x1 = {1, 2, 3};
    std::vector<double> y1 = {0.25, 0.5, 0.75};
    double res1 = getSlope(x1, y1);

    std::vector<double> x2 = {4, 5, 6};
    std::vector<double> y2 = {0.75, 0.5, 0.25};
    double res2 = getSlope(x2, y2);

    std::vector<double> x3 = {1, 2, 3};
    std::vector<double> y3 = {0.1, 0.6, 0.8};
    double res3 = getSlope(x3, y3);

    std::vector<double> x4 = {4, 5, 6};
    std::vector<double> y4 = {0.9, 0.45, 0.3};
    double res4 = getSlope(x4, y4);

    std::vector<double> x5 = {1, 2, 3};
    std::vector<double> y5 = {0.225, 0.63, 0.874};
    double res5 = getSlope(x5, y5);

    std::cout<<"res1=" + std::to_string(res1) + " " + "res2=" + std::to_string(res2) + " " +
               "res3=" + std::to_string(res3) + " " + "res4=" + std::to_string(res4) + " " +
               "res5=" + std::to_string(res5);
}

void
MainWindow::initWebSocketServer()
{
    m_pWebSocketServer = new QWebSocketServer(QStringLiteral("Server"), QWebSocketServer::NonSecureMode, this);

    const quint16 port = PORT;
    if (m_pWebSocketServer->listen(QHostAddress::Any, port)) {
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection,
                this, &MainWindow::onNewConnection);
        //connect(m_pWebSocketServer, &QWebSocketServer::closed, this, &MainWindow::closed);
    }
}

void
MainWindow::closeWebSocketServer()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
    m_waitings.clear();
}

static
QString
getIPAddress()
{
    QString s;
    bool prev = false;
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {
            if (prev)
                s+= " ;";
            s += address.toString();
            prev = true;
        }
    }
    return s;
}


void
MainWindow::build()
{
    QLabel *m_emotionalLabel = new QLabel(tr("Emotional support: "), this);
    m_emotionalSB = new QDoubleSpinBox(this);
    m_emotionalSB->setMinimum(1.0);
    m_emotionalSB->setMaximum(12.0);
    m_emotionalSB->setSingleStep(0.1);
    QLabel *m_presenceLabel = new QLabel(tr("Social presence: "), this);
    m_presenceSB = new QDoubleSpinBox(this);
    m_presenceSB->setMinimum(1.0);
    m_presenceSB->setMaximum(12.0);
    m_presenceSB->setSingleStep(0.1);


    QLabel *nbRunsL = new QLabel(tr("Nb runs: "), this);
    m_nbRunsSB = new QSpinBox(this);
    m_nbRunsSB->setMinimum(1);
    QLabel *nbTrialsL = new QLabel(tr("Nb trials: "), this);
    m_nbTrialsSB = new QSpinBox(this);
    m_nbTrialsSB->setMinimum(1);

    QLabel *startRunL = new QLabel(tr("Start run stim: "), this);
    m_startRunCB = new QComboBox(this);
    QLabel *endRunL = new QLabel(tr("End run stim: "), this);
    m_endRunCB = new QComboBox(this);
    QLabel *startTrialL = new QLabel(tr("Start trial stim: "), this);
    m_startTrialCB = new QComboBox(this);
    QLabel *endTrialL = new QLabel(tr("End trial stim: "), this);
    m_endTrialCB = new QComboBox(this);
    QLabel *endMeasureL = new QLabel(tr("End measure stim: "), this);
    m_endMeasureCB = new QComboBox(this);

    QLabel *nbTrialsProgressL = new QLabel(tr("Nb trials for progress computation: "), this);
    m_nbTrialsProgressSB = new QSpinBox(this);
    m_nbTrialsProgressSB->setMinimum(1);
    m_nbTrialsProgressSB->setValue(DEFAULT_NB_TRIALS_FOR_PROGRESS);


    QHBoxLayout *hla = new QHBoxLayout;
    hla->addWidget(m_emotionalLabel);
    hla->addWidget(m_emotionalSB);
    QHBoxLayout *hlb = new QHBoxLayout;
    hlb->addWidget(m_presenceLabel);
    hlb->addWidget(m_presenceSB);
    QGroupBox *userGB = new QGroupBox(tr("User's profile"), this);
    QVBoxLayout *vla = new QVBoxLayout;
    vla->addLayout(hla);
    vla->addLayout(hlb);
    userGB->setLayout(vla);


    QHBoxLayout *hl1 = new QHBoxLayout;
    hl1->addWidget(nbRunsL);
    hl1->addWidget(m_nbRunsSB);
    hl1->addWidget(nbTrialsL);
    hl1->addWidget(m_nbTrialsSB);
    hl1->addStretch();
    QHBoxLayout *hl2 = new QHBoxLayout;
    hl2->addWidget(startRunL);
    hl2->addWidget(m_startRunCB);
    QHBoxLayout *hl2b = new QHBoxLayout;
    hl2b->addWidget(endRunL);
    hl2b->addWidget(m_endRunCB);
    QHBoxLayout *hl3 = new QHBoxLayout;
    hl3->addWidget(startTrialL);
    hl3->addWidget(m_startTrialCB);
    QHBoxLayout *hl3b = new QHBoxLayout;
    hl3b->addWidget(endTrialL);
    hl3b->addWidget(m_endTrialCB);
    QHBoxLayout *hl3c = new QHBoxLayout;
    hl3c->addWidget(endMeasureL);
    hl3c->addWidget(m_endMeasureCB);

    QHBoxLayout *hl4 = new QHBoxLayout;
    hl4->addWidget(nbTrialsProgressL);
    hl4->addWidget(m_nbTrialsProgressSB);

    QGroupBox *configGB = new QGroupBox(tr("Preferences"), this);
    QVBoxLayout *vlb = new QVBoxLayout;
    vlb->addLayout(hl1);
    vlb->addLayout(hl2);
    vlb->addLayout(hl2b);
    vlb->addLayout(hl3);
    vlb->addLayout(hl3b);
    vlb->addLayout(hl3c);
    vlb->addLayout(hl4);
    configGB->setLayout(vlb);



    QLabel *ipLabel = new QLabel(tr("IP address: "), this);
    QLabel *ipLabelV = new QLabel(this);
    ipLabelV->setText(getIPAddress());
    QLabel *portLabel = new QLabel(tr("Port: "), this);
    QLabel *portLabelV = new QLabel(this);
    portLabelV->setText(QString::number(PORT));
    QHBoxLayout *hL0 = new QHBoxLayout;

    m_connectionL = new QLabel(this);

    hL0->addWidget(ipLabel);
    hL0->addWidget(ipLabelV);
    hL0->addWidget(portLabel);
    hL0->addWidget(portLabelV);
    hL0->addStretch();
    hL0->addWidget(m_connectionL);


    QGroupBox *serverGB = new QGroupBox(tr("Server"), this);
    serverGB->setLayout(hL0);


    m_signalsL = new QVBoxLayout;
    QGroupBox *signalsGB = new QGroupBox(tr("Signals"), this);
    signalsGB->setLayout(m_signalsL);

    QVBoxLayout *vl2 = new QVBoxLayout;

    m_infoLabel = new QLabel(this);
    m_sendLabel0 = new QLabel(this);
    m_sendLabel = new QLabel(this);

    vl2->addWidget(userGB);
    vl2->addWidget(serverGB);
    vl2->addWidget(configGB);
    vl2->addWidget(signalsGB);
    vl2->addWidget(m_infoLabel);
    vl2->addWidget(m_sendLabel0);
    vl2->addWidget(m_sendLabel);

    setLayout(vl2);
}

void
MainWindow::populateCBs()
{
    init_OpenViBE_maps();

    for (std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.begin();
         it != OVTK_stringToIntMap.end();
         ++it) {

        const QString n = QString::fromStdString(it->first);
        const int v = it->second;

        m_startRunCB->addItem(n, v);
        m_endRunCB->addItem(n, v);
        m_startTrialCB->addItem(n, v);
        m_endTrialCB->addItem(n, v);
        m_endMeasureCB->addItem(n, v);
    }

    /*
  //set default values
  setCB(m_startRunCB, DEFAULT_START_RUN);
  setCB(m_endRunCB, DEFAULT_END_RUN);
  setCB(m_startTrialCB, DEFAULT_START_TRIAL);
  setCB(m_endTrialCB, DEFAULT_END_TRIAL);
  setCB(m_endMeasureCB, DEFAULT_END_MEASURE);
  */

}


void
MainWindow::populateSignals()
{
    assert(m_signalsL);

    QLabel *labelS = new QLabel(m_stimulationName, this);
    m_stimulationLabel = new QLabel(tr("-"), this);
    QHBoxLayout *hbS = new QHBoxLayout;
    hbS->addWidget(labelS);
    hbS->addWidget(m_stimulationLabel);
    m_signalsL->addLayout(hbS);


    QLabel *labelP = new QLabel(m_performanceName, this);
    m_performanceLabel = new QLabel(tr("-"), this);
    QHBoxLayout *hbP = new QHBoxLayout;
    hbP->addWidget(labelP);
    hbP->addWidget(m_performanceLabel);
    m_signalsL->addLayout(hbP);

}

void
MainWindow::startThreads()
{
    m_running = true;

    StimulationThread *stimTh = new StimulationThread(&m_running, m_stimulationName, this);
    connect(stimTh, &StimulationThread::receivedStimulation, this, &MainWindow::processStimulation);
    //connect(stimTh, &StimulationThread::finished, stimTh, &QObject::deleteLater);
    m_stimulationThread = stimTh;
    stimTh->start();

    PerformanceThread *perfTh = new PerformanceThread(&m_running, m_performanceName, this);
    connect(perfTh, &PerformanceThread::receivedPerformance, this, &MainWindow::processPerformance);
    //connect(perfTh, &PerformanceThread::finished, perfTh, &QObject::deleteLater);
    m_performanceThread = perfTh;
    perfTh->start();

    m_performances.clear();
    m_stimulation = 0;
}


MainWindow::~MainWindow()
{
    m_running = false;

    m_stimulationThread->wait();

    m_performanceThread->wait();
}


void MainWindow::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &MainWindow::processReceivedTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &MainWindow::processReceivedBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &MainWindow::socketDisconnected);

    m_clients << pSocket;
    m_waitings << false;
    assert(m_waitings.size() == m_clients.size());

    updateConnectionState();
}

/*
  Process text message received from client
*/
void MainWindow::processReceivedTextMessage(const QString &/*message*/)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    assert(m_waitings.size() == m_clients.size());
    for (int i=0; i<m_clients.size(); ++i) {
        if (m_clients[i] == pClient) {
            if (! m_waitings[i]) {
                qDebug()<<"ERROR: not waiting and still receiving an answer";
            }
            //TODO: check the kind of message
            m_waitings[i] = false;
            break;
        }
    }
}

/*
  Process binary message received from client.
*/
void MainWindow::processReceivedBinaryMessage(const QByteArray &message)
{
    qDebug()<<"rcvBin msg";

    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    assert(m_waitings.size() == m_clients.size());
    for (int i=0; i<m_clients.size(); ++i) {
        if (m_clients[i] == pClient) {
            if (! m_waitings[i]) {
                qDebug()<<"ERROR: not waiting and still receiving an answer";
            }

            QByteArray ba = message;
            QDataStream strm(&ba, QIODevice::ReadOnly);
            quint8 r;
            strm >> r;
            if (strm.status() == QDataStream::Ok) {
                qDebug()<<"Answer received: "<<r;
            }

            m_waitings[i] = false;

            //updateTrigger();

            break;
        }
    }
}

void MainWindow::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    if (pClient) {
        //m_clients.removeAll(pClient);
        //m_waitings.clear();
        //pClient->deleteLater();

        assert(m_waitings.size() == m_clients.size());
        QList<bool>::iterator itW = m_waitings.begin();
        QList<QWebSocket *>::iterator itC = m_clients.begin();
        for ( ; itC != m_clients.end(); ) {
            if (*itC == pClient) {
                pClient->deleteLater();
                itW = m_waitings.erase(itW);
                itC = m_clients.erase(itC);
                break;
            }
            else {
                ++itC;
                ++itW;
            }
        }
        assert(m_waitings.size() == m_clients.size());

        updateConnectionState();
    }
}

/*
  Send message to client.
 */
bool MainWindow::sendMessage(const QByteArray &msg)
{
    bool sent = false;
    assert(m_waitings.size() == m_clients.size());
    for (int i=0; i<m_clients.size(); ++i) {
        if (! m_waitings[i]) {
            m_waitings[i] = true;
            m_clients[i]->sendBinaryMessage(msg);
            sent = true;
        }
    }
    return sent;
}

void
MainWindow::updateConnectionState()
{
    if (! m_clients.empty()) {
        m_connectionL->setStyleSheet("QLabel { background-color : green; color : white; }");
        const int nbClients = m_clients.size();
        if (nbClients == 1)
            m_connectionL->setText(tr("1 client"));
        else
            m_connectionL->setText(tr("%1 clients").arg(nbClients));
    }
    else {
        m_connectionL->setStyleSheet("QLabel { background-color : red; color : white; }");
        m_connectionL->setText(tr("0 client"));
    }
}


void
MainWindow::setUserEmotionalSupport(float v)
{
    assert(m_emotionalSB);
    m_emotionalSB->setValue(v);
}

void
MainWindow::setUserSocialPresence(float v)
{
    assert(m_presenceSB);
    m_presenceSB->setValue(v);
}


static
void
setCB(QComboBox *cb, int s)
{
    std::map<int, std::string>::const_iterator it = OVTK_intToStringMap.find(s);
    if (it != OVTK_intToStringMap.end())
        cb->setCurrentText(QString::fromStdString(it->second));
}

//warning: several stimulation names may correspond to the same int value.

void MainWindow::setStartRunStim(int v)
{
    setCB(m_startRunCB, v);
    m_startRunStim = v;
}

void MainWindow::setStartRunStim(const QString &t)
{
    std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.find(t.toStdString());
    if (it != OVTK_stringToIntMap.end()) {
        m_startRunCB->setCurrentText(t);
        m_startRunStim = it->second;
    }
}


int MainWindow::startRunStim() const
{
    return m_startRunStim;
}

void MainWindow::setEndRunStim(int v)
{
    setCB(m_endRunCB, v);
    m_endRunStim = v;
}

void MainWindow::setEndRunStim(const QString &t)
{
    std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.find(t.toStdString());
    if (it != OVTK_stringToIntMap.end()) {
        m_endRunCB->setCurrentText(t);
        m_endRunStim = it->second;
    }
}

int MainWindow::endRunStim() const
{
    return m_endRunStim;
}

void MainWindow::setStartTrialStim(int v)
{
    setCB(m_startTrialCB, v);
    m_startTrialStim = v;
}

void MainWindow::setStartTrialStim(const QString &t)
{
    std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.find(t.toStdString());
    if (it != OVTK_stringToIntMap.end()) {
        m_startTrialCB->setCurrentText(t);
        m_startTrialStim = it->second;
    }
}

int MainWindow::startTrialStim() const
{
    return m_startTrialStim;
}

void MainWindow::setEndTrialStim(int v)
{
    setCB(m_endTrialCB, v);
    m_endTrialStim = v;

}

void MainWindow::setEndTrialStim(const QString &t)
{
    std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.find(t.toStdString());
    if (it != OVTK_stringToIntMap.end()) {
        m_endTrialCB->setCurrentText(t);
        m_endTrialStim = it->second;
    }
}

int MainWindow::endTrialStim() const
{
    return m_endTrialStim;
}

void MainWindow::setEndMeasureStim(int v)
{
    setCB(m_endMeasureCB, v);
    m_endMeasureStim = v;
}

void MainWindow::setEndMeasureStim(const QString &t)
{
    std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.find(t.toStdString());
    if (it != OVTK_stringToIntMap.end()) {
        m_endMeasureCB->setCurrentText(t);
        m_endMeasureStim = it->second;
    }
}

int MainWindow::endMeasureStim() const
{
    return m_endMeasureStim;
}


void MainWindow::setNbRuns(int n)
{
    assert(m_nbRunsSB);
    m_nbRunsSB->setValue(n);
}

int MainWindow::nbRuns() const
{
    assert(m_nbRunsSB);
    return m_nbRunsSB->value();
}

void MainWindow::setNbTrials(int n)
{
    assert(m_nbTrialsSB);
    m_nbTrialsSB->setValue(n);
}

int MainWindow::nbTrials() const
{
    assert(m_nbTrialsSB);
    return m_nbTrialsSB->value();
}

int MainWindow::nbTrialsTotal() const
{
    return nbRuns()*nbTrials();
}

int MainWindow::nbTrialsForProgress() const
{
    return m_nbTrialsProgressSB->value();
}

bool
MainWindow::readConfigFile(const QString &filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug()<<"ERROR: Unable to read config file: "<<filename;
        return false;
    }

    const QChar SEP = '=';
    const QString PERF = "perf";
    const QString STIM = "stim";
    const QString NBRUNS = "runs";
    const QString NBTRIALS = "trials";
    const QString RUNSTART = "runStart";
    const QString RUNEND = "runEnd";
    const QString TRIALSTART = "trialStart";
    const QString TRIALEND = "trialEnd";
    const QString MEASUREEND = "measureEnd";

    init_OpenViBE_maps();

    int line = 1;
    QStringList list;
    const qint64 bufSize = 2048;
    char buf[bufSize];
    while (!file.atEnd()) {
        qint64 lineLength = file.readLine(buf, bufSize);
        if (lineLength > 0) {
            QString s = QLatin1String(buf);
            s = s.trimmed();
            if (s.left(1) == QStringLiteral("#") || s.length() == 0)
                continue; // skip comments and empty lines

            list = s.split(SEP);
            if (list.count() == 2) {
                const QString &s0 = list.at(0);
                const QString &s1 = list.at(1);
                if (s0 == RUNSTART) {
                    auto it = OVTK_stringToIntMap.find(s1.toStdString());
                    if (it != OVTK_stringToIntMap.end()) {
                        setStartRunStim(s1);
                    }
                    else {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n unknown stimulation "<<s1;
                    }
                }
                else if (s0 == RUNEND) {
                    auto it = OVTK_stringToIntMap.find(s1.toStdString());
                    if (it != OVTK_stringToIntMap.end()) {
                        setEndRunStim(s1);
                    }
                    else {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n unknown stimulation "<<s1;
                    }
                }
                else if (s0 == TRIALSTART) {
                    auto it = OVTK_stringToIntMap.find(s1.toStdString());
                    if (it != OVTK_stringToIntMap.end()) {
                        setStartTrialStim(s1);
                    }
                    else {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n unknown stimulation "<<s1;
                    }
                }
                else if (s0 == TRIALEND) {
                    auto it = OVTK_stringToIntMap.find(s1.toStdString());
                    if (it != OVTK_stringToIntMap.end()) {
                        setEndTrialStim(s1);
                    }
                    else {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n unknown stimulation "<<s1;
                    }
                }
                else if (s0 == MEASUREEND) {
                    auto it = OVTK_stringToIntMap.find(s1.toStdString());
                    if (it != OVTK_stringToIntMap.end()) {
                        setEndMeasureStim(s1);
                    }
                    else {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n unknown stimulation "<<s1;
                    }
                }
                else if (s0 == NBRUNS) {
                    bool ok;
                    int v = s1.toUInt(&ok);
                    if (! ok) {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n invalid value for "<<NBRUNS;
                    }
                    else {
                        setNbRuns(v);
                    }
                }
                else if (s0 == NBTRIALS) {
                    bool ok;
                    int v = s1.toUInt(&ok);
                    if (! ok) {
                        qDebug()<<"Warning: line "<<line<<" in config file "<<filename<<"\n invalid value for "<<NBTRIALS;
                    }
                    else {
                        setNbTrials(v);
                    }

                }
                else if (s0 == PERF) {
                    m_performanceName = s1.simplified();
                }
                else if (s0 == STIM) {
                    m_stimulationName = s1.simplified();
                }
                else {
                    qDebug()<<"Warning: ignore line "<<line<<" in config file "<<filename<<"\n  "<<s;
                }
            }
            else {
                qDebug()<<"Warning: ignore line "<<line<<" in config file "<<filename<<"\n  "<<s;
            }
        }
        ++line;
    }

    if (m_performanceName.isEmpty()
            && m_stimulationName.isEmpty()) {
        qDebug()<<"ERROR: No input specified in config file: "<<CONFIG_FILENAME;
        return false;
    }
    if (m_startRunStim == INVALID_STIM ||
            m_endRunStim == INVALID_STIM ||
            m_startTrialStim == INVALID_STIM ||
            m_endMeasureStim == INVALID_STIM ) {

        if (m_startRunStim == INVALID_STIM)
            qDebug()<<"ERROR: no stimulation specified for "<<RUNSTART;
        if (m_endRunStim == INVALID_STIM)
            qDebug()<<"ERROR: no stimulation specified for "<<RUNEND;
        if (m_startTrialStim == INVALID_STIM)
            qDebug()<<"ERROR: no stimulation specified for "<<TRIALSTART;
        if (m_endMeasureStim == INVALID_STIM)
            qDebug()<<"ERROR: no stimulation specified for "<<MEASUREEND;

        return false;
    }



    return true;
}

void
MainWindow::processStimulation(int st)
{
    QObject *s = sender();

    assert(m_stimulationThread == s);
    m_stimulationLabel->setText(QString::fromStdString(OVTK_intToStringMap[st]));
    m_stimulation = st;
    updateStimulation();
}

void
MainWindow::processPerformance(float p)
{
    QObject *s = sender();

    assert(m_performanceThread == s);
    m_performanceLabel->setText(QString::number(p));
    addPerformance(p);
    updatePerformance();
}

void
MainWindow::addPerformance(float p)
{
    //TODO: check p is in [0; 1]
    assert(p>=0.f && p<=1.f);
    //Enregistre l'ensemble des performances
    m_performances.push_back(p);

    //store only m_nbTrialsForProgress last performance values.
    /*const int sz = m_performances.size();
    if (sz == nbTrialsForProgress()) {
        for (int i=1; i<sz; ++i) {
            m_performances[i-1] = m_performances[i];
        }
        m_performances[sz-1] = p;
    }
    else {
        m_performances.push_back(p);
    }*/
}


/*
  Return a probability, that is a random number in [0; 1]
 */
static inline
float
random01()
{
    //std::cout<<"rand="<<(float)GEN()/RD.max();
    return (float)GEN()/RD.max();
}

/*
  check that probability @a p1 is "superior to" probability @a p2.

  As both probabilities are in [0;1]
  it actually means p1 < p2.
 */
static inline
bool
checkProba(float p1, float p2)
{
    assert(p1>=0.f && p1<=1.f);
    assert(p2>=0.f && p2<=1.f);
    return p1 < p2;
}


/*
  Get random FaceBehaviorType among three FaceBehaviorTypes.
 */
static inline
FaceBehaviorType
getRandomFaceBehavior(FaceBehaviorType f1, FaceBehaviorType f2, FaceBehaviorType f3)
{
    float proba = random01();
    if (proba < 1.f/3.f) {
        return f1;
    }
    else if (proba < 2.f/3.f) {
        return f2;
    }
    return f3;
}

/*
  Get random FaceBehaviorType among two FaceBehaviorTypes.
 */
static inline
FaceBehaviorType
getRandomFaceBehavior(FaceBehaviorType f1, FaceBehaviorType f2)
{
    float proba = random01();
    if (proba < 0.5f)
        return f1;
    return f2;
}

float
MainWindow::getProbabilityOfPhraseCategory(PhraseCategory c)
{
    assert(c < SIZE_PhraseCategory);
    return m_phraseCategoryProbas[c];
}

float
MainWindow::getProbabilityOfEmotionCategory(EmotionCategory c)
{
    assert(c < SIZE_EmotionCategory);
    return m_emotionCategoryProbas[c];
}



/*
  Get random PhraseCatagory among two PhraseCategory according to their respective probability.
*/
MainWindow::PhraseCategory
MainWindow::getRandomPhraseCategoryFromTwo(float proba, PhraseCategory c1, PhraseCategory c2)
{
    assert(proba>=0.0f && proba<=1.0f);
    bool isFullC1 = m_phraseCategory2Set[c1].isCacheFull();
    bool isFullC2 = m_phraseCategory2Set[c2].isCacheFull();

    if(isFullC1 && isFullC2)
    {
        std::cerr<<"TEST_getRandomPhraseCategoryFromTwo_ALLFULL \n";
        m_phraseCategory2Set[c1].emptyCache();
        m_phraseCategory2Set[c2].emptyCache();
    }
    else if (isFullC1)
    {
        return c2;
    }
    else if (isFullC2)
    {
        return c1;
    }

    const float proba_c1 = getProbabilityOfPhraseCategory(c1);
    const float proba_c2 = getProbabilityOfPhraseCategory(c2);
    const float sum = proba_c1 + proba_c2;
    if (proba*sum < proba_c1)
        return c1;
    return c2;

}

/*
  Get random PhraseCatagory among two PhraseCategory according to their respective probability.
*/
MainWindow::PhraseCategory
MainWindow::getRandomPhraseCategoryFromList(int size, PhraseCategory classes[])
{
    //checking not all phrases have already been done.
    bool allFull = true;
    for (int i=0; i<size; i++)
    {
        allFull = allFull && (m_phraseCategory2Set[classes[i]].isCacheFull());
    }

    //if so, emptying all caches to enable the re-use of phrases already said
    if (allFull)
    {
        for (int i=0; i<size; i++)
        {
            m_phraseCategory2Set[classes[i]].emptyCache();
        }
    }


    //float proba = ;
    //finding a phrase category for which not all phrases have already been said
    int randIdx = 0;
    do{
        randIdx = int(std::floor(random01()*size));
    } while (m_phraseCategory2Set[classes[randIdx]].isCacheFull());

    //std::cerr << "PROBA CATEGORY " << proba << " NUM CATEGORY " << int(std::floor(proba*(sizeof(classes)/sizeof(classes[0]))))<<" SIZE OF "<< int(sizeof(classes)/sizeof(classes[0])) << "\n";
    //float probas[sizeof(classes)/sizeof(classes[0])] = {};
    //float sum = 0;
    //float sum_actu = 0;

    /*for (int i = 0; i < sizeof(classes)/sizeof(classes[0]) ; i++){
        probas[i] = getProbabilityOfPhraseCategory(classes[i]);
        sum += getProbabilityOfPhraseCategory(classes[i]);
    }

    for (int i = 0; i < sizeof(classes)/sizeof(classes[0]) ; i++){
        sum_actu += probas[i];
        if(proba*sum>sum_actu && proba*sum<sum_actu+probas[i]){
            return classes[i];
        }
    }*/
    return classes[randIdx];
}

static
QString
removePathAndExtension(const QString &file)
{
    return QFileInfo(file).baseName();
}

/*
  Get random file for the given PhraseCategory.
 */
QString
MainWindow::getRandomPhraseFilename(PhraseCategory phraseCategory)
{
    assert(phraseCategory < m_phraseCategory2Set.size());
    //std::cerr<<"ERROR 21 Index \n" << phraseCategory << " Nom " << m_phraseCategory2String[phraseCategory].toStdString() + "\n";
    const int index = m_phraseCategory2Set[phraseCategory].randomIndex();
    if (index < m_phraseCategory2Set[phraseCategory].size()) {
        return removePathAndExtension(m_phraseCategory2Set[phraseCategory].filename(index));
        //We remove path & extension from filename to sent it to the client
    }
    else {
        qDebug()<<"WARNING: no phrase for category; "<<phraseCategory;
        return QString();
    }
}


MainWindow::EmotionCategory
MainWindow::getRandomEmotion(MainWindow::EmotionCategory e1, MainWindow::EmotionCategory e2)
{
    const float proba = random01();
    //TODO: code duplication with getRandomPhraseCategory
    const float proba_e1 = getProbabilityOfEmotionCategory(e1);
    const float proba_e2 = getProbabilityOfEmotionCategory(e2);
    const float sum = proba_e1 + proba_e2;
    if (proba*sum < proba_e1)
        return e1;
    return e2;
}

MainWindow::EmotionCategory
MainWindow::getRandomEmotion(MainWindow::EmotionCategory e1, MainWindow::EmotionCategory e2, MainWindow::EmotionCategory e3)
{
    const float proba = random01();
    //TODO: code duplication with getRandomPhraseCategory
    const float proba_e1 = getProbabilityOfEmotionCategory(e1);
    const float proba_e2 = getProbabilityOfEmotionCategory(e2);
    const float proba_e3 = getProbabilityOfEmotionCategory(e2);
    const float sum = proba_e1 + proba_e2 + proba_e3;
    const float ps = proba*sum;
    if (ps < proba_e1)
        return e1;
    else if (ps < proba_e1+proba_e2)
        return e2;
    return e3;
}


/*
  Send BehaviorBlock to clients/companions.
 */
bool
MainWindow::sendBehaviorBlock(const BehaviorBlock &bl)
{
    //serialize BehaviorBlock in a QByteArray
    QByteArray msg;
    QDataStream strm(&msg, QIODevice::WriteOnly);
    bool ok = serialize(strm, bl);

    if (ok) {
        bool sent = sendMessage(msg);
        if (! sent) {
            ok = sent;
        }

    }

    return ok;
}


void
MainWindow::initFaceBehaviorType2String()
{
    m_phraseCategory2String[Temporal_StartSession] = "Temporal_StartSession";
    m_phraseCategory2String[Temporal_MiddleSession] = "Temporal_MiddleSession";
    m_phraseCategory2String[Temporal_Next] = "Temporal_Next";
    m_phraseCategory2String[Temporal_NearEndSession] = "Temporal_NearEndSession";
    m_phraseCategory2String[Temporal_LastTrialOfSession] = "Temporal_LastTrialOfSession";
    m_phraseCategory2String[Temporal_EndSession] = "Temporal_EndSession";
    m_phraseCategory2String[Progress_Good] = "Progress_Good";
    m_phraseCategory2String[Progress_Good_nPD] = "Progress_Good_nPD";
    m_phraseCategory2String[Progress_Good_PE] = "Progress_Good_PE";
    m_phraseCategory2String[Progress_VeryGood] = "Progress_VeryGood";
    m_phraseCategory2String[Progress_VeryGood_PE] = "Progress_VeryGood_PE";
    m_phraseCategory2String[Progress_VeryGood_nPD] = "Progress_VeryGood_nPD";
    m_phraseCategory2String[Result_Good] = "Result_Good";
    m_phraseCategory2String[Result_Good_PE] = "Result_Good_PE";
    m_phraseCategory2String[Result_Good_PD] = "Result_Good_PD";
    m_phraseCategory2String[Result_VeryGood] = "Result_VeryGood";
    m_phraseCategory2String[Result_VeryGood_nPD] = "Result_VeryGood_nPD";
    m_phraseCategory2String[Result_VeryGood_PE] = "Result_VeryGood_PE";
    m_phraseCategory2String[Support_Effort] = "Support_Effort";
    m_phraseCategory2String[Support_Effort_PE] = "Support_Effort_PE";
    m_phraseCategory2String[Support_Effort_PD] = "Support_Effort_PD";
    m_phraseCategory2String[General_Effort] = "General_Effort";
    m_phraseCategory2String[General_Effort_PD] = "General_Effort_PD";
    m_phraseCategory2String[General_Effort_PE] = "General_Effort_PE";
    m_phraseCategory2String[General_Empathy] = "General_Empathy";
    m_phraseCategory2String[General_Empathy_PE] = "General_Empathy_PE";
    m_phraseCategory2String[General_Empathy_PD] = "General_Empathy_PD";
    m_phraseCategory2String[Strategy_Change] = "Strategy_Change";
    m_phraseCategory2String[Strategy_Keep] = "Strategy_Keep";
    m_phraseCategory2String[Strategy_Keep_nPD] = "Strategy_Keep_nPD";
    m_phraseCategory2String[Temp_Category] = "Temp_Category";

    m_emotions2String[Ecstasy] = "Ecstasy";
    m_emotions2String[Joy] = "Joy";
    m_emotions2String[Serenity] = "Serenity";
    m_emotions2String[Amazement] = "Amazement";
    m_emotions2String[Surprise] = "Surprise";
    m_emotions2String[Distraction] = "Distraction";
    m_emotions2String[Admiration] = "Admiration";
    m_emotions2String[Trusting] = "Trusting";
    m_emotions2String[Acceptance] = "Acceptance";
    m_emotions2String[Thoughtful] = "Thoughtful";
    m_emotions2String[Sadness] = "Sadness";
    m_emotions2String[Boredom] = "Boredom";
    m_emotions2String[Disgusted] = "Disgusted";
    m_emotions2String[Anger] = "Anger";
    m_emotions2String[Annoyance] = "Annoyance";


    //TODO: should be available from BehaviorType.hpp
    m_faceBehaviorType2String[Idle] = "Idle";
    m_faceBehaviorType2String[Idle] = "Neutre_02";
    m_faceBehaviorType2String[Idle] = "Neutre_03";
    m_faceBehaviorType2String[Idle] = "Neutre_04";
    m_faceBehaviorType2String[Blink] = "Blink";
    m_faceBehaviorType2String[Happy] = "Happy";
    m_faceBehaviorType2String[Surprised] = "Surprised";
    m_faceBehaviorType2String[Idle] = "Trust";
    m_faceBehaviorType2String[Sad] = "Sad";
    m_faceBehaviorType2String[Angry] = "Angry";
    m_faceBehaviorType2String[Bored] = "Bored";
}

void
MainWindow::updateSentView0(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1)
{
    QString text = QString("(") + m_phraseCategory2String[phraseCategory1] + QString(", ") + m_faceBehaviorType2String[face1] + QString(" ") + QString::number(intensity1) + QString(")");
    m_sendLabel0->setText(text);
}

void
MainWindow::updateSentView0(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1,
                            PhraseCategory phraseCategory2, FaceBehaviorType face2, float intensity2)
{
    QString text = QString("( (") + m_phraseCategory2String[phraseCategory1] + QString(", ") + m_faceBehaviorType2String[face1] + QString(" ") + QString::number(intensity1) + QString(") + (")
            + m_phraseCategory2String[phraseCategory2] + QString(", ") + m_faceBehaviorType2String[face2] + QString(" ") + QString::number(intensity2) + QString(") )");
    m_sendLabel0->setText(text);
}

void
MainWindow::updateSentView0(PhraseCategory phraseCategory1, EmotionCategory emotion1, EmotionCategory emotion2)
{
    QString text = QString("(") + m_phraseCategory2String[phraseCategory1] + QString(", (") + m_emotions2String[emotion1] + QString(" ") + m_emotions2String[emotion2] + QString(") )");
    m_sendLabel0->setText(text);
}

void
MainWindow::updateSentView0(PhraseCategory phraseCategory1,
                            EmotionCategory emotion1a, EmotionCategory emotion1b,
                            PhraseCategory phraseCategory2,
                            FaceBehaviorType face2, float intensity2)
{
    QString text = QString("(") + m_phraseCategory2String[phraseCategory1] + QString(", (") + m_emotions2String[emotion1a] + QString(" ") + m_emotions2String[emotion1b] + QString(")\n+ ") + m_phraseCategory2String[phraseCategory2] + QString(", ") + m_faceBehaviorType2String[face2] + QString(" ") + QString::number(intensity2) + QString(") )");
    m_sendLabel0->setText(text);
}

void
MainWindow::updateSentView(FaceBehaviorType face1, float intensity1, const QString &phraseFilename1)
{
    QString text = QString("(") + m_faceBehaviorType2String[face1] + QString(" ") + QString::number(intensity1) + QString(", ") + phraseFilename1.trimmed() + QString(" )");
    m_sendLabel->setText(text);
}

void
MainWindow::updateSentView(FaceBehaviorType face1, float intensity1, const QString &phraseFilename1,
                           FaceBehaviorType face2, float intensity2, const QString &phraseFilename2)
{
    QString text = QString("(") + m_faceBehaviorType2String[face1] + QString(" ") + QString::number(intensity1) + QString(", ") + phraseFilename1.trimmed() + " ) \n+ (" + m_faceBehaviorType2String[face2] + QString(" ") + QString::number(intensity2) + phraseFilename2.trimmed() + QString(")");
    m_sendLabel->setText(text);
}

void
MainWindow::updateSentView(FaceBehaviorType face1a, float intensity1a,
                           FaceBehaviorType face1b, float intensity1b,
                           const QString &phraseFilename1)
{
    QString text = QString("(") + m_faceBehaviorType2String[face1a] + QString(" ") + QString::number(intensity1a) + QString(" + ") +  m_faceBehaviorType2String[face1b] + QString(" ") + QString::number(intensity1b) + QString(", ") + phraseFilename1.trimmed() + " )";
    m_sendLabel->setText(text);
}

void
MainWindow::updateSentView(FaceBehaviorType face1a, float intensity1a,
                           FaceBehaviorType face1b, float intensity1b, const QString &phraseFilename1,
                           FaceBehaviorType face2, float intensity2, const QString &phraseFilename2)
{
    QString text = QString("(") + m_faceBehaviorType2String[face1a] + QString(" ") + QString::number(intensity1a) + QString(" + ") +  m_faceBehaviorType2String[face1b] + QString(" ") + QString::number(intensity1b) + QString(", ") + phraseFilename1.trimmed() + " ) \n+ (" + m_faceBehaviorType2String[face2] + QString(" ") + QString::number(intensity2) + phraseFilename2.trimmed() + QString(")");
    m_sendLabel->setText(text);
}


/*
  Send one phrase and one face.
  Phrase is synchronized between AttackPeak & Relax sync points of Face.

  A phrase is picked randomly among available phrases for category @a phraseCategory1.
 */
void
MainWindow::sendOneBehavior(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1)
{
    enableInfoLabels();
    updateSentView0(phraseCategory1, face1, intensity1);

    const QString phraseFilename1 = getRandomPhraseFilename(phraseCategory1);

    if (! phraseFilename1.isEmpty()) {

        updateSentView(face1, intensity1, phraseFilename1);

        BehaviorBlock bl;

        FaceBehavior *fb = new FaceBehavior(face1, intensity1);
        SoundFileSpeechBehavior *sb = new SoundFileSpeechBehavior(phraseFilename1);

        bl.addBehavior(fb);
        bl.addBehavior(sb);

        //synchronize phrase between AttackPeak & Relax of face.
        bl.addSynchronization(Synchronization(fb, Behavior::AttackPeak, sb, Behavior::Start));
        bl.addSynchronization(Synchronization(fb, Behavior::Relax, sb, Behavior::End));

        qDebug()<<"sendOneBehavior";

        sendBehaviorBlock(bl);
    }
}

/*
  Send two phrases and two faces.
  Phrases are synchronized between AttackPeak & Relax sync points of Faces.
  Faces are synchronized to be one after the other.
 */
void
MainWindow::sendTwoBehaviors(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1,
                             PhraseCategory phraseCategory2, FaceBehaviorType face2, float intensity2)
{
    enableInfoLabels();
    updateSentView0(phraseCategory1, face1, intensity1, phraseCategory2, face2, intensity2);
    const QString phraseFilename1 = getRandomPhraseFilename(phraseCategory1);
    const QString phraseFilename2 = getRandomPhraseFilename(phraseCategory2);


    if (! phraseFilename1.isEmpty() && ! phraseFilename2.isEmpty()) {

        updateSentView(face1, intensity1, phraseFilename1, face2, intensity2, phraseFilename2);

        BehaviorBlock bl;

        FaceBehavior *fb1 = new FaceBehavior(face1, intensity1);
        SoundFileSpeechBehavior *sb1 = new SoundFileSpeechBehavior(phraseFilename1);
        FaceBehavior *fb2 = new FaceBehavior(face2, intensity2);
        SoundFileSpeechBehavior *sb2 = new SoundFileSpeechBehavior(phraseFilename2);

        bl.addBehavior(fb1);
        bl.addBehavior(sb1);
        bl.addBehavior(fb2);
        bl.addBehavior(sb2);

        //synchronize phrase1 between AttackPeak & Relax of face1.
        bl.addSynchronization(Synchronization(fb1, Behavior::AttackPeak, sb1, Behavior::Start));
        bl.addSynchronization(Synchronization(fb1, Behavior::Relax, sb1, Behavior::End));
        //synchronize phrase2 between AttackPeak & Relax of face2.
        bl.addSynchronization(Synchronization(fb2, Behavior::AttackPeak, sb2, Behavior::Start));
        bl.addSynchronization(Synchronization(fb2, Behavior::Relax, sb2, Behavior::End));
        //synchronize End of face2 with Start of face1
        bl.addSynchronization(Synchronization(fb2, Behavior::End, fb1, Behavior::Start));

        qDebug()<<"sendTwoBehaviors";

        sendBehaviorBlock(bl);
    }
}

/*
  Send one phrase and two faces.
  The two Faces are synchronized to be one after the other.
  Phrase is synchronized between AttackPeak sync point of first Face and Relax sync point of second Face.
 */
void
MainWindow::sendOneBehaviorB(PhraseCategory phraseCategory1, EmotionCategory perf_emotion1a, EmotionCategory perf_emotion1b)
{
    enableInfoLabels();
    updateSentView0(phraseCategory1, perf_emotion1a, perf_emotion1b);
    FaceBehaviorPair perf1a = getFaceBehaviorPair(perf_emotion1a);
    FaceBehaviorPair perf1b = getFaceBehaviorPair(perf_emotion1b);
    const QString phraseFilename1 = getRandomPhraseFilename(phraseCategory1);
    if (! phraseFilename1.isEmpty()) {
        FaceBehaviorType face1a = perf1a.face;
        float intensity1a = perf1a.intensity;
        FaceBehaviorType face1b = perf1b.face;
        float intensity1b = perf1b.intensity;
        updateSentView(face1a, intensity1a, face1b, intensity1b, phraseFilename1);
        BehaviorBlock bl;
        FaceBehavior *fb1 = new FaceBehavior(face1a, intensity1a);
        FaceBehavior *fb2 = new FaceBehavior(face1b, intensity1b);
        SoundFileSpeechBehavior *sb = new SoundFileSpeechBehavior(phraseFilename1);
        bl.addBehavior(fb1);
        bl.addBehavior(fb2);
        bl.addBehavior(sb);
        //synchronize phrase between AttackPeak & Relax of face.
        bl.addSynchronization(Synchronization(fb1, Behavior::AttackPeak, sb, Behavior::Start));
        bl.addSynchronization(Synchronization(fb2, Behavior::Relax, sb, Behavior::End));
        bl.addSynchronization(Synchronization(fb1, Behavior::End, fb2, Behavior::Start));
        qDebug()<<"sendOneBehaviorB";

        sendBehaviorBlock(bl);
    }
}

/*
  Send two phrases and three faces
  First two faces happen during the first phrase.
  Third face happens during the seconde phrase.

  First phrase is synchronized between AttackPeak sync point of first Face and Relax sync point of second Face.
  Second phrases is synchronized between AttackPeak & Relax sync points of third Face.
  Faces are synchronized to be one after the other.
 */
void
MainWindow::sendTwoBehaviorsB(PhraseCategory phraseCategory1,
                              EmotionCategory emotion1a, EmotionCategory emotion1b,
                              PhraseCategory phraseCategory2,
                              FaceBehaviorType face2, float intensity2)
{
    enableInfoLabels();
    updateSentView0(phraseCategory1, emotion1a, emotion1b, phraseCategory2, face2, intensity2);

    FaceBehaviorPair perf1a = getFaceBehaviorPair(emotion1a);
    FaceBehaviorPair perf1b = getFaceBehaviorPair(emotion1b);

    const QString phraseFilename1 = getRandomPhraseFilename(phraseCategory1);
    const QString phraseFilename2 = getRandomPhraseFilename(phraseCategory2);

    if (! phraseFilename1.isEmpty() && ! phraseFilename2.isEmpty()) {

        FaceBehaviorType face1a = perf1a.face;
        float intensity1a = perf1a.intensity;
        FaceBehaviorType face1b = perf1b.face;
        float intensity1b = perf1b.intensity;

        updateSentView(face1a, intensity1a, face1b, intensity1b, phraseFilename1, face2, intensity2, phraseFilename2);

        BehaviorBlock bl;

        FaceBehavior *fb1a = new FaceBehavior(face1a, intensity1a);
        FaceBehavior *fb1b = new FaceBehavior(face1b, intensity1b);
        SoundFileSpeechBehavior *sb1 = new SoundFileSpeechBehavior(phraseFilename1);
        FaceBehavior *fb2 = new FaceBehavior(face2, intensity2);
        SoundFileSpeechBehavior *sb2 = new SoundFileSpeechBehavior(phraseFilename2);

        bl.addBehavior(fb1a);
        bl.addBehavior(fb1b);
        bl.addBehavior(sb1);
        bl.addBehavior(fb2);
        bl.addBehavior(sb2);

        //synchronize phrase1 between AttackPeak & Relax of face1.
        bl.addSynchronization(Synchronization(fb1a, Behavior::AttackPeak, sb1, Behavior::Start));
        bl.addSynchronization(Synchronization(fb1b, Behavior::Relax, sb1, Behavior::End));
        bl.addSynchronization(Synchronization(fb1a, Behavior::End, fb1b, Behavior::Start));
        //synchronize phrase2 between AttackPeak & Relax of face2.
        bl.addSynchronization(Synchronization(fb2, Behavior::AttackPeak, sb2, Behavior::Start));
        bl.addSynchronization(Synchronization(fb2, Behavior::Relax, sb2, Behavior::End));
        //synchronize End of face2 with Start of face1
        bl.addSynchronization(Synchronization(fb2, Behavior::End, fb1b, Behavior::Start));

        qDebug()<<"sendTwoBehviorsB";

        sendBehaviorBlock(bl);

    }
}

void
MainWindow::setEnabledInfoLabels(bool onoff)
{
    assert(m_infoLabel);
    assert(m_sendLabel0);
    assert(m_sendLabel);
    m_infoLabel->setEnabled(onoff);
    m_sendLabel0->setEnabled(onoff);
    m_sendLabel->setEnabled(onoff);
}

void
MainWindow::disableInfoLabels()
{
    setEnabledInfoLabels(false);
}

void
MainWindow::enableInfoLabels()
{
    setEnabledInfoLabels(true);
}

std::vector<double>
MainWindow::initProgVector(int size){
    std::vector<double> result(size);
    for (int i = 0; i < size; i++){
        result[i] = (double)(i+1);
    }
    return result;
}

double
MainWindow::getSlope(const std::vector<double>& x, const std::vector<double>& y){
    const auto n    = x.size();
    const auto s_x  = std::accumulate(x.begin(), x.end(), 0.0);
    const auto s_y  = std::accumulate(y.begin(), y.end(), 0.0);
    const auto s_xx = std::inner_product(x.begin(), x.end(), x.begin(), 0.0);
    const auto s_xy = std::inner_product(x.begin(), x.end(), y.begin(), 0.0);
    const auto a    = (n * s_xy - s_x * s_y) / (n * s_xx - s_x * s_x);
    return a;
}

bool greater_than (float i, float j) { return (i<j); }

float
MainWindow::calcThresh(std::vector<float> vec_performances, int quartile, double coeff){
    //Retourne un vecteur avec valeurs triées du plus petit au plus grand
    std::sort(vec_performances.begin(), vec_performances.end(), greater_than);
    int index = std::abs(vec_performances.size()*(0.25*quartile));
    return (float)(vec_performances[index]/coeff);
}

float
MainWindow::getProgress()
{
    float progress = 0.f;
    const int sz = m_performances.size();
    if (sz < DEFAULT_NB_TRIALS_FOR_PROGRESS || m_NUM_SESSION == 1) { //unable to compute derivative, and thus progress
        progress = 0.5f; //return average progress
    }
    else {
        std::vector<float> std_performances = m_performances.toStdVector();
        std::vector<float> sub_perf;
        std::copy(std_performances.end()-DEFAULT_NB_TRIALS_FOR_PROGRESS, std_performances.end(), std::back_inserter(sub_perf));

        std::vector<double> doubleVec(sub_perf.begin(), sub_perf.end());
        progress = getSlope(initProgVector(sz), doubleVec);

        //compute sum of derivatives and minimum performance
        /*float sumDeriv = 0.f;
        float minPerf = m_performances[0];
        for (int i=1; i<m_performances.size(); ++i) {
            sumDeriv += (m_performances[i]-m_performances[i-1]);
            if (m_performances[i] < minPerf)
                minPerf = m_performances[i];
        }

        {//DEBUG
            std::cerr<<"getProgress: "<<m_performances.size()<<" perfs: ";
            for (int i=0; i<m_performances.size(); ++i)
                std::cerr<<m_performances[i]<<" ";
            std::cerr<<"\n";
            std::cerr<<"                 derivatives:";
            for (int i=1; i<m_performances.size(); ++i)
                std::cerr<<m_performances[i]-m_performances[i-1]<<" ";
            std::cerr<<"\n";
            std::cerr<<"getProgress: sumDeriv="<<sumDeriv<<" minPerf="<<minPerf<<"\n";
        }//DEBUG

        if (sumDeriv > 0.1f) {
            progress = m_performances.back();
        }
        else if (sumDeriv < -0.1f) {
            progress = minPerf;
        }
        else  {
            progress = 0.5f; //average progress
        }*/
    }
    return progress;
}

//get last performance
float
MainWindow::getResult()
{
    if (! m_performances.empty()) {
        return m_performances.back();
    }
    return 0.5f; //average perf
}

void
MainWindow::initCategories()
{
    m_phraseCategoryThresholds.clear();
    m_phraseCategoryThresholds.resize(SIZE_PhraseCategory);
    m_phraseCategoryThresholds[Progress_Good] = 0.6f;
    m_phraseCategoryThresholds[Progress_VeryGood] = 0.75f;
    m_phraseCategoryThresholds[Result_Good] = 0.6f;
    m_phraseCategoryThresholds[Result_VeryGood] = 0.75f;

    m_phraseCategoryProbas.clear();
    m_phraseCategoryProbas.resize(SIZE_PhraseCategory);
    m_phraseCategoryProbas[Progress_Good] = 0.5f;
    m_phraseCategoryProbas[Progress_VeryGood] = 0.5f;
    m_phraseCategoryProbas[Result_Good] = 0.5f;
    m_phraseCategoryProbas[Result_VeryGood] = 0.5f;

    m_emotionCategoryProbas.clear();
    m_emotionCategoryProbas.resize(SIZE_EmotionCategory);
    m_emotionCategoryProbas[Ecstasy] = 1.f/3.f;
    m_emotionCategoryProbas[Joy] = 1.f/3.f;
    m_emotionCategoryProbas[Serenity] = 1.f/3.f;
    m_emotionCategoryProbas[Amazement] = 1.f/3.f;
    m_emotionCategoryProbas[Surprise] = 1.f/3.f;
    m_emotionCategoryProbas[Distraction] = 1.f/3.f;
    m_emotionCategoryProbas[Admiration] = 1.f/3.f;
    m_emotionCategoryProbas[Trusting] = 1.f/3.f;
    m_emotionCategoryProbas[Acceptance] = 1.f/3.f;
    m_emotionCategoryProbas[Sadness] = 1.f/3.f;
    m_emotionCategoryProbas[Thoughtful] = 1.f/3.f;
    m_emotionCategoryProbas[Disgusted] = 1.f/3.f;
    m_emotionCategoryProbas[Boredom] = 1.f/3.f;
    m_emotionCategoryProbas[Anger] = 1.f/3.f;
    m_emotionCategoryProbas[Annoyance] = 1.f/3.f;

    m_emotions2Faces.clear();
    m_emotions2Faces.resize(SIZE_EmotionCategory);
    m_emotions2Faces[Ecstasy] = FaceBehaviorPair(Happy, 1.0f);
    m_emotions2Faces[Joy] = FaceBehaviorPair(Happy, 0.7f);
    m_emotions2Faces[Serenity] = FaceBehaviorPair(Happy, 0.4f);
    m_emotions2Faces[Amazement] = FaceBehaviorPair(Surprised, 1.0f);
    m_emotions2Faces[Surprise] = FaceBehaviorPair(Surprised, 0.7f);
    m_emotions2Faces[Distraction] = FaceBehaviorPair(Surprised, 0.4f);
    m_emotions2Faces[Admiration] = FaceBehaviorPair(Trust, 1.0f);
    m_emotions2Faces[Trusting] = FaceBehaviorPair(Trust, 0.7f);
    m_emotions2Faces[Acceptance] = FaceBehaviorPair(Trust, 0.4f);
    m_emotions2Faces[Sadness] = FaceBehaviorPair(Sad, 1.0f);
    m_emotions2Faces[Thoughtful] = FaceBehaviorPair(Sad, 0.5f);
    m_emotions2Faces[Disgusted] = FaceBehaviorPair(Bored, 1.0f);
    m_emotions2Faces[Boredom] = FaceBehaviorPair(Bored, 0.5f);
    m_emotions2Faces[Anger] = FaceBehaviorPair(Angry, 1.0f);
    m_emotions2Faces[Annoyance] = FaceBehaviorPair(Angry, 0.5f);
}

void
MainWindow::readPhrases(const QString &filename)
{
    //Consider that on each (non empty) line
    //We have: category phraseFile

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug()<<"ERROR: Unable to read config file: "<<filename;
        exit(10);
    }

    QMap<QString, PhraseCategory> m;
    m["Temporal_StartSession"] = Temporal_StartSession;
    m["Temporal_MiddleSession"] = Temporal_MiddleSession;
    m["Temporal_Next"] = Temporal_Next;
    m["Temporal_NearEndSession"] = Temporal_NearEndSession;
    m["Temporal_LastTrialOfSession"] = Temporal_LastTrialOfSession;
    m["Temporal_EndSession"] = Temporal_EndSession;
    m["Progress_Good"] = Progress_Good;
    m["Progress_Good_PE"] = Progress_Good_PE;
    m["Progress_Good_nPD"] = Progress_Good_nPD;
    m["Progress_VeryGood"] = Progress_VeryGood;
    m["Progress_VeryGood_PE"] = Progress_VeryGood_PE;
    m["Progress_VeryGood_nPD"] = Progress_VeryGood_nPD;
    m["Result_Good"] = Result_Good;
    m["Result_Good_PE"] = Result_Good_PE;
    m["Result_Good_PD"] = Result_Good_PD;
    m["Result_VeryGood"] = Result_VeryGood;
    m["Result_VeryGood_nPD"] = Result_VeryGood_nPD;
    m["Result_VeryGood_PE"] = Result_VeryGood_nPD;
    m["Support_Effort"] = Support_Effort;
    m["Support_Effort_PD"] = Support_Effort_PD;
    m["Support_Effort_PE"] = Support_Effort_PE;
    m["General_Effort"] = General_Effort;
    m["General_Effort_PE"] = General_Effort_PE;
    m["General_Effort_PD"] = General_Effort_PD;
    m["General_Empathy_PD"] = General_Empathy_PD;
    m["General_Empathy_PE"] = General_Empathy_PE;
    m["General_Empathy"] = General_Empathy;
    m["Strategy_Change"] = Strategy_Change;
    m["Strategy_Keep"] = Strategy_Keep;
    m["Strategy_Keep_nPD"] = Strategy_Keep_nPD;
    m["Temp_Category"] = Temp_Category;


    m_phraseCategory2Set.clear();
    m_phraseCategory2Set.resize(SIZE_PhraseCategory);


    QString phrasesPath = PHRASES_PATH;
    if (! QFileInfo(phrasesPath).isDir()) {
        phrasesPath = "../ClientCompanion/rsrc/voices/fr/phrases/";
        if (! QFileInfo(phrasesPath).isDir()) {
            qDebug()<<"ERROR: Unable find directory phrasesPath ";
            exit(10);
        }
    }
    phrasesPath += "/";


    const QString SEP = " ";

    size_t nbReadOks = 0;

    int line = 1;
    QStringList list;
    const qint64 bufSize = 2048;
    char buf[bufSize];
    while (!file.atEnd()) {
        qint64 lineLength = file.readLine(buf, bufSize);
        if (lineLength > 0) {
            QString s = QLatin1String(buf);
            s = s.trimmed();
            if (s.left(1) == QStringLiteral("#") || s.length() == 0)
                continue; // skip comments and empty lines
            list = s.split(SEP);
            if (list.count() == 2) {
                const QString &s0 = list.at(0);
                const QString &s1 = list.at(1);
                auto it = m.find(s0);
                if (it != m.end()) {
                    const bool readOk = m_phraseCategory2Set[it.value()].addPhraseFile(phrasesPath+s1);
                    //CachedPhraseSet will load the file to read its phrase (thus it needs a complete file path).
                    //If file can not be read, nothing will be added to the category.
                    nbReadOks += readOk;
                }
                else {
                    qDebug()<<"Warning: line"<<line<<" in file "<<filename<<"\n"<<s0<<" is not a valid PhraseCategory";
                }
            }
            ++line;
        }

    }
    if (nbReadOks == 0) {
        qDebug()<<"ERROR: unable to read any phrase from directory: "<<phrasesPath;
        exit(10);
    }

}

MainWindow::FaceBehaviorPair
MainWindow::getFaceBehaviorPair(EmotionCategory e)
{
    assert(e < m_emotions2Faces.size());
    return m_emotions2Faces[e];
}

std::string
MainWindow::labelPerf(float result){
    std::string lRes = "";
    if(result<m_perf_thresh_bad_S1)
        lRes = "Bad";
    else if(result<m_perf_thresh_good_S1)
        lRes = "Neutral";
    else
        lRes = "Good";
    return lRes;
}
std::string
MainWindow::labelProg(float progress){
    std::string lProg = "";
    if(progress<m_prog_thresh_bad)
        lProg = "Bad";
    else if(progress<m_prog_thresh_good)
        lProg = "Neutral";
    else
        lProg = "Good";
    return lProg;
}

/*
  We consider that m_endMeasureStim is sent after the performance computed
after m_endTrialStim in the OpenViBE scenario. Thus when we receive such a
stimulation, we have all the information to decide what behavior to launch.
 */

void
MainWindow::updateStimulation()
{
    //S'il y a une simulation
    if (m_stimulation != 0) {
        //DEBUG
        /*if (m_stimulation == m_endMeasureStim) {
                //always send a simple
                sendOneBehavior(Progress_Good, Happy, 0.9);
            }*/
        const int s = m_stimulation;

        std::cerr<<"received stim="<<OVTK_intToStringMap[s]<<"\n";

        PhraseCategory temp_phraseCategory;
        FaceBehaviorType temp_face;
        float temp_intensity = 0.0f;

        PhraseCategory perf_phraseCategory;
        EmotionCategory perf_emotion1 = None;
        EmotionCategory perf_emotion2 = None;

        //m_emotionalSB
        if (s == m_startRunStim) {

            std::cerr<<"TEST1 - current Run = "<< (int)m_currentRun<<" m_NUM_RUN = "<<(int)m_NUM_RUN<<"\n";
            if ((m_currentRun == -1 && m_NUM_RUN == 1) || (m_currentRun == -1 && m_NUM_RUN == 2 && m_NUM_SESSION == 1)) {
                if(m_NUM_SESSION == 1){
                    temp_phraseCategory = Temporal_StartSessionS1;
                }
                else{
                    temp_phraseCategory = Temporal_StartSession;
                }
                temp_face = Happy;
                temp_intensity = 0.6f;

                m_currentRun = m_NUM_RUN;
                if(m_NUM_RUN != 1){
                    m_currentTrialTotal = (m_NUM_RUN-1)*m_nbTrialsSB->value();
                    m_lastSocialInteraction = (m_NUM_RUN-1)*m_nbTrialsSB->value();
                }
                else{
                    m_currentTrialTotal = 0;
                    m_lastSocialInteraction = 0;
                }

                m_trialOngoing = false;

            }
            else if (m_currentRun == -1 && m_NUM_RUN != 1){
                m_currentRun = m_NUM_RUN;
                m_currentTrialTotal = (m_NUM_RUN-1)*m_nbTrialsSB->value();
                m_trialOngoing = false;
                m_lastSocialInteraction = (m_NUM_RUN-1)*m_nbTrialsSB->value();
            }
            else
                ++m_currentRun;
            //
            std::time_t t = time(0); //Get time now
            struct tm * now = localtime( & t );
            saveFile.open("Sjt"+std::to_string(m_NUM_SUJET)+"S"+std::to_string(m_NUM_SESSION)+"R"+std::to_string(m_currentRun)+"M" + std::to_string(now->tm_mon+1) + "D" + std::to_string(now->tm_mday) + "H" + std::to_string(now->tm_hour) + "m" + std::to_string(now->tm_min) + ".txt");
            saveFile << "START\n";
        }
        else if (s == m_startTrialStim) {
            if(m_currentRun != -1){
                ++m_currentTrialTotal;
            }
            m_trialOngoing = true;
            disableInfoLabels();
        }
        else if (s == m_endMeasureStim && m_currentRun!=-1 && !(m_NUM_SESSION==1 && m_currentRun==1)) {
            m_result_act = getResult();
            m_progress_act = getProgress();

            std::cerr<< "RESULT "<<m_result_act<<" PROGRESS "<<m_progress_act<<"\n";
            //Si on est dans une zone de presence sociale
            const float proba = random01();
            std::cerr<<"TEST2 -  current Trial = "<<m_currentTrialTotal<<" proba = "<<proba<<"\n" ;

            //Introduction d'aleatoire dans le comportement du CA
            std::cerr<<"PresenceSocial? current Run -"<< m_currentRun << " CurrentTrialTotal-" << m_currentTrialTotal << " -->"<<std::to_string(std::abs(m_currentTrialTotal - (m_lastSocialInteraction+(int)m_presenceSB->value())))<<"\n";
            if ((proba > 0.7 && std::abs(m_currentTrialTotal - (m_lastSocialInteraction+(int)m_presenceSB->value()))<=2)|| m_currentTrialTotal == m_lastSocialInteraction+(int)m_presenceSB->value()+2) {
                std::cerr<<"TEST3_You're in \n" ;

                //En fonction des performances et du progrès.
                if((m_NUM_SESSION!=1 && ADAPTED_FB == true) || (m_NUM_SESSION==1 && m_currentRun>2 && ADAPTED_FB == true)){
                    float actual_perf_thresh_good;
                    float actual_perf_thresh_bad;
                    if(m_NUM_SESSION==1 && m_currentRun>2){
                        actual_perf_thresh_good = m_perf_thresh_good_R2;
                        actual_perf_thresh_bad = m_perf_thresh_bad_R2;
                    }
                    else{
                        actual_perf_thresh_good = m_perf_thresh_good_S1;
                        actual_perf_thresh_bad = m_perf_thresh_bad_S1;
                    }
                    //Défini l'ojectif de l'interaction - Etre Clair ou Motiver
                    float proba_motiv = random01();
                    m_gMotivate = (proba_motiv>0.5);

                    float last_result = 0.5;
                    if(m_performances.size()>2)
                        last_result = m_performances[m_performances.size() - 2];

                    //Si les résultats = performances sont bonnes
                    if (m_result_act > actual_perf_thresh_good) {
                        if(m_progress_act > m_prog_thresh_good) {
                            if(m_gMotivate){
                                //Phrases
                                if(last_result < m_perf_thresh_good_S1){
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Progress_VeryGood_PE, Result_VeryGood_PE);
                                }
                                else{
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Progress_Good_PE, Result_VeryGood_PE);
                                }
                            }
                            else {
                                if(last_result < m_perf_thresh_good_S1)
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Strategy_Keep_nPD, Result_VeryGood_nPD);
                                else
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Progress_Good_nPD, Strategy_Keep_nPD);
                            }
                        }
                        else if (m_progress_act > m_prog_thresh_bad && m_progress_act < m_prog_thresh_good) {
                            //mesurer le temps pour lequel les résultats sont bons
                            perf_phraseCategory = getRandomPhraseCategoryFromList(4, new PhraseCategory[4] {Result_Good_PD, Result_Good_PE, Support_Effort_PD, Support_Effort_PE});
                        }
                        else {
                            perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Strategy_Change, General_Effort_PD);
                        }
                    }
                    //Si les résultats = performances sont moyennes
                    else if (m_result_act> actual_perf_thresh_bad && m_result_act < actual_perf_thresh_good){
                        if(m_progress_act > m_prog_thresh_good) {
                            if(m_gMotivate){
                                if(last_result < actual_perf_thresh_bad)
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Progress_VeryGood_PE, Result_Good_PE);
                                else
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Progress_Good_PE, Support_Effort_PE);
                            }
                            else {
                                if(last_result < actual_perf_thresh_bad)
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Strategy_Keep_nPD, Progress_VeryGood_nPD);
                                else
                                    perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Progress_Good_nPD, Strategy_Keep_nPD);
                            }
                        }
                        else if (m_progress_act > actual_perf_thresh_bad && m_progress_act < m_prog_thresh_good) {
                            //mesurer le temps pour lequel les résultats sont bons
                            perf_phraseCategory = getRandomPhraseCategoryFromList(4, new PhraseCategory[4] {General_Effort_PD, General_Effort_PE, Support_Effort_PD, Support_Effort_PE});
                        }
                        else {
                            if(last_result > actual_perf_thresh_good)
                                perf_phraseCategory = getRandomPhraseCategoryFromList(3, new PhraseCategory[3] {Strategy_Change, General_Empathy_PD, General_Effort_PD});
                            else
                                perf_phraseCategory = getRandomPhraseCategoryFromList(3, new PhraseCategory[3] {Strategy_Change, General_Empathy_PD, General_Effort_PD});
                        }
                    }
                    //Si les résultats sont mauvais
                    else {
                        if(m_progress_act > m_prog_thresh_good) {
                            if(m_gMotivate){
                                perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Support_Effort_PE, Progress_Good_PE);
                            }
                            else {
                                perf_phraseCategory = getRandomPhraseCategoryFromTwo(proba, Strategy_Keep_nPD, Progress_Good_nPD);
                            }
                        }
                        else if (m_progress_act > m_prog_thresh_bad && m_progress_act < m_prog_thresh_good) {
                            //mesurer le temps pour lequel les résultats sont bons
                            perf_phraseCategory = getRandomPhraseCategoryFromList(4, new PhraseCategory[4] {General_Effort_PD, General_Effort_PE, General_Empathy_PD, General_Empathy_PE});
                        }
                        else {
                            if(last_result > actual_perf_thresh_bad)
                                perf_phraseCategory = getRandomPhraseCategoryFromList(3, new PhraseCategory[3] {Strategy_Change, General_Empathy_PD, General_Effort_PD});
                            else
                                perf_phraseCategory = getRandomPhraseCategoryFromList(3, new PhraseCategory[3] {Strategy_Change, General_Empathy_PD, General_Effort_PD});
                        }
                    }
                }
                //Feedback non adapté
                else if((m_NUM_SESSION==1 && m_currentRun==2) || ADAPTED_FB == false){
                    std::cerr<<"TEST4_False_FB\n" ;
                    perf_phraseCategory = getRandomPhraseCategoryFromList(6, new PhraseCategory[6] {General_Effort_PD, General_Effort_PE, General_Empathy_PD, General_Empathy_PE, Support_Effort_PD, Support_Effort_PE});
                }
                //std::cerr<<"TEST5_Do you have something to say ?  \n"<<m_phraseCategory2String[perf_phraseCategory].toStdString() ;
                //Choix de une ou plusieurs émotion(s) en fonction de la phrase sélectionnée
                if (perf_phraseCategory == General_Effort_PD)
                    perf_emotion1 = Serenity;
                else if ((perf_phraseCategory == General_Effort_PE) || (perf_phraseCategory == Strategy_Keep_nPD) || (perf_phraseCategory == Result_Good_PE) || (perf_phraseCategory == Result_Good_PD))
                    perf_emotion1 = Joy;
                else if (perf_phraseCategory == Support_Effort_PD)
                    perf_emotion1 = Acceptance;
                else if (perf_phraseCategory == Support_Effort_PE)
                    perf_emotion1 = Trusting;
                else if (perf_phraseCategory == Strategy_Change){
                    perf_emotion1 = Distraction;
                    perf_emotion2 = Serenity;
                }
                else if (perf_phraseCategory == Result_VeryGood_nPD || (perf_phraseCategory == Result_VeryGood_PE))
                    perf_emotion1 = Ecstasy;
                else if (perf_phraseCategory == Progress_Good_nPD || (perf_phraseCategory == Progress_Good_PE)){
                    perf_emotion1 = Surprise;
                    perf_emotion2 = Trusting;
                }
                else if ((perf_phraseCategory == Progress_VeryGood_nPD) || (perf_phraseCategory == Progress_VeryGood_PE)){
                    perf_emotion1 = Amazement;
                    perf_emotion2 = Admiration;
                }
                else if ((perf_phraseCategory == General_Empathy_PD) || (perf_phraseCategory == General_Empathy_PE)){
                    perf_emotion1 = Sadness;
                    perf_emotion2 = Trusting;
                }

                //TEMPORAL
                if (m_currentTrialTotal == nbTrialsTotal()/2) {
                    float proba = random01();
                    if (checkProba(proba, m_phraseCategoryProbas[Temporal_MiddleSession])) {
                        temp_phraseCategory = Temporal_MiddleSession;
                        temp_face = Happy;
                        temp_intensity = 0.6f;
                    }
                }
                else if (m_currentTrialTotal >= nbTrialsTotal()*0.8) {
                    float proba = random01();
                    if (checkProba(proba, m_phraseCategoryProbas[Temporal_NearEndSession])) {
                        temp_phraseCategory = Temporal_NearEndSession;
                        temp_face = Happy;
                        temp_intensity = 0.6f;
                        m_phraseCategoryProbas[Temporal_NearEndSession] = 0; //to avoid to say message twice
                    }
                }
                else if (m_currentTrialTotal == nbTrialsTotal()-2) { //alternativement, on pourrait dire ça sur m_endRunStim & m_currentRun = m_nbRun-1
                    float proba = random01();
                    if (checkProba(proba, m_phraseCategoryProbas[Temporal_LastTrialOfSession])) {
                        temp_phraseCategory = Temporal_LastTrialOfSession;
                        temp_face = Happy;
                        temp_intensity = 0.6f;
                    }
                }
                m_trialOngoing = false;
            }
            //Mise à jour du vecteur de progrès servant à calculer les seuils de progrès
            if(m_NUM_SESSION == 1 && m_currentRun>2) {
                if (m_performances.count() > DEFAULT_NB_TRIALS_FOR_PROGRESS)
                    m_progress.push_back(getProgress());
            }

            if (temp_intensity != 0.f && ((perf_phraseCategory == Support_Effort_PD || perf_phraseCategory == Support_Effort_PE)|| (m_currentRun==2 && m_NUM_SESSION==1) || m_currentRun==nbRuns() || m_currentRun==1)) {
                m_lastSocialInteraction = m_currentTrialTotal;
                m_last_perf_phraseCategory = temp_phraseCategory;
                m_last_perf_emotion1 = Joy;
                m_last_perf_emotion2 = Joy;
            }
            else if (perf_emotion1 != None) {
                m_lastSocialInteraction = m_currentTrialTotal;
                m_last_perf_phraseCategory = perf_phraseCategory;
                m_last_perf_emotion1 = perf_emotion1;
                m_last_perf_emotion2 = perf_emotion2;
            }

            //Sauvegarde dans fichier de log
            if (m_NUM_SESSION!=1){
                std::string lRes = labelPerf(m_result_act);
                std::string lProg = labelProg(m_progress_act);
                saveFile << "Session_" + std::to_string(m_NUM_SESSION) + "-Run_" + std::to_string(m_currentRun) + "-Trial_" + std::to_string(m_currentTrialTotal)
                            + "-SeuilPerfBad_" + std::to_string(m_perf_thresh_bad_S1) + "-SeuilPerfGood_" + std::to_string(m_perf_thresh_good_S1)
                            + "-SeuilProgBad_" + std::to_string(m_prog_thresh_bad) + "-SeuilProgGood_" + std::to_string(m_prog_thresh_good)
                            + "-Perf_" + std::to_string(m_result_act) + "-LPerf" + lRes + "-Prog_" + std::to_string(m_progress_act) + "-LProg_" + lProg
                            + "-Category_" + m_phraseCategory2String[m_last_perf_phraseCategory].toStdString() + "-Emotion1_" + m_emotions2String[m_last_perf_emotion1].toStdString() + "-Emotion2_" + m_emotions2String[m_last_perf_emotion2].toStdString() + "\n";
            }
            else {
                std::string lRes = labelPerf(m_result_act);
                saveFile << "Session_" + std::to_string(m_NUM_SESSION) + "-Run_" + std::to_string(m_currentRun) + "-Trial_" + std::to_string(m_currentTrialTotal)
                            + "-SeuilPerfBad_" + std::to_string(m_perf_thresh_bad_R2) + "-SeuilPerfGood_" + std::to_string(m_perf_thresh_good_R2)
                            + "-Perf_" + std::to_string(m_result_act) + "-LPerf" + lRes
                            + "-Category_" + m_phraseCategory2String[m_last_perf_phraseCategory].toStdString() + "-Emotion1_" + m_emotions2String[m_last_perf_emotion1].toStdString() + "-Emotion2_" + m_emotions2String[m_last_perf_emotion2].toStdString()+ "\n";
            }

        }
        else if (s == m_endTrialStim) {
            m_trialOngoing = false;
            std::cerr<<"LASTSOCIALINTERACTION = "<<m_lastSocialInteraction<<" "<<"CURRENTTRIAL = "<<m_currentTrialTotal<<"\n";
            if(m_lastSocialInteraction != m_currentTrialTotal) {
                m_last_perf_phraseCategory = Null;
                m_last_perf_emotion1 = None;
                m_last_perf_emotion2 = None;
            }
        }
        else if (s == m_endRunStim) {
            m_trialOngoing = false;
            std::cerr << "FIN ? nbRuns = " << nbRuns() << "\n";
            if(m_NUM_SESSION==1 && m_currentRun < nbRuns()){
                if(m_currentRun == 2) {
                    m_perf_thresh_bad_R2 = calcThresh(m_performances.toStdVector(), 1, COEFF_R2_BAD);
                    m_perf_thresh_good_R2 = calcThresh(m_performances.toStdVector(), 3, COEFF_R2_GOOD);
                }
                saveFile << "THRESH :\n m_perf_thresh_bad_R2 = " + std::to_string(m_perf_thresh_bad_R2) + "\n"
                            + " m_perf_thresh_good_R2 = " + std::to_string(m_perf_thresh_good_R2) + "\n";
            }
            if(m_NUM_SESSION == 1  && m_currentRun == nbRuns()) {
                m_perf_thresh_bad_S1 = calcThresh(m_performances.toStdVector(), 1, COEFF_S1_PERF_BAD);
                m_perf_thresh_good_S1 = calcThresh(m_performances.toStdVector(), 3, COEFF_S1_PERF_GOOD);
                m_prog_thresh_bad = calcThresh(m_progress.toStdVector(), 3, COEFF_S1_PROG_BAD);
                m_prog_thresh_good = calcThresh(m_progress.toStdVector(), 3, COEFF_S1_PROG_GOOD);
                saveFile << "THRESH :\n m_perf_thresh_bad_S1 = " + std::to_string(m_perf_thresh_bad_S1) + "\n"
                            + " m_perf_thresh_good_S1 = " + std::to_string(m_perf_thresh_good_S1) + "\n"
                            + " m_prog_thresh_bad = " + std::to_string(m_prog_thresh_bad) + "\n"
                            + " m_prog_thresh_good = " + std::to_string(m_prog_thresh_good) + "\n";
                std::cerr<< "THRESH :\n m_perf_thresh_bad_S1 = " + std::to_string(m_perf_thresh_bad_S1) + "\n"
                            + " m_perf_thresh_good_S1 = " + std::to_string(m_perf_thresh_good_S1) + "\n"
                            + " m_prog_thresh_bad = " + std::to_string(m_prog_thresh_bad) + "\n"
                            + " m_prog_thresh_good = " + std::to_string(m_prog_thresh_good) + "\n";
            }
            saveFile << "END\n";
            saveFile.close();

            if (m_currentRun == nbRuns()) {
                std::cerr<<"Au Revoir \n ";
                temp_phraseCategory = Temporal_EndSession;
                temp_face = Happy;
                temp_intensity = 0.6f;
            }

            //}
        }

        if (temp_intensity != 0.f && ((perf_phraseCategory == Support_Effort_PD || perf_phraseCategory == Support_Effort_PE)|| (m_currentRun==2 && m_NUM_SESSION==1) || m_currentRun==nbRuns() || m_currentRun==1)) {
            //case only temporal animation
            std::cerr<< "Envoie d'une animation temporelle \n";
            sendOneBehavior(temp_phraseCategory, temp_face, temp_intensity);
            //m_lastSocialInteraction = m_currentTrialTotal;
            //m_last_perf_phraseCategory = temp_phraseCategory;
            //m_last_perf_emotion1 = Joy;
            //m_last_perf_emotion2 = Joy;
        }
        else if (perf_emotion1 != None) {
            std::cerr<< "Envoie d'une animation non temporelle T1 - CATEGORY"<< m_phraseCategory2String[perf_phraseCategory].toStdString() <<"\n";

            //case only progress animation
            sendOneBehaviorB(perf_phraseCategory, perf_emotion1, perf_emotion2);
            std::cerr<< "Envoie d'une animation non temporelle T2\n";
            //m_lastSocialInteraction = m_currentTrialTotal;
            //m_last_perf_phraseCategory = perf_phraseCategory;
            //m_last_perf_emotion1 = perf_emotion1;
            //m_last_perf_emotion2 = perf_emotion2;
        }
    }
}

void
MainWindow::updatePerformance()
{
    assert(! m_performances.empty());
    const float result = getResult();
    const float progress = getProgress();
    m_infoLabel->setText(tr("last perf=%1  progress=%2").arg(result).arg(progress));
}

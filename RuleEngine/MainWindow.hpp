#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QMap>
#include <QVector>

#include "CachedDistancePhraseSet.hpp"

#include "../ClientCompanion/BehaviorType.hpp"

class QLabel;
class QSpinBox;
class QComboBox;
class QDoubleSpinBox;
class QVBoxLayout;

class QWebSocketServer;
class QWebSocket;

class BehaviorBlock;



class MainWindow : public QWidget //QMainWindow
{
  Q_OBJECT

public:
  MainWindow();
  ~MainWindow();

protected slots:
  void processStimulation(int s);
  void processPerformance(float p);
  
  void onNewConnection();
  void processReceivedTextMessage(const QString &message);
  void processReceivedBinaryMessage(const QByteArray &message);
  void socketDisconnected();
  bool sendMessage(const QByteArray &ba);
  //void updatePhrases();
  
  
protected:
  bool readConfigFile(const QString &filename);
  void build();
  void populateCBs();
  void populateSignals();
  void startThreads();
  
  void initWebSocketServer();
  void closeWebSocketServer();
  
  void updateStimulation();
  void updatePerformance();
  
  
  void setUserEmotionalSupport(float v);
  void setUserSocialPresence(float v);
  
  void setStartRunStim(int);
  void setStartRunStim(const QString &);
  int startRunStim() const;
  void setEndRunStim(int);
  void setEndRunStim(const QString &);
  int endRunStim() const;
  void setStartTrialStim(int);
  void setStartTrialStim(const QString &);
  int startTrialStim() const;
  void setEndTrialStim(int);
  void setEndTrialStim(const QString &);
  int endTrialStim() const;
  void setEndMeasureStim(int);
  void setEndMeasureStim(const QString &);
  int endMeasureStim() const;
  
  void setNbRuns(int);
  int nbRuns() const;
  void setNbTrials(int);
  int nbTrials() const;
  int nbTrialsTotal() const;

  int nbTrialsForProgress() const;

  void updateConnectionState();
  
  typedef enum {Ecstasy=0, Joy, Serenity,
		Amazement, Surprise, Distraction, 
		Admiration, Trusting, Acceptance,
		Sadness, Thoughtful,
		Disgusted, Boredom, 
		Anger, Annoyance,
		None,
		SIZE_EmotionCategory //must stay last
  } EmotionCategory;

  typedef enum {Temporal_StartSession=0, Temporal_StartSessionS1, Temporal_MiddleSession,
		Temporal_Next, Temporal_NearEndSession,
		Temporal_LastTrialOfSession, Temporal_EndSession, 
        Progress_Good, Progress_VeryGood,Progress_Good_nPD, Progress_Good_PE, Progress_VeryGood_PE, Progress_VeryGood_nPD,
        Result_Good, Result_VeryGood, Result_Good_PE, Result_Good_PD, Result_VeryGood_nPD, Result_VeryGood_PE,
        Support_Effort, General_Effort, General_Empathy,General_Empathy_PE, General_Empathy_PD, Support_Effort_PE, Support_Effort_PD, General_Effort_PD, General_Effort_PE,
        Strategy_Change, Strategy_Keep, Strategy_Keep_nPD,
        Temp_Category, Null,
		SIZE_PhraseCategory //must stay last
  } PhraseCategory;

  struct FaceBehaviorPair
  {
    FaceBehaviorType face;
    float intensity;

    FaceBehaviorPair(FaceBehaviorType f = Idle, float i = 1.0f)
      : face(f), intensity(i)
    {}
  };

  void initCategories();
  void readPhrases(const QString &filename);
  
  FaceBehaviorPair getFaceBehaviorPair(EmotionCategory e);

  EmotionCategory getRandomEmotion(EmotionCategory e1, EmotionCategory e2);
  EmotionCategory getRandomEmotion(EmotionCategory e1, EmotionCategory e2, EmotionCategory e3);

  
  float getProbabilityOfPhraseCategory(PhraseCategory c);
  float getProbabilityOfEmotionCategory(EmotionCategory c);

  PhraseCategory getRandomPhraseCategoryFromTwo(float proba, PhraseCategory c1, PhraseCategory c2);
  PhraseCategory getRandomPhraseCategoryFromList(int size, PhraseCategory classes[]);

  QString getRandomPhraseFilename(PhraseCategory phraseCategory);
  
  bool sendBehaviorBlock(const BehaviorBlock &bl);

  void sendOneBehavior(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1);

  void sendTwoBehaviors(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1, 
			PhraseCategory phraseCategory2, FaceBehaviorType face2, float intensity2);
  
  void sendOneBehaviorB(PhraseCategory phraseCategory1, EmotionCategory perf_emotion1, EmotionCategory perf_emotion2);
  
  void sendTwoBehaviorsB(PhraseCategory phraseCategory1,
			 EmotionCategory emotion1a, EmotionCategory emotion1b,
			 PhraseCategory phraseCategory2, 
			 FaceBehaviorType face2, float intensity2);

  void enableInfoLabels();
  void disableInfoLabels();
  void setEnabledInfoLabels(bool onoff);

  void initFaceBehaviorType2String();
  
  void updateSentView0(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1);
  void updateSentView0(PhraseCategory phraseCategory1, FaceBehaviorType face1, float intensity1,
		       PhraseCategory phraseCategory2, FaceBehaviorType face2, float intensity2);
  void updateSentView0(PhraseCategory phraseCategory1, EmotionCategory emotion1, EmotionCategory emotion2);
  void updateSentView0(PhraseCategory phraseCategory1,
		       EmotionCategory emotion1a, EmotionCategory emotion1b,
		       PhraseCategory phraseCategory2, 
		       FaceBehaviorType face2, float intensity2);


  void updateSentView(FaceBehaviorType face1, float intensity1, const QString &phraseFilename1);

  void updateSentView(FaceBehaviorType face1, float intensity1, const QString &phraseFilename1, 
		      FaceBehaviorType face2, float intensity2, const QString &phraseFilename2);

  void updateSentView(FaceBehaviorType face1a, float intensity1a, FaceBehaviorType face1b, float intensity1b, const QString &phraseFilename1);

  void updateSentView(FaceBehaviorType face1a, float intensity1a, 
		      FaceBehaviorType face1b, float intensity1b, const QString &phraseFilename1, 
		      FaceBehaviorType face2, float intensity2, const QString &phraseFilename2);


  void addPerformance(float p);
  void TestRegressionLineaire();
  std::string labelPerf(float result);
  std::string labelProg(float progress);

  float getProgress();
  float getResult();
  float calcThresh(std::vector<float> vec_performances, int quartile, double coeff);
  std::vector<double> initProgVector(int size);
  double getSlope(const std::vector<double>& x, const std::vector<double>& y);
  void breakTime( int seconds);

  
private:


  
  QDoubleSpinBox *m_emotionalSB;
  QDoubleSpinBox *m_presenceSB;
  
  QSpinBox *m_nbRunsSB;
  QSpinBox *m_nbTrialsSB;
  QComboBox *m_startRunCB;
  QComboBox *m_endRunCB;
  QComboBox *m_startTrialCB;
  QComboBox *m_endTrialCB;
  QComboBox *m_endMeasureCB;

  QSpinBox *m_nbTrialsProgressSB;
  
  QVBoxLayout *m_signalsL;
  
  QString m_performanceName;
  QString m_stimulationName;
  
  QThread *m_performanceThread;
  QThread *m_stimulationThread;
  
  QLabel *m_performanceLabel;
  QLabel *m_stimulationLabel;
  QLabel *m_infoLabel;
  QLabel *m_sendLabel0;
  QLabel *m_sendLabel;
  
  QVector<float> m_performances;
  QVector<float> m_progress;
  int m_stimulation;
  
  bool m_running;
  
  
  QLabel *m_emotionalSupportDirL;
  QLabel *m_socialPresenceDirL;
  
  QLabel *m_connectionL;
  QWebSocketServer *m_pWebSocketServer;
  QList<QWebSocket *> m_clients;
  QList<bool> m_waitings;
  
  
  int m_startRunStim;
  int m_endRunStim;
  int m_startTrialStim;
  int m_endTrialStim;
  int m_endMeasureStim;
  int m_lastSocialInteraction;

  float m_perf_thresh_good_R2;
  float m_perf_thresh_bad_R2;
  float m_perf_thresh_good_S1;
  float m_perf_thresh_bad_S1;

  float m_prog_thresh_good;
  float m_prog_thresh_bad;

  float m_result_act;
  float m_progress_act;

  PhraseCategory m_last_perf_phraseCategory;
  EmotionCategory m_last_perf_emotion1;
  EmotionCategory m_last_perf_emotion2;

  QVector<float> m_phraseCategoryThresholds;
  QVector<float> m_phraseCategoryProbas;

  QVector<float> m_emotionCategoryProbas;

  QVector<FaceBehaviorPair> m_emotions2Faces;

  //QVector<CachedDistancePhraseSet> m_phraseCategory2Set;
  QVector<CachedPhraseSet> m_phraseCategory2Set;
  
  QMap<PhraseCategory, QString> m_phraseCategory2String;
  QMap<FaceBehaviorType, QString> m_faceBehaviorType2String;
  QMap<EmotionCategory, QString> m_emotions2String;
  //QMap<PhraseCategory, EmotionCategory[]> m_emotionsByCategory;


  //int m_nbRuns;
  //int m_nbTrials;
  
  int m_NUM_SESSION;
  int m_NUM_RUN;
  int m_NUM_SUJET;
  int m_currentRun;
  int m_currentTrialTotal;
  bool m_trialOngoing;
  bool m_firstRunSignaled;
  bool m_midTrialSignaled;
  bool m_lastTrialSignaled;
  bool m_gMotivate;
  
};

#endif /* ! MAINWINDOW_HPP */

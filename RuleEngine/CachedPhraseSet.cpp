#include "CachedPhraseSet.hpp"

#include <cassert>
#include <QDebug>
#include <iostream>//DEBUG

CachedPhraseSet::CachedPhraseSet(int cacheSize)
	: PhraseSet()
{
	setCacheSize(cacheSize);
}

CachedPhraseSet::CachedPhraseSet(const QString &dirName, int cacheSize)
	: PhraseSet(dirName)
{
	const int sz = std::min(size(), cacheSize);
	setCacheSize(sz);
}

void
CachedPhraseSet::setCacheSize(int cacheSize)
{
	m_cache.reserve(cacheSize);
    m_cache.resize(0);
    assert(m_cache.size() == 0 && m_cache.capacity() == cacheSize);
}

bool
CachedPhraseSet::isInCache(int index, int &cacheIndex) const
{
	bool inCache = false;
	for (int i=0; i<m_cache.size(); ++i) {
		if (m_cache[i] == index) {
			inCache = true;
			cacheIndex = i;
			break;
		}
	}
	return inCache;	
}


bool
CachedPhraseSet::isInCache(int index) const
{
	int cacheIndex;
	return isInCache(index, cacheIndex);
}

int CachedPhraseSet::getCacheSize()
{
    return m_cache.size();
}

int CachedPhraseSet::getCacheCapacity()
{
    return m_cache.capacity();
}

 void CachedPhraseSet::emptyCache()
 {
     m_cache.resize(0);
 }

 bool CachedPhraseSet::isCacheFull()
 {
     return getCacheSize()==getCacheCapacity();
 }


//REM: declared 'const' (because called from randomIndex())
//      but still modify mutable m_cache
void
CachedPhraseSet::insertInCache(int index) const
{
	//TODO: check not already there ???

	if (m_cache.size() < m_cache.capacity()) {
		m_cache.push_back(index);
	}
	else {
		for (int i=1; i<m_cache.size(); ++i) { //start from 1
			m_cache[i-1] = m_cache[i];
		}
		m_cache[m_cache.size()-1] = index;
	}
}

int
CachedPhraseSet::randomIndex() const
{
    const int nbPhrases = size();
    if (nbPhrases <= 1)
		return 0;

    int index = nbPhrases;

	do {
        index = PhraseSet::randomIndex_aux();
        assert(index>=0 && index <size());
		if (isInCache(index)) {
            //qDebug()<<" "<<index<<" is in cache";
            if (m_cache.size() >= nbPhrases) {
                //cache has same size than set,
                //we have no chance to get an index not in cache.
                //Use the oldest cache entry
				index = m_cache[0];
				break;
			}
            index = nbPhrases;
		}
	}
    while (index >= nbPhrases);
    assert(index >= 0 && index < nbPhrases);
	qDebug()<<"insert in cache "<<index;

	insertInCache(index);

	return index;
}


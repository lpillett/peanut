#ifndef PHRASESET_HPP
#define PHRASESET_HPP

#include <QPair>
#include <QVector>
#include <cassert>
#include <random>

#include <QDebug>
#include <QDir>

class QString;

class PhraseSet
{
public:
	
	PhraseSet();

	/**
	 * @brief Constructor.
	 *
	 * Load all files/phrases from directory @a dirName.
	 */
	explicit PhraseSet(const QString &dirName);

	/**
	 * Load all files/phrases from directory @a dirName.
	 */
	bool load(const QString &dirName);

  /**
   * Add Phrase from Filename @ filename.
   */
  bool addPhraseFile(const QString &filename);
  
	/**
	 * Get number of available phrases.
	 */
	int size() const;

	/**
	 * Get phrase at index @a i.
	 *
	 * @warning @a i must be in [0; size()[
	 */
	QString phrase(int i) const;

	/**
	 * Get filename at index @a i.
	 *
	 * @warning @a i must be in [0; size()[
	 */
	QString filename(int i) const;

	/**
	 * Get random index in [0; size()[
	 */
	virtual int randomIndex() const;

	/**
	 * Get one random phrase from available phrases.
	 * 
	 * Get empty string if no phrase available.
	 */
	virtual QString randomPhrase() const;

protected:
    int randomIndex_aux() const;
    std::mt19937* gen;

protected:
	
  QVector< QPair<QString, QString> > m_filePhrases; //Store pairs with filename & phrase
};

#endif /* ! PHRASESET_HPP */

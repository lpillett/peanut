TEMPLATE = app
TARGET = RuleEngine
INCLUDEPATH += .

CONFIG+=c++11
CONFIG+=warn_on
CONFIG+=debug

QT += widgets
QT += websockets

# Input
HEADERS += MainWindow.hpp PerformanceThread.hpp StimulationThread.hpp PhraseSet.hpp CachedPhraseSet.hpp CachedDistancePhraseSet.hpp ../ClientCompanion/BehaviorBlock.hpp  ../ClientCompanion/Behavior.hpp  ../ClientCompanion/BehaviorType.hpp ../ClientCompanion/Synchronization.hpp  
SOURCES += main.cpp MainWindow.cpp PerformanceThread.cpp StimulationThread.cpp PhraseSet.cpp CachedPhraseSet.cpp CachedDistancePhraseSet.cpp ../ClientCompanion/BehaviorBlock.cpp ../ClientCompanion/Synchronization.cpp  

### OpenViBE defines
QMAKE_CXXFLAGS += -I/home/potioc/BORIS/OPENVIBE/openvibe_git/dist/include


#### LSL

unix {

QMAKE_CXXFLAGS += -I/home/potioc/BORIS/labstreaminglayer/LSL/liblsl/include
LIBS += -L/home/potioc/BORIS/labstreaminglayer/LSL/liblsl/build/src -llsl

#Use same version than OpenViBE
#QMAKE_CXXFLAGS += -I/home/potioc/BORIS/OPENVIBE/openvibe_git/dependencies/include
#LIBS += -L/home/potioc/BORIS/OPENVIBE/openvibe_git/dependencies/lib -llsl

#QMAKE_CXXFLAGS += -I/home/potioc/BORIS/TMP/labstreaminglayer_1.10.2/LSL/liblsl/include
#LIBS += -L/home/potioc/BORIS/TMP/labstreaminglayer_1.10.2/LSL/liblsl/build/src -llsl

#QMAKE_CXXFLAGS += -I/home/potioc/BORIS/labstreaminglayer_git/LSL/liblsl/include
#LIBS += -L/home/potioc/BORIS/labstreaminglayer_git/LSL/liblsl/build/src -llsl

#INCLUDEPATH += /home/mansenca/BCI/labstreaminglayer_git/LSL/liblsl/include
#LIBS += -L/home/mansenca/BCI/labstreaminglayer_git/LSL/liblsl/build/src -llsl

}
win32 {

INCLUDEPATH += "C:\Users\leapi\Documents\liblsl-1.04-src\liblsl-1.04-src\LSL\liblsl\include"
LIBS += "C:\Users\leapi\Documents\liblsl-1.04-src\liblsl-1.04-src\LSL\build-liblsl-Desktop_Qt_5_6_1_MinGW_32bit-Default\src\liblsl.dll"

}

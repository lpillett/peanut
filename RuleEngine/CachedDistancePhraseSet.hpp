#ifndef CACHEDDISTANCEPHRASESET_HPP
#define CACHEDDISTANCEPHRASESET_HPP

#include "CachedPhraseSet.hpp"



class CachedDistancePhraseSet : public CachedPhraseSet
{
public:
	explicit CachedDistancePhraseSet(int cacheSize = 3);
	CachedDistancePhraseSet(const QString &dirName, int cacheSize = 3);

	/**
	 * Get random index in [0; size()[
	 *
	 * Try to not give an index already in cache, 
	 * and an index corresponding to a phrase as far as possible to other phrases.
	 */
	virtual int randomIndex() const;
	

protected:
	QVector<unsigned int> computeDistances(int index) const;

	int furthestDistanceIndex() const;
	void insertInCache(int index) const;

protected:

	mutable QVector< QVector<unsigned int> > m_distances;

};

#endif /* ! CACHEDDISTANCEPHRASESET_HPP */

#include "Widget.hpp"

#include <cassert>

#include <QDebug>
#include <QBoxLayout>
#include <QPushButton>
#include <QGridLayout>
#include <QLabel>
#include <QCheckBox>
#include <QMessageBox>
#include <QTimer>
#include <QLCDNumber>


const int UPDATE_TIMER = 500; //in ms


Widget::Widget(QWidget *parent) :
	QWidget(parent),
	m_startTime(0)
{
	m_gridLayout = new QGridLayout;

	m_phraseLabel = new QLabel(this);

	QLabel *orderLabel = new QLabel(tr("Cochez l'ensemble des adjectifs qui caractérisent cette phrase"), this);

	m_cb1 = new QCheckBox(tr("Motivante"), this);
	m_cb2 = new QCheckBox(tr("Claire"), this);
	m_cb3 = new QCheckBox(tr("Amusante"), this);
	m_cb4 = new QCheckBox(tr("Affectueuse"), this);

	m_button = new QPushButton(tr("Valider"), this);

	m_chronoLCD = new QLCDNumber(this);


	connect(m_button, SIGNAL(clicked()), this, SLOT(validate()));

	//TODO: add chrono 

	m_gridLayout->addWidget(m_phraseLabel, 0, 0, 1, 2);
	m_gridLayout->addWidget(orderLabel, 1, 0, 1, 2);
	m_gridLayout->addWidget(m_cb1, 2, 0, 1, 1);
	m_gridLayout->addWidget(m_cb2, 2, 1, 1, 1);
	m_gridLayout->addWidget(m_cb3, 3, 0, 1, 1);
	m_gridLayout->addWidget(m_cb4, 3, 1, 1, 1);
	m_gridLayout->addWidget(m_chronoLCD, 4, 1, 1, 2);
	m_gridLayout->addWidget(m_button, 5, 1, 1, 2);

	setLayout(m_gridLayout);
	 
	m_launchTime = std::chrono::system_clock::now();

}

Widget::~Widget()
{
}

void
Widget::setFilename(const QString &filename)
{
	m_file.setFileName(filename);
	if (! m_file.open(QIODevice::WriteOnly | QIODevice::Text)) {
		QMessageBox::critical(this, tr("Unable to open filename"), tr("Error: unable to open filename").arg(filename));
		exit(10);
		return; //TODO: handle error
	}
	m_out.setDevice(&m_file);
}

void
Widget::setDelay(int delay)
{
	m_delay = delay;
}

void
Widget::setAnswerTime(int time)
{
	m_answerTime = time;
}

float
Widget::getTime() const
{
	 std::chrono::time_point<std::chrono::system_clock> p = std::chrono::system_clock::now();
	 float t = std::chrono::duration_cast<std::chrono::milliseconds>(p-m_launchTime).count();
	 return t;
}


void
Widget::setText(const QString &s)
{
	m_phraseLabel->setText(s);

	QTimer::singleShot(m_delay, this, SLOT(askQuestion()));
}

static 
int
convertTimeToSeconds(float time)
{
	return static_cast<int>(time/1000 + 0.5f);
}

void
Widget::askQuestion()
{
	show();

	m_startTime = getTime();

	m_chronoLCD->display(convertTimeToSeconds(m_answerTime));
	QTimer::singleShot(UPDATE_TIMER, this, SLOT(updateTimer()));
}

void
Widget::updateTimer()
{
  //qDebug()<<"updateTimer() m_startTime="<<m_startTime;
	if (m_startTime > 0) {
		float time = getTime();
		float elapsed = time - m_startTime;
		//qDebug()<<"elpased="<<elapsed<<"  m_answerTime="<<m_answerTime;
		if (elapsed < m_answerTime) {
			int display = static_cast<int>(m_answerTime-elapsed);
			m_chronoLCD->display(convertTimeToSeconds(display));
			QTimer::singleShot(UPDATE_TIMER, this, SLOT(updateTimer()));
		}
		else {
			validate(); //TODO: we risk to have nothing checked
		}
	}
}


void
Widget::validate()
{
	QWidget::hide();

	float endTime = getTime();
	
	//TODO: save in m_file

	m_out<<m_phraseLabel->text()<<" "
		 <<m_cb1->text()<<"="<<(m_cb1->checkState()==Qt::Checked)<<" "
		 <<m_cb2->text()<<"="<<(m_cb2->checkState()==Qt::Checked)<<" "
		 <<m_cb3->text()<<"="<<(m_cb3->checkState()==Qt::Checked)<<" "
		 <<m_cb4->text()<<"="<<(m_cb4->checkState()==Qt::Checked)<<" "
		 <<" st="<<m_startTime<<" et="<<endTime<<"\n"; 
	
	emit answered();

	m_startTime = 0;

	//uncheck all for next time
	m_cb1->setCheckState(Qt::Unchecked);
	m_cb2->setCheckState(Qt::Unchecked);
	m_cb3->setCheckState(Qt::Unchecked);
	m_cb4->setCheckState(Qt::Unchecked);

}



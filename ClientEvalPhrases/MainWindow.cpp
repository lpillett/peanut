#include "MainWindow.hpp"

#include <cassert>

#include <QApplication>
#include <QDirIterator>
#include <QPushButton>
#include <QGridLayout>
#include <QComboBox>
#include <QLabel>

#include <QDebug>
#include <QDataStream>

#include <QSettings>
#include <QStandardPaths>

#include "../ClientCompanion/BehaviorBlock.hpp"


#include "Widget.hpp"
#include "ConfigurationWidget.hpp"

static const QString SETTINGS_GROUP = "MainWindow";
static const QString SETTINGS_IPADDRESS="IPAdress";
static const QString SETTINGS_PORT="Port";

static const QString PHRASES_PATH = "../../ClientCompanion/rsrc/voices/fr/phrases/"; //TODO:UGLY: should be defined by build process !!!

MainWindow::MainWindow()
	: QWidget(nullptr),
	  m_configurationWidget(nullptr),
	  m_viewWidget(nullptr)
{
	buildGUI();

	populatePhrases(PHRASES_PATH);
}



void
MainWindow::buildGUI()
{
	m_configurationWidget = new ConfigurationWidget(this);

    connect(m_configurationWidget, SIGNAL(quit()), this, SLOT(close()));

	connect(m_configurationWidget, SIGNAL(connectionRequest(const QString &, int)), this, SLOT(onConnect(const QString &, int)));
	
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(m_configurationWidget);
    setLayout(mainLayout);

#ifdef Q_OS_ANDROID
	setWindowFlags(windowFlags()|Qt::FramelessWindowHint);
#endif //Q_OS_ANDROID

	restoreSettings();

	assert(m_configurationWidget);
	assert(m_viewWidget == nullptr);
}

void
MainWindow::saveSettings()
{
	const QString path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
	const QString filename = "config.ini";
	QSettings settings(path + "/"+ filename, QSettings::IniFormat) ;

	settings.beginGroup(SETTINGS_GROUP);
	settings.setValue(SETTINGS_IPADDRESS, m_ip);
	settings.setValue(SETTINGS_PORT, m_port);
	settings.endGroup();
}

void
MainWindow::restoreSettings()
{
	const QString path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
	const QString filename = "config.ini";
	
	QSettings settings(path + "/"+ filename, QSettings::IniFormat) ;

	settings.beginGroup(SETTINGS_GROUP);
	QString ipAddress = settings.value(SETTINGS_IPADDRESS).toString();
	int port = settings.value(SETTINGS_PORT).toInt();
	if (! ipAddress.isEmpty() && port != 0) {
		assert(m_configurationWidget);
		m_configurationWidget->setServerAddress(ipAddress, port);
	}
	settings.endGroup();
}

void
MainWindow::onConnect(const QString &ip, int port)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	assert(m_configurationWidget);
	m_configurationWidget->setConnectionEnabled(false);

	QString urlStr = "ws://"+ip+":"+QString::number(port);
	m_url = QUrl(urlStr);

	if (m_debug)
		qDebug()<<"try to connect to url: "<<m_url;

	connect(&m_webSocket, &QWebSocket::connected, this, &MainWindow::onConnected);
    //connect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::closed);
	connect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::onDisconnected);
	connect(&m_webSocket, static_cast<void(QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error), this, &MainWindow::connectionError);

	//connect(&m_webSocket, (&QWebSocket::stateChanged), this, &MainWindow::onStateChanged);

    m_webSocket.open(QUrl(m_url));

	m_ip = ip;
	m_port = port;
}

void
MainWindow::onConnected()
{
    if (m_debug)
        qDebug() << "WebSocket connected";

    connect(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &MainWindow::onTextMessageReceived);
    connect(&m_webSocket, &QWebSocket::binaryMessageReceived,
            this, &MainWindow::onBinaryMessageReceived);

    QApplication::restoreOverrideCursor();
	assert(m_configurationWidget);
	m_configurationWidget->setConnectionEnabled(true);


	changeGUI();
}

void
MainWindow::onDisconnected()
{
    if (m_debug)
        qDebug() << "WebSocket disconnected";

	disconnect(&m_webSocket, &QWebSocket::connected, this, &MainWindow::onConnected);
    //disconnect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::closed);
	disconnect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::onDisconnected);
	disconnect(&m_webSocket, static_cast<void(QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error), this, &MainWindow::connectionError);

	m_webSocket.abort(); //reset the socket 

	//emit closed();

	if (m_configurationWidget == nullptr) {
		changeAgent2Connection();
	}
	else {
		//case where we try to connect and connection failed

		QApplication::restoreOverrideCursor();
		assert(m_configurationWidget);
		m_configurationWidget->setConnectionEnabled(true);
	}
}

void
MainWindow::connectionError(QAbstractSocket::SocketError error)
{
	if (m_debug) {
		qDebug() << "WebSocket error: " << error;
	}
	QApplication::restoreOverrideCursor();
}

void
MainWindow::changeGUI()
{
	assert(m_configurationWidget);
	assert(m_viewWidget == nullptr);

	//save parameters before destoying m_configurationWidget
	const QString filename = m_configurationWidget->getFilename();
	int delay = m_configurationWidget->getDelay();
	int answerTime = m_configurationWidget->getAnswerTime();

	delete m_configurationWidget;
	m_configurationWidget = nullptr;

	delete layout();

	saveSettings(); //here ?

	m_viewWidget = new Widget(this);
	m_viewWidget->setFilename(filename);
	m_viewWidget->setDelay(delay);
	m_viewWidget->setAnswerTime(answerTime);
	m_viewWidget->hide();

	QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(m_viewWidget);
	mainLayout->setSpacing(0);
	mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    connect(m_viewWidget, SIGNAL(answered()), this, SLOT(behaviorEnd()));

	//Warning: does not work if we do it here
	//setWindowFlags(windowFlags()|Qt::FramelessWindowHint);

	assert(m_configurationWidget == nullptr);
	assert(m_viewWidget);
}

void
MainWindow::changeAgent2Connection()
{
	assert(m_configurationWidget == nullptr);
	assert(m_viewWidget);

	delete m_viewWidget;
	m_viewWidget = nullptr;
	
	delete layout();

	buildGUI();

	assert(m_configurationWidget);
	assert(m_viewWidget == nullptr);
}


void 
MainWindow::onTextMessageReceived(const QString &message)
{
    if (m_debug)
        qDebug() << "Message received:" << message;

	/*
	m_statusLabel->setText(tr("Received: %1").arg(message));

	QString rmsg = "msg ok "+QString::number(m_msgNo);
	m_webSocket.sendTextMessage(rmsg);
	++m_msgNo;
	*/
}

static
QString
readFile(const QString &filename)
{
	QString text;
	QFile f(filename);
	if (f.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		while (! f.atEnd())
		{
			QString line = f.readLine();
			text += line;
		}
	}
	else 
	{
		qDebug()<<"Unable to open file "<<filename;
	}
	return text;
}

static
QString 
removeExtension(const QString &file)
{
	const int pos = file.lastIndexOf(QStringLiteral("."));
	if (pos != -1)
		return file.left(pos);
	return file;
}

QString 
MainWindow::findFile(const QString &filename)
{
	const QString file_we = removeExtension(filename);
	auto it = m_files2phrases.find(file_we);
	if (it != m_files2phrases.end()) {
		return it.value();
	}
	return QString();
}

void
MainWindow::populatePhrases(const QString &dirName)
{
	QDirIterator it(dirName);
	while (it.hasNext()) {
		it.next();
		const QString file = it.fileName();
		const QString file_we = removeExtension(file);
		const QString phrase = readFile(it.filePath());
		m_files2phrases[file_we] = phrase;
	}

	if (m_files2phrases.isEmpty()) {
		qDebug()<<"ERROR: no phrases found";
	}
}

void
MainWindow::onBinaryMessageReceived(const QByteArray &message)
{
    if (m_debug)
        qDebug() << "Binary message received:" << message;
	
    QByteArray ba = message;
    BehaviorBlock bl;
    QDataStream strm(&ba, QIODevice::ReadOnly);
    bool ok = deserialize(strm, bl);
    if (ok) {
		
		//Change all soundFilenames according to current selected voice !
		const size_t numBehaviors = bl.numberOfBehaviors();
		for (size_t i=0; i<numBehaviors; ++i) {
			Behavior *b = bl.behavior(i);
			if (b->type() == Speech) {
				SpeechBehavior *sb = static_cast<SpeechBehavior *>(b);
				if (sb->speechType() == SoundFile) {
					SoundFileSpeechBehavior *sfsb = static_cast<SoundFileSpeechBehavior*>(sb);
					//QString filename = m_voicePath + "/" + sfsb->filename();
					QString text = findFile(sfsb->filename());
					//TODO:OPTIM: Should we just pass an index through the network ?
					//Should we check that the file is in the resources ?

					qDebug()<<"received behavior with sound file: "<<sfsb->filename();
					qDebug()<<"     ==> text="<<text;
					assert(m_viewWidget);
					if (! text.isEmpty())
						m_viewWidget->setText(text);
					else
						ok = false;
				}
			}
		}

    }
	
    sendAnswer(ok);
}

void
MainWindow::sendAnswer(bool ok)
{
  qDebug()<<"MainWindow::sendAnswer("<<ok<<")";

  quint8 v = static_cast<quint8>(ok);
  QByteArray msg;
  QDataStream strm(&msg, QIODevice::WriteOnly);
  strm << v;

  m_webSocket.sendBinaryMessage(msg);
}

void
MainWindow::behaviorEnd()
{
  sendAnswer(true);
}





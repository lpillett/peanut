#ifndef CONFIGURATIONWIDGET_HPP
#define CONFIGURATIONWIDGET_HPP

#include <QWidget>
#include <QMap>

class QButtonGroup;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QSpinBox;

class ConfigurationWidget : public QWidget
{
	Q_OBJECT

public:

	explicit ConfigurationWidget(QWidget *parent = 0);

	void setServerAddress(const QString &ip, int port);

	void getServerAddress(QString &ip, int &port);

	void setConnectionEnabled(bool);

	/*
	  delay in ms
	  that is time between the reception of a signal and time to display the question
	  (It should be roughly the maximum duration of phrases)
	*/
	void setDelay(int time);
	int getDelay() const;

	/*
	  Time to answer a question
	 */
	void setAnswerTime(int time);
	int getAnswerTime() const;
	
	/*
	  Filename where answers wille be saved
	 */
	void setFilename(const QString &filename);
	QString getFilename() const;

signals:

	void connectionRequest(const QString &ip, int port);
    void quit();

protected slots:

	void onConnect();
	void chooseFilename();

protected:

	void buildGUI();
	void setEnabledConnection(bool);
	void populateHosts();

protected:

	QLabel *m_myIPLabel;
	QLabel *m_hostLabel;
	QComboBox *m_hostCombo;
	QLabel *m_portLabel;
	QLineEdit *m_portLineEdit;
	QPushButton *m_connectButton;
	QPushButton *m_quitButton;

	QLineEdit *m_fileLE;
	QPushButton *m_fileB;
	QSpinBox *m_delaySB;
	QSpinBox *m_timeSB;

};

#endif /* ! CONFIGURATIONWIDGET_HPP */

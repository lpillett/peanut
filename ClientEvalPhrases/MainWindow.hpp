#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include <QtWebSockets/QWebSocket>

#include <QHash>

class Widget;
class ConfigurationWidget;


class MainWindow : public QWidget //QMainWindow
{
	Q_OBJECT

public:
	MainWindow();


signals:
	void closed();

protected slots:

	void onConnect(const QString &ip, int port);
    void onConnected();
    void onTextMessageReceived(const QString &message);
    void onBinaryMessageReceived(const QByteArray &message);
	void behaviorEnd();
	void connectionError(QAbstractSocket::SocketError error);

	void onDisconnected();

	//void onStateChanged(QAbstractSocket::SocketState state);

protected:
	void buildGUI();

	void changeGUI();
	void changeAgent2Connection();

	void sendAnswer(bool ok);

	void saveSettings();
	void restoreSettings();

	QString findFile(const QString &filename);
	void populatePhrases(const QString &dirName);



private:

	ConfigurationWidget *m_configurationWidget;

	Widget *m_viewWidget;

    QWebSocket m_webSocket;
    QUrl m_url;
    bool m_debug;

	QString m_ip;
	int m_port;

	QHash<QString, QString> m_files2phrases;
};

#endif /* ! MAINWINDOW_HPP */

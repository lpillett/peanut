#include "ConfigurationWidget.hpp"

#include <cassert>
#include <QAction>
#include <QButtonGroup>
#include <QComboBox>
#include <QFileDialog>
#include <QHostInfo>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QNetworkInterface>
#include <QPushButton>
#include <QSpinBox>

static const int DEFAULT_DELAY = 5000; //in ms
static const int DEFAULT_TIME = 12000; //in ms
static const QString DEFAULT_FILENAME = "output.txt";

static
QString
getIPAddress()
{
	QString s;
	bool prev = false;
	foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
	  if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {
	    if (prev) 
	      s+= " ;";
	    s += address.toString();
	    prev = true;
	  }
	}
	return s;
}
 


ConfigurationWidget::ConfigurationWidget(QWidget *parent)
	: QWidget(parent)
{
	buildGUI();
}

void 
ConfigurationWidget::buildGUI()
{
	QLabel *ipLabel = new QLabel(tr("My IP address: "));
	m_myIPLabel = new QLabel(this);
	m_myIPLabel->setText(getIPAddress());


	m_hostLabel = new QLabel(tr("&Server name:"), this);
    m_portLabel = new QLabel(tr("&port:"), this);
	
	m_hostCombo = new QComboBox(this);
    m_hostCombo->setEditable(true);
	populateHosts();
	//m_hostCombo->setSizePolicy(QSizePolicy::Expanding, m_hostCombo->sizePolicy().verticalPolicy());
	m_hostCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    m_portLineEdit = new QLineEdit(this);
    m_portLineEdit->setValidator(new QIntValidator(1, 65535, this));
	const int DEFAULT_PORT = 1234;
	m_portLineEdit->setText(QString::number(DEFAULT_PORT));

	m_hostLabel->setBuddy(m_hostCombo);
    m_portLabel->setBuddy(m_portLineEdit);

	m_connectButton = new QPushButton(tr("&Connect"), this);

	m_quitButton = new QPushButton(tr("&Quit"), this);

	connect(m_connectButton, SIGNAL(clicked()), this, SLOT(onConnect()));

	connect(m_quitButton, SIGNAL(clicked()), this, SIGNAL(quit()));

	QLabel *fileL = new QLabel(tr("Output filename: "), this);

	m_fileLE = new QLineEdit(this);
	m_fileLE->setText(DEFAULT_FILENAME);
	m_fileB = new QPushButton(tr("..."), this);
	connect(m_fileB, SIGNAL(clicked()), this, SLOT(chooseFilename()));

	QLabel *delayL = new QLabel(tr("Delay before question (ms):"), this);
	m_delaySB = new QSpinBox(this);
	m_delaySB->setMaximum(500000);
	m_delaySB->setValue(DEFAULT_DELAY);

	QLabel *timeL = new QLabel(tr("Time for answer (ms):"), this);
	m_timeSB = new QSpinBox(this);
	m_timeSB->setMaximum(500000);
	m_timeSB->setValue(DEFAULT_TIME);


	QHBoxLayout *hL0 = new QHBoxLayout;
	hL0->addWidget(ipLabel);
	hL0->addWidget(m_myIPLabel);
	hL0->addStretch();
	hL0->addWidget(m_quitButton);

	QHBoxLayout *hL1 = new QHBoxLayout;
	hL1->addWidget(m_hostLabel);
	hL1->addWidget(m_hostCombo);
	hL1->addWidget(m_portLabel);
	hL1->addWidget(m_portLineEdit);

	QHBoxLayout *hL2 = new QHBoxLayout;
	hL2->addWidget(fileL);	
	hL2->addWidget(m_fileLE);	
	hL2->addWidget(m_fileB);

	QHBoxLayout *hL3 = new QHBoxLayout;
	hL3->addWidget(delayL);	
	hL3->addWidget(m_delaySB);	
	hL3->addStretch();

	QHBoxLayout *hL4 = new QHBoxLayout;
	hL4->addWidget(timeL);	
	hL4->addWidget(m_timeSB);	
	hL4->addStretch();

	QVBoxLayout *vLayout = new QVBoxLayout;
	vLayout->addLayout(hL0);
	vLayout->addLayout(hL1);
	vLayout->addLayout(hL2);
	vLayout->addLayout(hL3);
	vLayout->addLayout(hL4);
	vLayout->addWidget(m_connectButton);

	setLayout(vLayout);
}


void
ConfigurationWidget::populateHosts()
{
	assert(m_hostCombo);

    // find out name of this machine
    QString name = QHostInfo::localHostName();
    if (!name.isEmpty()) {
        m_hostCombo->addItem(name);
        QString domain = QHostInfo::localDomainName();
        if (!domain.isEmpty())
            m_hostCombo->addItem(name + QChar('.') + domain);
    }
    if (name != QString("localhost"))
        m_hostCombo->addItem(QString("localhost"));
    // find out IP addresses of this machine
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // add non-localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (!ipAddressesList.at(i).isLoopback())
            m_hostCombo->addItem(ipAddressesList.at(i).toString());
    }
    // add localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i).isLoopback())
            m_hostCombo->addItem(ipAddressesList.at(i).toString());
    }	
}

void
ConfigurationWidget::setConnectionEnabled(bool enable)
{
	assert(m_connectButton);
	m_connectButton->setEnabled(enable);
}


void
ConfigurationWidget::setServerAddress(const QString &ip, int port)
{
	int count = m_hostCombo->count();
	m_hostCombo->addItem(ip);
	assert(count < m_hostCombo->count());
	m_hostCombo->setCurrentIndex(count);

	m_portLineEdit->setText(QString::number(port));
}

void
ConfigurationWidget::getServerAddress(QString &ip, int &port)
{
	ip = m_hostCombo->currentText();
	port = m_portLineEdit->text().toInt();
}

void
ConfigurationWidget::onConnect()
{
	assert(m_hostCombo);
	assert(m_portLineEdit);

	emit connectionRequest(m_hostCombo->currentText(), m_portLineEdit->text().toInt());
}

void
ConfigurationWidget::setEnabledConnection(bool enable)
{
	assert(m_connectButton);
	m_connectButton->setEnabled(enable);
}

void
ConfigurationWidget::setDelay(int time)
{
	m_delaySB->setValue(time);
}

int
ConfigurationWidget::getDelay() const
{
	return m_delaySB->value();
}

void
ConfigurationWidget::setAnswerTime(int time)
{
	m_timeSB->setValue(time);
}

int
ConfigurationWidget::getAnswerTime() const
{
	return m_timeSB->value();
}
	
void
ConfigurationWidget::setFilename(const QString &filename)
{
	m_fileLE->setText(filename);
}

QString
ConfigurationWidget::getFilename() const
{
	return m_fileLE->text();
}

void
ConfigurationWidget::chooseFilename()
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Output filename"));
	if (! filename.isEmpty()) {
		m_fileLE->setText(filename);
	}

}

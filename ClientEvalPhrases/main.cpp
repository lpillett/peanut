#include <QApplication>

#include <iostream>
#include "MainWindow.hpp"

#include <QDirIterator> //DEBUG !!!


int
main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QCoreApplication::setOrganizationName("inria");
    QCoreApplication::setOrganizationDomain("inria.fr");
    QCoreApplication::setApplicationName("QtCompanion");

	MainWindow w;

    w.show();
	
	return app.exec();
};

#ifndef WIDGET_HPP
#define WIDGET_HPP

#include <chrono>
#include <QWidget>
#include <QFile>
#include <QTextStream>

class QGridLayout;
class QLabel;
class QCheckBox;
class QPushButton;
class QLCDNumber;

class Widget : public QWidget
{
	Q_OBJECT

public:
	explicit Widget(QWidget *parent = nullptr);

	~Widget();

	void setText(const QString &s);

	void setFilename(const QString &filename);
	void setDelay(int delay); //ms
	void setAnswerTime(int time); //ms

signals:
  void answered();

protected slots:
	void validate();
	void askQuestion();
	void updateTimer();

protected:
	float getTime() const;

protected:
	QGridLayout *m_gridLayout;
	QLabel *m_phraseLabel;
	QCheckBox *m_cb1;
	QCheckBox *m_cb2;
	QCheckBox *m_cb3;
	QCheckBox *m_cb4;
	QPushButton *m_button;

	QLCDNumber *m_chronoLCD;

	QString m_filename;
	int m_delay;
	int m_answerTime;

	QFile m_file;
	QTextStream m_out;

	std::chrono::time_point<std::chrono::system_clock> m_launchTime;


	float m_startTime;

};


#endif /* ! WIDGET_HPP */


TEMPLATE = app
TARGET = ClientEvalPhrases
INCLUDEPATH += .

CONFIG+=debug
#CONFIG+=release
CONFIG += c++11
CONFIG += warn_on
CONFIG += console

linux-g++|macx-clang++ {
QMAKE_CXXFLAGS += -Wall -Wextra
}

QT += widgets
#QT += svg
QT += multimedia
QT += websockets

QMAKE_RESOURCE_FLAGS += -threshold 0 -compress 9
#QMAKE_RESOURCE_FLAGS += -no-compress


# Input
HEADERS += MainWindow.hpp Widget.hpp ConfigurationWidget.hpp ../ClientCompanion/BehaviorBlock.hpp ../ClientCompanion/Synchronization.hpp 

SOURCES += main.cpp MainWindow.cpp Widget.cpp ConfigurationWidget.cpp ../ClientCompanion/BehaviorBlock.cpp ../ClientCompanion/Synchronization.cpp 

macx {
QMAKE_MACOSX_DEPLOYMENT_TARGET=10.7
CONFIG += x86_64
QMAKE_CXXFLAGS += -stdlib=libc++ -std=c++11 
QMAKE_LDFLAGS += -macosx-version-min=$$QMAKE_MACOSX_DEPLOYMENT_TARGET
QMAKE_LDFLAGS += -stdlib=libc++ -std=c++11
}


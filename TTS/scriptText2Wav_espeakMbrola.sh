
file=$1

#paths
ESPEAK=/home/potioc/BORIS/espeak-1.48.04-source/src/speak
MBROLA_EXE=/home/potioc/BORIS/MBROLA/mbrola-linux-i386

## espeak need "mbrola" executable in path
tmpDir="tmp$$"
while [ -d ${tmpDir} ]
do
	tmpDir="tmp$$"
done
mkdir ${tmpDir}
cp ${MBROLA_EXE} ${tmpDir}/mbrola
OLD_PATH=$PATH
export PATH=${tmpDir}:$PATH



MBROLA_DIR=$(dirname ${MBROLA_EXE})

voice=mb-fr1

#output files
phofile="${file}.pho"
wavfile="${file}_espeakMbrola_${voice}.wav"


#generate phones for text file in MBROLA format with espeak
${ESPEAK} -f ${file} -v mb-fr1 --pho --phonout ${phofile}


echo "BORIS: TODO: mbrola must be installed ???"


#generate wav file from phones with MBROLA
#${MBROLA_EXE} -I ${LIA_PHON_DIR}/data/initfile.lia ${MBROLA_DIR}/${voice}/${voice} ${olafile} ${wavfile}


rm -rf ${tmpDir}
export PATH=$OLD_PATH


DIR=$1

VOICE=$2

if [ "$#" -ne 2 ]
then
    echo "ERROR: wrong number of arguments"
    echo "Usage: $0 directory voice"
    exit 10
fi

EXT_IN=".txt"
EXT_OUT=".aiff"

echo "VOICE: ${VOICE}"

for i in $(find ${DIR} -name "*${EXT_IN}")
do

    OUTPUT_FILE="${i%${EXT_IN}}${EXT_OUT}"

    rm -f "${OUTPUT_FILE}"
    cmd="/usr/bin/say -v ${VOICE} -o ${OUTPUT_FILE} -f ${i}"

    echo "cmd=$cmd"

    $cmd

done

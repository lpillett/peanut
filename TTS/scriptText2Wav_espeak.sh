
file=$1  #text file

#paths
ESPEAK=/home/potioc/BORIS/espeak-1.48.04-source/src/speak

voice=fr

#voice=fr+f4  
# The variants are +m1 +m2 +m3 +m4 +m5 +m6 +m7 for male voices and +f1 +f2 +f3 +f4 which simulate female voices by using higher pitches. Other variants include +croak and +whisper. 


#output file
wavfile="${file}_espeak_${voice}.wav"


#generate wav file from phones with MBROLA
${ESPEAK} -f ${file} -v ${voice} -w ${wavfile}


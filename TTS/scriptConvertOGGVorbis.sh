FROM_DIR=$1
TO_DIR=$2

FROM_EXT=".wav"
TO_EXT=".ogg"
TO_CODEC="-acodec libvorbis"


if [ ! -d ${FROM_DIR} ]
then
	echo "ERROR: invalid input dir: ${FROM_DIR}"
	exit 10
fi
if [ ! -d ${TO_DIR} ]
then
	echo "ERROR: invalid output dir: ${TO_DIR}"
	exit 10
fi


for file in ${FROM_DIR}/*${FROM_EXT}
do

	outFilename="${TO_DIR}/$(basename ${file} ${FROM_EXT})${TO_EXT}"

	#echo "$outFilename"

	avconv -i ${file} ${TO_CODEC} ${outFilename}

done



txtfile=$1  #text file

#paths
LIA_PHON_DIR=/home/potioc/BORIS/lia_phon
MBROLA_EXE=/home/potioc/BORIS/MBROLA/mbrola-linux-i386

MBROLA_DIR=$(dirname ${MBROLA_EXE})

voice=fr4 #fr1, fr2, fr3, fr4, fr5, fr6, fr7  #see ${MBROLA_DIR}

#output files
olafile="${txtfile}.ola"
wavfile="${txtfile}_liaMbrola_${voice}.wav"

export LIA_PHON_REP=$LIA_PHON_DIR


#generate phones for text file in MBROLA format

#text file must be in ISO-8859-1
txtfile2="/tmp/lia$$.txt"
iconv --to-code=ISO-8859-1  ${txtfile} > ${txtfile2}

${LIA_PHON_DIR}/script/lia_text2mbrola < ${txtfile2} > ${olafile}

#generate wav file from phones with MBROLA
${MBROLA_EXE} -I ${LIA_PHON_DIR}/data/initfile.lia ${MBROLA_DIR}/${voice}/${voice} ${olafile} ${wavfile}


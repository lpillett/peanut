
DIR=$1

SCRIPT=/home/potioc/BORIS/Qt5/QtCompanionWebSockets/TTS/scriptWav2Phon_sphinx3_en.sh

if [ "$#" -ne 1 ]
then
    echo "ERROR: wrong number of parameters"
	echo "Usage: $0 directoryToProcess"
	exit 10
fi


if [ ! -f ${SCRIPT} ]
then
	echo "ERROR: script not found: ${SCRIPT}"
	exit 10
fi
if [ ! -d ${DIR} ]
then 
	echo "ERROR: not a valid directory: ${DIR}"
	exit 11
fi


for i in ${DIR}/*.txt 
do 
	wavFile="${DIR}/$(basename $i .txt).wav"

	if [ ! -f ${wavFile} ]
	then
		echo "ERROR: wav file not found: ${wavFile}"
		exit 10
	fi

	${SCRIPT} ${wavFile} $i 

done

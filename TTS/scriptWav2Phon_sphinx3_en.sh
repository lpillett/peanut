
wavfile=$1  #wav file
txtfile=$2  #text file

if [ "$#" -ne 2 ]
then
    echo "ERROR: wrong number of parameters"
	echo "Usage: $0 wavFile textFile"
	exit 10
fi


#paths
SPHINXBASE_LIBS_DIR=/home/potioc/BORIS/SPHINX/sphinxbase_svn/src/libsphinxbase/.libs
SPHINX_FE=/home/potioc/BORIS/SPHINX/sphinxbase_svn/src/sphinx_fe/.libs/sphinx_fe
SPHINX_ALIGN=/home/potioc/BORIS/SPHINX/sphinx3_svn/src/programs/sphinx3_align


MODEL_DIR=/home/potioc/BORIS/SPHINX/Models/English

ARGFILE=${MODEL_DIR}/cmusphinx-en-us-5.2/feat.params
DICT=${MODEL_DIR}/cmudict.06d
HMM=${MODEL_DIR}/cmusphinx-en-us-5.2
MDEF=${MODEL_DIR}/cmusphinx-en-us-5.2/mdef


tmpDir="tmp$$"
while [ -d ${tmpDir} ]
do
	tmpDir="tmp$$"	
done
mkdir ${tmpDir}

wavDir="${tmpDir}/WAV"
mkdir ${wavDir}

#convert to mono 16bit 
wavfileConverted="${tmpDir}/WAV/$(basename $wavfile)"
sox ${wavfile} -c 1 -r 16000 ${wavfileConverted}
#-b 16 

#remove punctuation, and convert upper case to upper case
txtfileConverted="${tmpDir}/$(basename $txtfile)"
sed -e '/^$/d' -e 's/[.,;:!?]/ /g' -e 's/^[ \t]*//;s/[ \t]*$//' -e 's/\(.*\)/\U\1/' ${txtfile} > ${txtfileConverted}

cepDir="${tmpDir}/cep"
mkdir ${cepDir}

ctlfile="${tmpDir}/wav_files.ctl"
echo $(basename ${wavfileConverted} .wav) > ${ctlfile}
#echo $(pwd)/${wavfileConverted} > ${ctlfile}

#extract cepstral features
cmd="${SPHINX_FE} -argfile ${ARGFILE} -verbose yes -c ${ctlfile} -mswav yes -di ${wavDir} -ei "wav" -do ${cepDir} -eo "mfc" -remove_silence no"
# -fdict /home/potioc/BORIS/SPHINX/Models/French/lium_french_f0/noisedict -lowerf 133.33334 -upperf 6855.49756 -nfilt 40

echo ""
echo "####################################################"
echo ""
echo "$cmd"
echo ""
$cmd

PREV_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${SPHINXBASE_LIBS_DIR}:$LD_LIBRARY_PATH




phonesDir="${tmpDir}/phones"
mkdir ${phonesDir}

wordsfile="${tmpDir}/outputWords.txt"
outsentfile="${tmpDir}/outsent.txt"

#echo $(pwd)/${wavfileConverted} > ${ctlfile}


cmd="${SPHINX_ALIGN} -cepdir ${cepDir} -ctl ${ctlfile} -dict ${DICT} -hmm ${HMM} -mdef ${MDEF} -insent ${txtfileConverted} -hyp ${wordsfile} -outsent ${outsentfile} -phsegdir ${phonesDir}"
echo ""
echo "####################################################"
echo ""
echo "${cmd}"
echo ""
${cmd}
res=$?

outputPhones="${phonesDir}/$(basename ${wavfile} .wav).phseg"

if [ "$res" -ne 0 ] || [ ! -f ${outputPhones} ]
then	
	echo ""
	echo ""
	echo "ERROR: phoneme extraction failed for text file:"
	echo "       $txtfile"
	echo "       Look at sphinx_align output & directory ${tmpDir}"
	echo "       (you may have words that are not in the dictionary, or your sound is too short)"
else
	cmd="cp ${outputPhones} $(dirname ${wavfile})/"
	${cmd}

	rm -rf ${tmpDir}
fi



export LD_LIBRARY_PATH=$PREV_LD_LIBRARY_PATH

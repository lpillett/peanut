

DIR=ClientCompanion/rsrc/anims

for dir in ${DIR}/*
do

	for webpfile in ${dir}/*.webp
	do

		pngfile="${dir}/$(basename ${webpfile} .webp).png"

		dwebp ${webpfile} -o ${pngfile}
		optipng -o5 ${pngfile}
		if [ -f ${pngfile} ]
		then
			rm ${webpfile}
		fi

	done

done


/*
  Crop images in order to minimize the number of transparent pixels (and thus image sizes) 
 but that they all have the same size (and thus origin in animation).

 */

#include <cassert>
#include <limits>
#include <iostream>
#include <QDir>
#include <QImage>
#include <QDebug>

//update @a minPt (resp @a maxPt) with minimum (resp @a max) coordinate of non transparent pixel 
void 
updatePoints(const QImage &img, QPoint &minPt, QPoint &maxPt)
{
	//TODO:OPTIM: no pixel() calls

#if 0

	for (int i=0; i<std::min(img.height(), minPt.y()+1); ++i) {
		for (int j=0; j<img.width(); ++j) {
			if (qAlpha(img.pixel(i, j)) != 0) {
				minPt.setY(i);
				if (j < minPt.x())
					minPt.setX(j);
			}
		}
	}
	for (int i=0; i<img.height(); ++i) {
		for (int j=0; j<std::min(img.width(), minPt.x()+1); ++j) {
			if (qAlpha(img.pixel(i, j)) != 0) {
				minPt.setX(j);
			}
		}
	}


	for (int i=maxPt.y()+1; i<img.height(); ++i) {
		for (int j=0; j<img.width(); ++j) {
			if (qAlpha(img.pixel(i, j)) != 0) {
				maxPt.setY(i);
				if (maxPt.x() < j)
					maxPt.setX(j);
			}
		}
	}
	for (int i=0; i<img.height(); ++i) {
		for (int j=maxPt.x(); j<img.width(); ++j) {
			if (qAlpha(img.pixel(i, j)) != 0)
				maxPt.setX(j);
		}
	}

#else

	for (int y=0; y<img.height(); ++y) {
		for (int x=0; x<img.width(); ++x) {
			if (qAlpha(img.pixel(x, y)) != 0) {
				if (x < minPt.x())
					minPt.setX(x);
				if (x > maxPt.x())
					maxPt.setX(x);
				if (y < minPt.y())
					minPt.setY(y);
				if (y > maxPt.y())
					maxPt.setY(y);
			}
		}
	}
	


#endif

}


//crop in place
void
cropImage(QImage &img, QPoint &minPt, QPoint &maxPt)
{
	assert((minPt.x() < maxPt.x() && minPt.y() < maxPt.y()));
	const int x = minPt.x();
	const int y = minPt.y();
	const int w = maxPt.x()-minPt.x()+1;
	const int h = maxPt.y()-minPt.y()+1;
	QRect r(x, y, w, h);

	img = img.copy(r);
}


//convert in place
void
convertImage(QImage &img)
{
	//change white pixels in black pixels
	//QRgb in = qRgb(106, 57, 57); //qRgb(255, 255, 255);
	//QRgb out = qRgb(0, 0, 0);

	//TODO:OPTIM: no pixel()/setPixel() calls
	for (int y=0; y<img.height(); ++y) {
		for (int x=0; x<img.width(); ++x) {
			//if (img.pixel(i, j) == in)
			const int alpha = qAlpha(img.pixel(x, y));
			if (alpha != 0)
				img.setPixel(x, y, qRgba(0, 0, 0, alpha));
		}
	}
	
}

void
computeCropRectangle(const QString &dirname,
					 QPoint &minPt,
					 QPoint &maxPt,
					 bool &sameSize,
					 bool &firstImageProcessed,
					 QSize &size)
{
	QDir in(dirname);
	if (!in.exists()) {
		qDebug()<<"invalid input directory "<<dirname;
		return;
	}
	
	//process png files in directory
	QStringList filters;
    filters << "*.png";
	QFileInfoList list = in.entryInfoList(filters);
	for (int i=0; i<list.count(); ++i) {
		const QFileInfo &file = list.at(i);
		QString basename = file.fileName();
		const QString inFilename = file.absoluteFilePath();
		QImage img(inFilename);
		if (img .isNull()) {
			qDebug()<<"Warning: unable to load image "<<inFilename;
		}
		else {
			if (! firstImageProcessed) {
				sameSize = true;
				firstImageProcessed = true;
				size = img.size();
				updatePoints(img, minPt, maxPt);
			}
			else {
				if (img.size() != size) {
					sameSize = false;
					qDebug()<<"Warning: all images have not the same size: "<<inFilename;
				}
				else {
					updatePoints(img, minPt, maxPt);
				}
			}
		}
		
	}

	//process sub-directories
	QFileInfoList listDir = in.entryInfoList(QDir::AllDirs|QDir::NoDotAndDotDot);
	for (int i=0; i<listDir.count(); ++i) {
		const QString lDirName = listDir.at(i).absoluteFilePath();
		qDebug()<<"computeCropRectangle "<<lDirName;
		computeCropRectangle(lDirName,
							 minPt,
							 maxPt,
							 sameSize,
							 firstImageProcessed,
							 size);
	}

}

void
cropImages(const QString &inputDirName,
		   const QString &rootInputDirName,
		   const QString &rootOutputDirName,
		   QDir &outRoot, 
		   QPoint minPt, QPoint maxPt)
{
	QDir in(inputDirName);
	if (!in.exists()) {
		qDebug()<<"invalid input directory "<<inputDirName;
		return;
	}
	

	QString outputDirName = rootOutputDirName;
	if (inputDirName != rootInputDirName) {
		outputDirName = inputDirName;
		outputDirName.replace(rootInputDirName, rootOutputDirName, Qt::CaseSensitive);
		
		qDebug()<<" mkdir "<<outputDirName;

		outRoot.mkdir(outputDirName);
	}

	QDir out(outputDirName);
	if (!out.exists()) {
		qDebug()<<"invalid output directory "<<outputDirName;
		return;
	}

	//process png files from current directory
	QStringList filters;
    filters << "*.png";
	QFileInfoList list = in.entryInfoList(filters);
	for (int i=0; i<list.count(); ++i) {
		const QFileInfo &file = list.at(i);
		QString basename = file.fileName();
		if (out.exists(basename)) {
			qDebug()<<"Warning: file exists "<<basename;
		}
		else {
			const QString inFilename = file.absoluteFilePath();
			QImage img(inFilename);
			if (img .isNull()) {
				qDebug()<<"Warning: unable to load image "<<inFilename;
			}
			else {

				cropImage(img, minPt, maxPt);
				//convertImage(img);

				QString outFilename = out.filePath(basename);

				bool writeOk = img.save(outFilename);
				if (! writeOk) {
					qDebug()<<"Warning: unable to save image "<<outFilename;
				}

			}
			
		}

	}


	//process sub-directories
	QFileInfoList listDir = in.entryInfoList(QDir::AllDirs|QDir::NoDotAndDotDot);
	for (int i=0; i<listDir.count(); ++i) {
		const QString &lDirName = listDir.at(i).absoluteFilePath();
		cropImages(lDirName,
				   rootInputDirName,
				   rootOutputDirName,
				   out, 
				   minPt, maxPt);
	}
}


int
main(int argc, char *argv[])
{
	if (argc != 3) {
		std::cerr<<"usage: "<<argv[0]<<" inputDir outputDir\n";
		exit(10);
	}

	QString inputDir=argv[1];
	QString outputDir=argv[2];

	QDir in(inputDir);
	if (!in.exists()) {
		qDebug()<<"inavlid input directory "<<inputDir;
		exit(10);
	}
				 
	QDir out(outputDir);
	if (!out.exists()) {
		qDebug()<<"invalid output directory "<<outputDir;
		exit(10);
	}

	QPoint minPt(std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
	QPoint maxPt(0, 0);
	bool sameSize = true;
	bool firstImageProcessed = false;
	QSize size;

	
	computeCropRectangle(inputDir,
						 minPt,
						 maxPt,
						 sameSize,
						 firstImageProcessed,
						 size);

	if (! (minPt.x() < maxPt.x() && minPt.y() < maxPt.y())) {
		qDebug()<<"ERROR: invalid minPt=("<<minPt.x()<<", "<<maxPt.x()<<") maxPt=("<<maxPt.x()<<", "<<maxPt.y()<<")";
		sameSize = false;
	}
	if (sameSize) {
		qDebug()<<"will crop the images from [(0; 0) "<<size.width()<<"x"<<size.height()<<"] to [("<<minPt.x()<<"; "<<minPt.y()<<") "<<maxPt.x()-minPt.x()<<"x"<<maxPt.y()-minPt.y()<<"]";
	}
	else {
		qDebug()<<"images have not the same size. Exiting...";
		exit(10);
	}

	//2nd pass

	cropImages(inputDir, inputDir, outputDir, out, minPt, maxPt);



	return 0;
};

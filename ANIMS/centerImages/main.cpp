
/*

  This program resize all images from input directory to the largest size
  and save images in output directory.

 */


#include <cassert>
#include <limits>
#include <iostream>
#include <QDir>
#include <QImage>
#include <QDebug>


QImage
centerImage(const QImage &img, int newWidth, int newHeight)
{
	assert(img.width() <= newWidth && img.height() <= newHeight);
	assert(img.format() == QImage::Format_ARGB32
		   || img.format() == QImage::Format_RGB32);

	if (img.width() < newWidth || img.height() < newHeight) 
	{
		QImage newImg(newWidth, newHeight, img.format());
		newImg.fill(qRgba(0, 0, 0, 0));
		
		int x0 = newWidth/2-img.width()/2;
		int y0 = newHeight/2-img.height()/2;
		for (int y=0; y<img.height(); ++y) {
			assert(y0+y<newImg.height());
			assert(y<img.height());
			QRgb *dst = ((QRgb *)newImg.scanLine(y0 + y)) + x0;
			const QRgb *src = (const QRgb *)img.scanLine(y);
			for (int x=0; x<img.width(); ++x) {
				assert(x0+x<newImg.width());
				assert(x<img.width());
				*dst = *src;
				++dst;
				++src;
			}
		}

		return newImg;
	}

	return img;
}


/*
//convert in place
void
convertImage(QImage &img)
{
	//change white pixels in black pixels
	//QRgb in = qRgb(106, 57, 57); //qRgb(255, 255, 255);
	QRgb out = qRgb(0, 0, 0);

	//TODO:OPTIM: no pixel()/setPixel() calls
	for (int i=0; i<img.height(); ++i) {
		for (int j=0; j<img.width(); ++j) {
			//if (img.pixel(i, j) == in)
			const int alpha = qAlpha(img.pixel(i, j));
			if (alpha != 0)
				img.setPixel(i, j, qRgba(0, 0, 0, alpha));
		}
	}
	
}
*/


int
main(int argc, char *argv[])
{
	if (argc != 3) {
		std::cerr<<"usage: "<<argv[0]<<" inputDir outputDir\n";
		exit(10);
	}

	QString inputDir=argv[1];
	QString outputDir=argv[2];

	QDir in(inputDir);
	if (!in.exists()) {
		qDebug()<<"inavlid input directory "<<inputDir;
		exit(10);
	}
				 
	QDir out(outputDir);
	if (!out.exists()) {
		qDebug()<<"invalid output directory "<<inputDir;
		exit(10);
	}

	QSize maxSize;

	QStringList filters;
    filters << "*.png";
    //dir.setNameFilters(filters);
	QFileInfoList list = in.entryInfoList(filters);
	for (int i=0; i<list.count(); ++i) {
		const QFileInfo &file = list.at(i);
		QString basename = file.fileName();
		if (out.exists(basename)) {
			qDebug()<<"Warning: file exists "<<basename;
		}
		else {
			const QString inFilename = file.absoluteFilePath();
			QImage img(inFilename);
			if (img .isNull()) {
				qDebug()<<"Warning: unable to load image "<<inFilename;
			}
			else {
				QSize size = img.size();
				if (size.width() > maxSize.width())
					maxSize.setWidth(size.width());
				if (size.height() > maxSize.height())
					maxSize.setHeight(size.height());
			}
		}
	}

	qDebug()<<"target size : "<<maxSize.width()<<"x"<<maxSize.height();


	//2nd pass
	for (int i=0; i<list.count(); ++i) {
		const QFileInfo &file = list.at(i);
		QString basename = file.fileName();
		if (out.exists(basename)) {
			qDebug()<<"Warning: file exists "<<basename;
		}
		else {
			const QString inFilename = file.absoluteFilePath();
			QImage img(inFilename);
			if (img .isNull()) {
				qDebug()<<"Warning: unable to load image "<<inFilename;
			}
			else {

				img = centerImage(img, maxSize.width(), maxSize.height());
				
				QString outFilename = out.filePath(basename);
				
				bool writeOk = img.save(outFilename);
				if (! writeOk) {
					qDebug()<<"Warning: unable to save image "<<outFilename;
				}

			}
			
		}

	}


	return 0;
};

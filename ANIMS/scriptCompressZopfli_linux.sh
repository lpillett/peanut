#!/bin/bash

DIR=$1

if [ "$#" -ne 1 ]
then
    echo "ERROR: wrong number of parameters"
	echo "Usage: $0 directory"
	echo "   it will compress all the images in the subdirectories of $directory" 
	exit 10
fi


ZOPFLIPNG=/home/potioc/BORIS/tools/ehoeks-zopfli-png/zopfli


for dir in ${DIR}/*
do
	echo "${dir}"

	for origfile in ${dir}/*.png
	do

		echo "#### ${origfile}"


		newfile="${origfile}.png"

		${ZOPFLIPNG} --i250 --png ${origfile} 
		# 5, 10, 15, 25, 50, 100, 250, 500, 1000
		#${ZOPFLIPNG} --i1000 --png ${origfile} 

		if [ -f ${newfile} ]
		then
			
			origSize=$(ls -l $origfile | cut -d' ' -f 5)
			newSize=$(ls -l $newfile | cut -d' ' -f 5)
			
			if [ ${newSize} -lt ${origSize} ]
			then
				echo "newSize=${newSize} < origSize=${origSize} GOOD"
				mv "${newfile}" "${origfile}"
			else
				echo "Unable to improve ${origfile} [orig=${origSize} new=${newSize}]"
				rm -f "${newfile}"
			fi
		fi

	done

done

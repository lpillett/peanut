#!/bin/bash

DIR=$1

if [ "$#" -ne 1 ]
then
    echo "ERROR: wrong number of parameters"
	echo "Usage: $0 directory"
	echo "   it will compress all images in the subdirectories of $directory" 
	exit 10
fi


for dir in ${DIR}/*
do
	echo "${dir}"

	for origfile in ${dir}/*.png
	do

		optipng -o5 $origfile

	done

done

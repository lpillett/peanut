#include "LipSyncPhsegFiles.hpp"

#include <cassert>

#include <QDebug>
#include <QFile>
#include <QStringList>

#include <iostream> //DEBUG

LipSyncPhsegFiles::LipSyncPhsegFiles()
  : m_phonemeToViseme()
{
}

LipSyncPhsegFiles::LipSyncPhsegFiles(const QString &filename)
  : m_phonemeToViseme()
{
	loadPhonemeToVisemeMapping(filename);
}


void 
LipSyncPhsegFiles::loadPhonemeToVisemeMapping(const QString &filename)
{
	//qDebug()<<"LipSyncPhsegFiles::loadPhonemeToVisemeMapping "<<filename;

	//fill m_phonemes && m_phonemeToViseme;

	QFile f(filename);
	if (f.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		while (! f.atEnd())
		{
			QString line = f.readLine();
			line = line.trimmed(); //remove whitespaces at start and end
			if (line.left(1) == QStringLiteral("#") || line.length() == 0)
				continue; // skip comments and empty lines

			QStringList strList = line.split(' ', QString::SkipEmptyParts);
			if (strList.size() > 1)
			{
				bool ok = false;
				int visemeNum = strList.at(1).toInt(&ok);
				if (ok)  
				{
					m_phonemeToViseme.insert(strList.at(0), visemeNum);
					//qDebug()<<" mapping: "<<strList.at(0)<<" to "<<visemeNum<<"";
				}
				else 
				{
					qDebug()<<"Warning: unable to convert the second string to int on line: "<<line;
				}
			}
		}

	}	
	else 
	{
		qDebug()<<"Unable to open file for Phoneme To Viseme";

	}
	
}


static inline 
FacePlanUnitType
visemeNumToFaceType(int visemeNum)
{
	int a = (int)Viseme_00 + visemeNum;
	if (a > (int)Viseme_24)
		a = (int)Viseme_00;
	return static_cast<FacePlanUnitType>(a);
}

//Get time in ms
static inline
float frameToTime(int frame)
{
	const float FRAME_RATE = 100.f;

	return 1000.f * frame / FRAME_RATE;
}

/*
static inline 
bool isPhone(const QString &phone)
{
	bool res=  (! phone.isEmpty()
				&& phone != QStringLiteral("SIL")
				&& phone[0] != '+');
	return res;
}
*/


TrackPlan<FacePlanUnit>
LipSyncPhsegFiles::sync(const QString &audioFilename)
{
	//For a given audio file xxxx.ext,
	//we suppose that there is a xxxx.phseg file 
	//in the same directory

	static const QString newExt = QStringLiteral(".phseg");

	TrackPlan<FacePlanUnit> v;

	int pos = audioFilename.lastIndexOf('.');
	if (pos != -1)
	{
		const QString phsegFilename = audioFilename.left(pos) + newExt;

		QFile f(phsegFilename);
		if (! f.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			qWarning()<<"unable to open filename: "<<phsegFilename;
		}		
		else 
		{
			while (!f.atEnd())
			{
				QString line = f.readLine();
				line = line.trimmed(); //remove whitespaces from start & end
				if (line.length() == 0)
					continue; // skip comments & empty lines
				
				QStringList strList = line.split(' ', QString::SkipEmptyParts);
				const int size = strList.size();
				assert(size >= 3);
				
				if (strList[0]==QStringLiteral("SFrm")) {
					//first line
					continue;
				}
				else if (strList[0]==QStringLiteral("Total")) {
					//last line
					break;
				}
				else {
					if (size < 4) {
						qDebug()<<"ERROR: wrong number of fields on line: "<<strList;
						continue;
					}
					
					assert(size >= 4);
					bool ok = false;
					const int startFrame = strList[0].toUInt(&ok);
					if (! ok) {
						qWarning()<<"ERROR: wrong first field parsing line: "<<strList;
						continue;
					}
					const int endFrame = strList[1].toUInt(&ok);
					if (! ok) {
						qWarning()<<"ERROR: wrong second field parsing line: "<<strList;
						continue;
					}
					
					if (endFrame <= startFrame) {
						qWarning()<<"Warning: wrong timing for line: "<<strList;
						continue;
					}
					
					float startTime = frameToTime(startFrame);
					float endTime = frameToTime(endFrame);
					
					QString phone = strList[3];
					
					int visemeNum = m_phonemeToViseme.value(phone, 0);
					
					FacePlanUnitType type = visemeNumToFaceType(visemeNum);
					
					
					float length = endTime - startTime;

					qDebug()<<"phone="<<phone<<" visemeNum="<<visemeNum<<" type="<<(int)(type)<<" s="<<startTime<<" e="<<endTime<<" l="<<length;
					

					bool add = true;

					if (! v.empty()) 
					{
						//For really close-in-time visemes :
						//- if visemes have the same type, we merge the two animations
						//- if the second one is silence/idle : we lengthen it  (to avoid to continue to move the lips when we should not)
						//- otherwise, we lengthen the shortest of the two (to have less chances to miss an animation because its length was too short)

						const float diff_threshold = 12; //shinx3_align has at least 10ms between 2 consecutives phones
						FacePlanUnit *prev = v.back();
						assert(prev->type() == FacePU);

						const float diff = startTime-prev->endTime();
						if (diff < diff_threshold) 
						{
							if (type == prev->faceType()) 
							{
								prev->add(endTime - prev->endTime());

								add = false;
							}
							else if (type == Viseme_00) 
							{
								startTime = prev->endTime();
								length = endTime-startTime;
							}
							//else if (prev->type() == Viseme_00)   //should we do it ?
							else 
							{
								if (prev->length() > length)
								{
									startTime = prev->endTime();
									length = endTime-startTime;
								}
								else 
								{
									assert(diff >= 0);
									prev->add(diff);
								}
							}
						}



					}
					
					if (add)
					{
						const float intensity = 1.0f;

						//TODO:UGLY : use defines for MOUTH & Set !!!
						int element = 0; //MOUTH;
						int index = 0;
						int segment = 1; //Set
						FacePlanUnit *pu = new FacePlanUnit(type, intensity, element, index, segment, startTime, length);
						pu->setLoopable(false); //B??? TODO??? what happens when we have 1 image & set loopable to false ???
						pu->setRequired(true);
						v.push_back(pu);
					}
				}
						
						
			}

		}

	}


	//DEBUG
	{
		qDebug()<<v.size()<<" animations for "<<audioFilename;
		float minL = 10000;
		int minLt = Viseme_00;
		float maxL = 0;
		int maxLt = Viseme_00;
		float sumL = 0;
		for(const FacePlanUnit *a : v) 
		{
			std::cerr<<(int)a->faceType()-(int)Viseme_00<<" [s="<<a->startTime()<<" e="<<a->endTime()<<" l="<<a->length()<<"], ";

			if (a->length() < minL) {
				minL = a->length();
				minLt = a->type();
			}
			if (a->length() > maxL) {
				maxL = a->length();
				maxLt = a->type();
			}
			sumL += a->length();
		}
		std::cerr<<"\n";

		std::cerr<<"*** min length="<<minL<<" for viseme="<<minLt-(int)Viseme_00<<"  => "<<minL/33.33<<" frames at 30fps\n";
		std::cerr<<"*** max length="<<maxL<<" for viseme="<<maxLt-(int)Viseme_00<<"  => "<<maxL/33.33<<" frames at 30fps\n";
		std::cerr<<"*** mean length="<<sumL/v.size()<<" => "<<sumL/v.size()/33.33<<" frames at 30fps\n";
	}


	return v;
}

#ifndef PLANUNIT_HPP
#define PLANUNIT_HPP

#include <QString>

#include "BehaviorType.hpp"

typedef enum {FacePU, SpeechPU, ControlPU} PlanUnitType;

typedef FaceBehaviorType FacePlanUnitType;
//Viseme_00 is for idle/silence !

typedef SpeechBehaviorType SpeechPlanUnitType;

typedef enum {EndBehavior} ControlPlanUnitType;



class PlanUnit
{
public:

	virtual ~PlanUnit() {}

	PlanUnit(PlanUnitType type, 
			 float intensity=1.0f, 
			 float startTime = 0.f,
			 float length = 1000.f) :
		m_type(type),
		m_intensity(intensity),
		m_startTime(startTime),
		m_length(length)
		{
			
		}
	
	PlanUnitType type() const { return m_type; }
	float intensity() const { return m_intensity; }
	float startTime() const { return m_startTime; }

	//length in milliseconds
	float length() const { return m_length; }

	float endTime() const { return m_startTime+m_length; }

	//shift startTime of the animation of @a time milliseconds
	void shiftOf(float time) { m_startTime += time; }

	//Set startTime of the animation to @a time (in milliseconds)
	void setStartTime(float time) { m_startTime = time; }
	
	//set endTime of the animation to @a time (in milliseconds)
	void setEndTime(float time) { m_startTime = time-m_length; }

	//set length to @a time (in milliseconds)
	void setLength(float time) { m_length = time; }

	//add @a time milliseconds at the end and to the length of the animation
	void add(float time) { m_length += time; }

protected:
	PlanUnitType m_type;
	float m_intensity;
	float m_startTime;
	float m_length;
};

//TODO: decorator of FacePlanUnit to add element/index/segment ?????

class FacePlanUnit : public PlanUnit
{
public:
	FacePlanUnit(FacePlanUnitType faceType, 
		     float intensity=1.0f, 
		     int element = 0, int index = 0, int segment = 0,
		     float startTime = 0.f,
		     float length = 1000.f) :
		PlanUnit(FacePU, intensity, startTime, length),
		m_faceType(faceType), 
		m_element(element), m_index(index), m_segment(segment),
		m_required(false),
		m_loopable(false)
		{
			
		}	

	FacePlanUnitType faceType() const { return m_faceType; }

	void setElement(int t) { m_element = t; }
	void setIndex(int i) { m_index = i; }
	void setSegment(int s) { m_segment = s; }

	void setLoopable(bool l) { m_loopable = l; }
	void setRequired(bool r) { m_required = r; }

	int element() const { return m_element; }
	int index() const { return m_index; }
	int segment() const { return m_segment; }

	bool loopable() const { return m_loopable; }
	bool required() const { return m_required; }

protected:
	FacePlanUnitType m_faceType;
	int m_element;
	int m_index;
	int m_segment;
	bool m_required;
	bool m_loopable;
};


class SpeechPlanUnit : public PlanUnit
{
public:
	SpeechPlanUnit(SpeechPlanUnitType speechType, 
		       float intensity=1.0f, 
		       float startTime = 0.f,
		       float length = -1.f) :
		PlanUnit(SpeechPU, intensity, startTime, length),
		m_speechType(speechType)
		{
			
		}

	SpeechPlanUnitType speechType() const { return m_speechType; }

protected:
	SpeechPlanUnitType m_speechType;
};

class SoundFilePlanUnit : public SpeechPlanUnit
{
public:
	SoundFilePlanUnit(const QString &filename,
			  float intensity=1.0f, 
			  float startTime = 0.f,
			  float length = -1.f) :
		SpeechPlanUnit(SoundFile, intensity, startTime, length),
		m_filename(filename)
		{
			
		}


	const QString &filename() const { return m_filename; }
	
protected:
	const QString m_filename;
};
	

class ControlPlanUnit : public PlanUnit
{
public:
	ControlPlanUnit(ControlPlanUnitType controlType, 
			float intensity=1.0f, 
			float startTime = 0.f,
			float length = -1.f) :
		PlanUnit(ControlPU, intensity, startTime, length),
		m_controlType(controlType)
		{
			
		}

	ControlPlanUnitType controlType() const { return m_controlType; }

protected:
	ControlPlanUnitType m_controlType;
};


#endif /* ! ANIMATION_HPP */

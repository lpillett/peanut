#include "MainWindow.hpp"

#include <cassert>

#include <QApplication>
#include <QDirIterator>
#include <QPushButton>
#include <QBoxLayout>
#include <QComboBox>
#include <QListWidget>
#include <QFileDialog>

#include <QDebug>
#include <QDataStream>

#include <QSettings>
#include <QStandardPaths>


#include "Widget.hpp"
#include "BehaviorBlock.hpp"
#include "ConfigurationWidget.hpp"

static const QString SETTINGS_GROUP = "MainWindow";
static const QString SETTINGS_IPADDRESS="IPAdress";
static const QString SETTINGS_PORT="Port";
//TODO: save face/lang/voice in settings !!

static const QString RSRC_FACES_ROOTPATH = ":/faces/";
static const QString RSRC_VOICES_ROOTPATH = ":/voices/";
static const QString MAPPING_FILENAME = "phoneme_viseme_mapping.txt";
static const QString VOICE_TO_FLAG_MAPPING_FILE = "voice_flag_mapping.txt";




MainWindow::MainWindow()
	: QWidget(nullptr),
	  m_configurationWidget(nullptr),
	  m_viewWidget(nullptr)
{
	buildGUI();
}



void
MainWindow::buildGUI()
{

	m_configurationWidget = new ConfigurationWidget(this);
	qDebug()<<"** MainWindow::buildGUI() m_configurationWidget="<<m_configurationWidget;
	populateFaces();
    populateVoices();

/*
	{
		//AFTER POPULATE
		QString faceName, faceImageFilename;
		m_configurationWidget->getSelectedFace(faceName, faceImageFilename);
		qDebug()<<"selected face="<<faceName;
	}
*/

    connect(m_configurationWidget, SIGNAL(quit()), this, SLOT(close()));

	connect(m_configurationWidget, SIGNAL(connectionRequest(const QString &, int)), this, SLOT(onConnect(const QString &, int)));

	
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(m_configurationWidget);
    setLayout(mainLayout);

	//setWindowTitle(tr("Client"));

#ifdef Q_OS_ANDROID
    setWindowFlags(windowFlags()|Qt::FramelessWindowHint);
#endif //Q_OS_ANDROID


	restoreSettings();

	assert(m_configurationWidget);
	assert(m_viewWidget == nullptr);

	qDebug()<<"** MainWindow::buildGUI() m_configurationWidget="<<m_configurationWidget<<" done";
}

void
MainWindow::saveSettings()
{
	const QString path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
	const QString filename = "config.ini";
	QSettings settings(path + "/"+ filename, QSettings::IniFormat) ;

	settings.beginGroup(SETTINGS_GROUP);
	settings.setValue(SETTINGS_IPADDRESS, m_ip);
	settings.setValue(SETTINGS_PORT, m_port);
	settings.endGroup();
}

void
MainWindow::restoreSettings()
{
	const QString path = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
	const QString filename = "config.ini";
	
	qDebug()<<"MainWindow::restoreSettings() "<<(path + "/"+ filename);

	QSettings settings(path + "/"+ filename, QSettings::IniFormat) ;

	settings.beginGroup(SETTINGS_GROUP);
	QString ipAddress = settings.value(SETTINGS_IPADDRESS).toString();
	int port = settings.value(SETTINGS_PORT).toInt();
	if (! ipAddress.isEmpty() && port != 0) {
		assert(m_configurationWidget);
		m_configurationWidget->setServerAddress(ipAddress, port);
	}
	settings.endGroup();
}

void
MainWindow::onConnect(const QString &ip, int port)
{
	qDebug()<<"** MainWindow::onConnect";

	QApplication::setOverrideCursor(Qt::WaitCursor);
	assert(m_configurationWidget);
	m_configurationWidget->setConnectionEnabled(false);

	QString urlStr = "ws://"+ip+":"+QString::number(port);
	m_url = QUrl(urlStr);

	if (m_debug)
		qDebug()<<"try to connect to url: "<<m_url;

	connect(&m_webSocket, &QWebSocket::connected, this, &MainWindow::onConnected);
    //connect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::closed);
	connect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::onDisconnected);
	connect(&m_webSocket, static_cast<void(QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error), this, &MainWindow::connectionError);

	//connect(&m_webSocket, (&QWebSocket::stateChanged), this, &MainWindow::onStateChanged);


    m_webSocket.open(QUrl(m_url));


	m_ip = ip;
	m_port = port;
}

void
MainWindow::onConnected()
{
	qDebug()<<"** MainWindow::onConnected";

    if (m_debug)
        qDebug() << "WebSocket connected";

    connect(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &MainWindow::onTextMessageReceived);
    connect(&m_webSocket, &QWebSocket::binaryMessageReceived,
            this, &MainWindow::onBinaryMessageReceived);

    QApplication::restoreOverrideCursor();
	assert(m_configurationWidget);
	m_configurationWidget->setConnectionEnabled(true);


	changeGUI();
}

void
MainWindow::onDisconnected()
{
	qDebug()<<"** MainWindow::onDisconnected()";


    if (m_debug)
        qDebug() << "WebSocket disconnected";

	disconnect(&m_webSocket, &QWebSocket::connected, this, &MainWindow::onConnected);
    //disconnect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::closed);
	disconnect(&m_webSocket, &QWebSocket::disconnected, this, &MainWindow::onDisconnected);
	disconnect(&m_webSocket, static_cast<void(QWebSocket::*)(QAbstractSocket::SocketError)>(&QWebSocket::error), this, &MainWindow::connectionError);

	m_webSocket.abort(); //reset the socket 

	//emit closed();

	if (m_configurationWidget == nullptr) {
		changeAgent2Connection();
	}
	else {
		//case where we try to connect and connection failed

		QApplication::restoreOverrideCursor();
		assert(m_configurationWidget);
		m_configurationWidget->setConnectionEnabled(true);
	}
}

/*
void
MainWindow::onStateChanged(QAbstractSocket::SocketState state)
{
	qDebug()<<" stateChanged="<<state;
}
*/

void
MainWindow::connectionError(QAbstractSocket::SocketError error)
{
	if (m_debug) {
		qDebug() << "WebSocket error: " << error;
	}
	QApplication::restoreOverrideCursor();
}

void
MainWindow::populateFaces()
{
	const QString facesPath = RSRC_FACES_ROOTPATH;

	QDirIterator it(facesPath);
	while (it.hasNext()) {
		QString v = it.next();
		if (v.endsWith(".xml", Qt::CaseInsensitive)) {
			
			QString faceFilename = v;
			faceFilename.replace(".xml", ".png", Qt::CaseInsensitive);

			bool found = false;
			QDirIterator it2(facesPath);
			while (it2.hasNext()) {
				QString v2 = it2.next();

				if (v2.indexOf(faceFilename, Qt::CaseInsensitive) == 0) {
					//found

					//QString name = v; //v.remove(".xml", Qt::CaseInsensitive);
					m_configurationWidget->addFace(v, v2);
					//qDebug()<<"  addFace name="<<name<<" file="<<v2<<"\n";

					found = true;
					break;
				}
			}
			if (! found) {
				m_configurationWidget->addFace(v, "");
			}
			
		}
	}

	
	m_configurationWidget->selectFirstFace();
}

static
QMap<QString, QString> 
loadVoiceToFlagFile(const QString &filename)
{
	QMap<QString, QString> m;

	QFile f(filename);
	if (f.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		while (! f.atEnd())
		{
			QString line = f.readLine();
			line = line.trimmed(); //remove whitespaces at start and end
			if (line.left(1) == QStringLiteral("#") || line.length() == 0)
				continue; // skip comments and empty lines

			QStringList strList = line.split(' ', QString::SkipEmptyParts);
			if (strList.size() == 2)
			{
				m[strList.at(0)] = strList.at(1);
			}
		}
	}
	return m;
}

void
MainWindow::populateVoices()
{
	const QString voicesPath = RSRC_VOICES_ROOTPATH;

	QString lang;

	QDirIterator itL(voicesPath);
	while (itL.hasNext()) {
		itL.next();
		
		lang = itL.fileName();

		qDebug()<<"lang="<<lang;

		QString voiceToFlagMapping  = itL.filePath() + "/" + VOICE_TO_FLAG_MAPPING_FILE;
		QMap<QString, QString> voice2flag = loadVoiceToFlagFile(voiceToFlagMapping);

		QDirIterator it(itL.filePath());
		while (it.hasNext()) {
			it.next();

			if (it.fileInfo().isDir()) {

				QString name = it.fileName();
				QString filename;
				
				//qDebug()<<"name="<<name;
				//qDebug()<<"populateVoices fileName="<<name<<" filePath="<<it.filePath();
				
				//get first file that has an extension of three characters (exclude phseg files)

				QDirIterator it2(it.filePath());
				while (it2.hasNext()) {
		
					filename = it2.next();
		
					if ((filename.length() - filename.lastIndexOf('.')) == 4) {
						QString flagFilename;
						auto itM = voice2flag.find(name);
						if (itM != voice2flag.end()) {
							flagFilename = itL.filePath() + "/" + itM.value();
						}

						m_configurationWidget->addVoice(lang, name, filename, flagFilename);

						break;
					}
				}
			}
		}
	}

	m_configurationWidget->selectLang(lang);
}

QString 
MainWindow::getLang() const
{
	assert(m_configurationWidget);
	QString lang, voiceName, voiceSoundFilename;
	m_configurationWidget->getSelectedVoice(lang, voiceName, voiceSoundFilename);
	
	return lang;
}

QString 
MainWindow::getVoicePath() const
{
	assert(m_configurationWidget);
	QString lang, voiceName, voiceSoundFilename;
	m_configurationWidget->getSelectedVoice(lang, voiceName, voiceSoundFilename);
	
	return RSRC_VOICES_ROOTPATH + lang + "/" + voiceName;
}

QString 
MainWindow::getMappingFilename() const
{
	assert(m_configurationWidget);
	QString lang, voiceName, voiceSoundFilename;
	m_configurationWidget->getSelectedVoice(lang, voiceName, voiceSoundFilename);
	
	return RSRC_VOICES_ROOTPATH + lang + "/" + MAPPING_FILENAME;
}



QString
MainWindow::getFaceFile() const
{
	//return QString(":/faces/animsF.xml");
	//TODO !!!!!
	//choose among available xml files !!!

	QString faceName, faceImageFilename;
	m_configurationWidget->getSelectedFace(faceName, faceImageFilename);
	
	return faceName;
	//TODO : get xml file for face
	//TODO : get directory prefix for voice name
	//          & append this to all incomming sound messages !

}

void
MainWindow::changeGUI()
{
	qDebug()<<"** MainWindow::changeGUI()";
	assert(m_configurationWidget);
	assert(m_viewWidget == nullptr);

	//save parameters before destoying m_configurationWidget
	m_voicePath = getVoicePath();
	m_faceFile = getFaceFile();
	QString mappingFile = getMappingFilename(); //warning: must be done befeore deleting m_configurationWidget

	qDebug()<<" m_voicePath="<<m_voicePath;
	qDebug()<<" mappingFile="<<mappingFile;

	delete m_configurationWidget;
	m_configurationWidget = nullptr;

	delete layout();


	saveSettings(); //here ?


	QString faceFile = m_faceFile;


	qDebug()<<"  MainWindow::changeGUI() faceFile="<<m_faceFile<<" mappingFile="<<mappingFile;

	m_viewWidget = new Widget(faceFile, mappingFile, this);

	QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(m_viewWidget);
	mainLayout->setSpacing(0);
	mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    connect(m_viewWidget, SIGNAL(behaviorEnd()), this, SLOT(behaviorEnd()));

	//Warning: does not work if we do it here
	//setWindowFlags(windowFlags()|Qt::FramelessWindowHint);


	assert(m_configurationWidget == nullptr);
	assert(m_viewWidget);
}

void
MainWindow::changeAgent2Connection()
{
	qDebug()<<"** MainWindow::changeAgent2Connection()";
	assert(m_configurationWidget == nullptr);
	assert(m_viewWidget);

	delete m_viewWidget;
	m_viewWidget = nullptr;
	
	delete layout();

	buildGUI();

	assert(m_configurationWidget);
	assert(m_viewWidget == nullptr);

	qDebug()<<"   MainWindow::changeAgent2Connection() done";
}


void 
MainWindow::onTextMessageReceived(const QString &message)
{
    if (m_debug)
        qDebug() << "Message received:" << message;

	/*
	m_statusLabel->setText(tr("Received: %1").arg(message));

	QString rmsg = "msg ok "+QString::number(m_msgNo);
	m_webSocket.sendTextMessage(rmsg);
	++m_msgNo;
	*/
}

static
QString 
findFile(const QString &dirName, const QString &filename)
{
	//find the file @a filename in @a dirName with any extension (of three characters) [it excludes phseg files]
	//TODO:OPTIM

	QDirIterator it(dirName);
	while (it.hasNext()) {
		it.next();

		QString file = it.fileName();
		if (file.startsWith(filename) && (file.length()-filename.length())==4) {

			qDebug()<<"for "<<filename<<" found "<<it.filePath();

			return it.filePath();
		}
	}
	return filename;
}


void
MainWindow::onBinaryMessageReceived(const QByteArray &message)
{
    if (m_debug)
        qDebug() << "Binary message received:" << message;
	
    QByteArray ba = message;
    BehaviorBlock bl;
    QDataStream strm(&ba, QIODevice::ReadOnly);
    bool ok = deserialize(strm, bl);
    if (ok) {
		
		//Change all soundFilenames according to current selected voice !
		const size_t numBehaviors = bl.numberOfBehaviors();
		for (size_t i=0; i<numBehaviors; ++i) {
			Behavior *b = bl.behavior(i);
			if (b->type() == Speech) {
				SpeechBehavior *sb = static_cast<SpeechBehavior *>(b);
				if (sb->speechType() == SoundFile) {
					SoundFileSpeechBehavior *sfsb = static_cast<SoundFileSpeechBehavior*>(sb);
					//QString filename = m_voicePath + "/" + sfsb->filename();
					QString filename = findFile(m_voicePath, sfsb->filename());
					//TODO:OPTIM: Should we just pass an index through the network ?
					//Should we check that the file is in the resources ?

					qDebug()<<"received behavior with sound file: "<<sfsb->filename();
					qDebug()<<"     ==> changed to "<<filename;

					sfsb->setFilename(filename);
				}
			}
		}


		const bool addOk = m_viewWidget->add(bl);
		
		if (addOk) {
			return;
		}
    }
	
    sendAnswer(false);
}

void
MainWindow::sendAnswer(bool ok)
{
  qDebug()<<"MainWindow::sendAnswer("<<ok<<")";

  quint8 v = static_cast<quint8>(ok);
  QByteArray msg;
  QDataStream strm(&msg, QIODevice::WriteOnly);
  strm << v;

  m_webSocket.sendBinaryMessage(msg);
}

void
MainWindow::behaviorEnd()
{
  sendAnswer(true);
}


#ifndef Q_OS_ANDROID

#include <QMouseEvent>

void
MainWindow::mousePressEvent(QMouseEvent *evt)
{
	m_oldPos = evt->globalPos();
}

void
MainWindow::mouseMoveEvent(QMouseEvent *evt)
{
		const QPoint delta = evt->globalPos() - m_oldPos;
		move(x()+delta.x(), y()+delta.y());
		m_oldPos = evt->globalPos();
}

#endif //Q_OS_ANDROID



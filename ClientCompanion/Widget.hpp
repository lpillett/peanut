#ifndef WIDGET_HPP
#define WIDGET_HPP

#include <memory>
#include <chrono>
#include <QTimer>
#include <QWidget>

class Planner;
class BehaviorBlock;

class Widget : public QWidget
{
	Q_OBJECT

public:
	explicit Widget(const QString &faceFile, const QString &mappingFile, QWidget *parent = nullptr);

	~Widget();


	bool add(BehaviorBlock &b);


signals:
  void behaviorEnd();

protected slots:
	void nextFrame();

protected:
	float getTime() const;

protected:
	std::shared_ptr<Planner> m_planner;
	
	QTimer *m_timer;
	std::chrono::system_clock::time_point m_startTime;
};


#endif /* ! WIDGET_HPP */

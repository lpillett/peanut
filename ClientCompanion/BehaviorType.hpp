#ifndef BEHAVIORTYPE_HPP
#define BEHAVIORTYPE_HPP


typedef enum {Face, Speech} BehaviorType;

typedef enum {Idle, Neutre_02, Neutre_03, Neutre_04, Blink, 
		  Happy, Surprised, Trust, Sad, Angry, Bored, 
	      Viseme_00=1000, Viseme_01, Viseme_02, Viseme_03, Viseme_04, Viseme_05, Viseme_06, Viseme_07, Viseme_08, Viseme_09, Viseme_10, Viseme_11, Viseme_12, Viseme_13, Viseme_14, Viseme_15, Viseme_16, Viseme_17, Viseme_18, Viseme_19, Viseme_20, Viseme_21, Viseme_22, Viseme_23, Viseme_24} FaceBehaviorType;

typedef enum {SoundFile, TTS} SpeechBehaviorType;


#endif /* ! BEHAVIORTYPE_HPP */

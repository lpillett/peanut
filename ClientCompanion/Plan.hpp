#ifndef PLAN_HPP
#define PLAN_HPP

#include "PlanUnit.hpp"

#include <cassert>
#include <algorithm>//sort
#include <QVector>
#include <cmath> //fabs

//#include <iostream>//DEBUG

template <typename PLANUNIT>
class TrackPlan : public QVector<PLANUNIT *>
{
public:

  float startTime() const {
    assert(checkOrdered());
    if (! this->empty()) {
      PLANUNIT *pu = this->front();
      assert(pu);
      return pu->startTime();
    }
    return 0.f;
  }
  
  float length() const { 
    return endTime()-startTime();
  }
  
  float endTime() const { 
    assert(checkOrdered());
    if (! this->empty()) 
      {
	PLANUNIT *pu = this->back();
	assert(pu);
	return pu->endTime();
      }
    return 0.f;
    
  }
  
  void shiftOf(float time) { 
/*
    for (PLANUNIT *pu : *this) 
      {
	pu->shiftOf(time);
      }
*/
	  //ensure that contiguous tracks stay contiguous

	  /*
	  {
		  if (this->size() > 0) {
			  std::cerr<<"######## shiftOf("<<time<<")\n";
			  for (int i=0; i<this->size(); ++i) {
				  std::cerr<<"    pu["<<i<<"] st="<<(*this)[i]->startTime()<<" l="<<(*this)[i]->length()<<" et="<<(*this)[i]->endTime()<<"\n";
			  }
		  }
	  }
	  */

	  const int sz = this->size();
	  if (sz > 0) {
		  const float eps = 10*std::numeric_limits<float>::epsilon();
		  bool isContiguous = (sz>1 && (fabs((*this)[0]->endTime()-(*this)[1]->startTime()) < eps));
		  (*this)[0]->shiftOf(time);
		  for (int i=1; i<sz; ++i) { //start from 1
			  bool nextIsContiguous = (i <sz-1
									  && (fabs((*this)[i]->endTime()-(*this)[i+1]->startTime()) < eps));
			  if (isContiguous)
				  (*this)[i]->setStartTime((*this)[i-1]->endTime());
			  else
				  (*this)[i]->shiftOf(time);
			  isContiguous = nextIsContiguous;
		  }
	  }

	  /*
	  {
		  if (this->size() > 0) {
			  std::cerr<<"------- \n";
			  for (int i=0; i<this->size(); ++i) {
				  std::cerr<<"    pu["<<i<<"] st="<<(*this)[i]->startTime()<<" l="<<(*this)[i]->length()<<" et="<<(*this)[i]->endTime()<<"\n";
			  }
			  std::cerr<<"######## shiftOf("<<time<<") done\n";
		  }
	  }
	  */

  }
  
  void setStartTime(float time) { 
    shiftOf(time-startTime());
  }
  
  void setEndTime(float time) { 
    shiftOf(time-endTime());
  }
  
  void sort() {
    std::sort(this->begin(), this->end(),
	      [](const PLANUNIT *pu1, const PLANUNIT *pu2) -> bool
	      {
		return pu1->startTime() < pu2->startTime();
	      });
  }
  
  bool checkOrdered() const {
    auto it = this->cbegin();
    auto itN = it+1;
    auto itEnd = this->cend();
    if (it != itEnd && itN != itEnd) 
      {
	for ( ; itN != itEnd ; ++it, ++itN) 
	  {
	    if ((*it)->startTime() > (*itN)->startTime())
	      return false;
	  }
      }
    return true;
  }
  
  bool checkOrderedAndNonIntersecting() const {
    auto it = this->cbegin();
    auto itN = it+1;
    auto itEnd = this->cend();
    if (it != itEnd && itN != itEnd) 
      {
	for ( ; itN != itEnd ; ++it, ++itN) 
	  {
	    if ((*it)->endTime() > (*itN)->startTime())
	      return false;
	  }
      }
    return true;
  }
  
	
};


template <typename PLANUNIT>
class Plan : public QVector< TrackPlan<PLANUNIT> >
{
public:
	
	float startTime() const {
		assert(checkOrdered());
		float minStartTime = 0;
		if (! this->empty()) {
			minStartTime = std::numeric_limits<float>::max();
			for (const auto &pl : *this) {
				if (! pl.empty()) {
					const float st = pl.startTime();
					if (st < minStartTime)
						minStartTime = st;
				}
			}
		}
		return minStartTime;
	}

	float length() const { 
		return endTime()-startTime();
	}
	
	float endTime() const { 
		assert(checkOrdered());
		float maxEndTime = 0; //std::numeric_limits<float>::min();
		if (! this->empty()) {
			maxEndTime = -std::numeric_limits<float>::max();
			for (const auto &pl : *this) {
				if (! pl.empty()) {
					const float et = pl.endTime();
					if (et > maxEndTime)
						maxEndTime = et;
				}
			}
		}
		return maxEndTime;
	}

	void shiftOf(float time) { 

	  /*
		if (! this->empty()) {
			std::cerr<<"****** before shiftOf("<<time<<") global st="<<this->startTime()<<" l="<<this->length()<<" et="<<this->endTime()<<"\n";
			for (int i=0; i<this->size(); ++i) {
				std::cerr<<"   track "<<i<<" st="<<(*this)[i].startTime()<<" l="<<(*this)[i].length()<<" et="<<(*this)[i].endTime()<<"\n";
			}
		}
	  */

		for (auto &pl : *this) {
			pl.shiftOf(time);
		}

		/*
		if (! this->empty()) {
			std::cerr<<"       after shiftOf("<<time<<") global st="<<this->startTime()<<" l="<<this->length()<<" et="<<this->endTime()<<"\n";
			for (int i=0; i<this->size(); ++i) {
				std::cerr<<"   track "<<i<<" st="<<(*this)[i].startTime()<<" l="<<(*this)[i].length()<<" et="<<(*this)[i].endTime()<<"\n";
			}
			std::cerr<<"****** shiftOf("<<time<<") done\n";
		}
		*/

	}

	void setStartTime(float time) { 
		shiftOf(time-startTime());
	}
	
	void setEndTime(float time) { 
		shiftOf(time-endTime());
	}
	
	bool checkOrdered() const {
		for (const auto &pl : *this) 
		{
			if (! pl.checkOrdered())
				return false;
		}
		return true;
	}

	bool checkOrderedAndNonIntersecting() const {
		for (const auto &pl : *this)
		{
			if (! pl.checkOrderedAndNonIntersecting())
				return false;
		}
		return true;
	}
	
	void append(const Plan &p) {
		int sz1 = this->size();
		int sz2 = p.size();

		if (sz2 > sz1)
			this->resize(sz2);

		for (int i=0; i<sz2; ++i) {
			TrackPlan<PLANUNIT> &tp1 = (*this)[i];
			const TrackPlan<PLANUNIT> &tp2 = p[i];
			for (PLANUNIT *pu : tp2) 
			{
				tp1.push_back(pu);
			}
		}
	}
	
	void sort() {
		for (auto &pl : *this)
		{
			pl.sort();
		}
	}


	template <typename PLANUNIT2>
	bool intersects(const Plan<PLANUNIT2> &p2) {
		if (startTime() < p2.endTime() && p2.startTime() < endTime())
			return true;
		return false;
	}

  //check if tracks are really empty
  bool empty() const {
    if (QVector< TrackPlan<PLANUNIT> >::empty())
      return true;
    for (const auto &t : *this)
      if (! t.empty())
	return false;
    return true;
  }
  

};


#endif /* ! PLAN_HPP */

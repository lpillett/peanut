#include "AnimationSequence.hpp"

#include <cassert>
#include <iostream> //DEBUG

AnimationSequenceSegments::AnimationSequenceSegments()
	: m_segments(3)
{

}

AnimationSequenceSegments::AnimationSequenceSegments(const AnimationSegment &sAttack,
													 const AnimationSegment &sSet,
													 const AnimationSegment &sRelax)
	: m_segments(3)
{
	m_segments[0] = sAttack;
	m_segments[1] = sSet;
	m_segments[2] = sRelax;
}

void 
AnimationSequenceSegments::set(const AnimationSegment &sAttack,
							   const AnimationSegment &sSet,
							   const AnimationSegment &sRelax)
{
	m_segments.resize(3);
	m_segments[0] = sAttack;
	m_segments[1] = sSet;
	m_segments[2] = sRelax;
}

void
AnimationSequenceSegments::set(int segmentIndex, const AnimationSegment &s)
{
	assert(m_segments.size() == 3);
	if (segmentIndex >=0 && segmentIndex <3)
		m_segments[segmentIndex] = s;
	
}

void
AnimationSequenceSegments::setAttack(const AnimationSegment &sAttack)
{
	assert(m_segments.size() == 3);
	m_segments[0] = sAttack;
}

void
AnimationSequenceSegments::setSet(const AnimationSegment &sSet)
{
	assert(m_segments.size() == 3);
	m_segments[1] = sSet;
}

void
AnimationSequenceSegments::setRelax(const AnimationSegment &sRelax)
{
	assert(m_segments.size() == 3);	
	m_segments[2] = sRelax;
}


int
AnimationSequenceSegments::numberOfSegments() const 
{
	return m_segments.size();
}
	
int
AnimationSequenceSegments::sizeOfSegment(int segmentIndex) const
{
	if (segmentIndex >=0 && segmentIndex<m_segments.size())
	{
		return m_segments[segmentIndex].size();
	}
	return 0;
}

const QPixmap *
AnimationSequenceSegments::pixmap(int segmentIndex, int pixmapIndex) const
{
	if (segmentIndex >=0 && segmentIndex<m_segments.size())
	{
		if (pixmapIndex < m_segments[segmentIndex].size())
			return &m_segments[segmentIndex].pixmap(pixmapIndex);
	}
	return nullptr;
}

bool
AnimationSequenceSegments::required(int segmentIndex) const
{
	if (segmentIndex >=0 && segmentIndex<m_segments.size())
	{
		return m_segments[segmentIndex].required();
	}
	return false;
}

bool
AnimationSequenceSegments::loopable(int segmentIndex) const
{
	if (segmentIndex >=0 && segmentIndex<m_segments.size())
	{
		return m_segments[segmentIndex].loopable();
	}
	return false;
}







AnimationSequenceReversible::AnimationSequenceReversible() :
	m_segment()
{

}

AnimationSequenceReversible::AnimationSequenceReversible(const AnimationSegment &s) :
	m_segment(s)
{

}

void
AnimationSequenceReversible::set(const AnimationSegment &s)
{
	m_segment = s;
}


int
AnimationSequenceReversible::numberOfSegments() const
{
	return m_segment.size() == 0 ? 0 : 3;
}

int
AnimationSequenceReversible::sizeOfSegment(int segmentIndex) const
{
	if (m_segment.size() >= 0 && segmentIndex < numberOfSegments()) 
	{
		if (segmentIndex == 1)
			return 1;
		else
			return m_segment.size();
	}
	return 0;
}

const QPixmap *
AnimationSequenceReversible::pixmap(int segmentIndex, int pixmapIndex) const
{
	//std::cerr<<"getPixmap "<<segmentIndex<<" "<<pixmapIndex<<"\n";

	if (m_segment.size() >= 0 && segmentIndex < numberOfSegments()) 
	{
		if (segmentIndex == 0)
		{
			if (pixmapIndex >= 0 && pixmapIndex < m_segment.size())
				return &m_segment.pixmap(pixmapIndex);
		}
		else if (segmentIndex == 1)
		{
			return &m_segment.pixmap(m_segment.size()-1);
		}
		else if (segmentIndex == 2)
		{
			if (pixmapIndex >= 0 && pixmapIndex < m_segment.size())
				return &m_segment.pixmap(m_segment.size()-1-pixmapIndex);
			
		}
	}
	return nullptr;
}

bool
AnimationSequenceReversible::required(int segmentIndex) const
{
	if (m_segment.size() >= 0 && segmentIndex < numberOfSegments()) 
	{
		if (segmentIndex == 0 || segmentIndex == 2)
		{
			return true;
		}
		else 
		{
			assert(segmentIndex == 1);
			return false;
		}
	}
	return false;
}

bool
AnimationSequenceReversible::loopable(int segmentIndex) const
{
	if (m_segment.size() >= 0 && segmentIndex < numberOfSegments()) 
	{
		if (segmentIndex == 0 || segmentIndex == 2)
		{
			return false;
		}
		else 
		{
			assert(segmentIndex == 1);
			return true;
		}
	}
	return false;
}



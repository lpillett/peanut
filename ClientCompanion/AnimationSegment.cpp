#include "AnimationSegment.hpp"

#include <cassert>
#include <QDir>
#include <QDebug>
#include <QImageReader>

AnimationSegment::AnimationSegment()
	: m_pixmaps(), 
	  m_required(false), 
	  m_loopable(false)
{

}

AnimationSegment::AnimationSegment(const QString &dirName, int startIndex, int endIndex)
	: m_pixmaps(), 
	  m_required(false), 
	  m_loopable(false)
{
	loadDirectory(dirName, startIndex, endIndex);
}

AnimationSegment::AnimationSegment(const QStringList &imageFileNames, int startIndex, int endIndex)
	: m_pixmaps(), 
	  m_required(false), 
	  m_loopable(false)
{
	loadFiles(imageFileNames, startIndex, endIndex);
}

static
QStringList
buildImageFilterList(const QList<QByteArray> &list)
{
  QStringList l;
  const int sz = list.size();
  l.reserve(sz);
  for (int i=0; i<sz; ++i) {
    const QString s = "*."+list[i];
    l << s;
  }
  return l;
}


static
QStringList
getReadImageFilterList()
{
  QList<QByteArray> list = QImageReader::supportedImageFormats();

  return buildImageFilterList(list);
}

bool
AnimationSegment::loadDirectory(const QString &dirName, int startIndex, int endIndex)
{
	QDir in(dirName);
	QStringList filters = getReadImageFilterList();
    //filters << "*.png";
	QStringList list = in.entryList(filters, QDir::Files|QDir::Readable, QDir::Name); //QDir::NoFilter, QDir::Name);
	if (list.isEmpty()) {
		qDebug()<<"Warning: no image file found in directory: "<<dirName;
		return false;
	}
	else {

		QStringList listA;
		listA.reserve(list.count());
		for (const QString &file : list)
			listA.push_back(in.absoluteFilePath(file));

		loadFiles(listA, startIndex, endIndex);
	}

	return true;
}

void
AnimationSegment::loadFiles(const QStringList &imageFileNames, int startIndex, int endIndex)
{
	//B: We load all the images as pixmaps to be able to switch fast from one image to the next.
	//B: But, is it a good solution to store all the pixmaps in memory ? Will we explose the graphic card memory ?
	//B: Should we use a QPixmapCache ?	

	const int count = imageFileNames.count();

	if (endIndex == -1)
		endIndex = count-1;
	if (startIndex < 0 || startIndex > endIndex || endIndex >= count)
		return;

	const int num = endIndex - startIndex+1;

	m_pixmaps.reserve(num);
	for (int i=startIndex; i<=endIndex; ++i) 
	{
		assert(i >= 0 && i < imageFileNames.count());
		const QString &filename = imageFileNames.at(i);
		QImage img(filename);
		if (! img.isNull()) 
		{
			m_pixmaps.push_back(QPixmap::fromImage(img)); 
		}
		else 
		{
			qDebug()<<"Warning: unable to load image file: "<<filename;
		}
	}	

}

int
AnimationSegment::size() const
{
	return m_pixmaps.count();
}

float
AnimationSegment::length(float fps) const
{
	assert(fps != 0.f);

	return m_pixmaps.count() / fps;
}

const QPixmap &
AnimationSegment::pixmap(int i) const
{
	assert(i < m_pixmaps.count());

	return m_pixmaps.at(i);
}

//TODO: getPosition()

#ifndef PIXMAPFACEPLANNER_HPP
#define PIXMAPFACEPLANNER_HPP

#include "FacePlanner.hpp"

#include <random>
#include <memory>

#include <QGraphicsPixmapItem>
#include <QMap>
#include <QString>
#include <QVector>
#include <QGraphicsView>

#include "AnimationSequence.hpp"


class PixmapFacePlanner : public QGraphicsView, public FacePlanner
{
	Q_OBJECT

public:

	PixmapFacePlanner(const QString &faceFile, QWidget *parent = nullptr);

	explicit PixmapFacePlanner(QWidget *parent = nullptr);



	~PixmapFacePlanner();

	void loadAnimationDescriptions(const QString &faceFile);

	virtual FacePlan currentPlan() const override;

	bool planForBehavior(const FaceBehaviorType type, float intensity, FacePlan &fp);

	virtual bool planForBehavior(const FaceBehavior *b, FacePlan &fp) override;

	virtual void setCurrentPlan(const FacePlan &p) override;
	
	virtual void play(float currTime) override;
	
	//Merge plan @a p with current plan
	//Takes ownership of FacePlanUnits.
        void mergePlan(FacePlan &plan); 

	enum {MOUTH=0, EYES, BROWS};

										  
protected slots:
	void blink();

protected:

  virtual void resizeEvent(QResizeEvent *event);

	void init();
	void initMaps();
	void setIdle();

	bool isCurrentPlanEmpty() const;

	int addElement(const QString &elementId);
	int numberOfElements() const;

private:
  PixmapFacePlanner(const PixmapFacePlanner &o);
  PixmapFacePlanner &operator=(const PixmapFacePlanner &o);
  
  
protected:

	typedef QMap<QString, FaceBehaviorType> BehaviorNameToFaceMap;
	BehaviorNameToFaceMap m_nameToType;

	typedef QMap<QString, int> ElementToIndexMap;
	ElementToIndexMap m_elementToIndex;

	//typedef QVector<AnimationSequence *> AnimationVector;
	//AnimationVector m_animations;

	struct AnimationIntensity 
	{
		//REM: we can not use a std::unique_ptr inside a QVector (but with a std::vector we could)
		std::shared_ptr<AnimationSequence> anim;
		float intensityMin;
		float intensityMax;
		
		AnimationIntensity(AnimationSequence *a = nullptr,
						   float iMin = 0.0f, float iMax = 1.0f)
			: anim(a), intensityMin(iMin), intensityMax(iMax)
			{}

		bool hasIntensity(float intensity) const { return intensity>=intensityMin && intensity<intensityMax; }
	};
		
protected:

	float dist(float intensity, const AnimationIntensity &ai) const;

protected:


	struct BehaviorElements
	{
		QVector<AnimationIntensity> animations;
	};

	struct FaceElements
	{
		QVector<BehaviorElements> behaviorElements;
	};


	//QVector<FaceElements> m_behaviors;
	QMap<FaceBehaviorType, FaceElements> m_behaviors;

	/*
	typedef QVector<AnimationSequence *> AnimationVector;
	typedef QVector<AnimationVector> ElementToAnimationVector;
	
	typedef QVector<ElementToAnimationVector> LevelVector;

	typedef QMap<FaceBehaviorType, LevelVector > FaceBehaviorToElementsAnimationsMap;
	FaceBehaviorToElementsAnimationsMap m_nameToAnimations;
	*/


	FacePlan m_currentPlan;

	QGraphicsScene *m_scene;
	QVector<QGraphicsPixmapItem *> m_elementItems;

	float m_lastTime;
  
	std::mt19937 m_randomEngine;
};

#endif /* ! PIXMAPFACEPLANNER_HPP */

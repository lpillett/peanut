#include "ConfigurationWidget.hpp"

#include <cassert>
#include <QAction>
#include <QButtonGroup>
#include <QComboBox>
#include <QHostInfo>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QNetworkInterface>
#include <QPushButton>
#include <QScrollArea>


static
QString
getIPAddress()
{
	QString s;
	bool prev = false;
	foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
	  if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost)) {
	    if (prev) 
	      s+= " ;";
	    s += address.toString();
	    prev = true;
	  }
	}
	return s;
}
 


ConfigurationWidget::ConfigurationWidget(QWidget *parent)
	: QWidget(parent),
	  m_player(nullptr, QMediaPlayer::LowLatency)
{
	buildGUI();

	assert(m_faceBG);
}

void 
ConfigurationWidget::buildGUI()
{
	QLabel *ipLabel = new QLabel(tr("My IP address: "));
	m_myIPLabel = new QLabel(this);
	m_myIPLabel->setText(getIPAddress());


	m_hostLabel = new QLabel(tr("&Server name:"), this);
    m_portLabel = new QLabel(tr("&port:"), this);
	
	m_hostCombo = new QComboBox(this);
    m_hostCombo->setEditable(true);
	populateHosts();
	//m_hostCombo->setSizePolicy(QSizePolicy::Expanding, m_hostCombo->sizePolicy().verticalPolicy());
	m_hostCombo->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);


    m_portLineEdit = new QLineEdit(this);
    m_portLineEdit->setValidator(new QIntValidator(1, 65535, this));
	const int DEFAULT_PORT = 1234;
	m_portLineEdit->setText(QString::number(DEFAULT_PORT));

	m_hostLabel->setBuddy(m_hostCombo);
    m_portLabel->setBuddy(m_portLineEdit);

	m_connectButton = new QPushButton(tr("&Connect"), this);

	m_quitButton = new QPushButton(tr("&Quit"), this);


	m_faceBG = new QButtonGroup(this);

	m_faceSA = new QScrollArea(this);
	m_faceW = new QWidget(this);
	QHBoxLayout *hLayoutW = new QHBoxLayout;
	m_faceW->setLayout(hLayoutW);
	m_faceSA->setWidget(m_faceW);

	m_faceSA->setWidgetResizable(true);

	
	m_langCB = new QComboBox(this);
	connect(m_langCB, SIGNAL(currentIndexChanged(int)), this, SLOT(updateVoices()));

	m_voiceCB = new QComboBox(this);
	//m_voiceCB->setSizePolicy(QSizePolicy::Expanding, m_voiceCB->sizePolicy().verticalPolicy());
	m_voiceCB->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	m_voicePlayB = new QPushButton(this);

	QIcon playIcon = QIcon::fromTheme("media-playback-start", QIcon(":/icons/play.png"));
	m_voicePlayB->setIcon(playIcon);
	
	connect(m_connectButton, SIGNAL(clicked()), this, SLOT(onConnect()));

	connect(m_quitButton, SIGNAL(clicked()), this, SIGNAL(quit()));


	connect(m_voicePlayB, SIGNAL(clicked()), this, SLOT(playSound()));

	connect(&m_player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), 
			this, SLOT(mediaStatusChanged(QMediaPlayer::MediaStatus)));

	QHBoxLayout *hL0 = new QHBoxLayout;
	hL0->addWidget(ipLabel);
	hL0->addWidget(m_myIPLabel);
	hL0->addStretch();
	hL0->addWidget(m_quitButton);

	QHBoxLayout *hL1 = new QHBoxLayout;
	hL1->addWidget(m_hostLabel);
	hL1->addWidget(m_hostCombo);
	hL1->addWidget(m_portLabel);
	hL1->addWidget(m_portLineEdit);

	

	QHBoxLayout *hLayout = new QHBoxLayout;
	hLayout->addWidget(m_langCB);
	hLayout->addWidget(m_voiceCB);
	hLayout->addWidget(m_voicePlayB);

	QVBoxLayout *vLayout = new QVBoxLayout;
	vLayout->addLayout(hL0);
	vLayout->addLayout(hL1);
	vLayout->addWidget(m_faceSA);
	vLayout->addLayout(hLayout);
	vLayout->addWidget(m_connectButton);

	setLayout(vLayout);
}


void
ConfigurationWidget::populateHosts()
{
	assert(m_hostCombo);

    // find out name of this machine
    QString name = QHostInfo::localHostName();
    if (!name.isEmpty()) {
        m_hostCombo->addItem(name);
        QString domain = QHostInfo::localDomainName();
        if (!domain.isEmpty())
            m_hostCombo->addItem(name + QChar('.') + domain);
    }
    if (name != QString("localhost"))
        m_hostCombo->addItem(QString("localhost"));
    // find out IP addresses of this machine
    QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
    // add non-localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (!ipAddressesList.at(i).isLoopback())
            m_hostCombo->addItem(ipAddressesList.at(i).toString());
    }
    // add localhost addresses
    for (int i = 0; i < ipAddressesList.size(); ++i) {
        if (ipAddressesList.at(i).isLoopback())
            m_hostCombo->addItem(ipAddressesList.at(i).toString());
    }	
}

void
ConfigurationWidget::setConnectionEnabled(bool enable)
{
	assert(m_connectButton);
	m_connectButton->setEnabled(enable);
}

void
ConfigurationWidget::playSound()
{
	const QString voiceName = m_voiceCB->currentText();
	if (! voiceName.isEmpty()) {
		auto it = m_voiceNameToSoundFilename.constFind(voiceName);
		if (it != m_voiceNameToSoundFilename.constEnd()) {
			
			const QString filename = it.value();

			setEnabledConnection(false);

			m_player.setMedia(QUrl("qrc"+filename));
			m_player.setVolume(100);
			m_player.play();
		}
	}
}

void
ConfigurationWidget::mediaStatusChanged(QMediaPlayer::MediaStatus m)
{
	if (m == QMediaPlayer::EndOfMedia
		|| m == QMediaPlayer::InvalidMedia) {

		setEnabledConnection(true);
		
	}

}

void
ConfigurationWidget::setServerAddress(const QString &ip, int port)
{
	int count = m_hostCombo->count();
	m_hostCombo->addItem(ip);
	assert(count < m_hostCombo->count());
	m_hostCombo->setCurrentIndex(count);

	m_portLineEdit->setText(QString::number(port));
}

void
ConfigurationWidget::getServerAddress(QString &ip, int &port)
{
	ip = m_hostCombo->currentText();
	port = m_portLineEdit->text().toInt();
}

void
ConfigurationWidget::onConnect()
{
	assert(m_hostCombo);
	assert(m_portLineEdit);

	emit connectionRequest(m_hostCombo->currentText(), m_portLineEdit->text().toInt());
}

void
ConfigurationWidget::setEnabledConnection(bool enable)
{
	assert(m_connectButton);
	m_connectButton->setEnabled(enable);
}

void
ConfigurationWidget::addFace(const QString &name, const QString &imageFilename)
{
	m_faceNameToImageFilename.insert(name, imageFilename);
	
	QImage img(imageFilename);

	QPushButton *button = new QPushButton(this);
	button->setIcon(QIcon(QPixmap::fromImage(img)));
	button->setIconSize(img.size());
	button->setCheckable(true);

	button->setStyleSheet("\
                QPushButton {   \
                    color:white;    \
                }   \
                QPushButton:checked{					\
                    background-color: rgb(180, 80, 80);	\
                    border: none;						\
                }										\
                ");

	assert(m_faceBG);
	m_faceBG->addButton(button);

	assert(m_faceW->layout());
	m_faceW->layout()->addWidget(button);
	
	m_faceButtonToName.insert(button, name);
}

void
ConfigurationWidget::selectFirstFace()
{
	QList<QAbstractButton *> buttons = m_faceBG->buttons();
	if (! buttons.empty()) {
		buttons.at(0)->setChecked(true);
	}
}

void
ConfigurationWidget::addVoice(const QString &lang, const QString &name, const QString &soundFilename, const QString &flagFilename)
{
	m_voiceNameToSoundFilename.insert(name, soundFilename);
	m_voiceNameToFlagFilename.insert(name, flagFilename);

	auto it = m_langToVoices.find(lang);
	if (it != m_langToVoices.end()) {
		it.value().push_back(name);
	}
	else {
		m_langToVoices[lang].push_back(name);
		m_langCB->addItem(lang);
	}
}


void
ConfigurationWidget::updateVoices()
{
	QString lang = m_langCB->currentText();
	if (lang != m_currentLang) {
		selectLang_aux(lang);
	}
}

void
ConfigurationWidget::selectLang_aux(const QString &lang)
{
	qDebug()<<"ConfigurationWidget::selectLang_aux lang="<<lang; 

	auto it = m_langToVoices.find(lang);
	if (it != m_langToVoices.end()) {
		m_voiceCB->clear();		
		auto v = it.value();
		for (const QString &name : v) {
			m_voiceCB->addItem(name);

			const QString &flagFile = m_voiceNameToFlagFilename[name];
			QImage img(flagFile);
			if (! img.isNull()) {
				m_voiceCB->setItemIcon(m_voiceCB->count()-1, QPixmap::fromImage(img));
			}
		}
		m_langCB->setCurrentText(lang);
		m_currentLang = lang;
	}
}

void
ConfigurationWidget::selectLang(const QString &lang)
{
	selectLang_aux(lang);
}



void
ConfigurationWidget::getSelectedFace(QString &name, QString &imageFilename)
{
	assert(m_faceBG);

	QAbstractButton *b = m_faceBG->checkedButton();
	if (b != nullptr) {
		auto itB = m_faceButtonToName.constFind(static_cast<QPushButton*>(b));
		if (itB != m_faceButtonToName.constEnd()) {
			name = itB.value();
			auto it = m_faceNameToImageFilename.constFind(name);
			assert(it != m_faceNameToImageFilename.constEnd());
			imageFilename = it.value();
			return;
		}
	}

	name = "";
	imageFilename = "";
}

void
ConfigurationWidget::getSelectedVoice(QString &lang, QString &name, QString &soundFilename)
{
	lang = m_langCB->currentText();
	QString voiceName = m_voiceCB->currentText();
	if (! voiceName.isEmpty()) {
		auto it = m_voiceNameToSoundFilename.constFind(voiceName);
		if (it != m_voiceNameToSoundFilename.constEnd()) {
			const QString filename = it.value();

			name = voiceName;
			soundFilename = filename;
			return;
		}
	}
	
	lang = "";
	name = "";
	soundFilename = "";
}

#ifndef BEHAVIORBLOCK_HPP
#define BEHAVIORBLOCK_HPP

#include <vector>
#include "Behavior.hpp"
#include "Synchronization.hpp"

class QDataStream;


class BehaviorBlock
{
public:

	BehaviorBlock();

	//delete all its Behaviors.
	~BehaviorBlock();


	void clear();
	
	//TODO: C++11

	//takes ownership of Behavior
	bool addBehavior(Behavior *b); 
	size_t numberOfBehaviors() const;
	Behavior *behavior(size_t i);
	const Behavior *behavior(size_t i) const;


	//delete behavior at index @a i
	//All behaviors at index j, with j > i, are shift of minus one.
	void removeBehavior(size_t i);

	bool hasBehavior(const Behavior *b) const;

	//takes ownership of Synchronization
	bool addSynchronization(const Synchronization &s); 
	size_t numberOfSynchronizations() const;
	Synchronization &synchronization(size_t i);
	const Synchronization &synchronization(size_t i) const;
	void removeSynchronization(size_t i);
	bool hasSynchronization(const Synchronization &s) const;

	//return true if synchronizations may be respected
	bool checkSynchronizations() const;

	//delete behavior at index @a i
	//All behaviors at index j, with j > i, are shift of minus one.
	//Delete all synchronization relative to this behavior
	void removeBehaviorAndCorrespondingSyncs(size_t i);


	BehaviorBlock(const BehaviorBlock&) = delete;
	BehaviorBlock &operator=(const BehaviorBlock&) = delete;

protected:
	std::vector<Behavior *> m_behaviors;
	std::vector<Synchronization> m_synchros;
};

bool serialize(QDataStream &strm, const BehaviorBlock &bl);
bool deserialize(QDataStream &strm, BehaviorBlock &bl);

#endif /* ! BEHAVIORBLOCK_HPP */

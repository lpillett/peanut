#ifndef SPEECHPLANNER_HPP
#define SPEECHPLANNER_HPP

#include "Behavior.hpp"

#include "FacePlan.hpp"
#include "SpeechPlan.hpp"


class SpeechPlanner
{
public:
	virtual ~SpeechPlanner() {}

	virtual SpeechPlan currentPlan() const = 0;

	virtual bool planForBehavior(const SpeechBehavior *b, SpeechPlan &sp, FacePlan &fp) const = 0;

	virtual void setCurrentPlan(const SpeechPlan &p) = 0;

	virtual void play(float time) = 0; //TODO: type for time

};

#endif /* ! SPEECHPLANNER_HPP */

#ifndef BEHAVIOR_HPP
#define BEHAVIOR_HPP

#include "BehaviorType.hpp"

#include <QString>

class Behavior
{
public:
	typedef enum {Start, AttackPeak, Relax, End} SyncPoint;
	
	Behavior(BehaviorType type = Face,
			 float amount = 1.f) :
		m_type(type),
		m_amount(amount)
		{}
	
	virtual ~Behavior() {}

	BehaviorType type() const { return m_type; }
	float amount() const { return m_amount; }

protected:
	BehaviorType m_type;
	float m_amount; //value in [0; 1]
};

class FaceBehavior : public Behavior
{
public:
	FaceBehavior(FaceBehaviorType type = Happy,
				 float amount = 1.f) :
		Behavior(Face, amount),
		m_faceType(type)
		{}
	
	FaceBehaviorType faceType() const { return m_faceType; }

	void setFaceType(FaceBehaviorType type) { m_faceType = type; }

protected:
	FaceBehaviorType m_faceType;
};

class SpeechBehavior : public Behavior
{
public:
	SpeechBehavior(SpeechBehaviorType type = SoundFile,
				   float amount = 1.f) :
		Behavior(Speech, amount),
		m_speechType(type)
		{}
	
	SpeechBehaviorType speechType() const { return m_speechType; }

protected:
	SpeechBehaviorType m_speechType;

};

class SoundFileSpeechBehavior : public SpeechBehavior
{
public:
	explicit SoundFileSpeechBehavior(float amount = 1.f) :
	  SpeechBehavior(SoundFile, amount),
	  m_filename()
               {}
	
	SoundFileSpeechBehavior(const QString &filename,
							float amount = 1.f) :
		SpeechBehavior(SoundFile, amount),
			m_filename(filename)
		{}
	
	const QString &filename() const { return m_filename; }

	void setFilename(const QString &filename) { m_filename = filename; }

protected:
	QString m_filename;
};


#endif /* ! BEHAVIOR_HPP */


for i in *.phseg
do
	f="$(basename $i .phseg).wav"
	
	if [ ! -f "$f" ]
	then
		echo "rm -f $i"
		rm -f $i
	fi

done

for i in *.wav
do
	f="$(basename $i .wav).phseg"
	
	if [ ! -f "$f" ]
	then
		echo "rm -f $i"
		rm -f $i
	fi

done

#ifndef CONTROLPLANNER_HPP
#define CONTROLPLANNER_HPP

#include <QObject>

#include "PlanUnit.hpp"

#include "ControlPlan.hpp"


class ControlPlanner : public QObject
{
  Q_OBJECT

public:
	virtual ~ControlPlanner() {}

	virtual ControlPlan currentPlan() const = 0;

	virtual void setCurrentPlan(const ControlPlan &p) = 0;

	virtual void play(float time) = 0; //TODO: type for time

signals:
  
  void controlReached(ControlPlanUnitType t);

};

#endif /* ! CONTROLPLANNER_HPP */

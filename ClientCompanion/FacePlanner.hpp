#ifndef FACEPLANNER_HPP
#define FACEPLANNER_HPP

#include <QVector>
#include "Behavior.hpp"
#include "PlanUnit.hpp"

//typedef QVector<FacePlanUnit *> FaceTrackPlan;
//typedef QVector<FaceTrackPlan> FacePlan;

#include "FacePlan.hpp"




class FacePlanner 
{
public:
	virtual ~FacePlanner() {}

	virtual FacePlan currentPlan() const = 0;

	virtual bool planForBehavior(const FaceBehavior *b, FacePlan &fp) = 0;

	virtual void setCurrentPlan(const FacePlan &p) = 0;

	virtual void play(float time) = 0; //TODO: type for time

};

#endif /* ! FACEPLANNER_HPP */

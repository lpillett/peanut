
set src_dir="%CD%"
call C:\Qt\Qt5.6.0\5.6\mingw49_32\bin\qtenv2.bat
cd %src_dir%

set ANDROID_HOME=C:\Program Files (x86)\Android\android-sdk
set ANDROID_NDK_HOST=windows
set ANDROID_NDK_PLATFORM=android-19
set ANDROID_NDK_ROOT=C:\ProgramData\Microsoft\AndroidNDK\android-ndk-r10e
set ANDROID_NDK_TOOLCHAIN_PREFIX=arm-linux-androideabi
set ANDROID_NDK_TOOLCHAIN_VERSION=4.8
set ANDROID_NDK_TOOLS_PREFIX=arm-linux-androideabi
set ANDROID_SDK_ROOT=C:\Program Files (x86)\Android\android-sdk
set ANDROID_API_VERSION=android-19

set ANT_PATH=C:\BORIS\apache-ant-1.9.6-bin\apache-ant-1.9.6\bin\ant.bat
set JAVA_HOME=C:\Program Files (x86)\Java\jdk1.7.0_55
set PATH=%PATH%;%ANDROID_HOME%\tools;%JAVA_HOME%\bin;%ANT_PATH%


set PATH_QT_ANDROID=C:\Qt\Qt5.6.0\5.6\android_armv7
set build_dir=..\build-ClientCompanion-Android
mkdir %build_dir%
cd %build_dir%
%PATH_QT_ANDROID%\bin\qmake -r -spec android-g++ %src_dir%/ClientCompanion.pro
mingw32-make
mingw32-make install INSTALL_ROOT=android-build
%PATH_QT_ANDROID%\bin\androiddeployqt --output android-build --verbose --input android-libClientCompanion.so-deployment-settings.json --ant %ANT_PATH%

#include <QApplication>

#include <iostream>
#include "../ClientCompanion/PixmapFacePlanner.hpp"

int
main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	if (argc != 2) {
		qDebug()<<"Usage: "<<argv[0]<<" xmlAnimationFilename";
		exit(10);
	}

	QString xmlAnimationFilename = argv[1];

	PixmapFacePlanner w;
	w.loadAnimationDescriptions(xmlAnimationFilename);
	
	qDebug()<<"parsing of "<<xmlAnimationFilename<<" done.";

	w.show();
	
	return app.exec();
};

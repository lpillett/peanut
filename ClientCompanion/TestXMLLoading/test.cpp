#include <iostream>
#include <memory>
#include <QVector>

class AnimationSequence
{
public:
	
	virtual ~AnimationSequence() {}

	
};

class AnimationSequence1 : public AnimationSequence
{
public:
	AnimationSequence1()
		{
			std::cerr<<"AnimationSequence1 "<<this<<"\n";
		}

	~AnimationSequence1()
		{
			std::cerr<<"~AnimationSequence1 "<<this<<"\n";
		}
	

};

class AnimationSequence2 : public AnimationSequence
{
public:
	AnimationSequence2()
		{
			std::cerr<<"AnimationSequence2 "<<this<<"\n";
		}

	~AnimationSequence2()
		{
			std::cerr<<"~AnimationSequence2 "<<this<<"\n";
		}
	
};

	struct AnimationIntensity 
	{
		std::unique_ptr<AnimationSequence> anim;
		float intensityMin;
		float intensityMax;
		
		AnimationIntensity(AnimationSequence *a = nullptr,
						   float iMin = 0.0f, float iMax = 1.0f)
			: anim(a), intensityMin(iMin), intensityMax(iMax)
			{}

		bool hasIntensity(float intensity) const { return intensity>=intensityMin && intensity<intensityMax; }
	};



int
main()
{
	
	QVector<AnimationIntensity> animations;

	{
		AnimationSequence *s = new AnimationSequence1;
		animations.push_back(AnimationIntensity(s));
	}
	{
		AnimationSequence *s = new AnimationSequence2;
		animations.push_back(AnimationIntensity(s));
	}

	for (int i=0; i<30; ++i) {
		AnimationSequence *s = nullptr;
		if ((i&1) == 0)
			s = new AnimationSequence1;
		else 
			s = new AnimationSequence2;

		animations.push_back(AnimationIntensity(s));
	}
	

	return 0;
}

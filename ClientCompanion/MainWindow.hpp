#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include <QtWebSockets/QWebSocket>

#include "BehaviorType.hpp"

class Widget;
class ConfigurationWidget;



class MainWindow : public QWidget //QMainWindow
{
	Q_OBJECT

public:
	MainWindow();


signals:
	void closed();

protected slots:

	void onConnect(const QString &ip, int port);
    void onConnected();
    void onTextMessageReceived(const QString &message);
    void onBinaryMessageReceived(const QByteArray &message);
	void behaviorEnd();
	void connectionError(QAbstractSocket::SocketError error);

	void onDisconnected();

	//void onStateChanged(QAbstractSocket::SocketState state);

protected:
	void buildGUI();
	void populateFaces();
	void populateVoices();

	QString getLang() const;
	QString getVoicePath() const;
	QString getMappingFilename() const;
	QString getFaceFile() const;

	void changeGUI();
	void changeAgent2Connection();

	void sendAnswer(bool ok);

#ifndef Q_OS_ANDROID
	void mousePressEvent(QMouseEvent *evt);
	void mouseMoveEvent(QMouseEvent *evt);
#endif //Q_OS_ANDROID

	void saveSettings();
	void restoreSettings();

private:

	ConfigurationWidget *m_configurationWidget;

	Widget *m_viewWidget;

    QWebSocket m_webSocket;
    QUrl m_url;
    bool m_debug;

	QString m_ip;
	int m_port;

#ifndef Q_OS_ANDROID
	QPoint m_oldPos;
#endif //Q_OS_ANDROID

	QString m_voicePath;
	QString m_faceFile;
};

#endif /* ! MAINWINDOW_HPP */

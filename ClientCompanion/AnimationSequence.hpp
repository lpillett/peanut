#ifndef ANIMATIONSEQUENCE_HPP
#define ANIMATIONSEQUENCE_HPP

#include "AnimationSegment.hpp"


//TODO: we should probably return also a QPoint for each QPixmap 
// It would allow to have QPixmaps of different sizes in animations

class AnimationSequence
{
public:

  virtual ~AnimationSequence() {}
  
  	virtual int numberOfSegments() const = 0;
	
	virtual int sizeOfSegment(int segmentIndex) const = 0;

	virtual const QPixmap *pixmap(int segmentIndex, int pixmapIndex) const = 0;

	//TODO: getPosition() !!!

	virtual bool required(int segmentIndex) const = 0;
	virtual bool loopable(int segmentIndex) const = 0;

protected:

};

class AnimationSequenceSegments : public AnimationSequence
{
public:

	enum {Attack=0, Set, Relax};

	AnimationSequenceSegments();

	AnimationSequenceSegments(const AnimationSegment &sAttack,
							  const AnimationSegment &sSet,
							  const AnimationSegment &sRelax);
	
	void set(const AnimationSegment &sAttack,
			 const AnimationSegment &sSet,
			 const AnimationSegment &sRelax);

	void set(int segmentIndex, const AnimationSegment &s);
	void setAttack(const AnimationSegment &sAttack);
	void setSet(const AnimationSegment &sSet);
	void setRelax(const AnimationSegment &sRelax);

	virtual int numberOfSegments() const override;
	
	virtual int sizeOfSegment(int segmentIndex) const override;

	virtual const QPixmap *pixmap(int segmentIndex, int pixmapIndex) const override;

	virtual bool required(int segmentIndex) const override;
	virtual bool loopable(int segmentIndex) const override;


protected:
	QVector<AnimationSegment> m_segments;
};


class AnimationSequenceReversible : public AnimationSequence
{
public:

	AnimationSequenceReversible();

	explicit AnimationSequenceReversible(const AnimationSegment &s);

	void set(const AnimationSegment &s);

	virtual int numberOfSegments() const override;
	
	virtual int sizeOfSegment(int segmentIndex) const override;

	virtual const QPixmap *pixmap(int segmentIndex, int pixmapIndex) const override;

	virtual bool required(int segmentIndex) const override;
	virtual bool loopable(int segmentIndex) const override;

protected:
	AnimationSegment m_segment;
};

#endif /* ! ANIMATIONSEQUENCE_HPP */

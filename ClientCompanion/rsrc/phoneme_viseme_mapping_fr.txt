﻿# This file contains a mapping between the french phoneme set (LIUM frenchWords62k)  to the viseme set used for animation.
# Each line starting by # is considered as a comment.
# On each other line we have a string representing the phoneme followed by an integer indicating the viseme

# Here we use the visemes from this blog
# http://www.bonbref.com/blog/004_visemes_phonemes_french_francais_synchronisation_labiale_lip_sync_vert_de_terre_projet_animation_documentaire_enfants_deforestation.jpg


aa 3
ai 3 
an 5
au 4
bb 8
ch 6
dd 5
ee 4 
ei 6
eu 4
ff 8
gg 1
gn 3
ii 6
in 6
jj 6
kk 8
ll 5
mm 8
nn 5
oe 4
on 4
oo 4
ou 4
pp 8
rr 6
ss 6
tt 5
un 2
uu 4
uy 1
vv 8
ww 4
yy 6
zz 6



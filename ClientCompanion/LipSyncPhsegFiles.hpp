#ifndef LIPSYNCPHSEGFILES_HPP
#define LIPSYNCPHSEGFILES_HPP

#include "LipSync.hpp"

#include <QHash>



class LipSyncPhsegFiles : public LipSync
{
public:
	LipSyncPhsegFiles();

	explicit LipSyncPhsegFiles(const QString &filename);

	void loadPhonemeToVisemeMapping(const QString &filename);

	//Get visemes from audio file
	virtual TrackPlan<FacePlanUnit> sync(const QString &audioFilename);

protected:
	QHash<QString, int> m_phonemeToViseme;	

};


#endif /* ! LIPSYNCPHSEGFILES_HPP */

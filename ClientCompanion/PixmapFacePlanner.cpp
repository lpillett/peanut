
#include "PixmapFacePlanner.hpp"

#include <cassert>
#include <random>
#include <QDebug>
#include <QFile>
#include <QTimer>
#include <QXmlStreamReader>
#include <QGraphicsScene>

#ifdef TIME_LOAD
#include <sys/time.h> //gettimeofday
#include <QGraphicsTextItem>
#endif //TIME_LOAD


//time between two blinks will be in interval ]BLINK_INTERVAL-BLINK_RANDOM; BLINK_INTERVAL+BLINK_RANDOM[
static const int BLINK_INTERVAL = 9300;
static const int BLINK_RANDOM = 1200; 


PixmapFacePlanner::PixmapFacePlanner(const QString &faceFile, QWidget *parent) :
	QGraphicsView(parent), 
	m_scene(nullptr),
	m_randomEngine(std::random_device()())
{
	init();
	
#ifdef TIME_LOAD
	struct timeval t0, t1;
	gettimeofday(&t0, 0);
#endif //TIME_LOAD

	loadAnimationDescriptions(faceFile);

#ifdef TIME_LOAD
	gettimeofday(&t1, 0);
	double t = (t1.tv_sec-t0.tv_sec)*1000.0+(t1.tv_usec-t0.tv_usec)/1000.0;
	QString tS = QString::number(t) + "ms";
	QGraphicsTextItem *textItem = new QGraphicsTextItem(tS);
	textItem->setScale(4);
	textItem->setZValue(10);
	textItem->setDefaultTextColor(QColor(255, 0, 0, 255));
	m_scene->addItem(textItem);
#endif //TIME_LOAD

	qDebug()<<"sceneRect="<<this->scene()->sceneRect()<<"\n";
	QRectF r = scene()->sceneRect();
	std::cerr<<"sceneRect x="<<r.x()<<" y="<<r.y()<<" w="<<r.width()<<" h="<<r.height()<<"\n";
	
	//QGraphicsView::fitInView(this->scene()->itemsBoundingRect(), Qt::KeepAspectRatio );
	//QGraphicsView::fitInView(this->scene()->sceneRect(), Qt::KeepAspectRatio );
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

PixmapFacePlanner::PixmapFacePlanner(QWidget *parent) :
	QGraphicsView(parent), 
	m_scene(nullptr),
	m_randomEngine(std::random_device()())
{
	init();
}



PixmapFacePlanner::~PixmapFacePlanner()
{

}

void
PixmapFacePlanner::resizeEvent(QResizeEvent */*event*/)
{
  QGraphicsView::fitInView(this->scene()->itemsBoundingRect(), Qt::KeepAspectRatio );
  //QGraphicsView::fitInView(this->scene()->sceneRect(), Qt::KeepAspectRatio );

}



void
PixmapFacePlanner::init()
{
	m_scene = new QGraphicsScene;
	QGraphicsView::setScene(m_scene);

	initMaps();

	//TODO: depends on anims.xml ????
	//TODO: go fullscreen on Android/RaspPi ???
	int width = 1000;
	int height = 720;
	setMinimumSize(width, height);

	QPoint mouthPos = QPoint(0, height/4);
	QPoint eyesPos = QPoint(0, -height/4);
	QPoint browsPos = QPoint(0, -height/4-height/8);
	m_elementItems[MOUTH]->setPos(mouthPos);
	m_elementItems[EYES]->setPos(eyesPos);
	m_elementItems[BROWS]->setPos(browsPos);

}

void 
PixmapFacePlanner::initMaps()
{
	//init m_nameToType
	m_nameToType[QStringLiteral("IDLE")] = Idle;
	m_nameToType[QStringLiteral("NEUTRE_02")] = Neutre_02;
	m_nameToType[QStringLiteral("NEUTRE_03")] = Neutre_03;
	m_nameToType[QStringLiteral("NEUTRE_04")] = Neutre_04;
    m_nameToType[QStringLiteral("BLINK")] = Blink;
	m_nameToType[QStringLiteral("HAPPY")] = Happy;
    m_nameToType[QStringLiteral("TRUST")] = Trust;
    m_nameToType[QStringLiteral("SURPRISED")] = Surprised;
	m_nameToType[QStringLiteral("SAD")] = Sad;
    m_nameToType[QStringLiteral("ANGRY")] = Angry;
    m_nameToType[QStringLiteral("BORED")] = Bored;
	m_nameToType[QStringLiteral("VISEME_00")] = Viseme_00;
	m_nameToType[QStringLiteral("VISEME_01")] = Viseme_01;
	m_nameToType[QStringLiteral("VISEME_02")] = Viseme_02;
	m_nameToType[QStringLiteral("VISEME_03")] = Viseme_03;
	m_nameToType[QStringLiteral("VISEME_04")] = Viseme_04;
	m_nameToType[QStringLiteral("VISEME_05")] = Viseme_05;
	m_nameToType[QStringLiteral("VISEME_06")] = Viseme_06;
	m_nameToType[QStringLiteral("VISEME_07")] = Viseme_07;
	m_nameToType[QStringLiteral("VISEME_08")] = Viseme_08;
	m_nameToType[QStringLiteral("VISEME_09")] = Viseme_09;
	m_nameToType[QStringLiteral("VISEME_10")] = Viseme_10;
	m_nameToType[QStringLiteral("VISEME_11")] = Viseme_11;
	m_nameToType[QStringLiteral("VISEME_12")] = Viseme_12;
	m_nameToType[QStringLiteral("VISEME_13")] = Viseme_13;
	m_nameToType[QStringLiteral("VISEME_14")] = Viseme_14;
	m_nameToType[QStringLiteral("VISEME_15")] = Viseme_15;
	m_nameToType[QStringLiteral("VISEME_16")] = Viseme_16;
	m_nameToType[QStringLiteral("VISEME_17")] = Viseme_17;
	m_nameToType[QStringLiteral("VISEME_18")] = Viseme_18;
	m_nameToType[QStringLiteral("VISEME_19")] = Viseme_19;
	m_nameToType[QStringLiteral("VISEME_20")] = Viseme_20;
	m_nameToType[QStringLiteral("VISEME_21")] = Viseme_21;
	m_nameToType[QStringLiteral("VISEME_22")] = Viseme_22;
	m_nameToType[QStringLiteral("VISEME_23")] = Viseme_23;
	m_nameToType[QStringLiteral("VISEME_24")] = Viseme_24;
	
	//Add some default elements
	addElement("MOUTH");
	addElement("EYES");
	addElement("BROWS");

	//m_behaviors.resize(m_nameToType.size());

}


int
PixmapFacePlanner::numberOfElements() const
{
	assert(m_elementToIndex.size() == m_elementItems.size());
	
	return m_elementToIndex.size();
}

/*
  Add element with id @a elementId if not present.
  Return its index.

  //Warning: we do toUpper() on elementId
  // That means that "Mouth" and "MOUTH" are the same element

 */
int 
PixmapFacePlanner::addElement(const QString &elementId)
{
	int elementIndex = -1;

	assert(m_elementToIndex.size() == m_elementItems.size());

	const QString elementIdUp = elementId.toUpper();

	auto itE = m_elementToIndex.find(elementIdUp);
	if (itE != m_elementToIndex.end())
	{
		elementIndex = itE.value();
	}
	else 
	{
		elementIndex = numberOfElements();
		m_elementToIndex[elementIdUp] = elementIndex;
		m_elementItems.resize(elementIndex+1);
		m_elementItems[elementIndex] = new QGraphicsPixmapItem;

		m_elementItems[elementIndex]->setTransformationMode(Qt::SmoothTransformation);
		
		m_scene->addItem(m_elementItems[elementIndex]);
		
		qDebug()<<" new QGraphicsPixmapItem : element="<<elementIdUp<<" ind="<<elementIndex;
		
		//REM:TODO? we had element even if animation is invalid
	}

	assert(m_elementToIndex.size() == m_elementItems.size());

	return elementIndex;
}

void
PixmapFacePlanner::loadAnimationDescriptions(const QString &faceFile)
{
	//qDebug()<<"PixmapFacePlanner::loadAnimationDescriptions "<<faceFile;

	//read animations files
	QFile f(faceFile);
	if (f.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QXmlStreamReader xml;
		xml.setDevice(&f);

		FaceBehaviorType type = Idle;
		AnimationSequence *seq = nullptr;
		bool added = true;
		float minIntensity = 0.0f;
		float maxIntensity = 1.0f;

		enum {NONE, SEGMENT, REVERSIBLE};
		int animationType = NONE;
		//enum {Attack=0, Set, Relax};
		//int segmentType = AnimationSequenceSegments::Set;
		
		QMap<QString, int> segmentTypeMap;
		segmentTypeMap[QStringLiteral("ATTACK")] = AnimationSequenceSegments::Attack;
		segmentTypeMap[QStringLiteral("SET")] = AnimationSequenceSegments::Set;
		segmentTypeMap[QStringLiteral("RELAX")] = AnimationSequenceSegments::Relax;

		int elementIndex = -1;

		while (! xml.atEnd()) 
		{
			if (xml.readNextStartElement())
			{
				//qDebug()<<"start elt "<<xml.name();

				if (xml.name() == "Element") 
				{
					if (xml.attributes().hasAttribute(QStringLiteral("id"))) 
					{
						const QString id = xml.attributes().value(QStringLiteral("id")).toString();

						const int elementIndex = addElement(id); //add element if not present
						assert(elementIndex>=0 && elementIndex<m_elementItems.size());
						assert(m_elementItems[elementIndex]);

						if (xml.attributes().hasAttribute(QStringLiteral("x"))) 
						{
							bool ok;
							int x = xml.attributes().value(QStringLiteral("x")).toString().toInt(&ok);
							if (ok) {
								m_elementItems[elementIndex]->setX(x);
							}
						}
						if (xml.attributes().hasAttribute(QStringLiteral("y"))) 
						{
							bool ok;
							int y = xml.attributes().value(QStringLiteral("y")).toString().toInt(&ok);
							if (ok) {
								m_elementItems[elementIndex]->setY(y);
							}
						}
						if (xml.attributes().hasAttribute(QStringLiteral("z"))) 
						{
							bool ok;
							int z = xml.attributes().value(QStringLiteral("z")).toString().toInt(&ok);
							if (ok) {
								m_elementItems[elementIndex]->setZValue(z);
							}
						}
					}
				}

				else if (xml.name() == "Animation") 
				{
					added = false;
					bool toSkip = false;
					minIntensity = 0.0f;
					maxIntensity = 1.0f;
					animationType = NONE;

					if (xml.attributes().hasAttribute(QStringLiteral("behavior"))) 
					{
						const QString behaviorStr = xml.attributes().value(QStringLiteral("behavior")).toString();
						//qDebug()<<"Animation "<<behaviorStr;
						auto itT = m_nameToType.find(behaviorStr.toUpper());
						if (itT != m_nameToType.end()) 
						{
							type = itT.value();
							
							if (xml.attributes().hasAttribute(QStringLiteral("element"))) 
							{
							  //elementIndex = -1;
								const QString element = xml.attributes().value(QStringLiteral("element")).toString();

								elementIndex = addElement(element);

								//qDebug()<<" element="<<element<<" ind="<<elementIndex;
							}
							else {
								qDebug()<<"Warning: element type not specified, line "<<xml.lineNumber();
								toSkip = true;
							}

							minIntensity = 0.0f;
							maxIntensity = 1.0f;
							if (xml.attributes().hasAttribute(QStringLiteral("intensityLow")))
							{
								bool ok;
								minIntensity = xml.attributes().value(QStringLiteral("intensityLow")).toString().toFloat(&ok);
								if (! ok) 
								{
									qDebug()<<"Warning: invalid low intensity, line "<<xml.lineNumber();
									toSkip = true;
								}
							}
							if (xml.attributes().hasAttribute(QStringLiteral("intensityHigh")))
							{
								bool ok;
								maxIntensity = xml.attributes().value(QStringLiteral("intensityHigh")).toString().toFloat(&ok);
								if (! ok) 
								{
									qDebug()<<"Warning: invalid high intensity, line "<<xml.lineNumber();
									toSkip = true;
								}
							}
							if (minIntensity > maxIntensity) {
								qDebug()<<"Warning: intensityHigh ("<<maxIntensity<<") superior to intensityLow ("<<minIntensity<<")";
								toSkip = true;
							}


						}
						else 
						{
							qDebug()<<"Warning: unknown behavior type: "<<behaviorStr<<", line "<<xml.lineNumber();
							toSkip = true;
						}
					}
					else {
						qDebug()<<"Warning: behavior type not specified, line "<<xml.lineNumber();
						toSkip = true;
					}

					if (! toSkip)
					{
						

					}
					else
					{
						xml.skipCurrentElement();
					}
				}

				else if (xml.name() == QStringLiteral("AnimationSegment"))
				{
					bool toSkip = false;

					int startIndex = 0;
					int endIndex = -1;

					//qDebug()<<"AnimationSegment ";
					QString dir;
					const QString dirStr = "dir";
					if (xml.attributes().hasAttribute(dirStr)) 
					{
						dir = xml.attributes().value(QStringLiteral("dir")).toString();
						//We does not check if directory is valid, has images and has enough images...
					}
					else 
					{
						qDebug()<<"Warning: AnimationSegment no dir attribute specified, line "<<xml.lineNumber();
						toSkip = true;
					}

					bool required = true;
					const QString requiredStr = QStringLiteral("required");
					if (!toSkip && xml.attributes().hasAttribute(requiredStr)) 
					{
						if (xml.attributes().value(requiredStr).toString().toUpper() == QStringLiteral("FALSE"))
							required = false;
					}
					bool loopable = false;
					const QString loopableStr = QStringLiteral("loopable");
					if (!toSkip && xml.attributes().hasAttribute(loopableStr)) 
					{
						if (xml.attributes().value(loopableStr).toString().toUpper() == QStringLiteral("TRUE"))
							loopable = true;
					}
					const QString startIndexStr = QStringLiteral("startIndex");
					if (!toSkip && xml.attributes().hasAttribute(startIndexStr)) 
					{
						bool ok;
						int si = xml.attributes().value(startIndexStr).toString().toInt(&ok);
						if (ok)
							startIndex = si;
						else
							qDebug()<<"Warning: inavlid value for "<<startIndexStr<<" attribute of AnimationSegment";
					}
					const QString endIndexStr = "endIndex";
					if (!toSkip && xml.attributes().hasAttribute(endIndexStr)) 
					{
						bool ok;
						int si = xml.attributes().value(endIndexStr).toString().toInt(&ok);
						if (ok)
							endIndex = si;
						else
							qDebug()<<"Warning: inavlid value for "<<endIndexStr<<" attribute of AnimationSegment";
					}
					


					if (!toSkip && xml.attributes().hasAttribute(QStringLiteral("type"))) 
					{
						QString segmentType = xml.attributes().value(QStringLiteral("type")).toString().toUpper();
						int segmentIndex = segmentTypeMap.value(segmentType, AnimationSequenceSegments::Relax+1);

						//qDebug()<<"AnimationSegment segmentType="<<segmentType<<" index="<<segmentIndex;


						if (segmentIndex >= 0 && segmentIndex < 3)
						{
							if (animationType == REVERSIBLE) 
							{
								qDebug()<<"Warning: AnimationSegment of type ATTACK but a previous segment was of type REVERSIBLE, line "<<xml.lineNumber();
								qDebug()<<"         If you have one REVERSIBLE segment in an animation, you can not have an other AnimationSegment.";
								
								toSkip = true;
							}
							else 
							{
								if (animationType == NONE) 
								{
									added = false;
									seq = new AnimationSequenceSegments();
									//qDebug()<<" new AnimationSequenceSegments";
								}
								animationType = SEGMENT;
								AnimationSequenceSegments *s = static_cast<AnimationSequenceSegments *>(seq);


								//TODO: if segment is specified two times, but no image was loaded for the first 
								//  no warning will be emited 

								assert(s);
								if (s->sizeOfSegment(segmentIndex) != 0) 
								{
									qDebug()<<"Warning: AnimationSegment of index "<<segmentIndex<<" is specified a second time, line "<<xml.lineNumber(); //TODO: better message : write segmentType instead of segmentIndex
									toSkip = true;
								}
								else 
								{
									AnimationSegment segm(dir, startIndex, endIndex);
									segm.setLoopable(loopable);
									segm.setRequired(required);
									s->set(segmentIndex, segm);
								}
							}

							if (! added) 
							{
								if (elementIndex == -1) {
									qDebug()<<"Warning: AnimationSegment is specified outside Animation block\n";
									toSkip = true;
								}
								else {

									assert(animationType != NONE);
									assert(elementIndex != -1);
									assert(seq != nullptr);
									assert(0.f <= minIntensity && minIntensity <= maxIntensity && maxIntensity <= 1.0f);
									//if type is not present in m_behaviors, it will be added
									if (elementIndex >= m_behaviors[type].behaviorElements.size())
										m_behaviors[type].behaviorElements.resize(elementIndex+1);
									assert(elementIndex < m_behaviors[type].behaviorElements.size());
									m_behaviors[type].behaviorElements[elementIndex].animations.push_back(AnimationIntensity(seq, minIntensity, maxIntensity));
									//qDebug()<<"ADD type="<<type<<" eltInd="<<elementIndex<<" intensity min="<<minIntensity<<" max="<<maxIntensity<<"\n";
									
									added = true;
								}
							}

						}
						else 
						{
							if (animationType == REVERSIBLE) 
							{
								qDebug()<<"Warning: AnimationSegment of type REVERSIBLE specified a second time, line "<<xml.lineNumber();
								qDebug()<<"         You can only have one REVERSIBLE segment in one animation.";
								toSkip = true;
							}
							else if (animationType != NONE)
							{

								qDebug()<<"Warning: AnimationSegment was previously set to a different type than REVERSIBLE, line "<<xml.lineNumber();
								qDebug()<<"         If you have one REVERSIBLE segment in an animation, you can not have an other AnimationSegment.";
								toSkip = true;
							}
							else 
							{
								assert(animationType == NONE);
								animationType = REVERSIBLE;
								added = false;
								AnimationSequenceReversible *s = new AnimationSequenceReversible;
								//qDebug()<<" new AnimationSequenceReversible";
								seq = nullptr;
								s->set(AnimationSegment(dir, startIndex, endIndex));
									
								if (! added) 
								{
									assert(animationType != NONE);
									assert(elementIndex != -1);
									assert(s != nullptr);
									assert(0.f <= minIntensity && minIntensity <= maxIntensity && maxIntensity <= 1.0f);
									//if type is not present in m_behaviors, it will be added
									if (elementIndex >= m_behaviors[type].behaviorElements.size())
										m_behaviors[type].behaviorElements.resize(elementIndex+1);
									assert(elementIndex < m_behaviors[type].behaviorElements.size());
									m_behaviors[type].behaviorElements[elementIndex].animations.push_back(AnimationIntensity(s, minIntensity, maxIntensity));
									//qDebug()<<"ADD type="<<type<<" elemt="<<elementIndex<<" intensity min="<<minIntensity<<" max="<<maxIntensity<<"\n";

									added = true;
								}

							}
						}


					}
					else 
					{
						qDebug()<<"Warning: AnimationSegment does not have a type, line "<<xml.lineNumber();
						toSkip = true;

					}


				}
			}
		}
	}
	else {
		qDebug()<<"ERROR: unable to open "<<faceFile;
		return;
	}

	setIdle();

	if (m_behaviors.find(Blink) != m_behaviors.end()) {
		QTimer::singleShot(BLINK_INTERVAL, this, SLOT(blink()));
	}
	else {
		qDebug()<<"Warning: no blink animation found.";
	}
}


FacePlan PixmapFacePlanner::currentPlan() const
{
	return m_currentPlan;
}

//compute distance between a given intensity and an AnimationIntensity
float PixmapFacePlanner::dist(float intensity, const AnimationIntensity &ai) const
{
	float d1 = ai.intensityMin-intensity;
	float d2 = ai.intensityMax-intensity;
	return std::min(d1*d1, d2*d2);
}

bool PixmapFacePlanner::planForBehavior(const FaceBehaviorType type, float intensity, FacePlan &plan)
{
	bool animationFound = false;

	if (m_behaviors.find(type) != m_behaviors.end()) {

		const FaceElements &fe = m_behaviors[type];
		for (int element=0; element<fe.behaviorElements.size(); ++element) {
		
			//- first get index of Animation for the given intensity
			const auto &animations = fe.behaviorElements[element].animations;
			int nbAnims = animations.size();
		
			qDebug()<<"planForBehavior type="<<type<<" intensity="<<intensity<<" element="<<element<<"/"<<fe.behaviorElements.size()<<" nbAnims="<<nbAnims;

			int index = -1;
			if (nbAnims == 1) 
			{
				//there is no choice, we take the only available animation (whatever the intensity)

				index = 0;
			}
			else if (nbAnims > 1)
			{
				//there are several choices, 
				//we take one randomly inside animations for the desired intensity
				//or, if none is available, we take the closest available.

				QVector<int> animationIndices;
				for (int i=0; i<nbAnims; ++i) 
				{
					std::cerr<<"   anim "<<i<<" intensities ["<<animations[i].intensityMin<<"; "<<animations[i].intensityMax<<"[\n";

					if (animations[i].hasIntensity(intensity)) {
						animationIndices.push_back(i);
					}
				}
				const int nbIndices = animationIndices.size();

				std::cerr<<"nbIndices="<<nbIndices<<"\n";

				if (nbIndices == 1) {
					index = animationIndices.back();
				}
				else if (nbIndices > 1) 
				{
					std::random_device rd;
					std::mt19937 gen(rd());
					std::uniform_int_distribution<int> dis(0, nbIndices-1);
					index = dis(gen);
				}
				else 
				{
					//no animation available specifically for this intensity
					// (we take the closest one)
				
					index = 0;
					float minDist = dist(intensity, animations[index]);
					for (int i=1; i<nbAnims; ++i)
					{
						float d = dist(intensity, animations[i]);
						if (d < minDist) 
						{
							index = i;
							minDist = d;
						}
					}
					qDebug()<<" closest animation: "<<index<<" intensities ["<<animations[index].intensityMin<<"; "<<animations[index].intensityMax<<"[ for intensity="<<intensity<<" : dist="<<minDist<<"\n";
				}

			}
		
			qDebug()<<"planForBehavior()  type="<<type<<" element="<<element<<" index="<<index;


			if (index != -1) 
			{
				assert(! fe.behaviorElements[element].animations.empty());

				plan.resize(element+1);
			
				assert(index>=0 && index<fe.behaviorElements[element].animations.size());

				const AnimationIntensity &ai = fe.behaviorElements[element].animations[index]; 
				assert(ai.anim != nullptr);
				const float intensity = 1.0f;
				float startTime = 0.f;
			
				int nbSegments = ai.anim->numberOfSegments();

				qDebug()<<"palnForBehavior()  type="<<type<<" element="<<element<<" index="<<index<<" : nbSegments="<<nbSegments;

				for (int segment=0; segment<nbSegments; ++segment) {
					int size = ai.anim->sizeOfSegment(segment);
					const float FPS = 30.f; //UGLY !!! //TODO: store it !?!
					float length = size * 1000.f / FPS; //in ms
					FacePlanUnit *a= new FacePlanUnit(type, intensity, element, index, segment, startTime, length);
					const bool r = ai.anim->required(segment);
					const bool l = ai.anim->loopable(segment);
					a->setRequired(r);
					a->setLoopable(l);
				
					//qDebug()<<"planForBehavior() add planUnit  type="<<type<<" elt="<<element<<" seg="<<segment<<" sz="<<size<<"frames st="<<startTime<<" len="<<length<<"ms  i="<<intensity<<" index="<<index<<" r="<<r<<" l="<<l;

					plan[element].push_back(a);

					//qDebug()<<"plan.empty()="<<plan.empty()<<"\n"; 

					startTime += length;
				}

				if (! plan.empty()) {
					animationFound = true;
				}
			
				//qDebug()<<"  plan.size()="<<plan.size();

			}

		}
	}

	if (! animationFound)
	  {
		  bool found = false;
		  auto it = m_nameToType.begin();
		  for (it = m_nameToType.begin(); it != m_nameToType.end(); ++it) {
			  if (it.value() == type) {
				  found = true;
				  qDebug()<<"No animation for behavior type="<<it.key();
				  break;
			  }
		  }
		  if (! found) {
			  qDebug()<<"No animation for behavior type="<<type;
		  }
	  }

	
/*
	{//DEBUG
		qDebug()<<"planForBehavior returned plan : size="<<plan.size();
		int i = 0;
		for ( const auto &t : plan) {
			qDebug()<<" track "<<i<<" size="<<t.size();
			++i;
		}
	}//DEBUG
*/



	return animationFound;
}


bool PixmapFacePlanner::planForBehavior(const FaceBehavior *b, FacePlan &fp)
{
  return planForBehavior(b->faceType(), b->amount(), fp);
}

void PixmapFacePlanner::setCurrentPlan(const FacePlan &p) 
{
	//TODO: check that plan does not have more tracks that we handle !!!

	m_currentPlan = p;
}

static
int
getFrame(float currTime, const PlanUnit *a, int segmentSize)
{
	float t = currTime - a->startTime();
	assert(a->length() != 0);
	int frame = static_cast<int>(segmentSize*t/a->length() + 0.5f);
	frame = frame >= segmentSize ? segmentSize-1 : frame;
	frame = frame < 0 ? 0 : frame;
	return frame;
}

void PixmapFacePlanner::play(float currTime) 
{
  //qDebug()<<"PixmapFacePlanner::play("<<currTime<<")  "<<m_currentPlan.size()<<" elements in current plan";

	const int nbElements = m_currentPlan.size();
	for (int i=0; i<nbElements; ++i) {

		FaceTrackPlan &tp = m_currentPlan[i];

		//if (tp.size() > 0)
		//qDebug()<<"PixmapFacePlanner::play("<<currTime<<") track "<<i<<" size="<<tp.size();
	
		//FaceTrackPlan::iterator it = tp.begin();
		for (auto it=tp.begin(); it!=tp.end(); ) {
			FacePlanUnit *a = *it;

			//qDebug()<<"a="<<a;
			//qDebug()<<"a="<<a<<": elt="<<a->element()<<" a->startTime()="<<a->startTime()<<" a->endTime()="<<a->endTime();

			if (a->endTime() < currTime) //it is possible that we never played this animation
			{
			  //qDebug()<<"### delete a="<<a<<" elt="<<a->element()<<" a->startTime()="<<a->startTime()<<" a->endTime()="<<a->endTime();
				delete *it;
				it = tp.erase(it);
				//qDebug()<<" tp.size()="<<tp.size();
			}
			else 
			{
				if (a->startTime() > currTime) 
				{
					break;
				}
				else 
				{
					assert(a->startTime() <= currTime && (currTime <= a->endTime() || a->length() == -1));
					
					
					//the planUnit is not necessary present
					//(it may be a viseme added by the speechPlanner and the facePlanner may not have an animation for it)
					
					//qDebug()<<"PixmapFacePlanner::play() : a->faceType()="<<a->faceType()<<"\n";

					auto itB = m_behaviors.find(a->faceType());
					if (itB != m_behaviors.end()) {

						assert(a->element() < itB->behaviorElements.size());
						assert(a->index() < itB->behaviorElements[a->element()].animations.size());
					
						//qDebug()<<"  PixmapFacePlanner::play type="<<a->faceType()<<" element="<<a->element()<<" index="<<a->index()<<" int="<<a->intensity();

						const AnimationIntensity &ai = m_behaviors[a->faceType()].behaviorElements[a->element()].animations[a->index()];

						//qDebug()<<"  animation int: min="<<ai.intensityMin<<" max="<<ai.intensityMax<<" anim="<<ai.anim;
						
						//assert(ai.hasIntensity(a->intensity())); //No, not necessarily !
						assert(ai.anim != nullptr);
						
						assert(a->segment() < ai.anim->numberOfSegments());
						int frame = getFrame(currTime, a, ai.anim->sizeOfSegment(a->segment()));
						const QPixmap *p = ai.anim->pixmap(a->segment(), frame);
						assert(p != nullptr);
						
						assert(a->element() < m_elementItems.size());
						//qDebug()<<"  elt="<<a->element()<<" eltItem="<<m_elementItems[a->element()]<<" pixmap size="<<p->size();
						m_elementItems[a->element()]->setPixmap(*p);

						++it;
					}
					else {
						delete *it;
						it = tp.erase(it);
					}

				}

			}

		}
	}

	m_lastTime = currTime;

}

bool
PixmapFacePlanner::isCurrentPlanEmpty() const
{
	for (const FaceTrackPlan &tp : m_currentPlan) {
		if (! tp.empty())
			return false;
	}
	
	return true;
}

void 
PixmapFacePlanner::blink()
{
	assert(m_elementItems.size() > EYES);
	assert(m_behaviors.find(Blink) != m_behaviors.end());

	if (isCurrentPlanEmpty()) { //should we check only elements/tracks where Blink will add something ???

	  FacePlan plan;
	  bool ok = planForBehavior(Blink, 1.0f, plan);
	  if (ok) {
	    //we have to use previous time to forecast next time... //UGLY
	    float newStartTime = m_lastTime + 2/30.f; //UGLY
	    plan.setStartTime(newStartTime);

	    mergePlan(plan);
	  }
	}

	//plan next blink
    std::uniform_int_distribution<> dis(0, BLINK_RANDOM);
	const int nextBlink = BLINK_INTERVAL + dis(m_randomEngine);
	QTimer::singleShot(nextBlink, this, SLOT(blink()));
}

static
bool
intersect(const FacePlanUnit &f1, const FacePlanUnit &f2)
{
	return f1.startTime() < f2.endTime() && f2.startTime() < f1.endTime();
}

static
bool
checkSorted(const FaceTrackPlan &t)
{
	bool sorted = true;
	FaceTrackPlan::const_iterator it = t.begin();
	FaceTrackPlan::const_iterator itNext = t.begin()+1;
	if (it != t.end() && itNext!=t.end()) 
	{
		for (; itNext!=t.end(); ++it, ++itNext) 
		{
			if ((*it)->endTime() > (*itNext)->startTime())
				return false;
		}
	}

	return sorted;
}


void
PixmapFacePlanner::mergePlan(FacePlan &plan) //TODO: add parameter: mode={Merge, Append, Replace} ??? 
{
	int sz = plan.size();
	if (sz > m_currentPlan.size())
	{
		m_currentPlan.resize(sz);
		if (sz > numberOfElements()) 
		{
			qDebug()<<"Warning: some elements of plan will not be merged";
			sz = numberOfElements();
		}
	}

	assert(sz <= m_currentPlan.size());

	for (int i=0; i<sz; ++i) {

		if (i >= plan.size())
			break;

		FaceTrackPlan &t = plan[i];
		FaceTrackPlan &ct = m_currentPlan[i];

		assert(checkSorted(t));
		assert(checkSorted(ct));

		if (! t.empty()) {

			//REPLACE mode

			bool intersection = false;
			for (const FacePlanUnit *f : t) 
			{
				assert(f != nullptr);

				for (const FacePlanUnit *cf : ct)
				{
					assert(cf != nullptr);

					if (intersect(*f, *cf))
					{
						intersection = true;
						break;
					}
				}
			}

			if (intersection) 
			{
				//we clear the current track/element
				for (FacePlanUnit *cf : ct)
				{
					delete cf;
				}
				//ct.clear();
				
				ct = t;
			}
			else 
			{
				//we merge 

				if (ct.empty()) 
				{
					ct = t;
				}
				else 
				{
					for (FacePlanUnit *f : t) 
					{
						bool inserted = false;
						//FaceTrackPlan::iterator itC = ct.begin();
						for (auto itC=ct.begin(); itC!=ct.end(); ++itC) 
						{
							FacePlanUnit *cf = *itC;
							if (f->endTime() <= cf->startTime()) {
								inserted = true;
								ct.insert(itC, f);
								break;
							}
						}
						if (! inserted)
							ct.push_back(f);
					}
				}
				
			}

		}
		
		assert(checkSorted(ct));
	}

}

void
PixmapFacePlanner::setIdle()
{
	/* REM: we cannot do :
	   FacePlan plan = planForBehavior(Idle, 1.0f);
	   mergePlan(plan);

	   As we do not know the current time,
	   we have no guarranty that the obtained plan would actually be played.
	*/

	const FaceBehaviorType type = Idle;
	
	if (m_behaviors.find(type) != m_behaviors.end()) {
		//assert(type < m_behaviors.size());
		const FaceElements &fe = m_behaviors[type];

		const int nbElems = fe.behaviorElements.size();

		assert(m_elementItems.size() >= nbElems);
	
		for (int element=0; element<nbElems; ++element) 
		{
			//- first get index of Animation for the given intensity
			const auto &animations = fe.behaviorElements[element].animations;
			if (! animations.empty()) 
			{
				const int index = 0; //always first (unique ?) available animation			
				assert(index>=0 && index<fe.behaviorElements[element].animations.size());

				const AnimationIntensity &ai = fe.behaviorElements[element].animations[index]; 
				assert(ai.anim != nullptr);
				//const float intensity = 1.0f;
				//float startTime = 0.f;
			
				const int nbSegments = ai.anim->numberOfSegments();

				assert(AnimationSequenceSegments::Set < nbSegments);

				const int segment = AnimationSequenceSegments::Set; //always Set segment
				const int frame = 0; //always first pixmap of segment
				/*
				  UGLY: it means that we can not have animations for idle state...
				*/

				if (frame < ai.anim->sizeOfSegment(segment)) {

					const QPixmap *p = ai.anim->pixmap(segment, frame);
					assert(p != nullptr);
			
					m_elementItems[element]->setPixmap(*p);
				}
			}
		
		}
	}
}

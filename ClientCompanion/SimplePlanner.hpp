#ifndef SIMPLEPLANNER_HPP
#define SIMPLEPLANNER_HPP

#include <memory>
#include "Planner.hpp"

#include "SpeechPlanner.hpp"
#include "FacePlanner.hpp"
#include "ControlPlanner.hpp"


class SimplePlanner : public Planner
{
public:
	SimplePlanner(std::shared_ptr<SpeechPlanner> speechPlanner,
				  std::shared_ptr<FacePlanner> facePlanner);

	virtual bool add(BehaviorBlock &bl, float currentTime) override;

	virtual void play(float time) override;

protected:
	std::shared_ptr<SpeechPlanner> m_speechPlanner;
	std::shared_ptr<FacePlanner> m_facePlanner;
	std::shared_ptr<ControlPlanner> m_controlPlanner;	
};

#endif //SIMPLEPLANNER_HPP

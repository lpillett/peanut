#!/bin/bash

SCRIPTNAME=$(basename $0)

RSRC_DIR=rsrc
QRC_EXT=".qrc"
QRC_FILE_GLOBAL=rsrc_global.qrc
QRC_FILE_VOICE_PREFIX=rsrc_voice_
QRC_FILE_FACE_PREFIX=rsrc_face_


if [ ! -d "${RSRC_DIR}" ]
then
	echo "ERROR: directory ${RSRC_DIR} does not exist"
	exit 10
fi

find ${RSRC_DIR} -name "*${QRC_EXT}" -delete

cd ${RSRC_DIR} 



function processFile
{
	QRC_FILE=$1
	file=$2
	
	ext=${file##*.}
	if [ "${ext}" != "${QRC_EXT}" ]
	then
		echo " <file>$file</file>" >> ${QRC_FILE}
	fi
}

#Add files recursively (except those with QRC_EXT extension)
function processPath
{
	QRC_FILE=$1
	dir=$2

	for i in ${dir}*
	do
		if [ -f "$i" ]
		then
			processFile ${QRC_FILE} ${i}
		elif [ -d "$i" ]
		then
			processPath ${QRC_FILE} "$i/"
		fi
	done
}

#everything except faces/ & voices/
function process_global
{
	QRC_FILE=$1

	for i in ${dir}*
	do
		if [ -f "$i" ]
		then
			ext=${i##*.}
			if [ "${ext}" != "${QRC_EXT}" ]
			then
				echo " <file>$i</file>" >> ${QRC_FILE}
			fi
		elif [ -d "$i" ]
		then
			if [ "$i" != "faces" ] && [ "$i" != "voices" ]
			then
				processPath ${QRC_FILE} "$i/"
			fi
		fi
	done
}

function write_header
{
	QRC_FILE=$1

	echo "<!-- Automatically generated by ${SCRIPTNAME}. Do not edit ! -->" > ${QRC_FILE}
	echo '<!DOCTYPE RCC>' >> ${QRC_FILE}
	echo '<RCC version="1.0">' >> ${QRC_FILE}
	echo '<qresource>'  >> ${QRC_FILE}
}

function write_footer
{
	QRC_FILE=$1

	echo '</qresource>' >> ${QRC_FILE}
	echo '</RCC>' >> ${QRC_FILE}
}


#processPath ""


write_header ${QRC_FILE_GLOBAL}
process_global ${QRC_FILE_GLOBAL} ""
write_footer ${QRC_FILE_GLOBAL}


for dir in voices/*
do
	if [ -d ${dir} ]
	then
		lang="$(basename ${dir})"
		echo "lang: ${lang}"
		
		QRC_FILE_LANG_GLOBAL="${QRC_FILE_VOICE_PREFIX}${lang}_global${QRC_EXT}"
		write_header ${QRC_FILE_LANG_GLOBAL}

		for dir2 in voices/${lang}/*
		do
			if [ -d ${dir2} ]
			then

				voice="$(basename ${dir2})"
				echo "  voice: ${voice}"
				QRC_FILE_VOICE="${QRC_FILE_VOICE_PREFIX}${lang}_${voice}${QRC_EXT}"
				write_header ${QRC_FILE_VOICE}
				processPath ${QRC_FILE_VOICE} ${dir2}
				write_footer ${QRC_FILE_VOICE}
				
			elif [ -f ${dir2} ]
			then
				processFile ${QRC_FILE_LANG_GLOBAL} ${dir2}
			fi

		done

		write_footer ${QRC_FILE_LANG_GLOBAL}

	fi
done


for dir in faces/*
do
	if [ -d ${dir} ]
	then		
		face="$(basename ${dir})"
		echo "face: ${face}"
		QRC_FILE_FACE="${QRC_FILE_FACE_PREFIX}${face}${QRC_EXT}"
		write_header ${QRC_FILE_FACE}
		processPath ${QRC_FILE_FACE} ${dir}
		write_footer ${QRC_FILE_FACE}
	fi
done

#include "BehaviorBlock.hpp"

#include <cassert>

#include <QDataStream>
#include <QHash>
#include <QMap>
#include <QDebug>

BehaviorBlock::BehaviorBlock()
{
}

BehaviorBlock::~BehaviorBlock()
{
	clear();
}

void BehaviorBlock::clear()
{	
	for (Behavior *b : m_behaviors)
		delete b;
	m_behaviors.clear();
	m_synchros.clear();
}

bool BehaviorBlock::addBehavior(Behavior *b)
{
	if (! hasBehavior(b))
	{
		m_behaviors.push_back(b);
		return true;
	}
	return false;
}
 
size_t BehaviorBlock::numberOfBehaviors() const
{
	return m_behaviors.size();
}

Behavior *BehaviorBlock::behavior(size_t i)
{
	if (i < numberOfBehaviors())
	{
		return m_behaviors[i];
	}
	return nullptr;
}

const Behavior *BehaviorBlock::behavior(size_t i) const
{
	if (i < numberOfBehaviors())
	{
		return m_behaviors[i];
	}	
	return nullptr;
}

void BehaviorBlock::removeBehavior(size_t i)
{
	if (i < numberOfBehaviors())
	{
		auto it = m_behaviors.begin()+i;
		assert(it != m_behaviors.end());
		delete *it;
		m_behaviors.erase(it);
	}
}

bool BehaviorBlock::hasBehavior(const Behavior *b) const
{
	return std::find(m_behaviors.begin(), m_behaviors.end(), b) != m_behaviors.end();
}




bool BehaviorBlock::addSynchronization(const Synchronization &s)
{
	if (s.isValid()) 
	{
		if (! hasSynchronization(s))
		{
			m_synchros.push_back(s);
			return true;
		}
	}
	return false;
}
 
size_t BehaviorBlock::numberOfSynchronizations() const
{
	return m_synchros.size();
}

Synchronization &BehaviorBlock::synchronization(size_t i)
{
	assert(i < numberOfSynchronizations());
	return m_synchros[i];
}

const Synchronization &BehaviorBlock::synchronization(size_t i) const
{
	assert(i < numberOfSynchronizations());
	return m_synchros[i];
}

void BehaviorBlock::removeSynchronization(size_t i)
{
	if (i < numberOfSynchronizations())
	{	
		auto it = m_synchros.begin()+i;
		assert(it != m_synchros.end());
		m_synchros.erase(it);
	}
}

bool BehaviorBlock::hasSynchronization(const Synchronization &s) const
{
	return std::find(m_synchros.cbegin(), m_synchros.cend(), s) != m_synchros.cend();
}

bool BehaviorBlock::checkSynchronizations() const
{
	for (const Synchronization &s : m_synchros) 
		assert(s.isValid());

	
	//we cannot do that here as we do not know the exact duration
	//of the behaviors

	return true;
}


void BehaviorBlock::removeBehaviorAndCorrespondingSyncs(size_t i)
{
	if (i < numberOfBehaviors())
	{
		auto it = m_behaviors.begin()+i;
		assert(it != m_behaviors.end());

		const Behavior *b = *it;

		for (size_t j=0; j<numberOfSynchronizations(); ) {

			const Synchronization &sync = synchronization(j);
			if (sync.behavior1() == b || sync.behavior2() == b) {
				removeSynchronization(j);
			}
			else {
				++j;
			}
		}

		delete *it;
		m_behaviors.erase(it);
	}
}



bool
serialize(QDataStream &strm, const BehaviorBlock &bl)
{
	QHash<const Behavior *, quint8> behavior2Index;

	const int nb = bl.numberOfBehaviors();

	quint8 nbi = nb;
	if (static_cast<int>(nbi) != nb) {
		qDebug()<<"ERROR: unable to serialize. Too many behaviors";
		return false;
	}


	strm << nbi;
	for (int i=0; i<nb; ++i) {
		const Behavior *b = bl.behavior(i);
		behavior2Index[b] = i;
		
		float f = b->amount();
		strm << f;

		//qDebug()<<"BehaviorBlock serialize amount= "<<f;

		quint8 ti = b->type();
		strm << ti;
		
		if (b->type() == Face) {
			quint8 fti = (static_cast<const FaceBehavior *>(b))->faceType();
			strm << fti;
		}
		else if (b->type() == Speech) {
			quint8 sti = (static_cast<const SpeechBehavior *>(b))->speechType();
			strm << sti;
			if ((static_cast<const SpeechBehavior *>(b))->speechType() == SoundFile) {
				strm << (static_cast<const SoundFileSpeechBehavior *>(b))->filename();
			}
		}
	}

	const int ns = bl.numberOfSynchronizations();
	quint8 nsi = ns;

	//qDebug()<<"serialize "<<ns<<" synchros";

	strm << nsi;
	for (int i=0; i<ns; ++i) {
		const Synchronization &s = bl.synchronization(i);
	
		quint8 b1i = behavior2Index.value(s.behavior1(), -1);
		quint8 s1i = s.syncPoint1();
		quint8 b2i = behavior2Index.value(s.behavior2(), -1);
		quint8 s2i = s.syncPoint2();
		//float off = s.offset();

		strm << b1i;
		strm << s1i;
		strm << b2i;
		strm << s2i;
		//strm << off;
	}

	return strm.status() == QDataStream::Ok;
}

bool 
deserialize(QDataStream &strm, BehaviorBlock &bl)
{
	quint8 nbi;
	strm >> nbi;
	if (strm.status() != QDataStream::Ok) {
		return false;
	}
	int nb = nbi;

	bl.clear();

	QMap<quint8, const Behavior *> index2Behavior;

	for (int i=0; i<nb; ++i) {

		float amount = 0.f;
		strm >> amount;

		//qDebug()<<"BehaviorBlock deserialize amount= "<<amount;

		quint8 ti = 0;
		strm >> ti;
		BehaviorType t = static_cast<BehaviorType>(ti);
		Behavior *b = nullptr;
		if (t == Face) {
			quint8 fti;
			strm >> fti;
			FaceBehaviorType ft = static_cast<FaceBehaviorType>(fti);
			b = new FaceBehavior(ft, amount);			
		}
		else if (t == Speech) {
			quint8 sti;
			strm >> sti;
			SpeechBehaviorType st = static_cast<SpeechBehaviorType>(sti);
			if (st == SoundFile) {
				QString filename;
				strm >> filename;
				b = new SoundFileSpeechBehavior(filename, amount);
			} 
		}

		if (strm.status() != QDataStream::Ok) {
		  delete b;
		  return false;
		}

		bl.addBehavior(b);
		index2Behavior[static_cast<quint8>(i)] = b;
	}

	if (strm.status() == QDataStream::Ok) {
		quint8 nsi = 0;
		strm >> nsi;
		int ns = nsi;

		for (int i=0; i<ns; ++i) {
			quint8 indexB1 = 0;
			strm >> indexB1;
			const Behavior *b1 = index2Behavior.value(indexB1, nullptr);
			if (b1 == nullptr || strm.status() != QDataStream::Ok) {
			  return false;
			}
			quint8 syncPt1i = 0;
			strm >> syncPt1i;
			Behavior::SyncPoint syncPt1 = static_cast<Behavior::SyncPoint>(syncPt1i);

			quint8 indexB2 = 0;
			strm >> indexB2;
			const Behavior *b2 = index2Behavior.value(indexB2, nullptr);
			if (b2 == nullptr || strm.status() != QDataStream::Ok) {
			  return false;
			}
			quint8 syncPt2i = 0;
			strm >> syncPt2i;
			Behavior::SyncPoint syncPt2 = static_cast<Behavior::SyncPoint>(syncPt2i);
			
			float offset = 0;
			//strm >> offset;

			bl.addSynchronization(Synchronization(b1, syncPt1, b2, syncPt2, offset));
	
			//qDebug()<<"deserialize Synchro: b1i="<<indexB1<<" syncPt1="<<syncPt1<<" b2i="<<indexB2<<" syncPt2="<<syncPt2<<"\n";
		
		}
		
	}

	return strm.status() == QDataStream::Ok;
}

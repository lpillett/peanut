#include "Widget.hpp"

#include <cassert>

#include <QDebug>
#include <QBoxLayout>

#include "SimplePlanner.hpp"
#include "SoundPlayerSpeechPlanner.hpp"
#include "PixmapFacePlanner.hpp"
#include "LipSyncPhsegFiles.hpp"


Widget::Widget(const QString &faceFile, const QString &mappingFile, QWidget *parent) :
	QWidget(parent), 
	m_planner(nullptr),
	m_timer(), m_startTime()
{
	std::shared_ptr<LipSync> lipSync(new LipSyncPhsegFiles(mappingFile));

	std::shared_ptr<SpeechPlanner> sp(new SoundPlayerSpeechPlanner(lipSync));
	
	PixmapFacePlanner *pfp = new PixmapFacePlanner(faceFile, this);
	std::shared_ptr<FacePlanner> fp(pfp);

	m_planner.reset(new SimplePlanner(sp, fp));

	connect(m_planner.get(), SIGNAL(controlReached(ControlPlanUnitType)), this, SIGNAL(behaviorEnd())); //TODO: are we sure we have only this signal ????
									

	m_timer = new QTimer(this);
	QObject::connect(m_timer, SIGNAL(timeout()), this, SLOT(nextFrame()));
	int msec = 33; //interval in milliseconds [int !]
	m_timer->start(msec); 

	m_startTime = std::chrono::system_clock::now();

	QHBoxLayout *l = new QHBoxLayout;
	l->addWidget(pfp);
	l->setSpacing(0);
	l->setContentsMargins(0, 0, 0, 0);
	setLayout(l);

}

Widget::~Widget()
{
	//TODO:BUG: remove "pfp" from widget
	//otherwise, due to shared_ptr, it will be deleted two times !!!???
}


float
Widget::getTime() const
{
	//B: should we use high_resolution_clock ???
	 std::chrono::time_point<std::chrono::system_clock> p = std::chrono::system_clock::now();
	 
	 float t = std::chrono::duration_cast<std::chrono::milliseconds>(p-m_startTime).count(); //.time_since_epoch()).count();
	 return t;
}

/*
static
int
getFrame(float currTime, const PlanUnit *a, int segmentSize)
{
	float t = currTime - a->startTime();
	assert(a->length() != 0);
	int frame = static_cast<int>(segmentSize*t/a->length() + 0.5f);
	frame = frame >= segmentSize ? segmentSize-1 : frame;
	frame = frame < 0 ? 0 : frame;
	return frame;
}
*/

void
Widget::nextFrame()
{
	const float time = getTime();

	m_planner->play(time);
}

bool
Widget::add(BehaviorBlock &b)
{
	const float time = getTime();

	return m_planner->add(b, time);
}





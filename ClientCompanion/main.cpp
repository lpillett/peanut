#include <QApplication>

#include <iostream>
#include "MainWindow.hpp"

#include <QDirIterator> //DEBUG !!!


/*
#ifdef Q_OS_ANDROID
#include <QAndroidJniObject>
#include <QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>

static
void keepScreenOn()
{
	QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid())
    {
        QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (window.isValid())
        {
            const int FLAG_KEEP_SCREEN_ON = 128;
            window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        }
    }	
}

static
void keepScreenOn2()
{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid())
    {
        QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (window.isValid())
        {
            const int FLAG_FULLSCREEN = 1024;
            window.callMethod<void>("addFlags", "(I)V", FLAG_FULLSCREEN);
            const int FLAG_KEEP_SCREEN_ON = 128;
            window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
        }
    }
}

static
void keepScreenOn3()
{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid())
    {
        QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (window.isValid())
        {
            const int FLAG_KEEP_SCREEN_ON = 128;
            window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
            const int FLAG_FULLSCREEN = 1024;
            window.callMethod<void>("addFlags", "(I)V", FLAG_FULLSCREEN);
        }
    }
}

//Add both flags simultaneously
static
void keepScreenOn4()
{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid())
    {
        QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
        if (window.isValid())
        {
            const int FLAG_KEEP_SCREEN_ON = 128;
            const int FLAG_FULLSCREEN = 1024;
            window.callMethod<void>("addFlags", "(I)V", FLAG_FULLSCREEN|FLAG_KEEP_SCREEN_ON);
        }
    }
}

#endif //Q_OS_ANDROID
*/

int
main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	QCoreApplication::setOrganizationName("inria");
    QCoreApplication::setOrganizationDomain("inria.fr");
    QCoreApplication::setApplicationName("QtCompanion");

	MainWindow w;

#ifdef Q_OS_ANDROID
    //keepScreenOn(); //to prevent going in sleep mode

    //keepScreenOn3();

        /*
         si je fais keepScreenOn()  [addFlag(FLAG_KEEP_SCREEN_ON)] +showFullscreen() : je n'ai pas la mise en veille mais j'ai la status bar !
            si je ne le fais pas : je n'ai pas la status bar mais j'ai la mise en veille...

         si je fais keepScreenOn2() [addFlag(FLAG_FULLSCREEN), addFlag(FLAG_KEEP_SCREEN_ON)], setWindowsFlags, setWindowsState, showFullscreen
               je n'ai pas la status bar mais j'ai la mise en veille !
         si je fais keepScreenOn2() [addFlag(FLAG_FULLSCREEN), addFlag(FLAG_KEEP_SCREEN_ON)]
               je n'ai pas la status bar mais j'ai la mise en veille !

         si je fais keepScreenOn3() [addFlag(FLAG_KEEP_SCREEN_ON), addFlag(FLAG_FULLSCREEN)]
                je n'ai pas la mise en veille mais j'ai la status bar !!!


          si je fais show()+keepScreenOn() : je n'ai pas la veille (?), j'ai las status bar, MAIS j'ai un écran noir !!!
          si je fais showFullScreen()+keepScreenOn() : je n'ai pas la veille ni la status bar MAIS j'ai un écran noir !!!


          keepScreenOn4()+show() : a l'air de fonctionner : ni status bar ni mise en veille !!!
                      Il faut donc mettre les deux flags à la fois (OR'ed) !!!

          */


    /*
    w.setWindowFlags(Qt::FramelessWindowHint);
    w.setWindowState(w.windowState()|Qt::WindowFullScreen);
    w.showFullScreen();
*/
    //w.show();

	qDebug()<<"MainWindow construction done";

	//Boris: keepScreenOn4() fonctionne correctement sur GalaxyNote2 (Android 4.4)
	// mais provoque un crash sur Nexus 7 (Android 5.1.1)
	// D'après monitor, l'erreur est :
	// "JNI GetStaticMethodID called with pending exception 'android.view.ViewRootImpl$CalledFromWrongThreadException' thrown in unknown throw location"
	//ce bug est référencé ici :
	// https://bugreports.qt.io/browse/QTBUG-49146

	
    //keepScreenOn4();
	//qDebug()<<"keepScreenOn4 done";

    w.show();

	qDebug()<<"show done";

#else
    w.show();
#endif //Q_OS_ANDROID
	
	return app.exec();
};

#ifndef PLANNER_HPP
#define PLANNER_HPP

#include <QObject>
#include "BehaviorBlock.hpp"
#include "PlanUnit.hpp"//ControlPlanUnitType

class Planner : public QObject
{
  Q_OBJECT

public:
	
	virtual ~Planner() {}

	virtual bool add(BehaviorBlock &b, float time) = 0;

	virtual void play(float time) = 0;

signals:
  
  void controlReached(ControlPlanUnitType ct);

};


#endif //PLANNER_HPP

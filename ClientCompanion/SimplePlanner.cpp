#include "SimplePlanner.hpp"

#include <cassert>
#include "SimpleControlPlanner.hpp"

#include "SimpleSynchronizer.hpp"


SimplePlanner::SimplePlanner(std::shared_ptr<SpeechPlanner> speechPlanner,
							 std::shared_ptr<FacePlanner> facePlanner) :
	m_speechPlanner(speechPlanner),
	m_facePlanner(facePlanner)
{
  m_controlPlanner.reset(new SimpleControlPlanner);

  connect(m_controlPlanner.get(), SIGNAL(controlReached(ControlPlanUnitType)), this, SIGNAL(controlReached(ControlPlanUnitType)));

}

void
SimplePlanner::play(float time)
{
	//std::cerr<<"play "<<time<<"\n";

	m_speechPlanner->play(time);
	m_facePlanner->play(time);
	
	m_controlPlanner->play(time);
}


#include <iostream>

bool
SimplePlanner::add(BehaviorBlock &bl, float currentTime)
{
	const int nbBehaviors = bl.numberOfBehaviors();

	qDebug()<<"\n*** SimplePlanner::add "<<nbBehaviors<<" behaviors & "<<bl.numberOfSynchronizations()<<" synchros";

	QVector<FacePlan> facePlans(nbBehaviors);
	QVector<SpeechPlan> speechPlans(nbBehaviors);

	bool res = true;

	for (int i=0; i<nbBehaviors; ++i) 
	{
		Behavior *b = bl.behavior(i);
		assert(b != nullptr);

		SpeechPlan sp;
		FacePlan fp;

		if (b->type() == Face) 
		{
			FaceBehavior *fB = static_cast<FaceBehavior *>(b);

			//res &= m_facePlanner->planForBehavior(fB, fp);

			//If no FacePlan (i.e. animation) is found for this FaceBehavior 
			// we try to replace it with Idle
			const bool resF = m_facePlanner->planForBehavior(fB, fp);
			if (! resF) {
				fB->setFaceType(Idle);
				res &= m_facePlanner->planForBehavior(fB, fp);
			}

			std::cerr<<"SimplePlanner::add() FaceBehavior "<<i<<" res="<<resF<<" fp.empty?="<<fp.empty()<<"\n";
			
		}
		else if (b->type() == Speech) 
		{
			const SpeechBehavior *sB = static_cast<const SpeechBehavior *>(b);

			const bool resS = m_speechPlanner->planForBehavior(sB, sp, fp);

			res &= resS;

			//TODO: pour chaque FacePlanUnit du FacePlan fp,
			//il faut vérifier si elle est présente dans m_facePlanner !?

			std::cerr<<"SimplePlanner::add() SpeechBehavior "<<i<<" res="<<resS<<" fp.empty?="<<fp.empty()<<" sp.empty?="<<sp.empty()<<"\n";

		}

		if (! res)
		  break;


		facePlans[i] = fp;
		speechPlans[i] = sp;
	}


	if (! res) {
	  qDebug()<<"ERROR: unable to make behavior plan";
	  return false;
	}

	res &= SimpleSynchronizer().add(bl, facePlans, speechPlans, currentTime);
	if (! res) {
	  qDebug()<<"ERROR: unable to fulfill constraints";
	  std::cerr<<"\n*** ERROR: unable to fulfill constraints\n";
	  return false;
	}


	//Once synchronization constraints are respected,
	//we will append new plans to current plans.
	//We have to shift their startTime...

	FacePlan cfp = m_facePlanner->currentPlan();
	SpeechPlan csp = m_speechPlanner->currentPlan();
	
	if (! cfp.empty() || ! csp.empty()) {
	    
	  float newStartTime = std::max(cfp.endTime(), csp.endTime());

	  newStartTime = std::max(newStartTime, currentTime);

      std::cerr<<"\ncurrent plan no empty\n";
      {//DEBUG
          std::cerr<<"current FacePlan: (currTime="<<currentTime<<")\n";
          int ti = 0;
          for (const auto &tp : cfp) {
              std::cerr<<" track "<<ti<<"\n";
              for (const auto *pu : tp) {
                  std::cerr<<"  pu "<<	pu->faceType()<<"  i="<<pu->intensity()<<" st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime() << "(elt="<<pu->element()<<", ind="<<pu->index()<<", seg="<<pu->segment()<<")\n";
              }
              ++ti;
          }
          std::cerr<<"current SpeechPlan: (currTime="<<currentTime<<")\n";
          ti = 0;
          for (const auto &tp : csp) {
              std::cerr<<" track "<<ti<<"\n";
              for (const auto *pu : tp) {
                std::cerr<<"  pu "<<	pu->speechType()<<"  i="<<pu->intensity()<<" st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime()<<"\n";
              }
              ++ti;
          }
      }//DEBUG

	  std::cerr<<"move new plans: newStartTime="<<newStartTime<<" (cfp et="<<cfp.endTime()<<" csp et="<<csp.endTime()<<" currentTime="<<currentTime<<")\n";
	  
	  //Get minimum start time of all tracks
	  float minStartTime = std::numeric_limits<float>::max();
	  for(FacePlan &p : facePlans)
	    {
			if (! p.empty()) {
				const float st = p.startTime();
				if (st < minStartTime)
					minStartTime = st;
			}
		}
	  for(SpeechPlan &p : speechPlans)
	    {
			if (! p.empty()) {
				const float st = p.startTime();
				if (st < minStartTime)
					minStartTime = st;
			}
	    }

	  //std::cerr<<"minStartTime="<<minStartTime<<"\n";
	  //std::cerr<<"shift of (newStartTime-minStartTime)="<<(newStartTime-minStartTime)<<"\n";
	  
	  //Shift the startTimes
	  for(FacePlan &p : facePlans)
	    {
			//std::cerr<<"fp old st="<<p.startTime()<<"\n";
			p.shiftOf(newStartTime-minStartTime);
			//std::cerr<<" new st="<<p.startTime()<<"\n";
	    }
	  for(SpeechPlan &p : speechPlans)
	    {
			//std::cerr<<"sp old st="<<p.startTime()<<"\n";
			p.shiftOf(newStartTime-minStartTime);
			//std::cerr<<" new st="<<p.startTime()<<"\n";
	    }

	}

	
	for(FacePlan &p : facePlans)
	{
		cfp.append(p);
	}
	cfp.sort();

	for(SpeechPlan &p : speechPlans)
	{
		csp.append(p);
	}
	csp.sort();


	{//DEBUG
		std::cerr<<"accepted FacePlan: (currTime="<<currentTime<<")\n";
		int ti = 0;
		for (const auto &tp : cfp) {
            std::cerr<<" track "<<ti<<"\n";
			for (const auto *pu : tp) {
                std::cerr<<"  pu "<<	pu->faceType()<<"  i="<<pu->intensity()<<" st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime() << "(elt="<<pu->element()<<", ind="<<pu->index()<<", seg="<<pu->segment()<<")\n";
			}
			++ti;
		}
		std::cerr<<"accepted SpeechPlan: (currTime="<<currentTime<<")\n";
		ti = 0;
		for (const auto &tp : csp) {
            std::cerr<<" track "<<ti<<"\n";
			for (const auto *pu : tp) {
              std::cerr<<"  pu "<<	pu->speechType()<<"  i="<<pu->intensity()<<" st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime()<<"\n";
			}
			++ti;
		}


		//std::cerr<<"EXIT DEBUG\n";
		//exit(10); //DEBUG

	}//DEBUG




	m_facePlanner->setCurrentPlan(cfp);
	m_speechPlanner->setCurrentPlan(csp);


	{//Add ControlPlanUnit to ControlPlanner to signal end of behavior
	  float startTime = 0;
	  startTime = std::max(startTime, cfp.endTime());
	  startTime = std::max(startTime, csp.endTime());
	  
	  const float length = startTime + 2/35; //TODO:UGLY: must be long enough to not be missed
	  const float intensity = 1.f; //Useless

	  ControlPlanUnit *cpu = new ControlPlanUnit(EndBehavior, intensity, startTime, length); 

	  ControlPlan cp;
	  cp.resize(1);
	  cp[0].push_back(cpu);
	  
	  ControlPlan ccp = m_controlPlanner->currentPlan();

	  ccp.append(cp);
	  ccp.sort();
	  
	  m_controlPlanner->setCurrentPlan(ccp);
	}
	


	return true;
}


#ifndef CONFIGURATIONWIDGET_HPP
#define CONFIGURATIONWIDGET_HPP

#include <QWidget>
#include <QMap>
#include <QMediaPlayer>

class QButtonGroup;
class QComboBox;
class QLabel;
class QLineEdit;
class QPushButton;
class QScrollArea;


class ConfigurationWidget : public QWidget
{
	Q_OBJECT

public:

	explicit ConfigurationWidget(QWidget *parent = 0);

	void setServerAddress(const QString &ip, int port);

	void addFace(const QString &name, const QString &imageFilename);
	void addVoice(const QString &lang, const QString &name, const QString &soundFilename, const QString &flagFilename="");

	void selectFirstFace();
	void selectLang(const QString &lang);

	void getServerAddress(QString &ip, int &port);
	void getSelectedFace(QString &name, QString &imageFilename);
	void getSelectedVoice(QString &lang, QString &name, QString &soundFilename);

	void setConnectionEnabled(bool);

signals:

	void connectionRequest(const QString &ip, int port);
    void quit();

protected slots:

	void onConnect();
	void playSound();
	void mediaStatusChanged(QMediaPlayer::MediaStatus m);
	void updateVoices();

protected:

	void buildGUI();
	void setEnabledConnection(bool);
	void populateHosts();
	void selectLang_aux(const QString &lang);

protected:

	QLabel *m_myIPLabel;
	QLabel *m_hostLabel;
	QComboBox *m_hostCombo;
	QLabel *m_portLabel;
	QLineEdit *m_portLineEdit;
	QPushButton *m_connectButton;
	QPushButton *m_quitButton;

	QMap<QString, QString> m_faceNameToImageFilename;
	QMap<QString, QString> m_voiceNameToSoundFilename;
	QMap<QString, QString> m_voiceNameToFlagFilename;
	QMap<QPushButton*, QString> m_faceButtonToName;
	QMap<QString, QVector<QString> > m_langToVoices;
	QString m_currentLang;

	QScrollArea *m_faceSA;
	QWidget *m_faceW;
	QButtonGroup *m_faceBG;
	QComboBox *m_langCB;
	QComboBox *m_voiceCB;
	QPushButton *m_voicePlayB;
	QMediaPlayer m_player;
};

#endif /* ! CONFIGURATIONWIDGET_HPP */

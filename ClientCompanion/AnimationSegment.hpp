#ifndef ANIMATIONSEGMENT_HPP
#define ANIMATIONSEGMENT_HPP

#include <QPixmap>
#include <QVector>

class AnimationSegment
{
public:

	AnimationSegment();

	AnimationSegment(const QString &dirName, int startIndex=0, int endIndex=-1);

    AnimationSegment(const QStringList &imageFileNames, int startIndex=0, int endIndex=-1);
	
	//If endIndex=-1, load from startIndex to the end of directory
	bool loadDirectory(const QString &dirName, int startIndex=0, int endIndex=-1);

	void loadFiles(const QStringList &imageFileNames, int startIndex=0, int endIndex=-1);

	int size() const;

	float length(float fps = 30.f) const;

	const QPixmap &pixmap(int i) const;

	bool required() const { return m_required; }
	void setRequired(bool r) { m_required = r; }

	bool loopable() const { return m_loopable; }
	void setLoopable(bool l) { m_loopable = l; }
		

protected:

	QVector<QPixmap> m_pixmaps;
	bool m_required;
	bool m_loopable;

};

#endif /* ! ANIMATIONSEGMENT_HPP */

#ifndef SIMPLESYNCHRONIZER_HPP
#define SIMPLESYNCHRONIZER_HPP

#include "BehaviorBlock.hpp"
#include "FacePlan.hpp"
#include "SpeechPlan.hpp"


class SimpleSynchronizer
{
public:
	SimpleSynchronizer();

	virtual bool add(const BehaviorBlock &bl, 
					 QVector<FacePlan> &facePlans,
					 QVector<SpeechPlan> &speechPlans,
					 float time);

protected:
	
};

#endif //SIMPLESYNCHRONIZER_HPP

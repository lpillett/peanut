
TEMPLATE = app
TARGET = ClientCompanion
INCLUDEPATH += .

CONFIG+=debug
#CONFIG+=release
CONFIG += c++11
CONFIG += warn_on
CONFIG += console

linux-g++|macx-clang++ {
QMAKE_CXXFLAGS += -Wall -Wextra
}

QT += widgets
#QT += svg
QT += multimedia
QT += websockets
android {
QT += androidextras
}

#RESOURCES += rsrc/rsrc.qrc
RESOURCES += rsrc/rsrc_global.qrc
#RESOURCES += rsrc/rsrc_face_animsF.qrc
#RESOURCES += rsrc/rsrc_face_animsM.qrc
#RESOURCES += rsrc/rsrc_face_animsN.qrc
#RESOURCES += rsrc/rsrc_face_animsN1.qrc
#RESOURCES += rsrc/rsrc_face_animsN2.qrc
#RESOURCES += rsrc/rsrc_face_animsN3.qrc
RESOURCES += rsrc/rsrc_face_animsL.qrc
RESOURCES += rsrc/rsrc_face_animsL1.qrc
RESOURCES += rsrc/rsrc_face_animsL2.qrc
RESOURCES += rsrc/rsrc_face_animsL3.qrc
RESOURCES += rsrc/rsrc_face_animsL4.qrc
RESOURCES += rsrc/rsrc_face_animsL5.qrc
RESOURCES += rsrc/rsrc_face_animsL6.qrc
RESOURCES += rsrc/rsrc_face_animsL7.qrc
RESOURCES += rsrc/rsrc_face_animsL8.qrc
RESOURCES += rsrc/rsrc_face_animsL9.qrc
RESOURCES += rsrc/rsrc_face_animsL10.qrc
RESOURCES += rsrc/rsrc_face_animsL11.qrc
RESOURCES += rsrc/rsrc_face_animsL12.qrc
RESOURCES += rsrc/rsrc_face_animsL13.qrc
RESOURCES += rsrc/rsrc_face_animsL14.qrc
RESOURCES += rsrc/rsrc_face_animsL15.qrc
RESOURCES += rsrc/rsrc_face_animsL16.qrc
RESOURCES += rsrc/rsrc_face_animsL17.qrc
RESOURCES += rsrc/rsrc_face_animsL18.qrc
RESOURCES += rsrc/rsrc_face_animsL19.qrc
RESOURCES += rsrc/rsrc_face_animsL20.qrc
RESOURCES += rsrc/rsrc_face_animsL21.qrc
RESOURCES += rsrc/rsrc_voice_fr_global.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Amelie.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Audrey.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Aurelie.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Chantal.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Nicolas.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Thomas.qrc
#RESOURCES += rsrc/rsrc_voice_fr_Google_fr.qrc
#RESOURCES += rsrc/rsrc_voice_en_global.qrc
#RESOURCES += rsrc/rsrc_voice_en_Alex.qrc
#RESOURCES += rsrc/rsrc_voice_en_Vicki.qrc
RESOURCES += rsrc/rsrc_voice_fr_Fabien.qrc
#RESOURCES += rsrc/rsrc_voice_en_Google_uk.qrc
#RESOURCES += rsrc/rsrc_voice_en_Google_us.qrc

#QMAKE_RESOURCE_FLAGS += -threshold 0 -compress 9
QMAKE_RESOURCE_FLAGS += -no-compress


# Input
HEADERS += MainWindow.hpp Widget.hpp AnimationSegment.hpp AnimationSequence.hpp PlanUnit.hpp LipSyncPhsegFiles.hpp FacePlan.hpp FacePlanner.hpp PixmapFacePlanner.hpp SpeechPlanner.hpp SoundPlayerSpeechPlanner.hpp Planner.hpp SimplePlanner.hpp SimpleSynchronizer.hpp BehaviorBlock.hpp Synchronization.hpp ControlPlan.hpp ControlPlanner.hpp SimpleControlPlanner.hpp ConfigurationWidget.hpp

SOURCES += main.cpp MainWindow.cpp Widget.cpp AnimationSegment.cpp AnimationSequence.cpp LipSyncPhsegFiles.cpp PixmapFacePlanner.cpp SoundPlayerSpeechPlanner.cpp SimplePlanner.cpp SimpleSynchronizer.cpp BehaviorBlock.cpp Synchronization.cpp SimpleControlPlanner.cpp ConfigurationWidget.cpp

macx {
QMAKE_MACOSX_DEPLOYMENT_TARGET=10.7
CONFIG += x86_64
QMAKE_CXXFLAGS += -stdlib=libc++ -std=c++11 
QMAKE_LDFLAGS += -macosx-version-min=$$QMAKE_MACOSX_DEPLOYMENT_TARGET
QMAKE_LDFLAGS += -stdlib=libc++ -std=c++11
}


DISTFILES += \
    android/AndroidManifest.xml \ 
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat \

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

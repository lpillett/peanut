#ifndef SOUNDPLAYERSPEECHPLANNER_HPP
#define SOUNDPLAYERSPEECHPLANNER_HPP

#include <memory>
#include <QMediaPlayer>
#include "SpeechPlanner.hpp"
#include "LipSync.hpp"


class SoundPlayerSpeechPlanner : public QObject, public SpeechPlanner
{
    Q_OBJECT

public:
	explicit SoundPlayerSpeechPlanner(std::shared_ptr<LipSync> lipSync);

	virtual SpeechPlan currentPlan() const override;

	virtual bool planForBehavior(const SpeechBehavior *b, SpeechPlan &sp, FacePlan &fp) const override;

	virtual void setCurrentPlan(const SpeechPlan &p) override;

	virtual void play(float currTime) override;


protected:
	void playSoundFile(const QString &filename);


protected slots:
    void mediaStatusChanged(QMediaPlayer::MediaStatus s);
    void error(QMediaPlayer::Error error);
    void stateChanged(QMediaPlayer::State state);

protected:
	SpeechPlan m_currentPlan;

	std::shared_ptr<LipSync> m_lipSync;

	QMediaPlayer m_player;

};


#endif /* ! SOUNDPLAYERSPEECHPLANNER_HPP */

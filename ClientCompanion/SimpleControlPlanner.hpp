#ifndef SIMPLECONTROLPLANNER_HPP
#define SIMPLECONTROLPLANNER_HPP


#include "ControlPlanner.hpp"
#include "ControlPlan.hpp"


class SimpleControlPlanner : public ControlPlanner
{
public:
	SimpleControlPlanner();

	virtual ControlPlan currentPlan() const override;

	virtual void setCurrentPlan(const ControlPlan &p) override;

	virtual void play(float currTime) override;

protected:
	ControlPlan m_currentPlan;

};


#endif /* ! SIMPLECONTROLPLANNER_HPP */

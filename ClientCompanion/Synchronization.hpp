#ifndef SYNCHRONIZATION_HPP
#define SYNCHRONIZATION_HPP


#include "Behavior.hpp"

class Synchronization
{
public:

	Synchronization();

	Synchronization(const Behavior *b1,
					Behavior::SyncPoint s1,
					const Behavior *b2,
					Behavior::SyncPoint s2,
					float offset = 0);
	
	void set(const Behavior *b1,
			 Behavior::SyncPoint s1,
			 const Behavior *b2,
			 Behavior::SyncPoint s2,
			 float offset = 0);
	
	bool operator==(const Synchronization &c) const
		{
			return m_b1 == c.m_b1 && m_s1 == c.m_s1 && m_b2 == c.m_b2 && m_s2 == c.m_s2 && m_offset == c.m_offset;
		}


	//check that Behaviors are not nullptr.
	bool isValid() const;

	const Behavior *behavior1() { return m_b1; }
	const Behavior *behavior1() const { return m_b1; }
	Behavior::SyncPoint syncPoint1() const { return m_s1; }
	const Behavior *behavior2() { return m_b2; }
	const Behavior *behavior2() const { return m_b2; }
	Behavior::SyncPoint syncPoint2() const { return m_s2; }
	float offset() const { return m_offset; }

	//swap behviors and syncPoints, and negate offset
	void reorder();

protected:
	const Behavior *m_b1;
	Behavior::SyncPoint m_s1;
	const Behavior *m_b2;
	Behavior::SyncPoint m_s2;
	float m_offset;
};


#endif /* ! SYNCHRONIZATION_HPP */

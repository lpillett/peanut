#ifndef LIPSYNC_HPP
#define LIPSYNC_HPP

#include "PlanUnit.hpp"

#include <QString>
#include "Plan.hpp"

class LipSync
{
public:
	
	virtual ~LipSync() {}
	
	//Get visemes from audio file
	virtual TrackPlan<FacePlanUnit> sync(const QString &audioFilename) = 0;
};


#endif /* LIPSYNC_HPP */

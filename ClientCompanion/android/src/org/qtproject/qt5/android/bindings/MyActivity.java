
package org.qtproject.qt5.android.bindings;

import android.os.Bundle;
import android.view.WindowManager.LayoutParams;
import org.qtproject.qt5.android.bindings.QtActivity;

public class MyActivity extends QtActivity
{

	/*
http://stackoverflow.com/questions/5712849/how-do-i-keep-the-screen-on-in-my-app
	 */

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN|android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	}
}

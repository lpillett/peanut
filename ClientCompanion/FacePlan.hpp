#ifndef FACEPLAN_HPP
#define FACEPLAN_HPP

#include <cmath>//floor

#include "Plan.hpp"

#include <QDebug> //DEBUG
#ifndef NDEBUG
#include <iostream> //DEBUG
#endif

typedef TrackPlan<FacePlanUnit> FaceTrackPlan;
//typedef Plan<FacePlanUnit> FacePlan;


class FacePlan : public Plan<FacePlanUnit>
{
public:


	/**
	 * Time before syncPoint index @a startIndex
	 */
	float timeBefore(int index)
		{
			float timeBefore = 0.f;
			for (FaceTrackPlan &ft : *this) {

				if (ft.empty())
					continue;
      
				float length = 0;
				for (int i=0; i<std::min(ft.size(), index); ++i) {
					length += ft[i]->length();
				}    
				if (length > timeBefore)
					timeBefore = length;
			}
			return timeBefore;
		}

	/**
	 * Time after syncPoint index @a index
	 */
	float timeAfter(int index)
		{
			float timeAfter = 0.f;
			for (FaceTrackPlan &ft : *this) {

				if (ft.empty())
					continue;
      
				float length = 0;
				for (int i=index; i<ft.size(); ++i) {
					length += ft[i]->length();
				}    
				if (length > timeAfter)
					timeAfter = length;
			}
			return timeAfter;
		}

    /**
     * Change startTime such as syncPoint @a index move to time @a time.
     */
    void shiftTo(int index, float time)
        {
        //we consider that on all FaceTrackPlans,
        // the @a index segment starts at the the same time.

            for (FaceTrackPlan &ft : *this) {

                if (ft.empty())
                    continue;

                if (index < ft.size()) {
                    assert(ft[0]->startTime() == this->startTime());
                    std::cerr<<"shiftTo("<<index<<", "<<time<<") & ft[index]->startTime="<<ft[index]->startTime()<<" => shiftOf("<<( time - ft[index]->startTime() )<<")\n";
                    shiftOf(time - ft[index]->startTime());
                    break;
                }
            }
        }

  
    /*
     Get length of fixed segments between @a startIndex and @a endIndex in @a fixedLength
     Get total (current) length of all segments between @a startIndex and @a endIndex in @a totalLength
     Get bumber of loopable segments in @a numLoopable
     */
    void getFixedLength(const FaceTrackPlan &ft, int startIndex, int endIndex,
                        float &fixedLength, float &totalLength, int &numLoopable) const {
        fixedLength = 0;
        totalLength = 0;
        numLoopable = 0;
        for (int i=startIndex; i<endIndex; ++i) {
            const FacePlanUnit *pu = ft[i];
            assert(pu);
            totalLength += pu->length();
            if (pu->required() == true && pu->loopable() == false) {
                fixedLength += pu->length();
            }
            if (pu->loopable()) {
                ++numLoopable;
            }
        }
    }

    void getFixedLength(int startIndex, int endIndex, float &fixedLength, float &totalLength, int &numLoopable) const {
        fixedLength = 0;
        totalLength = 0;
        numLoopable = 0;
        for (const FaceTrackPlan &ft : *this) {
            if (ft.empty())
                continue;
            float trackFixedLength, trackTotalLength;
            int trackNumLoopable;
            getFixedLength(ft, startIndex, endIndex, trackFixedLength, trackTotalLength, trackNumLoopable);
            fixedLength = std::max(fixedLength, trackFixedLength);
            totalLength = std::max(totalLength, trackTotalLength);
            numLoopable = std::max(numLoopable, trackNumLoopable);
            //Is it really this that we want ??
        }
    }



	/*
	  Extand/stretch loopable PlanUnits between @a startIndex and @a endIndex, 
	  to have all PlanUnits at @a startIndex starting at @a startTime and a total length of @a length.
	  If theses constraints can not be respected, return false. 

	*/
	bool stretch(float startTime, float len, int startIndex, int endIndex) {

    
		std::cerr<<"extend startTime="<<startTime<<" len="<<len<<" startIndex="<<startIndex<<" endIndex="<<endIndex<<"\n";

		//DEBUG
		{
			std::cerr<<" Before extend: st="<<this->startTime()<<" len="<<this->length()<<" et="<<this->endTime()<<"\n";
			int dbg_i_t = 0;
			for (FaceTrackPlan &ft : *this) {
				if (ft.empty())
					continue;
				for (int i=0; i<ft.size(); ++i) {
					FacePlanUnit *pu = ft[i];
					std::cerr<<" => track "<<dbg_i_t<<" pu "<<i<<" st="<<pu->startTime()<<" len="<<pu->length()<< " et="<<pu->endTime()<<" loop="<<pu->loopable()<<" required="<<pu->required()<<"\n";
				}
						
				++dbg_i_t;
			}
						
		}//DEBUG
    
		assert(checkContinuous());

		assert(startIndex < endIndex);
		assert(len > 0);

		//this->setStartTime(startTime);

		int dbg_i_t = 0;
		
		//float cStartTime = this->startTime();
		
		for (FaceTrackPlan &ft : *this) {

			if (ft.empty())
				continue;

			if (ft.size() < endIndex) {
				qDebug()<<" ft.size()="<<ft.size()<<" < endIndex="<<endIndex;
				return false;
			}

			const int tsz = ft.size();

#if 0
			float prevLength = 0;
			for (int i=0; i<startIndex; ++i) {
				prevLength += ft[i]->length();
			}
			std::cerr<<"  prevLength="<<prevLength<<" ft.startTime="<<ft.startTime()<<"\n";

			//assert(cStartTime <= ft.startTime());

			//std::cerr<<"  startTime="<<startTime<<" ft.startTime()="<<ft.startTime()<<" cStartTime="<<cStartTime<<" prevLength="<<prevLength<<"  (ft.startTime()-cStartTime -prevLength)="<<(ft.startTime()-cStartTime -prevLength)<<" => newStartTime=(startTime + ft.startTime()-cStartTime -prevLength)="<<(startTime + ft.startTime()-cStartTime -prevLength)<<"\n";
      
			//ft.setStartTime(startTime + (ft.startTime()-cStartTime -prevLength));
			ft.setStartTime(startTime - prevLength);

#else
			//move before
			float et = startTime;
			for (int i=startIndex-1; i>=0; --i) {
				ft[i]->setEndTime(et);
				et = ft[i]->startTime();
			}
			ft[startIndex]->setStartTime(startTime);
#endif
/*
			assert(checkContinuous());
		
			std::cerr<<"   => new startTime="<<ft.startTime()<<"\n";
			{//DEBUG
				for (int i=0; i<ft.size(); ++i) {
					FacePlanUnit *pu = ft[i];
					std::cerr<<" =>  pu "<<i<<"/"<<ft.size()<<" st="<<pu->startTime()<<" len="<<pu->length()<< " et="<<pu->endTime()<<"\n";
					if (i == startIndex)
						std::cerr<<"   diff="<<fabs(pu->startTime()-startTime)<<"\n";
				}

			}//DEBUG
*/

			assert(endIndex <= tsz);

			float fixedLength = 0;
            float trackTotalLength = 0;
			int numLoopable = 0;
            getFixedLength(ft, startIndex, endIndex, fixedLength, trackTotalLength, numLoopable);

            if (fixedLength > len) {
				qDebug()<<" fixedLen="<<fixedLength<<" >  len="<<len;
				return false;
			}
			if (numLoopable == 0) {
				qDebug()<<" numLoopable="<<0;
				return false;
			}

			//qDebug()<<" fixedLen="<<fixedLength<<" len="<<len;
			
			assert(fixedLength <= len);
			assert(numLoopable > 0);
			float extLength = (len-fixedLength);

			//TODO: check that if (fixedLength==length && we have only required PUs), then we mustExit !

			//TODO: if startIndex+1==endIndex (which will always be the case ???)
			// we could simplify the code !!!

			float st = startTime;
			
			//qDebug()<<" ft[startIndex]->startTime()="<<ft[startIndex]->startTime()<<" st="<<st<<" diff="<<fabs(ft[startIndex]->startTime() - st);
			//qDebug()<<" ft.startTime()="<<ft.startTime()<<" st="<<st<<" diff="<<fabs(ft.startTime() - st);

			
#ifndef NDEBUG
			const float eps = 0.0001;
			if (! (fabs(ft[startIndex]->startTime() - st) <= eps) ) {
				std::cerr<<" ft[startIndex]->startTime()="<<ft[startIndex]->startTime()<<" st="<<st<<" diff="<<fabs(ft[startIndex]->startTime() - st)<<"\n";
			}
#endif //NDEBUG

			//qDebug()<<"numLoopable="<<numLoopable<<" extLength="<<extLength<<" extLength/numLoopable="<<extLength/numLoopable<<"\n";
      
			assert(fabs(ft[startIndex]->startTime() - st) <= eps);
			for (int i=startIndex; i<endIndex; ++i) {
				FacePlanUnit *pu = ft[i];
				assert(pu);
				pu->setStartTime(st);
				if (pu->loopable()) {
			  
					float loopLength = extLength/numLoopable;
					float newLength = loopLength;
					if (pu->required()) {
						float l = pu->length(); //length of one step of loop
						if (l > extLength) {
							//not enough length to respect constraint
							qDebug()<<"not enough length to respect constraint : l="<<l<<" > extLength="<<extLength;
							return false;
						}
						newLength = loopLength; //floor(loopLength/l)*l;

						//qDebug()<<" l="<<l<<" loopLength/l="<<loopLength/l<<" floor(loopLength/l)="<<floor(loopLength/l)<<" floor(loopLength/l)*l="<<floor(loopLength/l)*l;


					}
					pu->setLength(newLength);
					//qDebug()<<" pu "<<i<<" new len="<<pu->length() ;

					//if (newLength > extLength)
					//qDebug()<<"newLength="<<newLength<<" extLength="<<extLength<<" diff="<<fabs(newLength-extLength);

					assert(newLength <= extLength);
					extLength -= newLength;
					--numLoopable;
				}
				st = pu->endTime();
			}

			if (extLength > std::numeric_limits<float>::epsilon())
			{
				qDebug()<<"not enough length to respect constraint : extLength="<<extLength<<" > eps";
				return false;
			}

			if (endIndex < tsz) { //shift all remaining PUs on the track
				assert(endIndex > 0);
				float endTime = ft[endIndex-1]->endTime();
				for (int i=endIndex; i<ft.size(); ++i) {
					FacePlanUnit *pu = ft[i];
					pu->setStartTime(endTime);
					endTime = pu->startTime();

					//qDebug()<<" => track "<<dbg_i_t<<" pu "<<i<<" st="<<pu->startTime()<<" len="<<pu->length()<< " et="<<pu->endTime();
				}
			}



			++dbg_i_t;

		}

		/**/
		//DEBUG
		{
			std::cerr<<" After extend:\n";
			int dbg_i_t = 0;
			for (FaceTrackPlan &ft : *this) {
				
				if (ft.empty())
					continue;
				for (int i=0; i<ft.size(); ++i) {
					FacePlanUnit *pu = ft[i];
					std::cerr<<" => track "<<dbg_i_t<<" pu "<<i<<" st="<<pu->startTime()<<" len="<<pu->length()<< " et="<<pu->endTime()<<"\n";
				}
				
				++dbg_i_t;
			}
		}//DEBUG
		/**/
		assert(checkContinuous());


		return true;

	}


	bool checkContinuous() const {
		
		bool res = true;

        const float eps = 0.09; //200*std::numeric_limits<float>::epsilon();

		int fti = 0;
		for (const FaceTrackPlan &ft : *this) {
			
            if (! ft.empty())  {
                const float st0 = ft[0]->startTime();
                for (int i=1; i<ft.size(); ++i) { //start from 1
                    const float diff = fabs((ft[i]->startTime()-st0) - (ft[i-1]->endTime()-st0) );
                    if (diff > eps) {
                        std::cerr<<"track "<<fti<<" non continuous: pu["<<i-1<<"] et="<<ft[i-1]->endTime()<<" != pu["<<i<<"] st="<<ft[i]->startTime()<<" (diff="<<fabs(ft[i]->startTime()-ft[i-1]->endTime())<<"\n";
                        std::cerr<<"                                et-st0="<<(ft[i-1]->endTime()-st0)<<" != pu["<<i<<"] st-st0="<<(ft[i]->startTime()-st0)<<" (diff="<<diff<<" > eps="<<eps<<")\n";
                        res &= false;
                    }
                }
			}
			++fti;
		}
		return res;
	}

	

};



#endif /* ! FACEPLAN_HPP */

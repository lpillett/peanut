
#include "SoundPlayerSpeechPlanner.hpp"

#include <cassert>
#include <QDebug>


#include "PixmapFacePlanner.hpp" //MOUTH


/*
//Cf http://doc.qt.io/qt-5/audiooverview.html
//Qt recommands to use QSoundEffect over QSound

//QSoundEffect probably load the whole file in memory
//(so could be faster) but only works for WAV files
//QMediaPlayer should support other file formats (ogg & mp3 for example)

//libqt5multimedia5 & libqt5multimedia5-plugins must be installed !?


	//QMediaPlayer/QSoundPlayer must not be destroyed before the sound is played
	//Thus can not be a local variable.


 */


SoundPlayerSpeechPlanner::SoundPlayerSpeechPlanner(std::shared_ptr<LipSync> lipSync)
	: m_lipSync(lipSync),
	  m_player(nullptr, QMediaPlayer::LowLatency)
{

#ifndef NDEBUG
    connect(&m_player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),
            this, SLOT(mediaStatusChanged(QMediaPlayer::MediaStatus)));
    connect(&m_player, SIGNAL(error(QMediaPlayer::Error)),
            this, SLOT(error(QMediaPlayer::Error)));
    connect(&m_player, SIGNAL(stateChanged(QMediaPlayer::State)),
            this, SLOT(stateChanged(QMediaPlayer::State)));
#endif //NDEBUG
}

SpeechPlan SoundPlayerSpeechPlanner::currentPlan() const
{
	return m_currentPlan;
}

static
bool
checkSorted(const SpeechTrackPlan &t)
{
	bool sorted = true;
	SpeechTrackPlan::const_iterator it = t.begin();
	SpeechTrackPlan::const_iterator itNext = t.begin()+1;
	if (it != t.end() && itNext!=t.end()) 
	{
		for (; itNext!=t.end(); ++it, ++itNext) 
		{
			if ((*it)->endTime() > (*itNext)->startTime())
				return false;
		}
	}

	return sorted;
}


bool SoundPlayerSpeechPlanner::planForBehavior(const SpeechBehavior *b, SpeechPlan &sp, FacePlan &fp) const
{
	if (b->speechType() == SoundFile) {

		QString filename = static_cast<const SoundFileSpeechBehavior *>(b)->filename();

		TrackPlan<FacePlanUnit> mouthUnits = m_lipSync->sync(filename);

		if (! mouthUnits.empty()) {
			
			//TODO: check that mouthUnits ends with silence/Viseme_00 
			
			if (fp.size() <= PixmapFacePlanner::MOUTH)
				fp.resize(PixmapFacePlanner::MOUTH+1);
			fp[PixmapFacePlanner::MOUTH] = mouthUnits;
			
		
			float startTime = mouthUnits.front()->startTime();
			float endTime = mouthUnits.back()->endTime();
			float length = endTime-startTime;
			float intensity = 1.0f;
			
			TrackPlan<SpeechPlanUnit> stp;
			stp.push_back(new SoundFilePlanUnit(filename, intensity, startTime, length));
			
			assert(checkSorted(stp));
			
			sp.resize(1);
			sp[0] = stp;
			
		}
		else 
		{
			qDebug()<<"Warning: no FacePlanUnit found for sound file: "<<filename;
			return false;
		}
	}
	else 
	{
		qDebug()<<"Warning: SpeechBehaviorType other than SoundFile not handled";
		return false;
	}
	
	return true;
}



void SoundPlayerSpeechPlanner::setCurrentPlan(const SpeechPlan &p) 
{
	for (const SpeechTrackPlan &stp : p)
		assert(checkSorted(stp));

	m_currentPlan = p;

	//TODO:preload next SoundFilePlanUnit file if any ???

}

void SoundPlayerSpeechPlanner::play(float currTime) 
{
	if (! m_currentPlan.empty()) {

		//Play only first track
		//We are not able to play several sounds simultaneously for now.
		int i = 0;

		SpeechTrackPlan &stp = m_currentPlan[i];

		//qDebug()<<"@@@ stp.size()="<<stp.size();


		SpeechTrackPlan::iterator it;
		for (it=stp.begin(); it!=stp.end(); ) 
		{
			const SpeechPlanUnit *a = *it;
			
			if (a->length() != -1 && a->endTime() < currTime) //it is possible that we never played this animation
			{
			  qDebug()<<"@@@ delete a="<<a<<" st="<<a->startTime()<<" l="<<a->length()<<" et="<<a->endTime();

				delete *it;
				it = stp.erase(it);
			}
			else 
			{
				if (a->startTime() > currTime) 
				{
					//TODO:preload next SoundFilePlanUnit file if any
					
					break;
				}
				else 
				{
					assert(a->startTime() <= currTime && (currTime <= a->endTime() || a->length() == -1));
					
					if (a->speechType() == SoundFile)
					{
						const QString &soundFilename = static_cast<const SoundFilePlanUnit *>(a)->filename();
						playSoundFile(soundFilename);
						
						delete *it;
						it = stp.erase(it);
					}
					else 
					{
						qDebug()<<"Warning: unhandled SpeechPlanUnit speechType";
					}
				}
			}
		}
	}
	
}

void SoundPlayerSpeechPlanner::playSoundFile(const QString &filename)
{
	qDebug()<<"SoundPlayerSpeechPlanner::playSoundFile() : "<<filename;

	//m_player.setMedia(QUrl::fromLocalFile(filename));
	m_player.setMedia(QUrl("qrc"+filename));
	m_player.setVolume(100);
	m_player.play();
}

void SoundPlayerSpeechPlanner::mediaStatusChanged(QMediaPlayer::MediaStatus s)
{
    if (s == QMediaPlayer::UnknownMediaStatus) {
        qDebug()<<"Warning: unknown media status";
    }
    else if (s == QMediaPlayer::NoMedia) {
        qDebug()<<"Warning: no media";
    }
    else if (s == QMediaPlayer::LoadingMedia) {
        qDebug()<<"Warning: loading media";
    }
    else if (s == QMediaPlayer::LoadedMedia) {
        qDebug()<<"Warning: loaded media";
    }
    else if (s == QMediaPlayer::StalledMedia) {
        qDebug()<<"Warning: stalled media";
    }
    else if (s == QMediaPlayer::BufferingMedia) {
        qDebug()<<"Warning: buffering media";
    }
    else if (s == QMediaPlayer::BufferedMedia) {
        qDebug()<<"Warning: buffered media";
    }
    else if (s == QMediaPlayer::EndOfMedia) {
        qDebug()<<"Warning: end of media";
    }
    else if (s == QMediaPlayer::InvalidMedia) {
        qDebug()<<"Warning: invalid media";
    }
}

void SoundPlayerSpeechPlanner::error(QMediaPlayer::Error error)
{
    if (error == QMediaPlayer::NoError) {
        qDebug()<<"NoError";
    }
    else if (error == QMediaPlayer::ResourceError) {
        qDebug()<<"ResourceError";
    }
    else if (error == QMediaPlayer::FormatError) {
        qDebug()<<"FormatError";
    }
    else if (error == QMediaPlayer::NetworkError) {
        qDebug()<<"NetworkError";
    }
    else if (error == QMediaPlayer::AccessDeniedError) {
        qDebug()<<"AccessDeniedError";
    }
    else if (error == QMediaPlayer::ServiceMissingError) {
        qDebug()<<"ServiceMissingError";
    }

}
void SoundPlayerSpeechPlanner::stateChanged(QMediaPlayer::State state)
{
    if (state == QMediaPlayer::StoppedState) {
        qDebug()<<"state : StoppedState";
    }
    else if (state == QMediaPlayer::PlayingState) {
        qDebug()<<"state : PlayingState";
    }
    else if (state == QMediaPlayer::PausedState) {
        qDebug()<<"state : PausedState";
    }
}

#include "SimpleSynchronizer.hpp"

#include <cassert>
#include <iostream>

/*
  TODO: verifier qu'il n'y a pas des synchros en double !!!

 */


SimpleSynchronizer::SimpleSynchronizer()
{

}

static
float
getStartTime(int ind,
	  QVector<FacePlan> &facePlans,
	  QVector<SpeechPlan> &speechPlans)
{
  assert(ind < facePlans.size());
  assert(ind < speechPlans.size());
  const bool emptyF = facePlans[ind].empty();
  const bool emptyS = speechPlans[ind].empty();
  if (! emptyF && ! emptyS) {
    return std::min(facePlans[ind].startTime(), speechPlans[ind].startTime());
  }
  else if (! emptyF) {
    return facePlans[ind].startTime();
  }
  else if (! emptyS) {
    return speechPlans[ind].startTime();
  }
  else {
    std::cerr<<"Warning: both facePlans && speechPlans are empty at index "<<ind<<"\n";
    assert(false);
    return 0.f;
  }
}

static
float
getEndTime(int ind,
	  QVector<FacePlan> &facePlans,
	  QVector<SpeechPlan> &speechPlans)
{
  assert(ind < facePlans.size());
  assert(ind < speechPlans.size());
  const bool emptyF = facePlans[ind].empty();
  const bool emptyS = speechPlans[ind].empty();
  if (! emptyF && ! emptyS) {
    return std::max(facePlans[ind].endTime(), speechPlans[ind].endTime());
  }
  else if (! emptyF) {
    return facePlans[ind].endTime();
  }
  else if (! emptyS) {
    return speechPlans[ind].endTime();
  }
  else {
    std::cerr<<"Warning: both facePlans && speechPlans are empty at index "<<ind<<"\n";
    assert(false);
    return 0.f;
  }
}

static
void
extendLength(int ind,
             QVector<FacePlan> &facePlans,
             QVector<SpeechPlan> &speechPlans,
             float lengthExtension)
{
  assert(ind < facePlans.size());
  assert(ind < speechPlans.size());

  const FacePlan &fp = facePlans[ind];
  const SpeechPlan &sp = speechPlans[ind];

  //keep last plan units
  std::vector<FacePlanUnit *> faceUnits;
  for (const auto &t : fp) {
    if (! t.empty()) {
        FacePlanUnit *pu = t.back();
        if (faceUnits.empty()) {
             faceUnits.push_back(pu);
        }
        else {
             const float currMaxTime = faceUnits.back()->endTime();
             const float trackMaxTime = pu->endTime();
             if (trackMaxTime > currMaxTime) {
                faceUnits.clear();
                faceUnits.push_back(pu);
             }
             else if (trackMaxTime == currMaxTime) {
                faceUnits.push_back(pu);
             }
        }
    }
  }

  std::vector<SpeechPlanUnit *> speechUnits;
  for (const auto &t : sp) {
    if (! t.empty()) {
        SpeechPlanUnit *pu = t.back();
        if (speechUnits.empty()) {
             speechUnits.push_back(pu);
        }
        else {
             const float currMaxTime = speechUnits.back()->endTime();
             const float trackMaxTime = pu->endTime();
             if (trackMaxTime > currMaxTime) {
                speechUnits.clear();
                speechUnits.push_back(pu);
             }
             else if (trackMaxTime == currMaxTime) {
                speechUnits.push_back(pu);
             }
        }
    }
  }

  if (! faceUnits.empty() && ! speechUnits.empty()) {
      float faceMaxTime = faceUnits.back()->endTime();
      float speechMaxTime = speechUnits.back()->endTime();
      const float eps = 20*std::numeric_limits<float>::epsilon();
      if (fabs(faceMaxTime-speechMaxTime) < eps) {
          for (auto pu : faceUnits) {
              std::cerr<<"  extend faceUnit from st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime();
              pu->setLength(pu->length()+lengthExtension);
              std::cerr<<" to st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime()<<"\n";
          }
          for (auto pu : speechUnits) {
              std::cerr<<"  extend speechUnit from st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime();
              pu->setLength(pu->length()+lengthExtension);
              std::cerr<<" to st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime()<<"\n";
          }
      }
      else if (faceMaxTime > speechMaxTime) {
          for (auto pu : faceUnits) {
              std::cerr<<"  extend faceUnit from st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime();
              pu->setLength(pu->length()+lengthExtension);
              std::cerr<<" to st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime()<<"\n";
          }
      }
      else if (faceMaxTime < speechMaxTime) {
          for (auto pu : speechUnits) {
              std::cerr<<"  extend speechUnit from st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime();
              pu->setLength(pu->length()+lengthExtension);
              std::cerr<<" to st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime()<<"\n";
          }
      }
   }
  else if (! faceUnits.empty())  {
      for (auto pu : faceUnits)
          pu->setLength(pu->length()+lengthExtension);
  }
  else if (! speechUnits.empty())  {
      for (auto pu : speechUnits)
          pu->setLength(pu->length()+lengthExtension);
  }
  else  {
      std::cerr<<"Warning: nothing to extend at index="<<ind<<"\n";
      assert(false);
  }
}

static
bool
checkFacePlanContinuity(int i, const QVector<FacePlan> &facePlans)
{
	assert(i < facePlans.size());

    const float eps = 0.0055; //10*std::numeric_limits<float>::epsilon();

	bool res = true;

	if (! facePlans[i].empty()) {
		
		const FacePlan &fp = facePlans[i];
		int ti = 0;
		for (const auto &ft : fp) {
			const int sz = ft.size();
			for (int j=1; j<sz; ++j) { //start from 1
				if (fabs(ft[j-1]->endTime()- ft[j]->startTime()) > eps) {
					std::cerr<<"FacePlan "<<i<<" track "<<ti<<" pu["<<j-1<<"]->et="<<ft[j-1]->endTime()<<" != pu["<<j<<"]->st="<<ft[j]->startTime()<<" (diff="<<fabs(ft[j-1]->endTime()- ft[j]->startTime())<<")\n";
					res = false;
				}
			}
		}
		++ti;
	}

	return res;
}

static
bool
checkFacePlansContinuity(const QVector<FacePlan> &facePlans)
{
  //const float eps = 10*std::numeric_limits<float>::epsilon();

	bool res = true;
	for (int i=0; i<facePlans.size(); ++i) {
		res |= checkFacePlanContinuity(i, facePlans);
	}
	
	return res;
}

static
bool
checkSyncSpeechFace(const QVector<FacePlan> &facePlans,
					const QVector<SpeechPlan> &speechPlans)
{
  assert(facePlans.size() == speechPlans.size());
  
  const float eps = 0.0003; //2*std::numeric_limits<float>::epsilon();

  for (int i=0; i<speechPlans.size(); ++i) {
	  if (! facePlans[i].empty() && ! speechPlans[i].empty()) {
		  if (fabs(facePlans[i].startTime()-speechPlans[i].startTime()) > eps) {
			  std::cerr<<"facePlans["<<i<<"].startTime()="<<facePlans[i].startTime()<<" != speechPlans["<<i<<"].startTime()="<<speechPlans[i].startTime()<<"  (diff="<<fabs(facePlans[i].startTime()-speechPlans[i].startTime()) <<")\n";
			  return false;
		  }
		  if (fabs(facePlans[i].endTime()-speechPlans[i].endTime()) > eps) {
			  std::cerr<<"facePlans["<<i<<"].endTime()="<<facePlans[i].endTime()<<" != speechPlans["<<i<<"].endTime()="<<speechPlans[i].endTime()<<"(diff="<<fabs(facePlans[i].endTime()-speechPlans[i].endTime()) <<")\n";
			  return false;
		  }
	  }
  }
  return true;
}


struct Node
{
  Node() :
    before(nullptr),
    after(nullptr),
    behaviorIndex(-1),
    offsetAfter(0)
  {}

  Node *before;
  Node *after;
  int behaviorIndex;
  float offsetAfter;
};

struct Chains
{
  QVector<Node> nodes;
  QVector<Node *> starts;

  explicit Chains(int nbBehaviors)
    : nodes(), starts()
  {
    nodes.reserve(nbBehaviors); 
    //Warning: We want no re-allocations !!!
    // otherwise we will have Nodes pointing on free memory !
			
  }


  void insertAlone(int ib) {
    assert(indexStart(ib) == -1);

    nodes.push_back(Node());
    Node *n = &(nodes.back());
    n->behaviorIndex = ib;
    starts.push_back(n);
  }
  
  
  /**
     We have a synchro between behaviors at indexes @a ib1 and @a ib2 :
     @a ib2 comes after @a ib1.

     Insert them in the tracks (graph of precedences).
  */
  bool insert(int ib1, int ib2, float offset) {
		
    std::cerr<<"insert ib1="<<ib1<<" ib2="<<ib2<<" off="<<offset<<"\n";

    int indexStartB1 = -1;
    int indexStartB2 = -1;
    Node *n1 = nullptr;
    Node *n2 = nullptr;
    for (int k=0; k<starts.size(); ++k) {
      //std::cerr<<" track "<<k<<" start="<<starts[k]<<"\n";
      for (Node *n = starts[k]; n != nullptr ; n=n->after) {
	//std::cerr<<" track "<<k<<" n="<<n<<"\n";
	assert(n != nullptr);
	if (n->behaviorIndex == ib1) {
	  std::cerr<<" track "<<k<<" n->behaviorIndex="<<n->behaviorIndex<<"==ib1  n1="<<n1<<"\n";
	  assert(n1 == nullptr);
	  indexStartB1 = k;
	  n1 = n;
	  if (indexStartB2 != -1)
	    break;
	}
	if (n->behaviorIndex == ib2) {
	  std::cerr<<" track "<<k<<" n->behaviorIndex="<<n->behaviorIndex<<"==ib2  n2="<<n2<<"\n";
	  assert(n2 == nullptr);
	  indexStartB2 = k;
	  n2 = n;
	  if (indexStartB1 != -1)
	    break;
	}
      }
    }
			
    if (n1 != nullptr && n1->after != nullptr) {
      std::cerr<<"ERROR: there is overlap...\n";
      return false;
    }
    if (n2 != nullptr && n2->before != nullptr) {
      std::cerr<<"ERROR: there is overlap2...\n";
      return false;
    }

    if (n1 != nullptr && n2 != nullptr) {
      if (indexStartB1 == indexStartB2) {
	std::cerr<<"ERROR: loop in constraints\n";
	return false;
      }
      //std::cerr<<" both nodes already in track\n";

      assert(indexStartB1 != -1);
      assert(indexStartB2 != -1);
      assert(indexStartB1 != indexStartB2);
      assert(starts[indexStartB2] == n2); //n2 necessary first as before==nullptr
      assert(n1->behaviorIndex == ib1);
      assert(n2->behaviorIndex == ib2);
      n1->after = n2;
      n2->before = n1;
      starts[indexStartB2] = nullptr;
    }
    else if (n1 != nullptr) {
      //found n1 but not n2

      std::cerr<<"n1 found but not n2\n";
      assert(indexStartB1 != -1);
      assert(indexStartB2 == -1);
      assert(n2 == nullptr);
      nodes.push_back(Node());
      n2 = &nodes[nodes.size()-1];
      n1->after = n2;
      n2->before = n1;
      n2->behaviorIndex = ib2;
      assert(n1->behaviorIndex == ib1);
      assert(n2->behaviorIndex == ib2);
    }
    else if (n2 != nullptr) {
      //found n2 but not n1

      std::cerr<<"n2 found but not n1\n";
      assert(indexStartB1 == -1);
      assert(indexStartB2 != -1);
      assert(n1 == nullptr);
      nodes.push_back(Node());
      n1 = &nodes[nodes.size()-1];
      n1->offsetAfter = offset;
      n1->after = n2;
      n2->before = n1;
      n1->behaviorIndex = ib1;
      assert(starts[indexStartB2] == n2);//n2 necessary first as before==nullptr
      starts[indexStartB2] = n1;
      assert(n1->behaviorIndex == ib1);
      assert(n2->behaviorIndex == ib2);
    }
    else {
      //found neither n1 nor n2

      std::cerr<<" neither n1 nor n2\n";
      assert(indexStartB1 == -1);
      assert(indexStartB2 == -1);
      nodes.push_back(Node());
      n1 = &(nodes.back());
      nodes.push_back(Node());
      n2 = &(nodes.back());
      n1->offsetAfter = offset;
      n1->after = n2;
      n2->before = n1;
      n1->behaviorIndex = ib1;
      n2->behaviorIndex = ib2;
      //indexStartB1 = starts.size();
      starts.push_back(n1);
      assert(n1->behaviorIndex == ib1);
      assert(n2->behaviorIndex == ib2);
    }	
    return true;
  }

  /**
    Return index of start for behavior at index @a ib
    or -1 if not found.
   */
  int indexStart(int ib) const
  {
    int indexStart = -1;
    for (int k=0; k<starts.size(); ++k) {
      for (Node *n = starts[k]; n != nullptr ; n=n->after) {
	assert(n != nullptr);
	if (n->behaviorIndex == ib) {
	  indexStart = k;
	  break;
	}
      }
    }
    return indexStart;
  }

  void insertMissings(int nbBehaviors)
  {
    assert(nbBehaviors <= nodes.capacity());

    QVector<bool> present(nbBehaviors, false);
    for (int i=0; i<starts.size(); ++i) {
      for (Node *n = starts[i]; n!= nullptr; n=n->after) {
	const int ind = n->behaviorIndex;
	present[ind] = true;
      }
    }
    for (int i=0; i<nbBehaviors; ++i) {
      if (! present[i]) {
	insertAlone(i);
      }
    }    
  }

  float startTime(int indexStart,
				  const QVector<FacePlan> &facePlans,
				  const QVector<SpeechPlan> &speechPlans)
  {
    assert(indexStart >=0 && indexStart < starts.size());
    const Node *n=starts[indexStart];
    const int ind = n->behaviorIndex;
    assert(ind>=0 && ind<facePlans.size() && speechPlans.size());
    float st = startTime(ind, facePlans, speechPlans);
    return st;
  }

  float endTime(int indexStart,
		const QVector<FacePlan> &facePlans,
		const QVector<SpeechPlan> &speechPlans) const 
  {
    assert(indexStart >=0 && indexStart < starts.size()); 
    const Node *l = starts[indexStart];
    for (const Node *n=l; n!=nullptr; n=n->after)
      l = n;
    assert(l);
    const int ind = l->behaviorIndex;
    assert(ind>=0 && ind<facePlans.size() && speechPlans.size());
    const float et = endTime(ind, facePlans, speechPlans);
    return et;
  }

  void shiftOf(int indexStart, float shift,
			   QVector<FacePlan> &facePlans,
			   QVector<SpeechPlan> &speechPlans)
  {
    assert(indexStart >=0 && indexStart < starts.size()); 
    for (Node *n=starts[indexStart]; n!=nullptr; n=n->after) {
      const int ind = n->behaviorIndex;
      assert(ind>=0 && ind<facePlans.size() && speechPlans.size());
      facePlans[ind].shiftOf(shift);
      speechPlans[ind].shiftOf(shift);
    }
  }

  /**
     Rigidify chain,
     or move all nodes of a chain after the previous one.
   */
  void rigidify(float newStartTime,
				QVector<FacePlan> &facePlans,
				QVector<SpeechPlan> &speechPlans)
  {
    const int nbChains = starts.size();
    for (int i=0; i<nbChains; ++i) {
      float startTime = newStartTime;
      for (Node *n = starts[i]; n!= nullptr; n=n->after) {
		  const int ind = n->behaviorIndex;
		  assert(ind < facePlans.size()); assert(ind < speechPlans.size());
		  std::cerr<<"rigidify move "<<ind<<" to st="<<startTime<<"\n";
		  //TODO: should we consider that facePlans[ind] & speechPlans[ind] do not have the same startTime ???

		  if (! facePlans[ind].empty())
			  std::cerr<<"  before: facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
		  if (! speechPlans[ind].empty())
			  std::cerr<<"  before: speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";


		  facePlans[ind].setStartTime(startTime);
		  speechPlans[ind].setStartTime(startTime);
		  if (! facePlans[ind].empty())
			  std::cerr<<"  after: facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
		  if (! speechPlans[ind].empty())
			  std::cerr<<"  after: speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
		  startTime = getEndTime(ind, facePlans, speechPlans);
      }
    }
  }
  
	
};

/*
  TODO: this code is very fragile !
  We handle a very limited number of cases !
  (Shall we use a Constraint Solver ? )

*/

/*
  We have some constraints :
  - we cannot have overlaps between SpeechPlans 
    (because we cannot play several sound files simultaneously)
    Overlaps between SpeechPlans & FacePlans or between FacePlans are ok.
  - SpeechPlans can only be synchronized by their Start or End syncPoints.
  - The FacePlan corresponding to a SpeechPlan must be empty or have the same length
  ( i.e., (speechPlans[i].empty() || facePlans[i].size() == speechPlans[i].size() || facePlans.empty) ).

  - FacePlans are extensible/stretchable

  - If a FacePlan is not overlapping with at least one SpeechPlan, it means that it is synchronized by Start or End
*/ 

typedef QHash<const Behavior*, int> BehaviorToIndex;

bool
fulfillExtremitiesConstraints(const BehaviorToIndex &b2i,
							  const BehaviorBlock &bl, 
							  QVector<FacePlan> &facePlans,
							  QVector<SpeechPlan> &speechPlans,
							  float currTime)
{
  assert(facePlans.size() == speechPlans.size());
  assert((int)bl.numberOfBehaviors() == facePlans.size());

  const int nbBehaviors = bl.numberOfBehaviors();
  const int nbSyncs = bl.numberOfSynchronizations();


  {//DEBUG

	  std::cerr<<facePlans.size()<<" facePlans : \n";
	  for (int j=0; j<facePlans.size(); ++j) {
		  if (! facePlans[j].empty()) {
			  std::cerr<<" "<<j<<" : st="<<facePlans[j].startTime()<<" et="<<facePlans[j].endTime()<<"\n";
		  }
		  else {
			  std::cerr<<" "<<j<<" : empty !\n";
		  }
	  }
	  std::cerr<<speechPlans.size()<<" speechPlans : \n";
	  for (int j=0; j<speechPlans.size(); ++j) {
		  if (! speechPlans[j].empty()) {
			  std::cerr<<" "<<j<<" : st="<<speechPlans[j].startTime()<<" et="<<speechPlans[j].endTime()<<"\n";
		  }
		  else {
			  std::cerr<<" "<<j<<" : empty !\n";
		  }
	  }


    for (int j=0; j<nbSyncs; ++j) 
      {
	const Synchronization &s = bl.synchronization(j);
	const Behavior *b1 = s.behavior1();
	const Behavior *b2 = s.behavior2();
	const int ib1 = b2i.value(b1, -1);
	const int ib2 = b2i.value(b2, -1);
	std::cerr<<"Synchro["<<j<<"] ib1="<<ib1<<" spt1="<<s.syncPoint1()<<" -- ib2="<<ib2<<" spt2="<<s.syncPoint2()<<"\n";
      }
    
  }//DEBUG
  assert(checkSyncSpeechFace(facePlans, speechPlans));
  assert(checkFacePlansContinuity(facePlans));

  
  //Gather facePlans overlapping with a SpeechPlan or another FacePlan
  // (non-overlapping facePlans can be considered having a fixed size; : i.e., will not be stretched)
  QVector<bool> facePlanHasOverlap(nbBehaviors, false);
  for (int j=0; j<nbSyncs; ++j) 
    {
      const Synchronization &s = bl.synchronization(j);

      const Behavior *b1 = s.behavior1();
      const Behavior *b2 = s.behavior2();
		
      int ib1 = b2i.value(b1, -1);
      int ib2 = b2i.value(b2, -1);
      assert(ib1 != -1); //otherwise, Synchronization is invalid
      assert(ib2 != -1);
      assert(ib1 < nbBehaviors && ib2 < nbBehaviors);
		
      const bool isSpeech1 = !speechPlans[ib1].empty();
      const bool isSpeech2 = !speechPlans[ib2].empty();

      //std::cerr<<"isSpeech1="<<isSpeech1<<" isSpeech2="<<isSpeech2;

      if (isSpeech1 && !isSpeech2) {
	//check intersection between SpeechPlan & FacePlan
	std::cerr<<"ib1="<<ib1<<" ib2="<<ib2<<" isSpeech1 && !isSpeech2: s.syncPoint1()="<<s.syncPoint1()<<" s.syncPoint2()="<<s.syncPoint2()<<"\n";
			
	if (! ((s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End)
	       || (s.syncPoint1() == Behavior::End && s.syncPoint2() == Behavior::Start)) )
	  {
	    facePlanHasOverlap[ib2] = true;
	  }
      }
      else if (! isSpeech1 && isSpeech2) {
	//check intersection between SpeechPlan & FacePlan
	std::cerr<<"ib1="<<ib1<<" ib2="<<ib2<<" !isSpeech1 && isSpeech2: s.syncPoint1()="<<s.syncPoint1()<<" s.syncPoint2()="<<s.syncPoint2()<<"\n";

	if (! ((s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End)
	       || (s.syncPoint1() == Behavior::End && s.syncPoint2() == Behavior::Start)) )
	  {
	    facePlanHasOverlap[ib1] = true;
	  }
      }
      else if (! isSpeech1 && ! isSpeech2) {
	//check intersection between two FacePlans
	if ( s.syncPoint1() == s.syncPoint2() 
	     || ((! (s.syncPoint1() == Behavior::Start || s.syncPoint1() == Behavior::End)) 
		 || (! (s.syncPoint2() == Behavior::Start || s.syncPoint2() == Behavior::End))) )
	  {
	    facePlanHasOverlap[ib1] = true;
	    facePlanHasOverlap[ib2] = true;
	  }
      }

    }

  
  {//DEBUG
    for (int i=0; i<facePlanHasOverlap.size(); ++i) {
      std::cerr<<"facePlanHasOverlap["<<i<<"]="<<facePlanHasOverlap[i]<<"\n";
    }
  }//
  


  assert(checkSyncSpeechFace(facePlans, speechPlans));
  
  Chains fixedChains(nbBehaviors); //SpeechPlans 
  Chains unfixedChains(nbBehaviors); //FacePlans

  for (int j=0; j<nbSyncs; ++j) 
    {
      const Synchronization &s = bl.synchronization(j);

      const Behavior *b1 = s.behavior1();
      const Behavior *b2 = s.behavior2();
      int ib1 = b2i.value(b1, -1);
      int ib2 = b2i.value(b2, -1);
      assert(ib1 != -1); //otherwise, Synchronization is invalid
      assert(ib2 != -1);
      assert(ib1 < nbBehaviors && ib2 < nbBehaviors);
      const bool isSpeech1 = !speechPlans[ib1].empty();
      const bool isSpeech2 = !speechPlans[ib2].empty();

      std::cerr<<"ib1="<<ib1<<" isSpeech1="<<isSpeech1<<" ; ib2="<<ib2<<" isSpeech2="<<isSpeech2<<"\n";

      if (isSpeech1 && isSpeech2) 
	{
	  //we have a synchronization between two speech plans
	  // it must be with extremities && without overlap

	  //TODO: we do not take offset into account
	  //      We could have two Plans synchronized by their Start (or End) 
	  //      but we an offset that make them non overlapping
	  if ( (! (s.syncPoint1() == Behavior::Start || s.syncPoint1() == Behavior::End)) 
	       || (! (s.syncPoint2() == Behavior::Start || s.syncPoint2() == Behavior::End)) )
	    {
	      std::cerr<<"Error: syncPoints between two speechPlanUnits elsewhere than between Start or End\n";
	      return false;
	    }
	  if (s.syncPoint1() == s.syncPoint2()) 
	    {
	      assert(s.syncPoint1() == Behavior::Start || s.syncPoint1() == Behavior::End);
	      //Overlap : We are not able to play two speechPlans simultaneously
	      std::cerr<<"Error: overlap between two speechPlanUnits\n";
	      return false;
	    }

	  //std::cerr<<"s.syncPoint1()="<<s.syncPoint1()<<" s.syncPoint2()="<<s.syncPoint2()<<"\n";
	  assert((s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End)
		 || (s.syncPoint1() == Behavior::End && s.syncPoint2() == Behavior::Start));
	  
	  if (s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End) {
	    std::swap(ib1, ib2);
	  }
	  const float offset = fabs(s.offset());
	  //we have : b1 followed by b2
	  const bool insertOk = fixedChains.insert(ib1, ib2, offset);
	  if (! insertOk)
	    return false;
			
	}
      else 
	{

	  const bool isFace1 = !isSpeech1 && !facePlans[ib2].empty() && facePlans[ib2].length()!=0;
	  const bool isFace2 = !isSpeech2 && !facePlans[ib1].empty() && facePlans[ib1].length()!=0;

	  if (isFace1 && isFace2) {
	    //synchro between two FacePlans

	    //As for SpeechPlans,
	    //we allow syncrhonization only on extreities && without overlap
	    
	    
	    //TODO: we do not take offset into account
	    //      We could have two Plans synchronized by their Start (or End) 
	    //      but we an offset that make them non overlapping
	    if ( (! (s.syncPoint1() == Behavior::Start || s.syncPoint1() == Behavior::End)) 
		 || (! (s.syncPoint2() == Behavior::Start || s.syncPoint2() == Behavior::End)) )
	      {
		std::cerr<<"Error: syncPoints between two FacePlanUnits elsewhere than between Start or End\n";
	      return false;
	    }
	    if (s.syncPoint1() == s.syncPoint2()) 
	      {
		assert(s.syncPoint1() == Behavior::Start || s.syncPoint1() == Behavior::End);
		//Overlap : We are not able to play two speechPlans simultaneously
		std::cerr<<"Error: overlap between two facePlanUnits\n";
		return false;
	      }	    

	    //std::cerr<<"s.syncPoint1()="<<s.syncPoint1()<<" s.syncPoint2()="<<s.syncPoint2();
	    assert((s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End)
		   || (s.syncPoint1() == Behavior::End && s.syncPoint2() == Behavior::Start));
	    
	    if (s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End) {
	      std::swap(ib1, ib2);
	    }
	    const float offset = fabs(s.offset());
	    //we have : b1 followed by b2
	    const bool insertOk = unfixedChains.insert(ib1, ib2, offset);
	    if (! insertOk)
	      return false;
	    
	  }

	  /*	  
	  //check if we have synchro between a speechPlan & a non-overlapping FacePlan
	  // or two non-overlapping FacePlans
		  
	  //(this is not so simple to check that two FacePlans do not overlap ???)
	  //(we can have a sync between Relax & AttackPeak but they do no overlap because of offset !!!)
	  
	  bool ok1 = isSpeech1 || (isFace1 && !facePlanHasOverlap[ib2]);
	  bool ok2 = isSpeech2 || (isFace2 && !facePlanHasOverlap[ib1]);

	  {//DEBUG
	  std::cerr<<"ib1="<<ib1<<" ok1="<<ok1<<" ; ib2="<<ib2<<" ok2="<<ok2;
	  if (!facePlans[ib2].empty())
	    std::cerr<<"facePlanHasOverlap[ib2]="<<facePlanHasOverlap[ib2];
	  if (!facePlans[ib1].empty())
	    std::cerr<<"facePlanHasOverlap[ib1]="<<facePlanHasOverlap[ib1];
	  }//DEBUG

	  
	  if (ok1 && ok2) {
	    //synchro between a speechPlan & a non-overlapping FacePlan or two non-overlapping FacePlans
	    assert((s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End)
		   || (s.syncPoint1() == Behavior::End && s.syncPoint2() == Behavior::Start));
	    if (s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End) {
	      std::swap(ib1, ib2);
	    }
	    float offset = fabs(s.offset());
	    //we have : b1 followed by b2
	    bool insertOk = fixedChains.insert(ib1, ib2, offset);
	    if (! insertOk)
	      return false;
	  }
	  else {
	    

	  }
	  */

	}

    }

  assert(checkSyncSpeechFace(facePlans, speechPlans));
  
  //move the plans in fixedChains to synchronize them
  // (making them start at currTime is arbitrary, it is not necessarily their final start time).
  fixedChains.rigidify(currTime, facePlans, speechPlans);

#ifndef NDEBUG
  {
    //check that facePlans corresponding to speechPlans have also moved
    for (int i=0; i<nbBehaviors; ++i) {
      if (! speechPlans[i].empty() && ! facePlans[i].empty()) {
	if (speechPlans[i].startTime() != facePlans[i].startTime()) {
	  std::cerr<<"ERROR: speechPlans["<<i<<"] st="<<speechPlans[i].startTime()<<" et="<<speechPlans[i].endTime()
		   <<" BUT facePlans["<<i<<"] st="<<facePlans[i].startTime()<<" et="<<facePlans[i].endTime()<<"\n";
	  exit(10);
	}
      }

    }

  }
 #endif //NDEBUG
  assert(checkSyncSpeechFace(facePlans, speechPlans));
 
  //move the plans in unfixedChains to synchronize them
  // (even if they don't all have their final length,
  // it will at least position those that will not be stretched)
  // (making them start at currTime is arbitrary, it is not necessarily their final start time).
  unfixedChains.rigidify(currTime, facePlans, speechPlans);

  assert(checkSyncSpeechFace(facePlans, speechPlans));
  assert(checkFacePlansContinuity(facePlans));

  //DEBUG
  {
	  std::cerr<<"**************** after rigidify \n";
	  for (int z=0; z<speechPlans.size(); ++z) {
		  if (! speechPlans[z].empty())
			  std::cerr<<"SpeechPlan["<<z<<"] st="<<speechPlans[z].startTime()<<" len="<<speechPlans[z].length()<<" et="<<speechPlans[z].endTime()<<"\n";
	  }
	  for (int z=0; z<facePlans.size(); ++z) {
		  if (! facePlans[z].empty())
			  std::cerr<<"FacePlan["<<z<<"] st="<<facePlans[z].startTime()<<" len="<<facePlans[z].length()<<" et="<<facePlans[z].endTime()<<"\n";
	  }
	  std::cerr<<"****************\n";	  
  }


  
  //check if have unfixedChains syncrhonized with fixedChains
  // and try to fixe their size

  QVector<bool> fixedLength(nbBehaviors, false);
  for (int i=0; i<nbBehaviors; ++i) {
    const bool isSpeech = !speechPlans[i].empty();
    if (isSpeech)
      fixedLength[i] = true;
  }
#ifndef NDEBUG
  //all plans affected to fixedChains are of fixed length
  for (int i=0; i<fixedChains.starts.size(); ++i) {
    for (Node *n = fixedChains.starts[i] ; n!= nullptr; n=n->after) {
      const int ind = n->behaviorIndex;
      assert(fixedLength[ind] == true);
    }
  }
#endif //NDEBUG


  //all speechPlans not in in fixedChains are added in fixedChains as a chain of one element
  {
    QVector<bool> present(nbBehaviors, false);
    for (int i=0; i<fixedChains.starts.size(); ++i) {
      for (Node *n = fixedChains.starts[i] ; n!= nullptr; n=n->after) {
	const int ind = n->behaviorIndex;
	present[ind] = true;
      }
    }
    for (int i=0; i<nbBehaviors; ++i) {
      if (fixedLength[i] && ! present[i]) {
		  fixedChains.insertAlone(i);
      }
    }
  }


  //all facePlans not in unfixedChains are added in unfixedChains as a chain of one element
  {
    QVector<bool> present(nbBehaviors, false);
    for (int i=0; i<unfixedChains.starts.size(); ++i) {
      for (Node *n = unfixedChains.starts[i]; n!= nullptr; n=n->after) {
	const int ind = n->behaviorIndex;
	present[ind] = true;
      }
    }
    for (int i=0; i<nbBehaviors; ++i) {
      if (! fixedLength[i] && ! present[i]) {
	unfixedChains.insertAlone(i);
      }
    }
  }

  assert(checkSyncSpeechFace(facePlans, speechPlans));
  assert(checkFacePlansContinuity(facePlans));
  
  //try to fixe size of nodes in unfixedChains

  QVector< QVector<int> > linkedFixedStarts(nbBehaviors);
  
  QVector<bool> fixedLength2 = fixedLength;
  int nbUChains = unfixedChains.starts.size();
  for (int i=0; i<nbUChains; ++i) {
	  Node *n = unfixedChains.starts[i];
	  if (n != nullptr) {

		  Node *nStartF = nullptr;
		  Synchronization sStartF;
		  Node *nEndF = nullptr;
		  Synchronization sEndF;

		  for ( ; n!=nullptr; n=n->after) {
			  const int ind = n->behaviorIndex;
			  assert(ind >=0 && ind < (int)bl.numberOfBehaviors());
			  assert(ind < facePlans.size());	assert(ind < speechPlans.size());

			  std::cerr<<"\n";
			  std::cerr<<"# start unfixed: ind="<<ind<<"\n";
	
			  for (int j=0; j<nbSyncs; ++j) {
				  Synchronization s = bl.synchronization(j);
	  
				  const Behavior *b1 = s.behavior1();
				  const Behavior *b2 = s.behavior2();
				  int ib1 = b2i.value(b1, -1);
				  int ib2 = b2i.value(b2, -1);
				  if (ib1 == ind || ib2 == ind) {
					  if (ib2 == ind) {
						  s.reorder();
						  std::swap(b1, b2);
						  std::swap(ib1, ib2);
					  }
					  assert(ib1 == ind);
					  if (fixedLength[ib2]) {

						  std::cerr<<" synchro["<<j<<"] between unfixed="<<ind<<" && fixed="<<ib2<<"\n";
						  std::cerr<<" synchro: ib1="<<b2i.value(s.behavior1(), -1)<<"="<<ib1<<" spt1="<<s.syncPoint1()<<" ib2="<<b2i.value(s.behavior2(), -1)<<"="<<ib2<<" spt2="<<s.syncPoint2()<<"\n"; 
	      
						  if (nStartF == nullptr) {
							  //we have a first synchro between a fixed and an unfixed plan
							  sStartF = s;
							  nStartF = n;
							  //we can only synchronized with extremities of SpeechPlans
							  assert(sStartF.syncPoint2() == Behavior::Start || sStartF.syncPoint2() == Behavior::End);

							  std::cerr<<" * first synchro : indStartF="<<nStartF->behaviorIndex<<"\n";
		
						  }
						  else { //nEndF
							  assert(nEndF == nullptr);
							  //we have a second synchro betwen a fixed and an unfixed plan
		
							  sEndF = s;
							  nEndF = n;

							  std::cerr<<" * second synchro : indEndF="<<nEndF->behaviorIndex<<"\n";

							  //we can only synchronized with extremities of SpeechPlans
							  assert(sEndF.syncPoint2() == Behavior::Start || sEndF.syncPoint2() == Behavior::End);


							  //DEBUG
							  {
								  std::cerr<<"****************\n";
								  for (int z=0; z<speechPlans.size(); ++z) {
									  if (! speechPlans[z].empty())
										  std::cerr<<"SpeechPlan["<<z<<"] st="<<speechPlans[z].startTime()<<" len="<<speechPlans[z].length()<<" et="<<speechPlans[z].endTime()<<"\n";
								  }
								  for (int z=0; z<facePlans.size(); ++z) {
									  if (! facePlans[z].empty())
										  std::cerr<<"FacePlan["<<z<<"] st="<<facePlans[z].startTime()<<" len="<<facePlans[z].length()<<" et="<<facePlans[z].endTime()<<"\n";
								  }
								  std::cerr<<"****************\n";
							  }



							  int ibS = b2i.value(sStartF.behavior2(), -1);
							  int ibE = b2i.value(sEndF.behavior2(), -1);

							  if (ibE != ibS) {
								  //check that fixedPlans are in the same fixedChain
		  
								  int indexStart1 = fixedChains.indexStart(ibS);
								  int indexStart2 = fixedChains.indexStart(ibE);
								  assert(indexStart1 != -1 && indexStart2 != -1); //both are fixedPlans
								  if (indexStart1 != indexStart2) {
									  std::cerr<<"ERROR: one or several face plans are synchronized to several speech plans that are not synchronized together\n";
									  return false;
								  }
								  //we could also stretch the FacePlan(s) to have the speechPlans size, and move the speechPlans... ?
							  }
		
							  float startTime = 0;
							  if (sStartF.syncPoint2() == Behavior::Start)
								  startTime = getStartTime(ibS, facePlans, speechPlans); //std::max(speechPlans[ibS].startTime(), facePlans[ibS].startTime());
							  else if (sStartF.syncPoint2() == Behavior::End)
								  startTime = getEndTime(ibS, facePlans, speechPlans); //std::max(speechPlans[ibS].endTime(), facePlans[ibS].endTime());
							  std::cerr<<"startTime="<<startTime<<"\n";
							  float endTime = -1;
							  if (sEndF.syncPoint2() == Behavior::Start)
								  endTime = getStartTime(ibE, facePlans, speechPlans); //std::max(speechPlans[ibE].startTime(), facePlans[ibE].startTime());
							  else if (sEndF.syncPoint2() == Behavior::End)
								  endTime = getEndTime(ibE, facePlans, speechPlans); //std::max(speechPlans[ibE].endTime(), facePlans[ibE].endTime());
							  std::cerr<<"endTime="<<endTime<<"\n";

							  int scp1 = sStartF.syncPoint1();
							  int scp2 = sEndF.syncPoint1();
							  if (scp1 > scp2) {
								  std::swap(scp1, scp2);
								  std::swap(startTime, endTime);
		  
                                  //TODO: should we also swap these ???????
								  //std::swap(ibS, ibE);
								  //std::swap(nStartF, nEndF);
								  //std::swap(sStartF, sEndF);
							  }

							  if (startTime > endTime) {
								  std::cerr<<"ERROR: crossing in constraint\n";
								  return false;
							  }

							  std::cerr<<"##############################################\n";

							  std::cerr<<" scp1="<<scp1<<" scp2="<<scp2<<" len="<<endTime-startTime<<" st="<<startTime<<"\n";
							  assert(scp1 <= scp2);
							  float length = endTime-startTime;
							  assert(speechPlans[ib1].empty());
							  assert(length > 0);
		
							  int indS = nStartF->behaviorIndex;
							  float prevStartTime = facePlans[indS].startTime();
		  
							  if (nStartF == nEndF) {
								  //One unfixed plan is synchronized with one or several fixed plans
		  
								  if (fixedLength2[ib1] && facePlans[ib1].length() != length) {
									  std::cerr<<"ERROR: facePlan already fixed to a different length\n";
									  return false;
								  }
								  assert(speechPlans[ib1].empty());
								  assert(checkFacePlanContinuity(ib1, facePlans));
								  bool ok = facePlans[ib1].stretch(startTime, length, scp1, scp2);
								  if (! ok) {
									  std::cerr<<"ERROR: unable to stretch face plan\n";
									  return false;
								  }
								  assert(checkFacePlanContinuity(ib1, facePlans));
								  fixedLength2[ib1] = true;

							  }
							  else {
                                  assert(nStartF != nEndF);
								  //several chained unfixed plans are synchronized with fixedPlans

                                  int indE = nEndF->behaviorIndex;

                                  //count nodes & size of fixed parts of unfixed plans
								  int nbNodes = 0;
                                  int numStretchables = 0;
                                  float fixedLength = 0;
                                  float totalLength = 0;
                                  std::vector<float> fixedLengths;
                                  std::vector<float> totalLengths;
                                  std::vector<bool> stretchables;

                                  //  (first do indS
                                  float nodeFixedLength = 0, nodeTotalLength = 0;
                                  int nodeNumLoopable = 0;
                                  facePlans[indS].getFixedLength(scp1, Behavior::End, nodeFixedLength, nodeTotalLength, nodeNumLoopable);
                                  fixedLengths.push_back(nodeFixedLength);
                                  totalLengths.push_back(nodeTotalLength);
                                  stretchables.push_back((nodeNumLoopable > 0));
                                  numStretchables += (nodeNumLoopable > 0);
                                  std::cerr<<" fixed length computation: nodeS: nodeFixedLen="<<nodeFixedLength<<" fixedLength="<<fixedLength<<" nodeTotalLen="<<nodeTotalLength<<" totalLength="<<totalLength<<"\n";
                                  fixedLength += nodeFixedLength;
                                  totalLength += nodeTotalLength;
                                  ++nbNodes;
                                  //  (then do nodes between indS & indE not included
                                  for (Node *n2=nStartF->after ; n2!=nEndF; n2=n2->after) {
									  assert(n2 != nullptr);
                                      const int ind = n2->behaviorIndex;
                                      facePlans[ind].getFixedLength(Behavior::Start, Behavior::End, nodeFixedLength, nodeTotalLength, nodeNumLoopable);
                                      fixedLengths.push_back(nodeFixedLength);
                                      totalLengths.push_back(nodeTotalLength);
                                      stretchables.push_back((nodeNumLoopable > 0));
                                      numStretchables += (nodeNumLoopable > 0);
                                      fixedLength += nodeFixedLength;
                                      totalLength += nodeTotalLength;
                                      std::cerr<<" fixed length computation: nodeI: nodeFixedLen="<<nodeFixedLength<<" fixedLength="<<fixedLength<<" nodeTotalLen="<<nodeTotalLength<<" totalLength="<<totalLength<<"\n";
                                      ++nbNodes;
								  }
                                  //  (do indE
                                  facePlans[indE].getFixedLength(Behavior::Start, scp2, nodeFixedLength, nodeTotalLength, nodeNumLoopable);
                                  fixedLengths.push_back(nodeFixedLength);
                                  totalLengths.push_back(nodeTotalLength);
                                  stretchables.push_back((nodeNumLoopable > 0));
                                  numStretchables += (nodeNumLoopable > 0);
                                  fixedLength += nodeFixedLength;
                                  totalLength += nodeTotalLength;
                                  std::cerr<<" fixed length computation: nodeE: nodeFixedLen="<<nodeFixedLength<<" fixedLength="<<fixedLength<<" nodeTotalLen="<<nodeTotalLength<<" totalLength="<<totalLength<<"\n";
                                  ++nbNodes;

								  assert(nbNodes > 0);


                                  /*
								  length += facePlans[indS].timeBefore(scp1);
								  length += facePlans[indE].timeAfter(scp2);
                                  */


								  std::cerr<<" ind="<<ind<<" indS="<<indS<<" indE="<<indE<<"\n";
                                  std::cerr<<"stretch ? "<<nbNodes<<" unfixed plans ("<<numStretchables<<" parts) with fixed parts of fixedLength="<<fixedLength<<"&totalLength="<<totalLength<<" VS fixed plans of length="<<length<<"\n";
								  assert(nbNodes > 1);
		  
                                  //if (length > fixedLength && numStretchables > 0) {
                                  if (length > totalLength && numStretchables > 0) {

                                      float len = length / static_cast<float>(numStretchables);
                                      std::cerr<<"  will stretch "<<numStretchables<<" (loopable) nodes (roughly) uniformally, to length="<<len<<"\n";

                                      //REM: we can not necessarily stretch all the nodes to the same length ("len").
                                      // If for example we have two nodes : one of length 1000 and the other of length 200
                                      // and the target length is length=1600,
                                      // if we stretched uniformally, it would mean to stretch to length 1600/2=800
                                      // and thus it would require to downsize first node, which is not possible.

                                      //So we stretch proportionnaly to current length.

                                      float growFactor = length/totalLength;
                                      assert(growFactor>=1.f);

                                      int h=0;
                                      // (do first node
                                      if (stretchables[h]) {
                                          if (fixedLength2[indS] && facePlans[indS].length() != len) {
                                              std::cerr<<"ERROR: facePlan already fixed (start)\n";
                                              return false;
                                          }
                                          assert(speechPlans[indS].empty());
                                          assert(checkFacePlanContinuity(indS, facePlans));
                                          bool ok1 = facePlans[indS].stretch(startTime, (growFactor*totalLengths[h]), scp1, Behavior::End);
                                          if (! ok1) {
                                              std::cerr<<"ERROR: unable to extend face plan (start)\n";
                                              return false;
                                          }
                                      }
                                      assert(checkFacePlanContinuity(indS, facePlans));
                                      fixedLength2[indS] = true;

                                      float lenSum = facePlans[indS].timeAfter(scp1); //.length();
                                      std::cerr<<" diff0="<<fabs(facePlans[indS].length() - len)<<"\n";
                                      startTime = facePlans[indS].endTime();
                                      ++h;
                                      //  (do nodes between indS & indE not included
                                      for (Node *n2=nStartF->after ; n2!=nEndF; n2=n2->after) {
                                            int ind2 = n2->behaviorIndex;
                                            if (stretchables[h]) {
                                              if (fixedLength2[ind2] && facePlans[ind2].length() != len) {
                                                  std::cerr<<"ERROR: facePlan already fixed (end)\n";
                                                  return false;
                                              }
                                              assert(speechPlans[ind2].empty());
                                              assert(checkFacePlanContinuity(ind2, facePlans));
                                              bool ok3 = facePlans[ind2].stretch(startTime, (growFactor*totalLengths[h]), Behavior::Start, Behavior::End);
                                              if (! ok3) {
                                                std::cerr<<"ERROR: unable to extend face plan (middle)\n";
                                                return false;
                                              }
                                            }
                                            assert(checkFacePlanContinuity(ind2, facePlans));
                                            fixedLength2[ind2] = true;
                                            lenSum += facePlans[ind2].length();
                                            startTime = facePlans[ind2].endTime();
                                            ++h;
                                        }
                                        // (do end node
                                        assert(lenSum < length);
                                        len = length-lenSum; //to be sure to have the correct length
                                        assert(len >= totalLengths[h]);
                                        if (stretchables[h]) {
                                            if (fixedLength2[indE] && facePlans[indE].length() != len) {
                                              std::cerr<<"ERROR: facePlan already fixed (end)\n";
                                              return false;
                                            }
                                            assert(checkFacePlanContinuity(indE, facePlans));
                                            bool ok2 = facePlans[indE].stretch(startTime, len, Behavior::Start, scp2);
                                            if (! ok2) {
                                                std::cerr<<"ERROR: unable to extend face plan (end)\n";
                                                return false;
                                            }
                                        }
                                        assert(checkFacePlanContinuity(indE, facePlans));
                                        fixedLength2[indE] = true;

                                      }
                                      else {
                                        // // ! (length > fixedLength && numStretchables > 0)
                                        // ! (length > totalLength && numStretchables > 0)


                                      //impl: totalLength vs FixedLength
                                      //impl: on suppose que la difference n'est pas enorme...

                                        //if (length < fixedLength) {
                                        if (length < totalLength) {

                                            //We can not stretch unfixed chain
                                            //On contrary, as fixed part of unfixed chain is bigger than fixed chain
                                            // we will stretch fixed chain !
                                            // I am really not sure it works in all (synchronization) cases...


                                            std::cerr<<"fixed chain is shorter than fixed part of unfixed chain : we have to stretch fixed chain !\n";

                                            std::cerr<<"Before stretch (of fixed chain): \n";
                                            for (int z=0; z<speechPlans.size(); ++z) {
                                                if (! speechPlans[z].empty())
                                                    std::cerr<<"  SpeechPlan["<<z<<"] st="<<speechPlans[z].startTime()<<" len="<<speechPlans[z].length()<<" et="<<speechPlans[z].endTime()<<"\n";
                                            }
                                            for (int z=0; z<facePlans.size(); ++z) {
                                                if (! facePlans[z].empty())
                                                    std::cerr<<"  FacePlan["<<z<<"] st="<<facePlans[z].startTime()<<" len="<<facePlans[z].length()<<" et="<<facePlans[z].endTime()<<"\n";
                                            }
                                            std::cerr<<"\n";



                                            //We extend last element of fixed chain
                                            // (Really not sure it works in all cases !!)
                                            const int indexStart1 = fixedChains.indexStart(ibS);
                                            Node *nF = fixedChains.starts[indexStart1];
                                            Node *nLast = nF;
                                            for (Node *n2=nF; n2!=nullptr; n2=n2->after)
                                                nLast = n2;
                                            assert(nLast != nullptr);
                                            assert(getEndTime(nF->behaviorIndex, facePlans, speechPlans) <= getEndTime(nLast->behaviorIndex, facePlans, speechPlans) ); //just to check that nF is before nLast in time, to be sure that nLast is the last in time
                                            //float lengthExtension = fixedLength - length;
                                            float lengthExtension = totalLength - length;
                                            std::cerr<<" lengthExtension="<<lengthExtension<<" of end of fixed chain\n";
                                            extendLength(nLast->behaviorIndex, facePlans, speechPlans, lengthExtension);

                                            //We move unfixedChain to correspond to fixed chain (and add it to fixedChain)
                                            // (first do first node (as me move syncPoint1 to correspond to startTime)
                                            assert(nStartF->behaviorIndex == indS);
                                            facePlans[indS].shiftTo(scp1, startTime);
                                            startTime = facePlans[indS].endTime();
                                            fixedLength2[indS] = true;
                                            // (do other nodes
                                            for (Node *n2=nStartF->after; n2!=nullptr; n2=n2->after) {
                                                const int ind = n2->behaviorIndex;
                                                facePlans[ind].setStartTime(startTime);
                                                startTime = facePlans[ind].endTime();
                                                fixedLength2[ind] = true;
                                            }
                                            assert(fixedLength2[indS]==true);
                                            assert(fixedLength2[indE]==true);


                                            std::cerr<<"After stretch (of fixed chain): \n";
                                            for (int z=0; z<speechPlans.size(); ++z) {
                                                if (! speechPlans[z].empty())
                                                    std::cerr<<"  SpeechPlan["<<z<<"] st="<<speechPlans[z].startTime()<<" len="<<speechPlans[z].length()<<" et="<<speechPlans[z].endTime()<<"\n";
                                            }
                                            for (int z=0; z<facePlans.size(); ++z) {
                                                if (! facePlans[z].empty())
                                                    std::cerr<<"  FacePlan["<<z<<"] st="<<facePlans[z].startTime()<<" len="<<facePlans[z].length()<<" et="<<facePlans[z].endTime()<<"\n";
                                            }
                                            std::cerr<<"\n";
                                        }
                                        else {
                                            assert(numStretchables == 0);
                                            std::cerr<<"ERROR: nothing is stretchable \n";
                                            return false;
                                        }

                                      }
		    
							  } //(nStart == nEnd)

							  //assert(! facePlans[indS].empty());  //????DEBUG
							  float newStartTime = facePlans[indS].startTime();
							  float offset = newStartTime-prevStartTime;
							  std::cerr<<"prevStartTime="<<prevStartTime<<" newStartTime="<<newStartTime<<" => offset="<<offset<<"\n";

							  //we move all nodes before in the unfixed chain
							  // && all their linked nodes
							  for (Node *n2=unfixedChains.starts[i]; n2!=nStartF; n2=n2->after) {
								  const int ind = n2->behaviorIndex;
								  std::cerr<<" ### move prev in chain : shift face: "<<ind<<" of offset="<<offset<<"\n";
		  
								  {//DEBUG
									  std::cerr<<"  before move:\n";
									  std::cerr<<"   facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
									  std::cerr<<"   speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
								  }//DEBUG

								  assert(facePlans[ind].checkContinuous());
								  facePlans[ind].shiftOf(offset);
								  speechPlans[ind].shiftOf(offset);

								  {//DEBUG
									  std::cerr<<"  after move:\n";
									  std::cerr<<"   facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
									  std::cerr<<"   speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
								  }//DEBUG
								  assert(facePlans[ind].checkContinuous());


								  for (int k=0; k<linkedFixedStarts[ind].size(); ++k) {
									  const int start = (linkedFixedStarts[ind])[k];
									  for (Node *n3=fixedChains.starts[start]; n3!=nullptr; n3=n3->after) {
										  const int ind = n3->behaviorIndex;
										  std::cerr<<" move linked fixedB : shift speech: "<<ind<<" of offset="<<offset<<"\n";
										  {//DEBUG
											  std::cerr<<" before move:\n";
											  std::cerr<<"   facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
											  std::cerr<<"   speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
										  }//DEBUG
										  
										  facePlans[ind].shiftOf(offset);
										  speechPlans[ind].shiftOf(offset);

										  {//DEBUG
											  std::cerr<<" after move:\n";
											  std::cerr<<"   facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
											  std::cerr<<"   speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
										  }//DEBUG

									  }
								  }

							  }
							  //we move all previously linked nodes in linkedFixedStarts
							  for (int k=0; k<linkedFixedStarts[nStartF->behaviorIndex].size(); ++k) {
								  const int start = (linkedFixedStarts[nStartF->behaviorIndex])[k];
								  for (Node *n2=fixedChains.starts[start]; n2!=nullptr; n2=n2->after) {
									  const int ind = n2->behaviorIndex;
									  std::cerr<<"   move linked fixed [start="<<start<<"] : shift speech: "<<ind<<" of offset="<<offset<<"\n";
									  {//DEBUG
										  std::cerr<<"   before move:\n";
										  std::cerr<<"     facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
										  std::cerr<<"     speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
									  }//DEBUG

									  facePlans[ind].shiftOf(offset);
									  speechPlans[ind].shiftOf(offset);

									  {//DEBUG
										  std::cerr<<"   after move:\n";
										  std::cerr<<"     facePlans["<<ind<<"] st="<<facePlans[ind].startTime()<<" len="<<facePlans[ind].length()<<" et="<<facePlans[ind].endTime()<<"\n";
										  std::cerr<<"     speechPlans["<<ind<<"] st="<<speechPlans[ind].startTime()<<" len="<<speechPlans[ind].length()<<" et="<<speechPlans[ind].endTime()<<"\n";
									  }//DEBUG

								  }
							  }
							  //add fixedChains start to linkedFixedStarts if no present
							  {
								  int indexStartFixed = fixedChains.indexStart(ibS);

								  std::cerr<<" for ibS="<<ibS<<" start="<<indexStartFixed<<"\n";

								  bool found = false;
								  for (int k=0; k<linkedFixedStarts[nStartF->behaviorIndex].size(); ++k) {
									  if ((linkedFixedStarts[nStartF->behaviorIndex])[k] == indexStartFixed) {
										  found = true;
										  break;
									  }
								  }
								  if (! found) {
									  std::cerr<<" add start fixed "<<indexStartFixed<<" (start="<<fixedChains.starts[indexStartFixed]->behaviorIndex<<") chain to move when "<<indS<<" is moved\n";
									  linkedFixedStarts[nStartF->behaviorIndex].push_back(indexStartFixed);
								  }

								  {//DEBUG
									  std::cerr<<"For ind="<<nStartF->behaviorIndex<<" the linked starts are: ";
									  for (int i=0; i<linkedFixedStarts[nStartF->behaviorIndex].size(); ++i) 
										  std::cerr<<(linkedFixedStarts[nStartF->behaviorIndex])[i]<<" ";
									  std::cerr<<"\n";
								  }//DEBUG


							  }

							  assert(checkSyncSpeechFace(facePlans, speechPlans));
							  assert(checkFacePlansContinuity(facePlans));
		
							  //We move all plans down the chain 
							  // 
							  {
								  if (nEndF->after != nullptr)
									  std::cerr<<" move all plans down the chain \n";

								  int indE = nEndF->behaviorIndex;
								  float startTime = facePlans[indE].endTime();
								  for (Node *n2=nEndF->after ; n2!=nullptr; n2=n2->after) {
									  const int ind2 = n2->behaviorIndex;
									  assert(ind2 >=0 && ind2 < (int)bl.numberOfBehaviors());
									  assert(ind2 < facePlans.size()); assert(ind2 < speechPlans.size());
									  //TODO: should we consider that facePlans[ind] & speechPlans[ind] do not have the same startTime ???
									  std::cerr<<"  move facePlan "<<ind2<<" new startTime="<<startTime<<"\n";
									  assert(facePlans[ind2].checkContinuous());
									  facePlans[ind2].setStartTime(startTime);
									  assert(facePlans[ind2].checkContinuous());
									  speechPlans[ind2].setStartTime(startTime);
									  startTime = getEndTime(ind2, facePlans, speechPlans); //std::max(facePlans[ind2].endTime(), speechPlans[ind2].endTime());

									  std::cerr<<"    facePlans["<<ind2<<"] st="<<facePlans[ind2].startTime()<<" et="<<facePlans[ind2].endTime()<<"\n";
								  }


								  //TODO!!! we should also move linked nodes
								  // and also check that it is possible !!!
							  }

							  assert(checkSyncSpeechFace(facePlans, speechPlans));
							  assert(checkFacePlansContinuity(facePlans));
		
		
							  //sStartF = sEndF;
							  //nStartF = nEndF;
							  nStartF = nullptr;
							  nEndF = nullptr;
						  }
	      
					  }
	    
				  }
			  }
		  }

	  }
  }

  {//DEBUG
    for (int i=0; i<fixedLength2.size(); ++i) {
      if (! fixedLength2[i])
	std::cerr<<"Warning: facePlan "<<i<<" still has no fixed length...\n";
    }
  }//DEBUG

  
  //From here, we consider that all FacePlans have a fixed length.

  
  //we check if we have overlap between SpeechPlans 
  for (int i=0; i<nbBehaviors; ++i) {
    for (int j=i+1; j<nbBehaviors; ++j) {
      if (speechPlans[i].intersects(speechPlans[j])) {
		  std::cerr<<"ERROR: we have overlap between speechplans after shifting non-overlapping parts\n";

		  std::cerr<<"speechPlans["<<i<<"] st="<<speechPlans[i].startTime()<<" et="<<speechPlans[i].endTime()<<"\n";
		  std::cerr<<"speechPlans["<<j<<"] st="<<speechPlans[j].startTime()<<" et="<<speechPlans[j].endTime()<<"\n";

		  return false;
      }
    }
  }


  //We move everything to the correct startTime
  assert(nbBehaviors>0);
  float prevStartTime = getStartTime(0, facePlans, speechPlans);
  for (int i=1; i<nbBehaviors; ++i) { //start from 1
    const float st = getStartTime(i, facePlans, speechPlans);
    if (st < prevStartTime) {
      prevStartTime = st;
    }
  }
  float newStartTime = currTime;
  float offset = newStartTime-prevStartTime;
  std::cerr<<"Finally move everything : prevStartTime="<<prevStartTime<<" newStartTime="<<newStartTime<<" offset="<<offset<<"\n";
  {//DEBUG
    std::cerr<<"before move: \n";
    for (int i=0; i<nbBehaviors; ++i) {
      std::cerr<<i<<" speechPlan st="<<speechPlans[i].startTime()<<" et="<<speechPlans[i].endTime()<<" ; facePlan st="<<facePlans[i].startTime()<<" et="<<facePlans[i].endTime()<<"\n";
    }
  }//DEBUG
  
  for (int i=0; i<nbBehaviors; ++i) { //start from 1
    speechPlans[i].shiftOf(offset);
    facePlans[i].shiftOf(offset);
  }
  
  {//DEBUG
    std::cerr<<"after move: [currTime="<<currTime<<"]\n";
    for (int i=0; i<nbBehaviors; ++i) {
      std::cerr<<i<<" speechPlan st="<<speechPlans[i].startTime()<<" et="<<speechPlans[i].endTime()<<" ; facePlan st="<<facePlans[i].startTime()<<" et="<<facePlans[i].endTime()<<"\n";
    }
  }//DEBUG
  
  /*


  //check if we can extend FacePlans synchronized with fixed parts
	
  assert(actualNbChains <= 1);
  QVector<bool> fixedLength(nbBehaviors, false);
  //all plans affected to a track are considered of fixed length
  // (including face plans overlapping with nothing )
  for (int i=0; i<nbChains; ++i) {
    for (Node *n = fixedChains.starts[i] ; n!= nullptr; n=n->after) {
      const int ind = n->behaviorIndex;
      fixedLength[ind] = true;
    }
  }
  for (int i=0; i<nbBehaviors; ++i) {
    if (!speechPlans[i].empty())
      fixedLength[i] = true;
  }
	

  for (int i=0; i<nbSyncs; ++i) 
    {
      Synchronization s = bl.synchronization(i);

      const Behavior *b1 = s.behavior1();
      const Behavior *b2 = s.behavior2();		
      int ib1 = b2i.value(b1, -1);
      int ib2 = b2i.value(b2, -1);
      assert(ib1 != -1); //otherwise, Synchronization is invalid
      assert(ib2 != -1);
      assert(ib1 < nbBehaviors && ib2 < nbBehaviors);
		
      bool isFixed1 = fixedLength[ib1];
      bool isFixed2 = fixedLength[ib2];
		
      std::cerr<<"ib1="<<ib1<<" isFixed1="<<isFixed1<<" ; ib2="<<ib2<<" isFixed2="<<isFixed2;

      if ((isFixed1 || isFixed2) && (isFixed1 != isFixed2)) {
	//only one of the two is fixed

	if (! isFixed2) {
	  s.reorder();
	  std::swap(b1, b2);
	  std::swap(ib1, ib2);
	  std::swap(isFixed1, isFixed2);

	  std::cerr<<"swap: isFixed1="<<isFixed1<<" isFixed2="<<isFixed2;
	}
	assert(s.behavior1() == b1);
	assert(s.behavior2() == b2);
	assert(! isFixed1);
	assert(isFixed2);
			
	bool doubleSync = false;
	for (int j=i+1; j<nbSyncs; ++j) 
	  {
	    Synchronization s2 = bl.synchronization(j);
				
	    if (s2.behavior1()==b1 || s2.behavior2()==b1) {
	      //we found a second synchronization for b1

	      if (s2.behavior1() != b1)
		s2.reorder();
	      assert(s2.behavior1() == b1);
					
	      int ib3 = b2i.value(s2.behavior2(), -1);
	      assert(ib3 != -1 && ib3 < nbBehaviors);
	      bool isFixed3 = fixedLength[ib3];
	      if (isFixed3) {
		//We have a non-fixed plan sync'ed with two fixed plans
		//We will try to extend the non-fixed plan
						
		//s is between ib1 (non-fixed) & ib2 (fixed)
		// and s2 is between ib1 (non-fixed) & ib3 (fixed)

		doubleSync = true;

		std::cerr<<"s("<<i<<"):  b1="<<s.behavior1()<<" sync1="<<s.syncPoint1()<<" sync2="<<s.syncPoint2()<<" b2="<<s.behavior2()<<"\n";
		std::cerr<<"s2("<<j<<"): b1="<<s2.behavior1()<<" sync1="<<s2.syncPoint1()<<" sync2="<<s2.syncPoint2()<<" b2="<<s2.behavior2()<<"\n";
		std::cerr<<" ib1="<<ib1<<" ib2="<<ib2<<" ib3="<<ib3<<"\n";
		std::cerr<<"ib2="<<ib2<<" st="<<std::max(speechPlans[ib2].startTime(), facePlans[ib2].startTime())<<"\n";
		std::cerr<<"ib2="<<ib2<<" et="<<std::max(speechPlans[ib2].endTime(), facePlans[ib2].endTime())<<"\n";
		std::cerr<<"ib3="<<ib3<<" st="<<std::max(speechPlans[ib3].startTime(), facePlans[ib3].startTime())<<"\n";
		std::cerr<<"ib3="<<ib3<<" et="<<std::max(speechPlans[ib3].endTime(), facePlans[ib3].endTime())<<"\n";

		assert(s.syncPoint2() == Behavior::Start || s.syncPoint2() == Behavior::End);
		assert(s2.syncPoint2() == Behavior::Start || s2.syncPoint2() == Behavior::End);
						
		float startTime = 0;
		if (s.syncPoint2() == Behavior::Start)
		  startTime = std::max(speechPlans[ib2].startTime(), facePlans[ib2].startTime());
		else if (s.syncPoint2() == Behavior::End)
		  startTime = std::max(speechPlans[ib2].endTime(), facePlans[ib2].endTime());
		std::cerr<<"startTime="<<startTime<<"\n";

		float endTime = -1;
		if (s2.syncPoint2() == Behavior::Start)
		  endTime = std::max(speechPlans[ib3].startTime(), facePlans[ib3].startTime());
		else if (s2.syncPoint2() == Behavior::End)
		  endTime = std::max(speechPlans[ib3].endTime(), facePlans[ib3].endTime());
		std::cerr<<"endTime="<<endTime<<"\n";
						

		int scp1 = s.syncPoint1();
		int scp2 = s2.syncPoint1();
		if (scp1 > scp2) {
		  std::swap(scp1, scp2);
		  std::swap(startTime, endTime);
		}
						
		if (startTime > endTime) {
		  std::cerr<<"ERROR: crossing in constraint\n";
		  return false;
		}

		std::cerr<<" scp1="<<scp1<<" scp2="<<scp2<<" len="<<endTime-startTime<<" st="<<startTime<<"\n";
		assert(scp1 <= scp2);
		float length = endTime-startTime;
		assert(speechPlans[ib1].empty());
		assert(length > 0);
		bool ok = facePlans[ib1].stretch(startTime, length, scp1, scp2);
		if (! ok) {
		  std::cerr<<"ERROR: unable to extend face plan\n";
		  return false;
		}
						
	      }
	    }
	  }
	//TODO: case doubleSync == false
	//i.e., facePlan constrained only by one (or zero ?) fxied plan !

      }
    }
  */

					
  return true;
}


bool
SimpleSynchronizer::add(const BehaviorBlock &bl, 
						QVector<FacePlan> &facePlans,
						QVector<SpeechPlan> &speechPlans,
						float currentTime)
{
  const int nbBehaviors = bl.numberOfBehaviors();

  std::cerr<<"\n*** SimpleSynchronizer::add "<<nbBehaviors<<" behaviors & "<<bl.numberOfSynchronizations()<<" synchros\n";

  assert(facePlans.size() == nbBehaviors
	 && speechPlans.size() == nbBehaviors);

  if (nbBehaviors == 0) 
	  return true;


  //Try to respect synchronisation constraints



  /*
    {//DEBUG
    std::cerr<<"syncInfos:\n";
    int k=0;
    for (const auto &v : syncInfos) {
    std::cerr<<" syncInfo "<<k<<" : size="<<v.size();
    ++k;
    }		
    }//DEBUG
  */


  bool mustExit = false;



  BehaviorToIndex b2i;
  //const int nbBehaviors = bl.numberOfBehaviors();
  b2i.reserve(nbBehaviors);
  for (int i=0; i<nbBehaviors; ++i) {
    b2i[bl.behavior(i)] = i;
  }
	


  //first pass : handle synchronizations between two SpeechPlans
  //TODO: several passes while these constraints are not respected ???? 
	
  bool ok = fulfillExtremitiesConstraints(b2i, bl, facePlans, speechPlans, currentTime);
  if (! ok) {
    mustExit = true;
  }

	
  /*

  //second pass : extend all behaviors synchronized to exactly one SpeechPlan
  if (! mustExit)
  {
  for (int i=0; i<nbBehaviors; ++i) 
  {
  const Behavior *b = bl.behavior(i);
  assert(b != nullptr);
  SpeechPlan &sp = speechPlans[i];
  FacePlan &fp = facePlans[i];
  const QVector<SyncInfo> &syncs = syncInfos[i];
  if (!sp.empty() && !syncs.empty()) 
  {
  //this is a speechPlan, with at least one synchronization attached
				
  std::cerr<<syncs.size()<<" synchros on a speech plan\n";
				
  //bool found = false;
  SyncInfo sy1 = syncs[0];
  Synchronization s1 = bl.synchronization(sy1.syncIndex);
  if (s1.behavior1() != b)
  s1.reorder();
  assert(s1.behavior1() == b);
				
  for (int k=1; k<syncs.size(); ++k) {
  SyncInfo sy2 = syncs[k];
  Synchronization s2 = bl.synchronization(sy2.syncIndex);
  int b2Index = sy2.behaviorIndex;
  if (s2.behavior2() == b) {
  s2.reorder();
  }
					
  if ((s1.behavior1() == s2.behavior1() && s1.behavior2() == s2.behavior2()) ||
  (s1.behavior1() == s2.behavior2() && s1.behavior2() == s2.behavior1())) 
  {
  //two synchros concern the two same behaviors (one is a speech plan)
						
  std::cerr<<"two synchros between two same behaviors\n";
  std::cerr<<" sync1: b1="<<s1.behavior1()<<" sy1="<<s1.syncPoint1()<<" b2="<<s1.behavior2()<<" sy1="<<s1.syncPoint2();
  std::cerr<<" sync2: b1="<<s2.behavior1()<<" sy1="<<s2.syncPoint1()<<" b2="<<s2.behavior2()<<" sy2="<<s2.syncPoint2();
						
  assert(s1.behavior1() == s2.behavior1() && s1.behavior2() == s2.behavior2());
  assert(s1.behavior1() == b);
  assert(s1.behavior2() == bl.behavior(b2Index));
						
  //const Behavior *b2 = s1.behavior2();
						
  if (s1.syncPoint1() <= s2.syncPoint1() &&
  s2.syncPoint2() <= s1.syncPoint2() &&		  
  s2.syncPoint2() < s2.syncPoint1() &&
  s1.syncPoint1() < s1.syncPoint2()) //TODO: handle offsets
  {
  std::cerr<<"ERROR: crossing 1\n";
  mustExit = true;
  break;
  }
						
  if (s2.syncPoint1() <= s1.syncPoint1() &&
  s1.syncPoint2() <= s2.syncPoint2() &&		  
  s1.syncPoint2() < s1.syncPoint1() &&
  s2.syncPoint1() < s2.syncPoint2()) //TODO: handle offsets
  {
  std::cerr<<"ERROR: crossing 2\n";
  mustExit = true;
  break;
  }
						
  SpeechPlan &sp2 = speechPlans[b2Index];
  if (! sp2.empty()) {
  //We have two synchros between two SpeechPlans
  // we do not allow that 
  //[For now, we are not able to play two soundfiles at the same time]
							
  std::cerr<<"ERROR: Two synchros between two speechplans\n";
  mustExit = true;
  break;
  }

  const float length = sp.length();
						
  FacePlan &fp2 = facePlans[b2Index];
						
  std::cerr<<" extend facePlan to length="<<length;
  std::cerr<<" sp.size()="<<sp.size()<<" fp.size()="<<fp.size()<<" fp2.size()="<<fp2.size();
  std::cerr<<" sp : st="<<sp.startTime()<<" len="<<sp.length()<<" et="<<sp.endTime();
  assert(s1.behavior2() == bl.behavior(b2Index));
  int dbg = 0;
  for (const auto &t : fp2) {
  std::cerr<<"  track "<<dbg<<" size="<<t.size();
  ++dbg;
  }
						

  int scp1 = s1.syncPoint2();
  int scp2 = s2.syncPoint2();
  if (scp1 > scp2)
  std::swap(scp1, scp2);
						
  bool ok = fp2.stretch(sp.startTime(), length, scp1, scp2);
  if (! ok) {
  std::cerr<<"ERROR: unable to extend face plan\n";
  mustExit = true;
  break;
  }
						
  std::cerr<<" => fp2 : st="<<fp2.startTime()<<" len="<<fp2.length()<<" et="<<fp2.endTime();
						
  }
  }
				
  if (mustExit) {
  break;
  }
				

  }

  }
  }
  */

  /*
    if (! mustExit) {

    //old second  pass : between speechplans
    for (int i=0; i<nbBehaviors; ++i) 
    {
    const Behavior *b = bl.behavior(i);
    assert(b != nullptr);

    SpeechPlan &sp = speechPlans[i];
    //FacePlan &fp = facePlans[i];

    if (! syncInfos[i].empty()) 
    {
    for (const SyncInfo &si : syncInfos[i]) 
    {
    Synchronization s = bl.synchronization(si.syncIndex);
    if (s.behavior1() != b) 
    s.reorder();
				
    std::cerr<<"i="<<i<<" si.behaviorIndex="<<si.behaviorIndex<<"\n\n";

    SpeechPlan &sp2 = speechPlans[si.behaviorIndex];
    FacePlan &fp2 = facePlans[si.behaviorIndex];

    if (! sp.empty() && ! sp2.empty() ) //two SpeechPlanUnits
    {
    if ( (! (s.syncPoint1() == Behavior::Start || s.syncPoint1() == Behavior::End)) 
    || (! (s.syncPoint2() == Behavior::Start || s.syncPoint2() == Behavior::End)) )
    {
    std::cerr<<"Error: syncPoints between two speechPlanUnits elsewhere than between Start or End\n";
    mustExit = true;
    break;
    }
    else 
    {
    std::cerr<<"*** synchro between two speech plan units  b1="<<s.behavior1()<<" s1="<<s.syncPoint1()<<" b2="<<s.behavior2()<<" s2="<<s.syncPoint2()<<" offset="<<s.offset();

    if (s.syncPoint1() == Behavior::End && s.syncPoint2() == Behavior::Start)
    {
    if (s.offset() < 0)
    {
    std::cerr<<"Error: sync between two speechPlanUnits between End and Start with negative offset\n";
    //We are not able to play two sound simultaneously
    mustExit = true;
    break;
    }
    else 
    {
    const float newStartTime = sp.endTime() + s.offset();
    if (fp2.startTime() != newStartTime) 
    {
    fp2.setStartTime(newStartTime);
    sp2.setStartTime(newStartTime);
    std::cerr<<"*** move startTime"<<newStartTime;
    }
    }
    }
    else if (s.syncPoint1() == Behavior::Start && s.syncPoint2() == Behavior::End)
    {
    if (s.offset() > 0)
    {
    std::cerr<<"Error: sync between two speechPlanUnits between Start and End with positive offset\n";
    //We are not able to play two sound simultaneously
    mustExit = true;
    break;
    }
    else 
    {
    const float newEndTime = sp.startTime() + s.offset();
    if (fp2.endTime() != newEndTime) 
    {
    fp2.setEndTime(newEndTime);
    sp2.setEndTime(newEndTime);
    std::cerr<<"*** move endTime"<<newEndTime<<"\n";
    }
    }
							

    }

    }
    }

				


    }

    }

    if (mustExit) 
    break;

    }

    }
  */

  /*
  //check if we have overlap between speechPlans 
  for (int i=0; i<nbBehaviors; ++i) 
  {
  SpeechPlan &sp = speechPlans[i];
  if (! sp.empty()) 
  {
			
  for (int j=i+1; j<nbBehaviors; ++j) 
  {
  SpeechPlan &sp2 = speechPlans[j];
  if (! sp2.empty()) 
  {
  if (sp.intersects(sp2)) {
  std::cerr<<"ERROR: intersection between two speech plans\n";
  std::cerr<<" st1="<<sp.startTime()<<" et1="<<sp.endTime()<<" && st2="<<sp2.startTime()<<" et2="<<sp2.endTime();
  mustExit = true;
  break;
  }
  }
  }
  if (mustExit) 
  break;
  }
  }
  */

  if (mustExit) 
    {
      //We must delete all PlanUnit

      for(FacePlan &p : facePlans)
	{
	  for(FaceTrackPlan &tp : p)
	    {
	      for(FacePlanUnit *pu : tp)
		{
		  delete pu;
		}
	    }
	}
      for(SpeechPlan &p : speechPlans)
	{
	  for(SpeechTrackPlan &tp : p)
	    {
	      for(SpeechPlanUnit *pu : tp)
		{
		  delete pu;
		}
	    }
	}

      return false;
    }

  /*
  //Once synchronization constraints are respected,
  //we will append new plans to current plans.
  //We have to shift their startTime...
  float newStartTime = currentTime;

  //Get minimum start time of all tracks
  float minStartTime = std::numeric_limits<float>::max();
  for(FacePlan &p : facePlans)
  {
  const float st = p.startTime();
  if (st < minStartTime)
  minStartTime = st;
  }
  for(SpeechPlan &p : speechPlans)
  {
  const float st = p.startTime();
  if (st < minStartTime)
  minStartTime = st;
  }
	

  //Shift the startTimes
  for(FacePlan &p : facePlans)
  {
  p.shiftOf(newStartTime-minStartTime);
  }
  for(SpeechPlan &p : speechPlans)
  {
  p.shiftOf(newStartTime-minStartTime);
  }
  */


  /*
    {//DEBUG

    std::cerr<<"FINAL FACE PLANS\n";
    for(FacePlan &p : facePlans)
    {
    for(FaceTrackPlan &tp : p)
    {
    for(FacePlanUnit *pu : tp)
    {
    std::cerr<<"pu ty="<<pu->faceType()<<" st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime();
    }
    }
    }
    std::cerr<<"FINAL SPEECH PLANS\n";
    for(SpeechPlan &p : speechPlans)
    {
    for(SpeechTrackPlan &tp : p)
    {
    for(SpeechPlanUnit *pu : tp)
    {
    std::cerr<<"pu ty="<<pu->speechType()<<" st="<<pu->startTime()<<" len="<<pu->length()<<" et="<<pu->endTime();
    }
    }
    }

    }//DEBUG
  */

  return true;
}


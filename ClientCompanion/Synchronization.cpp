#include "Synchronization.hpp"

Synchronization::Synchronization() :
	m_b1(nullptr),
	m_s1(Behavior::End),
	m_b2(nullptr),
	m_s2(Behavior::Start),
	m_offset(0)
{

}

Synchronization::Synchronization(const Behavior *b1,
								 Behavior::SyncPoint s1,
								 const Behavior *b2,
								 Behavior::SyncPoint s2,
								 float offset) :
	m_b1(b1),
	m_s1(s1),
	m_b2(b2),
	m_s2(s2),
	m_offset(offset)
{

}
	
void
Synchronization::set(const Behavior *b1,
					 Behavior::SyncPoint s1,
					 const Behavior *b2,
					 Behavior::SyncPoint s2,
					 float offset)
{
	m_b1 = b1;
	m_s1 = s1;
	m_b2 = b2;
	m_s2 = s2;
	m_offset = offset;
}
	
bool Synchronization::isValid() const
{
	return m_b1!=nullptr && m_b2!=nullptr && m_b1!=m_b2;

}

void Synchronization::reorder()
{
	//if (m_s1 > m_s2) {
	std::swap(m_b1, m_b2);
	std::swap(m_s1, m_s2);
	m_offset = -m_offset;
	//}
}


#include "SimpleControlPlanner.hpp"

#include <cassert>
#include <QDebug>

#include <iostream>

SimpleControlPlanner::SimpleControlPlanner()
{
}

ControlPlan SimpleControlPlanner::currentPlan() const
{
	return m_currentPlan;
}

//TODO: code duplication !!!
static
bool
checkSorted(const ControlTrackPlan &t)
{
	bool sorted = true;
	ControlTrackPlan::const_iterator it = t.begin();
	ControlTrackPlan::const_iterator itNext = t.begin()+1;
	if (it != t.end() && itNext!=t.end()) 
	{
	  int pu = 0;
		for (; itNext!=t.end(); ++it, ++itNext) 
		{
			if ((*it)->endTime() > (*itNext)->startTime()) {
                std::cerr<<" pu ind="<<pu<<" : endTime="<<(*it)->endTime()<<"  >  next startTime="<<(*itNext)->startTime()<<"\n";

                std::cerr<<"\n";
                std::cerr<<"pu ind="<<pu<<" : st="<<(*it)->startTime()<<" len="<<(*it)->length()<<" et="<<(*it)->endTime()<<"\n";
                std::cerr<<"pu ind="<<pu+1<<" : st="<<(*itNext)->startTime()<<" len="<<(*itNext)->length()<<" et="<<(*itNext)->endTime()<<"\n";


				return false;
			}
            ++pu;
        }
	}

	return sorted;
}


void SimpleControlPlanner::setCurrentPlan(const ControlPlan &p) 
{
	for (const ControlTrackPlan &stp : p)
		assert(checkSorted(stp));

	m_currentPlan = p;

	//TODO:preload next SoundFilePlanUnit file if any ???

}

void SimpleControlPlanner::play(float currTime) 
{
	if (! m_currentPlan.empty()) {

		//Play only first track
		int i = 0;

		ControlTrackPlan &stp = m_currentPlan[i];

		ControlTrackPlan::iterator it;
		for (it=stp.begin(); it!=stp.end(); ) 
		{
			const ControlPlanUnit *a = *it;
			
			if (a->length() != -1 && a->endTime() < currTime) //it is possible that we never played this animation
			{
			  //qDebug()<<"@@@ delete a="<<a<<" st="<<a->startTime()<<" l="<<a->length()<<" et="<<a->endTime();
				delete *it;
				it = stp.erase(it);
			}
			else 
			{
				if (a->startTime() > currTime) 
				{
				  break;
				}
				else 
				{
					assert(a->startTime() <= currTime && (currTime <= a->endTime() || a->length() == -1));
					
					ControlPlanUnitType ct = a->controlType();
					delete *it;
					it = stp.erase(it);
					
					emit controlReached(ct);
				}
			}
		}
	}
	
}


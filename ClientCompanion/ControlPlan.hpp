#ifndef CONTROLPLAN_HPP
#define CONTROLPLAN_HPP

#include "Plan.hpp"

typedef TrackPlan<ControlPlanUnit> ControlTrackPlan;
typedef Plan<ControlPlanUnit> ControlPlan;


#endif /* ! CONTROLPLAN_HPP */

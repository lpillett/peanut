\babel@toc {french}{}
\contentsline {section}{\numberline {0.1}Architecture globale}{2}{section.0.1}%
\contentsline {subsection}{\numberline {0.1.1}Scénario OpenViBE}{3}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {0.1.2}Le moteur de règles}{4}{subsection.0.1.2}%
\contentsline {subsection}{\numberline {0.1.3}Compagnon}{4}{subsection.0.1.3}%
\contentsline {subsection}{\numberline {0.1.4}Adaptation de l'architecture}{4}{subsection.0.1.4}%
\contentsline {section}{\numberline {0.2}Compagnon}{5}{section.0.2}%
\contentsline {subsubsection}{Commandes}{6}{section*.2}%
\contentsline {subsubsection}{Architecture}{6}{section*.3}%
\contentsline {subsubsection}{Animation}{6}{section*.4}%
\contentsline {subsubsection}{Implémentation}{6}{section*.5}%
\contentsline {subsubsection}{Ressources Qt}{7}{section*.6}%
\contentsline {section}{\numberline {0.3}Moteur de règles}{8}{section.0.3}%
\contentsline {section}{\numberline {0.4}Extension du compagnon}{8}{section.0.4}%
\contentsline {subsection}{\numberline {0.4.1}Création des animations}{8}{subsection.0.4.1}%
\contentsline {subsection}{\numberline {0.4.2}Ajout d'un nouveau visage}{8}{subsection.0.4.2}%
\contentsline {subsubsection}{format XML}{9}{section*.7}%
\contentsline {paragraph}{Animation}{9}{section*.8}%
\contentsline {paragraph}{AnimationSegment}{9}{section*.9}%
\contentsline {subsection}{\numberline {0.4.3}Création de nouveaux messages}{10}{subsection.0.4.3}%
\contentsline {subsubsection}{Text-To-Speech}{10}{section*.10}%
\contentsline {subsubsection}{Alignement}{10}{section*.11}%
\contentsline {subsubsection}{Compression}{11}{section*.12}%
\contentsline {subsection}{\numberline {0.4.4}Ajout de nouvelles voix}{11}{subsection.0.4.4}%
\contentsline {subsection}{\numberline {0.4.5}Compilation et déploiement}{11}{subsection.0.4.5}%
\contentsline {subsubsection}{Génération du fichier ressources}{11}{section*.13}%
\contentsline {subsubsection}{Compilation avec QtCreator}{11}{section*.14}%
\contentsline {subsubsection}{Installation sur smartphone Android}{11}{section*.15}%
\contentsline {chapter}{\numberline {A}Optimisation des images}{13}{appendix.A}%
\contentsline {section}{\numberline {A.1}PNG}{13}{section.A.1}%
\contentsline {paragraph}{OptiPNG}{13}{section*.17}%
\contentsline {paragraph}{Zopfli}{13}{section*.18}%
\contentsline {paragraph}{Benchmarks}{14}{section*.19}%
\contentsline {section}{\numberline {A.2}WebP versus PNG}{15}{section.A.2}%
\contentsline {paragraph}{benchmarks}{15}{section*.20}%
\contentsline {paragraph}{Compression avec perte}{16}{section*.21}%
\contentsline {chapter}{\numberline {B}TTS}{17}{appendix.B}%
\contentsline {section}{\numberline {B.1}TTS sous OSX}{17}{section.B.1}%
\contentsline {section}{\numberline {B.2}TTS via Google}{17}{section.B.2}%
\contentsline {section}{\numberline {B.3}Étude de l'existant TTS}{18}{section.B.3}%
\contentsline {subsubsection}{TTS opensource}{18}{section*.22}%
\contentsline {paragraph}{espeak}{19}{section*.23}%
\contentsline {paragraph}{MBrola}{19}{section*.24}%
\contentsline {paragraph}{MaryTTS}{19}{section*.25}%
\contentsline {paragraph}{gnuspeech}{19}{section*.26}%
\contentsline {paragraph}{Festival}{19}{section*.27}%
\contentsline {paragraph}{CMU Flite}{20}{section*.28}%
\contentsline {paragraph}{FreeTTS}{20}{section*.29}%
\contentsline {paragraph}{Sinsy}{20}{section*.30}%
\contentsline {paragraph}{emofilt}{20}{section*.31}%
\contentsline {subsubsection}{TTS commerciaux}{20}{section*.32}%
\contentsline {paragraph}{Voxygen}{20}{section*.33}%
\contentsline {paragraph}{acapela}{21}{section*.34}%
\contentsline {paragraph}{Nuance}{21}{section*.35}%
\contentsline {paragraph}{NeoSpeech}{21}{section*.36}%
\contentsline {paragraph}{Ivona}{21}{section*.37}%
\contentsline {paragraph}{oddcast}{21}{section*.38}%
\contentsline {paragraph}{CereProc}{22}{section*.39}%
\contentsline {chapter}{\numberline {C}Alignement avec Sphinx3}{23}{appendix.C}%
\contentsline {section}{\numberline {C.1}Installation}{23}{section.C.1}%
\contentsline {section}{\numberline {C.2}Utilisation}{24}{section.C.2}%
\contentsline {paragraph}{Conversion en WAV}{24}{section*.40}%
\contentsline {paragraph}{Alignement}{24}{section*.41}%
\contentsline {section}{\numberline {C.3}Troobleshooting}{24}{section.C.3}%
\contentsline {chapter}{\numberline {D}Compression des fichiers son}{26}{appendix.D}%
\contentsline {section}{\numberline {D.1}Benchmark}{26}{section.D.1}%
\contentsline {section}{\numberline {D.2}Windows}{27}{section.D.2}%
\contentsline {section}{\numberline {D.3}Linux}{27}{section.D.3}%
\contentsline {chapter}{\numberline {E}Installation de l'environnement de développement sous Windows}{28}{appendix.E}%
\contentsline {section}{\numberline {E.1}Git}{28}{section.E.1}%
\contentsline {section}{\numberline {E.2}OpenViBE}{28}{section.E.2}%
\contentsline {section}{\numberline {E.3}CMake}{29}{section.E.3}%
\contentsline {section}{\numberline {E.4}Android}{29}{section.E.4}%
\contentsline {section}{\numberline {E.5}Ant}{30}{section.E.5}%
\contentsline {section}{\numberline {E.6}Qt for Android}{30}{section.E.6}%
\contentsline {section}{\numberline {E.7}LSL}{30}{section.E.7}%
\contentsline {chapter}{\numberline {F}Installation de l'environnement de développement sous linux}{32}{appendix.F}%
\contentsline {section}{\numberline {F.1}OpenViBE}{32}{section.F.1}%
\contentsline {section}{\numberline {F.2}Android}{33}{section.F.2}%
\contentsline {section}{\numberline {F.3}Qt for Android}{34}{section.F.3}%
\contentsline {chapter}{\numberline {G}Compilation}{35}{appendix.G}%
\contentsline {section}{\numberline {G.1}Qt Creator}{35}{section.G.1}%
\contentsline {section}{\numberline {G.2}Script}{36}{section.G.2}%
\contentsline {section}{\numberline {G.3}Bibliothèques dynamiques}{37}{section.G.3}%
\contentsline {chapter}{\numberline {H}Execution}{38}{appendix.H}%
\contentsline {chapter}{\numberline {I}Déploiement}{39}{appendix.I}%
\contentsline {paragraph}{Activation du mode développeur}{39}{section*.42}%
\contentsline {paragraph}{Vérifier que le smartphone est correctement reconnu}{39}{section*.43}%
\contentsline {paragraph}{Déploiement effectif}{40}{section*.44}%
\contentsline {chapter}{\numberline {J}Découpage des fichiers Ressources}{41}{appendix.J}%
\contentsline {chapter}{\numberline {K}Plein écran et non mise en veille avec Qt/Android}{42}{appendix.K}%
\contentsline {section}{\numberline {K.1}Appel JNI from main}{42}{section.K.1}%
\contentsline {section}{\numberline {K.2}Héritage de QtActivity}{43}{section.K.2}%
\contentsline {chapter}{\numberline {L}Synfig}{45}{appendix.L}%
\contentsline {chapter}{\numberline {M}LSL}{46}{appendix.M}%
\contentsline {section}{\numberline {M.1}LSL version 1.10.2}{46}{section.M.1}%
\contentsline {section}{\numberline {M.2}LSL version git}{47}{section.M.2}%
\contentsline {chapter}{\numberline {N}Alcatel OneTouch Idol 3}{49}{appendix.N}%
\contentsline {section}{\numberline {N.1}Sous Windows}{49}{section.N.1}%
\contentsline {subsection}{\numberline {N.1.1}driver MTP}{50}{subsection.N.1.1}%

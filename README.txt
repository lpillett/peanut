
Dependencies :
 Qt5.3 or greater
 (because QtWebSockets is used and it is only available in Qt 5.3 or greater)


To be able to compile and deploy for Android device, it is required to install the Qt version packaged for Android http://www.qt.io/download-open-source/#section-3
For example :
- "Qt 5.6.0 for Android (Linux 64-bit, 717 MB)" for linux
- "Qt 5.6.0 for Android (Window 32-bit, 1.1 GB)" for Windows


If compilation for Android is not required, you can install Qt binaries for your linux distribution.
On Fedora (21 or greater), you can install the required packages with the following command done as root :
dnf install qt5-qtwebsockets-devel qt5-qtmultimedia-devel
On Ubuntu (15.04 or greater), you can install the required packages with the following command :
sudo apt-get install libqt5websockets5-dev qtmultimedia5-dev libqt5multimedia5 libqt5multimedia5-plugins
On older distributions (Ubuntu 14.04 for example), you will have to install Qt5 manually. You can find instructions to download Qt5 on this webpage :
 http://www.qt.io/download-open-source/


Code is in C++11, thus a decent C++ compiler is required (at least gcc 4.9, clang 3.6, or better).

Code should compile on linux (tested on Fedora 23 & Ubuntu 15.06, with gcc 5 & clang 3.7), on OS X (tested on OS X El Capitan 10.11.14, with clang), and Windows 7 (with MinGW provided by Qt5 for Windows).

#ifndef ENTRY_DIALOG_HPP
#define ENTRY_DIALOG_HPP

#include <cassert>
#include <QPushButton>
#include <QLineEdit>
#include <QDialog>
#include <QDialogButtonBox>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QSlider>


class EntryDialog : public QDialog
{
	Q_OBJECT

public:
	explicit EntryDialog(QWidget *parent = 0) 
		: QDialog(parent)
	{
		build();
        updateOk();
	}

	void get(QString &name, int &value, int &freq)
	{
		name = m_nameLE->text();
		value = m_valS->value();
		freq = m_freqSB->value();
	}

public slots:

	void updateOk()
	{
		assert(m_nameLE);
		assert(m_buttonBox);
		QPushButton *okButton = m_buttonBox->button(QDialogButtonBox::Ok);
		assert(okButton);
		bool enable = false;
		if (! m_nameLE->text().isEmpty()) {
			enable = true;
		}
		okButton->setEnabled(enable);
	}

protected:
    void build() 
	{
		QLabel *nameL = new QLabel(tr("Name: "), this);
		m_nameLE = new QLineEdit(this);
		QLabel *valL = new QLabel(tr("Value: "), this);
		m_valS = new QSlider(Qt::Horizontal, this);
		QLabel *freqL = new QLabel(tr("Frequency (nb trials): "), this);
		m_freqSB = new QSpinBox(this);
		m_freqSB->setMinimum(1);

		m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
														   | QDialogButtonBox::Cancel);
		connect(m_buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
		connect(m_buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

		connect(m_nameLE, SIGNAL(textChanged(const QString &)), this, SLOT(updateOk()));

		QHBoxLayout *hl1 = new QHBoxLayout;
		hl1->addWidget(nameL);
		hl1->addWidget(m_nameLE);
		QHBoxLayout *hl2 = new QHBoxLayout;
		hl2->addWidget(valL);
		hl2->addWidget(m_valS);
		QHBoxLayout *hl3 = new QHBoxLayout;
		hl3->addWidget(freqL);
		hl3->addWidget(m_freqSB);
		QVBoxLayout *vl = new QVBoxLayout;
		vl->addLayout(hl1);
		vl->addLayout(hl2);
		vl->addLayout(hl3);
		vl->addWidget(m_buttonBox);
		
		setLayout(vl);
	}

protected:
	QLineEdit *m_nameLE;
	QSlider *m_valS;
	QSpinBox *m_freqSB;
	QDialogButtonBox *m_buttonBox;
};

#endif /* ! ENTRY_DIALOG_HPP */

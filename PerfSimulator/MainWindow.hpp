#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <memory>
#include <QMainWindow>
#include <QVector>
#include <QMap>
class QLabel;
class QSpinBox;
class QComboBox;
class QSlider;
class QPushButton;
class QScrollArea;
class QGroupBox;
class QTimer;

class PerfEntry;

namespace lsl {
	class stream_outlet;
}

class MainWindow : public QWidget //QMainWindow
{
	Q_OBJECT

public:
	MainWindow();
	~MainWindow();

protected slots:
	void addEntry();
	void play();
	void pause();
	void stop();
    void abort();

  //void updateTime();
	void endTrialUpdate();
	void endPauseUpdate();

	void removeEntry();

protected:
	void build();

	void populateCBs();
	void populatePerfs();
	void addEntry(const QString &name, int value, int freq);

	void createStimOutlet(const QString &name);

	void sendStim(int v);
	void sendStartRun();
	void sendEndRun();
	void sendStartTrial();
	void sendEndTrial();
	void sendEndMeasure();
	void resetStarts();
	void sendValues();

	void updateRunTrial();

private:

	QSpinBox *m_nbRunsSB;
	QSpinBox *m_nbTrialsSB;
	QSpinBox *m_trialDurationSB;
	QSpinBox *m_pauseDurationSB; 
	QComboBox *m_startRunCB;
	QComboBox *m_endRunCB;
	QComboBox *m_startTrialCB;
	QComboBox *m_endTrialCB;
	QComboBox *m_endMeasureCB;
	QLabel *m_outletNameL;

	QPushButton *m_addPerfEntryB;
	QScrollArea *m_entriesSA;

	QPushButton *m_playB;
	QPushButton *m_pauseB;
	QPushButton *m_stopB;

	QGroupBox *m_stimGB;
	QGroupBox *m_perfGB;

	QLabel *m_currentRunL;
	QLabel *m_currentTrialL;

  //QTimer *m_timer;

	std::shared_ptr<lsl::stream_outlet> m_stimOutlet;

	QVector<PerfEntry *> m_perfEntries;
	QVector<int> m_starts;
	QVector<std::shared_ptr<lsl::stream_outlet> > m_perfOutlets;

	QMap<QPushButton *, int> m_delBtn2PerfEntryIndex;

	int m_currentRun;
	int m_currentTrial;

	bool m_playing;
	bool m_paused;
};

#endif /* ! MAINWINDOW_HPP */

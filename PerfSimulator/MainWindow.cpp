#include "MainWindow.hpp"

#include <cassert>
#include <QBoxLayout>
#include <QDebug>
#include <QFile>
#include <QLabel>
#include <QSpinBox>
#include <QSlider>
#include <QComboBox>
#include <QGroupBox>
#include <QScrollArea>
#include <QPushButton>
#include <QTimer>

#include <iostream>//DEBUG

#include "lsl_cpp.h"

#include "EntryDialog.hpp"

const QString DEFAULT_START_RUN = "OVTK_StimulationId_BaselineStart";
const QString DEFAULT_END_RUN = "OVTK_GDF_End_Of_Session";
const QString DEFAULT_START_TRIAL = "OVTK_GDF_Start_Of_Trial";
const QString DEFAULT_END_TRIAL = "OVTK_GDF_End_Of_Trial";
const QString DEFAULT_END_MEASURE = "OVTK_StimulationId_TrialStop";

const QString DEFAULT_STIM_OUTLET_NAME = "openvibeMarkers";

const int DEFAULT_TRIAL_DURATION = 12;
const int DEFAULT_PAUSE_DURATION = 20; //pause between two trials
const int DEFAULT_NB_RUNS = 3;
const int DEFAULT_NB_TRIALS = 5;



class PerfEntry : public QWidget
{
public:
	explicit PerfEntry(QWidget *parent = 0)
		: QWidget(parent)
	{
		build();
	}

	void set(const QString &name, int value, int freq =0)
	{
		assert(value>=0 && value<=100);

		m_nameL->setText(name);
		m_valS->setValue(value);
		m_freqSB->setValue(freq);
	}

	float getPerf() const
	{
		const float perf = (m_valS->value()-m_valS->minimum())/(float)(m_valS->maximum()-m_valS->minimum());
		return perf;
	}
	
	int getFreq() const
	{
		return m_freqSB->value();
	}

	QPushButton *getDelButton() { return m_delB; }

protected:

	void build()
	{
		m_nameL = new QLabel(this);
		QLabel *vL = new QLabel(tr("Perf: "), this);
		m_valS = new QSlider(Qt::Horizontal, this);
		QLabel *fL = new QLabel(tr("Freq: "), this);
		m_freqSB = new QSpinBox(this);
		m_delB = new QPushButton(tr("Del"), this);
		
		QHBoxLayout *hLayout = new QHBoxLayout;
		hLayout->addWidget(m_nameL);
		hLayout->addWidget(vL);
		hLayout->addWidget(m_valS);
		hLayout->addWidget(fL);
		hLayout->addWidget(m_freqSB);
		hLayout->addWidget(m_delB);
		setLayout(hLayout);
	}

protected:
	QLabel *m_nameL;
	QSlider *m_valS;
	QSpinBox *m_freqSB;
	QPushButton *m_delB;
};


MainWindow::MainWindow()
	: QWidget(),
	  m_currentRun(0),
	  m_currentTrial(0),
	  m_playing(false),
	  m_paused(false)
{

	build();

}

void
MainWindow::build()
{
	QVBoxLayout *vl = new QVBoxLayout;

	QLabel *nbRunsL = new QLabel(tr("Nb runs: "), this);
	m_nbRunsSB = new QSpinBox(this);
	m_nbRunsSB->setMinimum(1);
	QLabel *nbTrialsL = new QLabel(tr("Nb trials: "), this);
	m_nbTrialsSB = new QSpinBox(this);
	m_nbTrialsSB->setMinimum(1);

	QLabel *trialDurationL = new QLabel(tr("Trial duration (s): "), this);
	m_trialDurationSB = new QSpinBox(this);
	m_trialDurationSB->setMinimum(1);

	QLabel *pauseDurationL = new QLabel(tr("Pause duration (s): "), this);
	pauseDurationL->setToolTip(tr("Pause between two trials (in seconds)"));
	m_pauseDurationSB = new QSpinBox(this);
	m_pauseDurationSB->setMinimum(1);



	
	QLabel *startRunL = new QLabel(tr("Start run stim: "), this);
	m_startRunCB = new QComboBox(this);
	QLabel *endRunL = new QLabel(tr("End run stim: "), this);
	m_endRunCB = new QComboBox(this);
	QLabel *startTrialL = new QLabel(tr("Start trial stim: "), this);
	m_startTrialCB = new QComboBox(this);
	QLabel *endTrialL = new QLabel(tr("End trial stim: "), this);
	m_endTrialCB = new QComboBox(this);
	QLabel *endMeasureL = new QLabel(tr("End measure stim: "), this);
	m_endMeasureCB = new QComboBox(this);

	QLabel *outletName = new QLabel(tr("Stimulation: "), this);
	m_outletNameL = new QLabel(this);


	m_addPerfEntryB = new QPushButton(tr("Add..."), this);
	connect(m_addPerfEntryB, SIGNAL(clicked()), this, SLOT(addEntry()));

	m_entriesSA = new QScrollArea(this);
	QVBoxLayout *vlSA = new QVBoxLayout;
	m_entriesSA->setLayout(vlSA);

	m_playB = new QPushButton(tr("Play"), this);
    //m_pauseB = new QPushButton(tr("Pause"), this);
    m_stopB = new QPushButton(tr("Abort"), this);
	connect(m_playB, SIGNAL(clicked()), this, SLOT(play()));
    //connect(m_pauseB, SIGNAL(clicked()), this, SLOT(pause()));
    connect(m_stopB, SIGNAL(clicked()), this, SLOT(abort()));
	
	//TODO
    //m_pauseB->setEnabled(false);
	m_stopB->setEnabled(false);
	//TODO end

	QLabel *currentRunL = new QLabel(tr("Current run: "), this);
	m_currentRunL = new QLabel(this);
	QLabel *currentTrialL = new QLabel(tr("Current trial: "), this);
	m_currentTrialL = new QLabel(this);


	QHBoxLayout *hl1 = new QHBoxLayout;
	hl1->addWidget(nbRunsL);
	hl1->addWidget(m_nbRunsSB);
	hl1->addWidget(nbTrialsL);
	hl1->addWidget(m_nbTrialsSB);
	hl1->addWidget(trialDurationL);
	hl1->addWidget(m_trialDurationSB);
	hl1->addWidget(pauseDurationL);
	hl1->addWidget(m_pauseDurationSB);
	hl1->addStretch();
	
	QHBoxLayout *hl2 = new QHBoxLayout;
	hl2->addWidget(startRunL);
	hl2->addWidget(m_startRunCB);
	QHBoxLayout *hl2b = new QHBoxLayout;
	hl2b->addWidget(endRunL);
	hl2b->addWidget(m_endRunCB);

	QHBoxLayout *hl3 = new QHBoxLayout;
	hl3->addWidget(startTrialL);
	hl3->addWidget(m_startTrialCB);
	QHBoxLayout *hl3b = new QHBoxLayout;
	hl3b->addWidget(endTrialL);
	hl3b->addWidget(m_endTrialCB);
	QHBoxLayout *hl3c = new QHBoxLayout;
	hl3c->addWidget(endMeasureL);
	hl3c->addWidget(m_endMeasureCB);

	QHBoxLayout *hln = new QHBoxLayout;
	hln->addWidget(outletName);
	hln->addWidget(m_outletNameL);
	hln->addStretch();
	
	QVBoxLayout *vl1 = new QVBoxLayout;
	vl1->addLayout(hl1);
	vl1->addLayout(hl2);
	vl1->addLayout(hl2b);
	vl1->addLayout(hl3);
	vl1->addLayout(hl3b);
	vl1->addLayout(hl3c);
	vl1->addLayout(hln);

	m_stimGB = new QGroupBox(tr("Stimulations"), this);
	m_stimGB->setLayout(vl1);

	QHBoxLayout *hl4 = new QHBoxLayout;
	hl4->addWidget(m_playB);
    //hl4->addWidget(m_pauseB);
	hl4->addWidget(m_stopB);
	hl4->addStretch();
	hl4->addWidget(currentRunL);
	hl4->addWidget(m_currentRunL);
	hl4->addWidget(currentTrialL);
	hl4->addWidget(m_currentTrialL);



	QVBoxLayout *vl2 = new QVBoxLayout;
	vl2->addWidget(m_addPerfEntryB);
	vl2->addWidget(m_entriesSA);	

	m_perfGB = new QGroupBox(tr("Performances"), this);
	m_perfGB->setLayout(vl2);

	vl->addWidget(m_stimGB);
	vl->addWidget(m_perfGB);
	vl->addLayout(hl4);

	setLayout(vl);


	populateCBs();
	m_nbRunsSB->setValue(DEFAULT_NB_RUNS);
	m_nbTrialsSB->setValue(DEFAULT_NB_TRIALS);
	m_trialDurationSB->setValue(DEFAULT_TRIAL_DURATION);
	m_pauseDurationSB->setValue(DEFAULT_PAUSE_DURATION);
	populatePerfs();
	m_outletNameL->setText(DEFAULT_STIM_OUTLET_NAME);
	
	createStimOutlet(m_outletNameL->text());

	updateRunTrial();

	//m_timer = new QTimer(this);
	//connect(m_timer, SIGNAL(timeout()), this, SLOT(updateTime()));
}

MainWindow::~MainWindow()
{
	//TODO: clean ???
}


#include "openvibeDefines.hpp"

static 
void 
setCB(QComboBox *cb, const QString &t)
{
	std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.find(t.toStdString());
	if (it != OVTK_stringToIntMap.end())
		cb->setCurrentText(t);	
}

void
MainWindow::populateCBs()
{
	init_OpenViBE_maps();

	for (std::map<std::string, int>::const_iterator it = OVTK_stringToIntMap.begin();
		 it != OVTK_stringToIntMap.end();
		 ++it) {
		
		const QString n = QString::fromStdString(it->first);
		const int v = it->second;

		m_startRunCB->addItem(n, v);
		m_endRunCB->addItem(n, v);
		m_startTrialCB->addItem(n, v);
		m_endTrialCB->addItem(n, v);
		m_endMeasureCB->addItem(n, v);
	}

	//set default values
	setCB(m_startRunCB, DEFAULT_START_RUN);
	setCB(m_endRunCB, DEFAULT_END_RUN);
	setCB(m_startTrialCB, DEFAULT_START_TRIAL);
	setCB(m_endTrialCB, DEFAULT_END_TRIAL);
	setCB(m_endMeasureCB, DEFAULT_END_MEASURE);

}

void
MainWindow::populatePerfs()
{
	addEntry("openvibePerfST", 50, 1);	
	//addEntry("openvibePerfLT", 50, 3);	
}

void
MainWindow::addEntry()
{
  EntryDialog diag(this);
  int ret = diag.exec();
  if (ret == QDialog::Accepted) {
	  QString name;
	  int value;
	  int freq;
	  diag.get(name, value, freq);
	  assert(! name.isEmpty());
	  addEntry(name, value, freq);
  }

}

void
MainWindow::removeEntry()
{
	QObject *o = sender();
	QPushButton *delBtn = static_cast<QPushButton *>(o);

	assert(m_delBtn2PerfEntryIndex.find(delBtn) != m_delBtn2PerfEntryIndex.end());
	const int index = m_delBtn2PerfEntryIndex[delBtn];
	assert(index < m_perfEntries.size());

	assert(m_perfEntries.size() == m_perfOutlets.size());
	assert(m_starts.size() == m_perfOutlets.size());
	
	delete m_perfEntries[index];

	m_perfEntries.remove(index);
	m_starts.remove(index);
	m_perfOutlets.remove(index);

	std::cerr<<"removeEntry index="<<index<<"\n";
	
	assert(m_perfEntries.size() == m_perfOutlets.size());
	assert(m_starts.size() == m_perfOutlets.size());

}

void
MainWindow::addEntry(const QString &name, int value, int freq)
{
	const std::string n = name.toStdString();
	const std::string type;
	const int channel_count = 1;
	const double nominal_srate = lsl::IRREGULAR_RATE;
	const lsl::channel_format_t channel_format = lsl::cf_float32;
	const std::string source_id = name.toStdString(); //TODO !!!!
	lsl::stream_info info(n, type, channel_count, nominal_srate, channel_format, source_id);

	std::shared_ptr<lsl::stream_outlet> outlet(new lsl::stream_outlet(info));
	m_perfOutlets.push_back(outlet);

	PerfEntry *pe = new PerfEntry;
	pe->set(name, value, freq);
	assert(m_entriesSA);
	assert(m_entriesSA->layout());
	m_entriesSA->layout()->addWidget(pe);
	m_perfEntries.push_back(pe);

	m_starts.push_back(m_currentTrial);
	

	QPushButton *delBtn = pe->getDelButton();
	m_delBtn2PerfEntryIndex[delBtn] = m_perfEntries.size()-1;
	connect(delBtn, SIGNAL(clicked()), this, SLOT(removeEntry()));

	std::cerr<<"addEntry index="<<m_perfEntries.size()-1<<"\n";

	assert(m_perfEntries.size() == m_perfOutlets.size());
	assert(m_starts.size() == m_perfOutlets.size());
	

	//TODO: connect destroy signal 
}

//TODO: advance run et trial
//TODO: quand on change de run, remsie à zero de tous les starts 

void
MainWindow::createStimOutlet(const QString &name)
{
	const std::string n = name.toStdString();
	const std::string type;
	const int channel_count = 1;
	const double nominal_srate = lsl::IRREGULAR_RATE;
	const lsl::channel_format_t channel_format = lsl::cf_int32;
	const std::string source_id = name.toStdString(); //TODO !!!!
	lsl::stream_info info(n, type, channel_count, nominal_srate, channel_format, source_id);

	m_stimOutlet.reset(new lsl::stream_outlet(info));
}

void
MainWindow::sendStim(int v)
{
	m_stimOutlet->push_sample(&v);
	std::cerr<<"Send Stim: "<<OVTK_intToStringMap[v]<<"\n";
}

void
MainWindow::sendStartRun()
{
	const int v = m_startRunCB->currentData().toInt();
	sendStim(v);
}
void
MainWindow::sendEndRun()
{
	const int v = m_endRunCB->currentData().toInt();
	sendStim(v);
}
void
MainWindow::sendStartTrial()
{
	const int v = m_startTrialCB->currentData().toInt();
	sendStim(v);
}
void
MainWindow::sendEndTrial()
{
	const int v = m_endTrialCB->currentData().toInt();
	sendStim(v);
}
void
MainWindow::sendEndMeasure()
{
	const int v = m_endMeasureCB->currentData().toInt();
	sendStim(v);
}



void
MainWindow::resetStarts()
{
	for (int i=0; i<m_starts.size(); ++i)
		m_starts[i] = 0;
}

void
MainWindow::sendValues()
{
	assert(m_perfEntries.size() == m_perfOutlets.size());
	assert(m_starts.size() == m_perfOutlets.size());

	const int nb = m_starts.size();
	for (int i=0; i<nb; ++i) {
		int t = m_currentTrial - m_starts[i];
		int f = m_perfEntries[i]->getFreq();
		if ((t+1) % f == 0 && t+1 >= f  ) {
			float v = m_perfEntries[i]->getPerf();
			m_perfOutlets[i]->push_sample(&v);

			std::cerr<<"t:"<<t<<" sendPerfs["<<i<<"]: "<<v<<"\n";
		}
	}
}

void
MainWindow::updateRunTrial()
{
    m_currentRunL->setText(QString::number(m_currentRun+1));
    m_currentTrialL->setText(QString::number(m_currentTrial+1));
    //+1 : to count runs&trials from 1 and not 0.
}

void
MainWindow::endTrialUpdate()
{
    if (m_playing) {
        sendEndTrial();

        sendValues();

        sendEndMeasure();

        const float pauseDuration = m_pauseDurationSB->value();

        QTimer::singleShot(pauseDuration*1000.0f, this, SLOT(endPauseUpdate()));
    }
}

void
MainWindow::endPauseUpdate()
{
    if (m_playing) {
        ++m_currentTrial;

        bool mustStop = false;

        if (m_currentTrial == m_nbTrialsSB->value()) {
            //goto next run
		
            ++m_currentRun;
		
            sendEndRun();

            if (m_currentRun < m_nbRunsSB->value()) {

                resetStarts();
			
                m_currentTrial = 0;
			
                sendStartRun();
                //TODO: wait?
                sendStartTrial();
            }
            else {
                mustStop = true;
                stop();

            }

        }
        else {

            sendStartTrial();
        }

        updateRunTrial();
  
        if (! mustStop) {
            const float trialInterval = m_trialDurationSB->value();
            QTimer::singleShot(trialInterval*1000.f, this, SLOT(endTrialUpdate()));
        }

    }
}

void
MainWindow::play()
{
	m_stimGB->setEnabled(false);
    m_playB->setEnabled(false);

	const float trialInterval = m_trialDurationSB->value();

	//std::cerr<<"trial interval="<<trialInterval<<"s\n";

    m_playing = true;
    //if (! m_paused) {
        m_currentRun = 0;
		m_currentTrial = 0;
		updateRunTrial();

		sendStartRun();
		//TODO: wait?
		sendStartTrial();
    //}
	
	//m_timer->start(trialInterval * 1000.f); //ms

    QTimer::singleShot(trialInterval * 1000.f, this, SLOT(endTrialUpdate())); //convert time in ms

    m_stopB->setEnabled(true);
}

void
MainWindow::pause()
{

}

void
MainWindow::stop()
{
    m_playing = false;

	m_stimGB->setEnabled(true);
    m_playB->setEnabled(true);
    m_stopB->setEnabled(false);

    m_currentRun = 0;
	m_currentTrial = 0;
}

void
MainWindow::abort()
{
    sendEndRun();
    stop();
}
